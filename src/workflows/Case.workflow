<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Research_Portal_Feedback_Case_Email_Alert</fullName>
        <description>Research Portal Feedback Case</description>
        <protected>false</protected>
        <recipients>
            <field>ResearcherCaseRoutedToEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>no.reply.portal.feedback@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Researcher_Email_Template/Researcher_Portal_Feedback_Template</template>
    </alerts>
    <alerts>
        <fullName>Research_Portal_Support_Case</fullName>
        <description>Research Portal Support Case</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>ResearcherCaseRoutedToEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>no.reply.research.support@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Researcher_Email_Template/Researcher_Portal_Support_Case_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>ISU_Case_Origin_Detail</fullName>
        <field>Case_Origin_Detail__c</field>
        <literalValue>ISU</literalValue>
        <name>ISU Case Origin Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusChangeDateTime</fullName>
        <field>StatusChangeDateTime__c</field>
        <formula>LastModifiedDate</formula>
        <name>UpdateStatusChangeDateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Priority_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Update Case Priority - High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Priority_Medium</fullName>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Update Case Priority - Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status</fullName>
        <description>Update Case Status to &quot;In Progress&quot;</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CaseStatusChanged</fullName>
        <actions>
            <name>Update_Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case Status to be changed once assigned to a queue or user to from &apos;&apos;New&apos;&apos; to ‘In Progress’</description>
        <formula>AND(ISCHANGED( OwnerId ),IsClosed != True)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ISU Case Origin Detail</fullName>
        <actions>
            <name>ISU_Case_Origin_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>International Enquiries</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TimeBasedWorkFlowLowPriorityCase</fullName>
        <active>true</active>
        <formula>AND(NOT(ISPICKVAL(Status,&quot;Closed&quot;)), ISPICKVAL(Priority,&quot;Low&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Case_Priority_High</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.StatusChangeDateTime__c</offsetFromField>
            <timeLength>16</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Case_Priority_Medium</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.StatusChangeDateTime__c</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TimeBasedWorkFlowMedPriorityCase</fullName>
        <active>true</active>
        <formula>AND(NOT(ISPICKVAL(Status,&quot;Closed&quot;)), ISPICKVAL(Priority,&quot;Medium&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Case_Priority_High</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.StatusChangeDateTime__c</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>UpdateStatusChangeDateTime</fullName>
        <actions>
            <name>UpdateStatusChangeDateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(IsCHANGED(Status) , NOT(ISPICKVAL(Status,&quot;Closed&quot;))),ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
