<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_the_Account_Owner_when_a_new_Contact_is_created</fullName>
        <description>Notify the Account Owner when a new Contact is created</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>industryconnect@rmit.edu.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>IBD_Email_Template_Folder/Notify_Account_Owner_when_a_new_Contact_is_created</template>
    </alerts>
    <rules>
        <fullName>Notify Account Owner when a new Contact is created</fullName>
        <actions>
            <name>Notify_the_Account_Owner_when_a_new_Contact_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(Owner.Id &lt;&gt; Account.OwnerId,  AccountId != null)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
