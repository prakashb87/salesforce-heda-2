<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Opportunity_Keys_Set_based_on_AdmitTerm</fullName>
        <field>Opportunity_Key__c</field>
        <formula>CASESAFEID(RecordType.Id) + &quot;.&quot; + CASESAFEID(Assign_To_Contact__c)  + &quot;.&quot; +CASESAFEID( Assign_To_Contact__r.AccountId ) + &quot;.&quot; + Admit_Term__c</formula>
        <name>Opportunity Keys  Set based on AdmitTerm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Key_To_Add_Stage</fullName>
        <field>Opportunity_Key__c</field>
        <formula>CASESAFEID(RecordType.Id) + &quot;.&quot; + CASESAFEID(Assign_To_Contact__c)  + &quot;.&quot; +CASESAFEID( Assign_To_Contact__r.AccountId ) + &quot;.&quot; + Admit_Term__c + &quot;.&quot; +   TEXT(LastModifiedDate)</formula>
        <name>Update Opportunity Key To Add Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity%3A Update Opportunity Key</fullName>
        <actions>
            <name>Opportunity_Keys_Set_based_on_AdmitTerm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Opportunity Key based on the Career, Recruitment Interest or Academic Interest, and Term, according to the formula used in the Opportunity Key field on the Interactions object.</description>
        <formula>AND(OR(RecordType.DeveloperName=&apos;Inquiry_Opportunities&apos;,RecordType.DeveloperName=&apos;International_Applicant_Opportunities&apos;,RecordType.DeveloperName=&apos;Non_School_Leaver_Applicant_Opportunities&apos;,RecordType.DeveloperName=&apos;School_Leaver_Applicant_Opportunities&apos;) ,Opportunity_Key__c &lt;&gt; CASESAFEID( RecordType.Id ) +&quot;.&quot; +  CASESAFEID( Assign_To_Contact__c ) +&quot;.&quot;+ CASESAFEID( Assign_To_Contact__r.AccountId ) +&quot;.&quot; + Admit_Term__c,  NOT(ISBLANK(Assign_To_Contact__c)), NOT(ISBLANK(Admit_Term__c)), NOT(ISPICKVAL( StageName , &apos;Closed Lost&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Key When Closed Lost</fullName>
        <actions>
            <name>Update_Opportunity_Key_To_Add_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inquiry Opportunities,International Applicant Opportunities,Non-School Leaver Applicant Opportunities,School Leaver Applicant Opportunities</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>This will update opportunity when Lst</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
