({
    //Method call to fetch publication details
    getContractDetails : function(component, event, helper, recordIdIs) {
        var action = component.get("c.getContracts");
        action.setParams({
            contractId: recordIdIs,
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.contractDetails", result[0]);
                this.getLinkedContacts(component, event, helper, recordIdIs);
            }
        });
        $A.enqueueAction(action);
    },
    getLinkedContacts : function(component, event, helper, recordId){
        var getLinkedContactsAction = component.get("c.getContractMilestonesList");
        
        getLinkedContactsAction.setParams({
            contractId: recordId
        });
        getLinkedContactsAction.setCallback(this, function(a) {
            var contractmilestoneslist = [];
            var state = a.getState();
            if (state == "SUCCESS") {
                var contractmilestoneslist = a.getReturnValue();
                if (!$A.util.isUndefined(contractmilestoneslist)) {                    
                    component.set("v.progressContractDetails", contractmilestoneslist);
                    var table = $('#progressList').DataTable({
                        "data": [],
                        "language": {
                            "emptyTable": $A.get("$Label.c.Noprogressitemshavebeenassociatedwiththiscontract")
                        },
                        "pageLength": 5,
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: $A.get("$Label.c.actionDate")
                        }, {
                            title: $A.get("$Label.c.process_stage")
                        }, {
                            title: $A.get("$Label.c.completedDate")
                        }],
                        "info": false,
                        "order": [[ 0, 'desc' ], [ 2, 'desc' ]],
                        "searching": false,
                        "dom": 'frt',
                        "drawCallback": function() {
                            // Show or hide "Load more" button based on whether there is more data available
                            $('#progresslist-load-more').toggle(this.api().page.hasMore());
                        },
                        "columnDefs": [{
                            type: 'natural',
                            targets: 1,
                            className: "table-title-column",
                            'render': function(data, type, full, meta) {
                                return "<div class='truncate' data-container='body' data-toggle='tooltip' data-placement='top' title='" + data + "'>" + data + "</div>";
                            }
                        },{
                            targets: [0,2],
                            "render": function(data, type, full, meta, row) {
                                if (type === 'display' || type === 'filter') {
                                    if (data !== '')
                                        return moment(data).format('DD/MM/YYYY');
                                    else
                                        return '';
                                }
                                
                                return data;
                            }
                        }]
                        
                    });
                    $('#progresslist-load-more').on('click', function() {
                        table.page.loadMore();
                    });
                    this.addLinkedContractRows(component, "#progressList", contractmilestoneslist, true);
                }
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(getLinkedContactsAction); 
    },
    
    addLinkedContractRows :function(component, tableId, dataset, isInit){
        var table = $(tableId).DataTable();
        
        if(!Array.isArray(dataset)){
            dataset = [dataset];
        }
        
        dataset.forEach(function(contract) {
            var temp = [];
            
            temp.push(contract["dueDate"] != undefined ? contract["dueDate"]:'');
            temp.push(contract["details"] != undefined ? contract["details"]:'');
            temp.push(contract["dateCompleted"] != undefined ? contract["dateCompleted"] : '');
            table.row.add(temp).draw();
        });
        
        if(!isInit){
            table.page.len(4).draw();           
        }   
    },
});