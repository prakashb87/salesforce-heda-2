({
    doInit : function(component, event, helper) {
        
        //ECB-5000 : System does not check if amount paid is correct before showing success page
        //ECB-5000 Starts - 21/12/2018
        var action = component.get('c.getresult');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.ProductBasket",response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
        
        //ECB-5000 Ends
        
        var urlString = window.location.href;
        var expectedHash = component.searchParam('HASH');
        
        var getRef= urlString.substring(urlString.indexOf('Ref'), urlString.indexOf("&TotAmt"));
        var receiptNum =urlString.substring(urlString.indexOf('&ReceiptNo'), urlString.indexOf("&MPB"));
        var totAm =urlString.substring(urlString.indexOf('&TotAmt'), urlString.indexOf("&ReceiptNo"));
        var urlWithoutStatus =urlString.substring(urlString.indexOf('Status'), urlString.indexOf("&"));
        var  MPB = urlString.substring(urlString.indexOf('&MPB'), urlString.indexOf("&HASH"));
        
        var finaUrl = getRef+receiptNum+totAm+MPB+'&'+urlWithoutStatus;
        
        // ECB-4670
        if(!receiptNum){
            helper.successPageRedirect(component);
        }
        var eUrl= $A.get("e.force:navigateToURL");
        
        if(expectedHash != ""){
            var hashAction = component.get("c.sendHashingDetails");
            hashAction.setParams({
                "urlStringParametersWithoutHash" : finaUrl,
                "expectedHash" : expectedHash
            });
            hashAction.setCallback(this, function(response){
                var state = response.getState();
                var hashDetailsResponse = response.getReturnValue();
                
                if(state === "SUCCESS" && hashDetailsResponse === true){
                    
                    //ECB-5000 : System does not check if amount paid is correct before showing success page           
                    //ECB-5000 Starts
                    var totalAmount = component.searchParam('TotAmt');
                    var finalProductBasket = component.get("v.ProductBasket");
                    var totalPrice = finalProductBasket.TotalPrice;
                    
                    if(totalPrice == totalAmount)	{
                        
                        var receiptNumber = component.searchParam('ReceiptNo');
                        var basketNumber = component.searchParam('MPB');
                        var totalAmt = component.searchParam('TotAmt');
                        
                        if(urlString.includes("ReceiptNo") && urlString.includes("MPB")) {
                            //ECB-3811: Updated the Code to make it IE 11 compatible
                            
                            var action = component.get("c.confirmEnroll");
                            action.setParams({
                                "receiptNo" : receiptNumber,
                                "basketNo"  : basketNumber
                            });
                            
                            action.setCallback(this, function(a){
                                var state = a.getState();
                                if (a.getState() === "SUCCESS"){
                                    
                                    //Start
                                    var action = component.get("c.getSuccessPageURL");
                                    action.setParams({
                                    });
                                    
                                    action.setCallback(this, function(a){
                                        var state = a.getState();
                                        console.log('State123==============>'+state);
                                        
                                        console.log('getReturnValue==============>'+a.getReturnValue());
                                        
                                        if (a.getState() === "SUCCESS"){
                                            component.set("v.successUrl", a.getReturnValue());
                                            component.set("v.isReady", true);
                                            
                                        }else{
                                            console.log("Failed>>> getSuccessPageURL");
                                        }
                                    });
                                    
                                    $A.enqueueAction(action);
                                    //End
                                    
                                }else{
                                    eUrl.setParams({
                                        "url": '/errorpage'
                                    });
                                    eUrl.fire();
                                    console.log("Enrolment unsuccessfull>>> ");
                                }
                            });
                            
                            $A.enqueueAction(action);
                        }
                    }
                    else{
                        eUrl.setParams({
                            "url": '/errorpage'
                        });
                        eUrl.fire();
                        console.log("Enrolment unsuccessfull>>> ");
                    }
                    //ECB-5000 Ends
                }else{
                    eUrl.setParams({
                        "url": '/errorpage'
                    });
                    eUrl.fire();
                    console.log("Enrolment unsuccessfull>>> ");
                }
            }); 
            
            $A.enqueueAction(hashAction); 
        }
    },
    
    NavigateToShopping : function(component, event, helper) {
        
        window.history.back();
    },
    
    NavigateToViewDashboard : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myproduct"
        });
        urlEvent.fire();     
    }, 
    
    //ECB-3811
    searchParam : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        var name;
        if(params){
            name = params.param1;
        }
        console.log('name:::'+name);
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        
    }    
})