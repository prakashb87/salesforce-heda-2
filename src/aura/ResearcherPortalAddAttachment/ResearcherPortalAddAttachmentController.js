({
    onInit:function(component, event, helper){
      let attachmentLabelsList ={
            'Popup_helptext1':'File size limit is 2MB.',
            'Popup_helptext3':'pdf, doc, docx, xls, xlsx, csv, png, jpeg.',
       }
     component.set('v.attachmentLabels', attachmentLabelsList)
    },
	 /* Open file upload modal*/
    openfileUpload: function(component, event, helper) {
      let attachmentId = component.get("v.attachmentId")
      $('#'+attachmentId+'-fileUpload').trigger("click");
    },
    /* file upload logic and validations*/
    fileUpload: function(component, event, helper) {
        event.stopPropagation();         
        let fileExists=false
        $("#fileNameError").hide();
        $("#fileTypeError").hide();
        $("#attachmentError").hide();
        $("#zeroSizeError").hide();
        var fileNames = [];
        var file = event.target.files[0];
        var fileType = file.name ? file.name.split('.').pop().toLowerCase():file.name;// file.type ? file.type.split('/')[1]:file.type
        var validFormat =  $A.get("$Label.c.validUploadFileFormat")
        var validFileFormat = validFormat.split(',')
        if(validFileFormat.indexOf(fileType) === -1){
            $("#fileTypeError").show()
            return
        }
        var FileSize = file.size / $A.get("$Label.c.file_Size_in_bytes"); //  2 MB
        var totalAttachFileList = component.get("v.attachFileList");
        totalAttachFileList.forEach(function(item){
            if(item.fileName == file.name){
                fileExists= true
            }
        })
        if(FileSize == 0){
            $("#zeroSizeError").show();
        } else if (FileSize > 2) {
            console.error('file size error');
            $("#attachmentError").show();
        } else if(fileExists){
             $("#fileNameError").show();
        } else {
            $("#attachmentError").hide(); 
             let attachmentId = component.get("v.attachmentId")
             let btnId = attachmentId+'-fileUpload'
            document.getElementById(btnId).disabled = true;
            var reader = new FileReader();
            reader.addEventListener("load", function (e) { // Setting up base64 URL on image
                var stream = reader.result.split(',')[1];
                totalAttachFileList.push({'fileName' : file.name, 'fileContent' : stream ,'contentType' : fileType });
                component.set("v.attachFileList", totalAttachFileList);
                
                $('.closefileimg').hide();
                var element = document.getElementById(file.name);   
                var width = 1; 
                var identity = setInterval(scene, 10); 
                function scene() { 
                    if (width >= 100) {
                        element.style.backgroundColor='#e1e1e1';
                        element.style.opacity='1'
                        clearInterval(identity); 
                        $('.closefileimg').show();
                        document.getElementById(btnId).disabled = false;
                        $('#'+btnId).val(null);
                    } else { 
                        width++;  
                        element.style.width = width + '%';
                        
                    }                     
                }
                 
            }, false);
            if (file) {
                reader.readAsDataURL(file)
            }
        }
    },
    deleteFeedImgClicked : function(component, event, helper) {
        console.log(event)
        let index = event.target.dataset['index']
        let fileName=event.target.nextElementSibling.id;
        let modalDetails= {
            "headingText":"Are you sure you want to delete this file?",
            "bodyText":fileName,
            "leftBtnText":"No - keep this file",
            "rightBtnText":"Yes - delete"
        }
        component.set('v.fileSelectedToDelete',fileName)
        var FileList = component.get("v.attachFileList");
        let modalBoxCmp= component.find("attachFileDelPopup")
        modalBoxCmp.modalBox(component, 'attachFileDelPopup', modalDetails, function(result){
            FileList.splice(index, 1)
            component.set("v.attachFileList", FileList)
            modalBoxCmp.hideModalBox()
        },index, 'modal-md',function(){
            modalBoxCmp.hideModalBox()
        });
    },
    onGetAllAttachedFiles: function(component, event, helper) {
        var FileList = component.get("v.attachFileList");
        let params = event.getParams().arguments
        let attachmentId = component.get("v.attachmentId")
        if(attachmentId == params.attachmentId){
            params.successCallback(FileList)
        }
    },
})