({
    //Start :: Added by Subhajit :: 245/233 :: 11/14/2018
    checkSandbox : function(component, event) {
        var checkSandboxAction = component.get("c.checkIsSandbox");
        
        checkSandboxAction.setCallback(this, function(response) {
            if(response.getState() === "SUCCESS") {
                var isSandbox = response.getReturnValue();
                console.log('@@@@@isSandbox ::'+isSandbox);
                component.set("v.isSandbox",isSandbox);
            }
        });
        
        $A.enqueueAction(checkSandboxAction);
    },
    //End :: Added by Subhajit :: 245/233 :: 11/14/2018
    getcheckInternalDualAccessUser : function(component, event, helper){
        var getcheckInternalDualAccessUserAction = component.get("c.checkInternalDualAccessUser");
        
        getcheckInternalDualAccessUserAction.setCallback(this, function(a) { 
            var state = a.getState();
            if (state == "SUCCESS") {
                 var result = a.getReturnValue();
                if (!$A.util.isUndefined(result)) {
					component.set('v.isInternalDualAccessUser', result);
                }
            }        
        });
          $A.enqueueAction(getcheckInternalDualAccessUserAction);
    }

})