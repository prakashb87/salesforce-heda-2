({	
    /* page load calling feedback types*/
    onInit: function(component, event, helper) {
        helper.setLabels(component, event, helper)
        component.set("v.feedbackFileList", []);
        var pathname = window.location.pathname.split("/s/")[1] = "research-support";
        var url = window.location.protocol+"//"+window.location.hostname + window.location.pathname.split("/s/")[0]+'/s/'+pathname;
        component.set("v.envURL", url);
        
        component.set('v.showLoader', true)
        var getFeedbackListAction = component.get("c.getAllfeedbackTopicInfo");
        getFeedbackListAction.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "SUCCESS") {
                var result = a.getReturnValue();  
                if (!$A.util.isUndefined(result)) {
                    helper.setFeedbackTopic(component, result)
                }
            } else if (state == "ERROR") {
                //component.set("v.supportTopics", "No Topics Available");
            }
            
        });       
        $A.enqueueAction(getFeedbackListAction);
        component.set("v.submittedSuccess", false);
    },
    /* setting labels*/
    setCommentLabel: function(component, event, helper) {
        let index = event.target.id ? event.target.id : event.target.parentElement.id
        let value = document.getElementsByName('feedback')[index].value
        var label=''
        switch(value){
            case 'Suggested Improvements': 
                label=$A.get("$Label.c.What_kind_of_improvements_you_would_like_to_see")
                break;
            case 'Articles & FAQs':
                label=$A.get("$Label.c.What_you_would_like_to_see_in_our_articles")
                break;
            case 'Other':
                label=$A.get("$Label.c.Anything_else_you_have_to_say_about_researcher_portal")
                break;
            default:
                label=$A.get("$Label.c.What_kind_of_improvements_you_would_like_to_see")
        }
        document.getElementById('commentLabel').innerText = label
    },
    /* Open file upload modal*/
    openfileUpload: function(component, event, helper) {
      $('#fileUpload').trigger('click');
    },
    /* file upload logic and validations*/
    fileUpload: function(component, event, helper) {
        event.stopPropagation();         
        let fileExists=false
        $("#fileNameError").hide();
        $("#fileTypeError").hide();
        $("#attachmentError").hide();
        var fileNames = [];
        var file = event.target.files[0];
        var fileType = file.name ? file.name.split('.').pop().toLowerCase():file.name;// file.type ? file.type.split('/')[1]:file.type
        var validFormat =  $A.get("$Label.c.validUploadFileFormat")
        var validFileFormat = validFormat.split(',')
        if(validFileFormat.indexOf(fileType) === -1){
            $("#fileTypeError").show()
            return
        }
        var FileSize = file.size / $A.get("$Label.c.file_Size_in_bytes"); //  1 MB
        var totalfeedbackFileList = component.get("v.feedbackFileList");
        totalfeedbackFileList.forEach(function(item){
            if(item.fileName == file.name){
                fileExists= true
            }
        })
        if (FileSize > 2) {
            console.error('file size error');
            $("#attachmentError").show();
        } else if(fileExists){
             $("#fileNameError").show();
        } else {
            $("#attachmentError").hide();   
            document.getElementById("fileUpload").disabled = true;
             $('.feedbackcancelbtn')[0].disabled = true;
        $('.feedbacksubmitbtn')[0].disabled = true;
            var reader = new FileReader();
            reader.addEventListener("load", function (e) { // Setting up base64 URL on image
                var stream = reader.result.split(',')[1];
                totalfeedbackFileList.push({'fileName' : file.name, 'fileContent' : stream ,'contentType' : fileType });
                component.set("v.feedbackFileList", totalfeedbackFileList);
                $('.closefileimg').hide();
                var element = document.getElementById(file.name);   
                var width = 1; 
                var identity = setInterval(scene, 10); 
                function scene() { 
                    if (width >= 100) {
                        element.style.backgroundColor='#e1e1e1';
                        element.style.opacity='1'
                        clearInterval(identity); 
                        $('.closefileimg').show();
                        document.getElementById("fileUpload").disabled = false;
                        $('#fileUpload').val(null);
                        $('.feedbackcancelbtn')[0].disabled = false;
        				$('.feedbacksubmitbtn')[0].disabled = false;
                    } else { 
                        width++;  
                        element.style.width = width + '%';
                        
                    }                     
                } 
            }, false);
            if (file) {
                reader.readAsDataURL(file)
            }
        }  
    },
    /* comment change error watch*/
    onChangeComment:function(component, event, helper){
        let commentValue = event.target.value
        if(commentValue.trim().length > 0){
            $("#commentError").hide();
        }
    },
    /* rating change error watch*/
    ratingSelected:function(component, event, helper){
        if(event.target.value){
            $("#ratingError").hide();
        }
    },
    /* feedback submit handling*/
    submitFeedback:function(component, event, helper){
        let feedbackAbout = $("input[name='feedback']:checked").val();
        let commentValue = $('#comment').val()
        let satisfactionRating = $("input[name='rating']:checked").val();

        if(commentValue.trim().length === 0){
            $("#commentError").show();
        } else {
            $("#commentError").hide();
        }
        if(!satisfactionRating){
            $("#ratingError").show();
        } else {
            $("#ratingError").hide();
        }
        if(commentValue.trim().length === 0 || !satisfactionRating){
            console.error('feedback form validation error')
        } else {
            $('.feedbackcancelbtn')[0].disabled = true;
            $('.feedbacksubmitbtn')[0].disabled = true;
            let formData = {
                portalFeedback: feedbackAbout,
                description : commentValue,
                satisfactionRating: satisfactionRating,
                recordType : $A.get("$Label.c.ResearcherPortalFeedback")               
            }
            let caseAttachments  = component.get("v.feedbackFileList")
            component.set('v.submitLoader', true)
            window.scrollTo(250,250)
            helper.submitFeedbackForm(component, event, helper,formData, caseAttachments);
            console.info('submitted data', formData)
            //component.set("v.submittedSuccess", true);
        }
        
    },
    handleComponentEvent:function (component, event, helper) {
        let params = event.getParam('arguments');
        let fileIndex = params.index ? parseInt(params.index):params.index;
        let FileList = component.get("v.feedbackFileList");
        //let feedbackFileDelId = component.get("v.feedbackFileDelId")
        //let fileIndex = feedbackFileDelId.split('-')[1]
        console.log(params.param1)
        if(params.param1 =='feedbackCancelPopup'){
            window.close()
        } else if(params.param1 == 'feedbackFileDelPopup'){
            console.log(component.get("v.feedbackFileList"))
            FileList.splice(fileIndex, 1)
            component.set("v.feedbackFileList", FileList)
        } 
        helper.hideModal(component, event, helper, params.param1, fileIndex)
        
    },
    callCancelPopup : function(component, event, helper) {
        helper.showModal(component, event, helper, 'feedbackCancelPopup')
    },
    closeFeedback: function(component, event, helper) {
        window.close()
    },
    resetFeedback: function(component) {
        location.reload()
    },
    deleteFeedImgClicked : function(component, event, helper) {
        console.log(event)
        let index = event.target.dataset['index']
        //let feedbackFileDelId = 'feedbackFileDelPopup-'+index
        //component.set("v.feedbackFileDelId", feedbackFileDelId)
        let fileName=event.target.nextElementSibling.id;
        let ModalData= {
            "headingText":"Are you sure you want to delete this file?",
            "bodyText":fileName,
            "leftBtnText":"No - keep this file",
            "rightBtnText":"Yes - delete"
        }
        component.set('v.feedbackFileDel',ModalData)
        helper.showModal(component, event, helper, 'feedbackFileDelPopup', index);
    },
    
})