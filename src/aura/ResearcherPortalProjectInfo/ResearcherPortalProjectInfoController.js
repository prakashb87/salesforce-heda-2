({
    onInit: function(component, event, helper) {
        //this function is used for init the list of added members for the selected project
        component.set("v.selectedMembers", []);
        helper.getcheckInternalDualAccessUser(component, event, helper); 
        helper.getProjectInfo(component, event, helper);       
    },
    roleChange : function(component, event, helper){
        var targetCmp = event.currentTarget;
        var index = targetCmp.dataset.index; 			 
        var selectedMember = component.get("v.selectedMembers");
        var data = selectedMember[index];
        data.memberRole = $('#roledropdown' + index).val();
    },
    
    statusChange : function(component, event, helper){			 
       // var projectRecord = component.get("v.projectRecord");
        var status = $('#statusdropdown').val();
        var statusMap = {
            Active: '',
            Closed: "The project is inactive and may still be re-opened"
        }
        console.log(statusMap[status])
        component.set("v.statusMsg", statusMap[status]);
    },
    
    enablePopperJS: function(component, event, helper) {
        jQuery("document").ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $("#addMember").on('shown.bs.modal', function(){
                $(this).find('input[type="search"]').focus();
            });
            
            $('#deleteMemberConfirmation').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var selectedIndex = button.data('index'); // Extract info from data-* attributes
                
                document.getElementById('modalMemberConfirmDelete').dataset.index = selectedIndex;
            });
        });    
        
    },
    
    clearMembers : function(component, event, helper){
        $('#searchInputMember').val("");
        component.set("v.server_result", []); 
    },
    
    searchQuery: function(component, event, helper) {
        // var inputKeyword = component.get("v.searchKeyword");
        var inputKeyword = $('#searchInputMember').val();
        var selectedMembers = component.get("v.selectedMembers");
        var projectMembers = component.get("v.projectMembers");
        
        if (inputKeyword.length > 2) {
            var searchAction = component.get("c.searchContactResults");
            
            searchAction.setParams({
                inputKeyword: inputKeyword
            });
            //Setting the Callback
            searchAction.setCallback(this, function(a) {
                //get the response state
                var state = a.getState();
                
                //check if result is successfull
                if (state == "SUCCESS") {
                    var result = a.getReturnValue();
                    if (!$A.util.isUndefined(result)){
                        var temp = [];
                    }                          
                    result.forEach(function(member){
                        helper.checkMemberfromResponse(component,member, temp, helper); 
                    });
                    //if (inputKeyword == $('#searchInputMember').val()){ //Added by Khushman
                    component.set("v.server_result", temp);
                    //  }
                } else if (state == "ERROR") {
                    alert('Error in calling server side action:' + JSON.stringify(a) + '\nReturn Value:' + JSON.stringify(a.getReturnValue()));
                }
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(searchAction);
            
        } else {
            component.set("v.server_result", []);
        }
        
    },    
    
    selectMember: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        
        var index = targetCmp.dataset.indexvar;
        
        var selectedMember = component.get("v.selectedMembers");
        
        var projectMembers = component.get("v.projectMembers");
        
        var server_result = component.get("v.server_result");
        var pjtType = component.get("v.projectRecord").IsRMProject;
        
        var item = server_result[index];
        item.memberRole = $A.get("$Label.c.Researcher");
        selectedMember.push(item);
        if(pjtType == true){
            var isRMResearcher = projectMembers.findIndex(function(member){
                return (member.memberRole ==$A.get("$Label.c.Researcher") && member.IsRMMember == true && item.contactId == member.contactId)
            });
            var selectedMemberIndex = selectedMember.findIndex(function(member){
                return item.contactId == member.contactId;
            });
            if(isRMResearcher != -1){
                item.memberRole = $A.get("$Label.c.CIDelegate");
                 setTimeout(function(){
                $('#roledropdown'+selectedMemberIndex).val('CI Delegate')
                $('#roledropdown'+selectedMemberIndex).attr('disabled',true)
            },10)
            }           
        }
        $('#searchInputMember').val("");
        component.set("v.server_result", []);
        component.set("v.selectedMembers", selectedMember);       
    },
    
    removeAddedMember: function(component, event) {
        var targetCmp = event.currentTarget;
        var index = targetCmp.dataset.index;
        
        var selectedMember = component.get("v.selectedMembers");
        
        if (index != undefined) {
            selectedMember.splice(index, 1);
        }
        
        component.set("v.selectedMembers", selectedMember);
    },
    
    deleteMember: function(component, event, helper) {
        
        $('#deleteMemberConfirmation').modal('hide');
        
        var recordId = helper.getRecordID(component);
        
        
        var targetCmp = event.currentTarget;
        var index = targetCmp.dataset.index;
        
        var projectMembers = component.get("v.projectMembers");
        
        var targetMember = projectMembers[index];
        
        //Function to delete the member in SF needs to be implemented
        var deleteMemberAction = component.get("c.deleteResearcherMemberSharing");
        var temp = [targetMember];
        deleteMemberAction.setParams({
            selectedMembersJSON: JSON.stringify(temp),
            projectId: recordId
        });
        
        //Setting the Callback
        deleteMemberAction.setCallback(this, function(a) {
            
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if (state == "SUCCESS") {
                projectMembers.splice(index, 1);
                component.set("v.projectMembers", projectMembers);
                var isext = projectMembers.filter(function(data) {
                    if (data.IsExternalContact === true) {
                        return data
                    }
                });
                component.set("v.IsExternalContactExists", isext);
                
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(deleteMemberAction);
    },
    
    closeModal: function(component, event, helper) {
        $('#addMember').modal('hide');
        helper.clearValues(component);
    },
    
    cancelDelete: function(component) {
        $('#deleteMemberConfirmation').modal('hide');
    },
    
    saveModal: function(component, event, helper) {        
        var recordId = helper.getRecordID(component);
        
        var selectedMember = component.get("v.selectedMembers");
        
        var projectInfo = component.get("v.projectRecord");        
        
        var addMemberAction = component.get("c.addMembers");
        addMemberAction.setParams({
            selectedMembersJSON: JSON.stringify(selectedMember),
            projectId: recordId
            
        });
        component.set("v.selectedMembers", []);
        $('#addMember').modal('hide');
        //Setting the Callback
        addMemberAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue(); 
                selectedMember = result;
                var temp = component.get('v.projectMembers');
                temp = temp.concat(selectedMember);
                $('#addMember').modal('hide');
                var isext = temp.filter(function(data) {
                    if (data.IsExternalContact === true) {
                        return data
                    }
                });
                component.set("v.IsExternalContactExists", isext);
                temp =  helper.sortProjectMembers(component,event,helper,temp);
                component.set('v.projectMembers', temp);
                component.set("v.selectedMembers", []);
                
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(addMemberAction);
    },
    openEditPjtStatusPopup : function(component, event, helper) {   
        var projectRecord = component.get("v.projectRecord");
        var statusMap = {
            Active: '',
            Closed: "The project is inactive and may still be re-opened"
        }       
        let modalDetails ={
            "headingText":"Update key details",
            "leftBtnText":"Cancel",
            "rightBtnText":"Save changes"
        }       
        component.set("v.startDate", projectRecord.projectStartDate); 
        component.set("v.endDate", projectRecord.projectEndDate);
         component.set("v.startdateValidationError", false);
        component.set("v.emptyStartdateValidationError", false);
        component.set("v.enddateValidationError", false);
        let modalBoxCmp= component.find("editKeyDetailsPopup")
        modalBoxCmp.modalBox(component, 'editKeyDetailsPopup', modalDetails, function(result){
            helper.updateProjectDetails(component, projectRecord)
        },'0', 'modal-sm',function(){
            modalBoxCmp.hideModalBox()
            component.set("v.modalErrorBox", false)
        });      
        $('#statusdropdown').val(projectRecord.groupStatus);
        component.set("v.statusMsg", statusMap[projectRecord.groupStatus]);
    },
    //start date validation
    startdateUpdate: function(component, event, helper) {
        $('.slds-align_absolute-center').attr("type", "button");
        var startDateVal = component.find("startDate").get("v.value");
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if (dd < 10) {
            dd = '0' + dd;
        }
        // if month is less then 10, then append 0 before date    
        if (mm < 10) {
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy + '-' + mm + '-' + dd;
        if (component.get("v.startDate") != undefined && component.get("v.enddate") != undefined) {
            component.set("v.startdateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
            component.set("v.enddateValidationError", false);
        } else if (component.find("enddate").get("v.value") != '' && component.find("startDate").get("v.value") >= component.find("enddate").get("v.value")) {
            component.set("v.startdateValidationError", false);
            component.set("v.enddateValidationError", true);
            component.set("v.emptyStartdateValidationError", false);
        } else {
            component.set("v.startdateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
            component.set("v.enddateValidationError", false);
        }
    },
    
    //End Date Validation - should be greater that start date, and start date shouldnot be empty
    endDateUpdate: function(component, event, helper) {
        $('.slds-align_absolute-center').attr("type", "button");
        var startDateField = component.get("v.startDate");
        if (component.find("enddate").get("v.value") != '' && component.find("enddate").get("v.value") <= component.find("startDate").get("v.value")) {
            component.set("v.enddateValidationError", true);
            component.set("v.emptyStartdateValidationError", false);
        } else if ((component.find("startDate").get("v.value") == "" || component.find("startDate").get("v.value") == null) && component.find("enddate").get("v.value") != '') {
            component.set("v.emptyStartdateValidationError", true);
            component.set("v.enddateValidationError", false);
        } else {
            component.set("v.enddateValidationError", false);
            component.set("v.emptyStartdateValidationError", false);
        }
    }
    
})