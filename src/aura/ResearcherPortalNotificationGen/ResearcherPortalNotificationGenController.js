({

    onRender: function(component, event, helper) {
        var contentAttrs = component.get('v.contentAttrs');
        var customColour = contentAttrs.customColour;

        var poundHexRegEx = new RegExp('(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)');
        var hexRegEx = new RegExp('(^[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)');

        if (poundHexRegEx.test(customColour)) {
            customColour = customColour;
        } else if (hexRegEx.test(customColour)) {
            customColour = '#' + customColour;
        } else {
            customColour = '';
        }
        component.set('v.customColour', customColour);
    },

    /**
     * Closes the notification and sets it as viewed using the OrchestraCMS Social API
     */
    close: function(component, event, helper) {
        if (component.find('noShowAgain').getElement().checked) {
            var originId = component.get('v.contentBundle.originId');
            console.log('### originId: ' + originId);
            // a4z0l0000009hScAAI

            var action = component.get('c.setViewed');
            action.setParams({
                "originId" : originId
            });
            action.setCallback(this, function(response) {
                if ('SUCCESS' == response.getState()) {
                    console.log('# Set Viewed Response: ' + response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }

        var notification = component.find('notification');
        notification.destroy();
    }
})