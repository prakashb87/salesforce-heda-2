({
    //Method call to fetch publication details
    getContractDetails : function(component, event, helper, recordIdIs) {
        var action = component.get("c.getContracts");
        action.setParams({
            contractId: recordIdIs,
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                result[0].submittedDate = result[0].submittedDate != undefined ? this.formatDate(result[0].submittedDate) : '';
                result[0].fullyExecutedDate = result[0].fullyExecutedDate != undefined ? this.formatDate(result[0].fullyExecutedDate) : '';
                component.set("v.contractDetails", result[0]);
                if(result[0].rmsMembers.length >0){
                    var authorsList = result[0].rmsMembers;
                       authorsList = authorsList.sort(function(x,y){
                        var a = String(x.role).toUpperCase(); 
                        var b = String(y.role).toUpperCase(); 
                        if (a > b){
                            return 1 
                        }                             
                        if (a < b) {
                            return -1 
                        }
                        
                        return 0; 
                    });
                    component.set("v.rmsMembers", authorsList);
                }
            }
        });
        $A.enqueueAction(action);
    },
    formatDate: function(dateString) {
        
        var date = new Date(dateString);
        
        var day = date.getDate();
        var month = date.getMonth()+1;
        var year = date.getFullYear();
        if(day<10){
            day = '0'+day;
        }
        
        if(month<10){
            month = '0'+month;
        }
        
        return day+'/'+month+'/'+year;
    }
});