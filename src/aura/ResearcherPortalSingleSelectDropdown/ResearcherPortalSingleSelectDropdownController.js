({
  
    onInit:function(component){
        let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
        window.addEventListener('click', function(event){
            if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("rp-ssdd-dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (!openDropdown.classList.contains('hide')) {
                openDropdown.classList.add('hide');
              }
            }
            } 
        });
        
        let singleSearchLabelInline = component.get("v.singleSearchLabelInline")
        let singleSearchWidthClass = component.get("v.singleSearchWidthClass")
        let classes =""
        if(singleSearchLabelInline && singleSearchWidthClass){
           classes= 'relative inline-block '+singleSearchWidthClass
        } else if(singleSearchLabelInline){
            classes= 'relative inline-block defaultWidth250'
        } else if(singleSearchWidthClass){
            classes= 'relative '+singleSearchWidthClass
        } else {
            classes = 'relative defaultWidth250'
        }
        component.set("v.singleSearchComponentBoxCss", classes)
        setTimeout(function(){
            let singleSearchDropdownData = component.get("v.singleSearchDropdownData")
            let singleSearchDefaultSelected = component.get("v.singleSearchDefaultSelected")
            if(singleSearchDefaultSelected){
                component.set('v.singleSearchSelectedValue',singleSearchDefaultSelected)
            }
            component.set("v.singleSearchFilterDropownData", singleSearchDropdownData)
        },10)
    },
    singleSearchDropDownBlurFunction:function(component, event, helper){
        let singleSearchDropdownSearchable = component.get('v.singleSearchDropdownSearchable')
        if(singleSearchDropdownSearchable){
         let id = component.get("v.singleSearchDropdownId")
         let singleSearchSelectedValue = component.get('v.singleSearchSelectedValue')
         if(singleSearchSelectedValue==""){
             $('#'+id+'-inputbox').val('')
         }
        }
    },
    toggleDropdown: function(component, event, helper) {
        let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
        let id = singleSearchDropdownId+'-List'
        document.querySelectorAll('.rp-ssdd-dropdown').forEach((e)=>{
            if(e.id==id){
            } else {
              $('#'+e.id).addClass("hide")
           }
        })
    },
    selectTopic: function(component, event, helper) {
        event.preventDefault()
        let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
        let singleSearchDropdownData = component.get("v.singleSearchDropdownData")
        let singleSearchParent = component.get('v.singleSearchParent')
        let selectedId = event.target.getAttribute('data-value')
        let filterData = singleSearchDropdownData.filter(function(item){
            return item.id == selectedId
        })
        if(filterData && filterData[0] && filterData[0].title){
            component.set('v.singleSearchSelectedValue', filterData[0].title)
            component.set('v.singleSearchSelectedObj', filterData[0])
            component.set('v.singleSearchDropdownDisabled', true)
            singleSearchParent.singleSearchDropdownCallback(singleSearchDropdownId, filterData[0])
        } else {
            singleSearchParent.singleSearchDropdownCallback(singleSearchDropdownId, '')
        }
    },
    singleSearchFilterForFunction:function(component, event, helper){
        let singleSearchDropdownSearchable = component.get('v.singleSearchDropdownSearchable')
        let singleSearchDropdownData = component.get("v.singleSearchDropdownData")
        let singleSearchFilterDropownData = component.get("v.singleSearchFilterDropownData")
        let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
        let id = singleSearchDropdownId+'-List'
        if(singleSearchDropdownSearchable){
            let val=event.target.value ? event.target.value.toUpperCase() :''
            if(val && val.trim() && val.trim().length>2){
                $('#'+id).removeClass('hide')
                let filteredData=  singleSearchDropdownData.filter(function(item){
                    let itemUpperCase=''
                    itemUpperCase = item.title.toUpperCase()
                    return itemUpperCase.includes(val);
                })
                if(filteredData && filteredData.length>0){
                    component.set("v.singleSearchFilterDropownData", filteredData)
                } else {
                    component.set("v.singleSearchFilterDropownData", [])
                }
            } else {
                $('#'+id).addClass('hide')
                component.set("v.singleSearchFilterDropownData", singleSearchDropdownData)
            }
        }
     },
     closeIconClicked:function(component, event, helper){
        component.set('v.singleSearchSelectedValue', '')
        component.set('v.singleSearchSelectedObj', '')
        component.set('v.singleSearchDropdownDisabled', false)
     },
     onGetSingleSelectValue: function(component, event, helper) {
         let params = event.getParams().arguments
        var singleSearchSelectedValue = component.get('v.singleSearchSelectedValue')
        var singleSearchSelectedObj = component.get("v.singleSearchSelectedObj");
         let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
            if(singleSearchSelectedValue){
                if(singleSearchDropdownId == params.singleSearchDropdownId){
                    params.successCallback(singleSearchSelectedObj)
               }
            } else {
                 params.successCallback('')
            }
    },
    onResetSingleSelectDropdown: function(component, event, helper) {
        let params = event.getParams().arguments
        let singleSearchDropdownId = component.get("v.singleSearchDropdownId")
        component.set('v.singleSearchSelectedValue', '')
        component.set('v.singleSearchSelectedObj', '')
        component.set('v.singleSearchDropdownDisabled', false)
    }
	
})