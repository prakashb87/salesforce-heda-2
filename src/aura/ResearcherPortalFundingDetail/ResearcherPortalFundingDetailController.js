({
    onInit: function(component, event, helper) {
        helper.getcheckInternalDualAccessUser(component, event, helper);
        $(document).click(function(e) {
            if ($(e.target).closest('#filterSelect').length === 0) {
                //$('#filterSelectDiv').removeClass('clicked');
                $('#filterSelectDiv').blur();
            }
        });
        component.set("v.projectSelected", false);
        
        var getProjectListAction = component.get("c.getMyResearchProjectListsForFunding");
        //Setting the Callback
        getProjectListAction.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if (state == "SUCCESS") {
               
                var result = a.getReturnValue();
                
                if (!$A.util.isUndefined(result)) {
                    if(component.get('v.isInternalDualAccessUser') == true){
                        result.lstResearchProject = [];
                        component.set("v.projects", result);
                    }
                    component.set("v.projects", result.lstResearchProject);
                    if(result.lstResearchProject.length ==0){
                         $('#emptytext').toggle(true);
                    }
                    else{
                       helper.getProjectDetails(component, result.lstResearchProject[0].projId); 
                    }                     
                    var pjtsLength = result.lstResearchProject.length;
                    if (pjtsLength == 1) {
                        // Project details
                        helper.getProjectDetails(component, result.lstResearchProject[0].projId, false);
                    }
                }
            } else if (state == "ERROR") {
                alert('Error in calling server side action');
            }
        });

        //adds the server-side action to the queue        
        $A.enqueueAction(getProjectListAction);

    },

    toggleDropdown: function(component, event, helper) {
        $('#RMprojects').val('');
        helper.restoreProjectList();
        $('#pjtDropdown').toggle(true);
        helper.toggleArrowState('#pjtDropdown','#searchboxArrow');

        $(document).click(function(e) {
            if ($(e.target).closest('#RMprojects').length === 0 && $(e.target).closest('.input-group-btn').length === 0) {
                $('#pjtDropdown').toggle(false);
                helper.toggleArrowState('#pjtDropdown','#searchboxArrow');
                var projectTitle = $('#projectTitle').text();
                $('#RMprojects').val(projectTitle);
            }
        });
    },

    toggleDropdownFromArrow: function(component, event, helper) {
        $('#RMprojects').val('');
        helper.restoreProjectList();
        $('#pjtDropdown').toggle();
        helper.toggleArrowState();

        if (!$('#pjtDropdown').is(':visible')) {
            var projectTitle = component.get("v.projectRecord").projectTitle;
            $('#RMprojects').val(projectTitle);
        }
    },

    addHandler: function(component, event, helper) {        
        $(document).click(function(e) {
            if ($(e.target).closest('#filterSelect').length === 0) {
                //$('#filterSelectDiv').removeClass('clicked');
                $('#filterSelectDiv').blur();
            }
        });
    },

    //function call to display the list of data for dropdown and filter based on input data entered
    filterForFunction: function(component, event, helper) {
        var input, filter, ul, li, a, i, j = 0;
        $("#pjtDropdown").css("display", "block");
        input = $("#RMprojects").val();
        filter = input.toUpperCase();
        a = $("#pjtDropdown li");
        for (i = 0; i < a.length; i++) {
            //a[i].removeAttribute("style");          
            if (a[i].attributes[1].value.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";                   
                if(j%2 === 0){
                   a[i].style.backgroundColor = "#ffffff";
                }
                else{
                   a[i].style.backgroundColor = "#eaeaea";                   
                }
                 j++;
            } else {
                a[i].style.display = "none";
            }             
        }      
    },

    navigateToProject: function(component, event, helper) {
        var targetCmp = event.currentTarget;

        var recordId = targetCmp.dataset.value;
        var getProjectDetailsAction = component.get("c.getProjectDetailViewByProjectId");

        helper.getProjectDetails(component, recordId, true);

    },
    
    changeArrow: function(component, event, helper) {
    	if($('#filterSelectDiv').hasClass('clicked')){
    		$('#filterSelectDiv').removeClass('clicked');
    	} else {
    		$('#filterSelectDiv').addClass('clicked');
    	}
     
    },
    
    resetArrow: function(component, event, helper) {
    	$('#filterSelectDiv').removeClass('clicked');    	
    },
    
    fetchExpenseItems: function(component, event, helper) {
        var selectedYear = $('#filterSelect').val();
        var projectId = component.get("v.projectRecord").projId;
        helper.getExpensesDetails(component, projectId, selectedYear);
    },
        // method call to toggle impact category dropdown and on edit based on data check the checkbox
    toggleYearDropdown: function(component, event, helper) {       
        var topicDropdown = $('#yearfilterDropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#yearfilterDropdown','#arrowyearfilter');            
        }
         else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#yearfilterDropdown','#arrowyearfilter');     
        }
        $(document).click(component, function(e) {
             if ($(e.target).closest('#yearfilterDropdown').length === 0 && $(e.target).closest('#yearfilter').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#yearfilterDropdown').is(":visible")) {
                console.log("inside if loop")
                $('#yearfilterDropdown').toggle(false);
                helper.toggleArrowState('#yearfilterDropdown', '#arrowyearfilter');
            }
        });
    },
    selectYear: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var recordId = targetCmp.dataset.value;
        component.set("v.selectedType", recordId);
        $('#yearfilterDropdown').toggle(false);
        helper.toggleArrowState('#yearfilterDropdown', '#arrowyearfilter');
        var projectId = component.get("v.projectRecord").projId;
        helper.getExpensesDetails(component, projectId, recordId);
    }
});