({
    onModalBox:function(component, event, helper) {
        let params = event.getParams().arguments
        component.set('v.modalBoxId',params.modalId)
        $("#"+params.modalId).on('show.bs.modal', function() {
			if(params.cssClass){
               $('#'+params.modalId+'-modalBox').addClass(params.cssClass)
            }            
        })
        $("#"+params.modalId).on('shown.bs.modal', function() {
          $('#'+params.modalId+'-closeBtn').focus();
          component.set('v.modalId', params.modalId)
          component.set('v.modalDetails', params.modalDetails)
          component.set('v.callback', params.successCallback)
          component.set('v.cancelCallback', params.cancelCallback)
          if(params.index){
             component.set('v.modalIndex', params.index) 
          }
           
        })
        $("#"+params.modalId).modal('show');
    },
    cancelModalBox:function(component, event, helper) {
        var modalId = component.get('v.modalId')
        var cancelCallback = component.get('v.cancelCallback')
        if(cancelCallback){
            cancelCallback()
        } else {
            $("#"+modalId).modal('hide');
        }
    },
    hideModalBox:function(component, event, helper) {
        var modalId = component.get('v.modalId')
        $("#"+modalId).modal('hide');
    },
    callPrimaryAction:function(component, event) {
        var modalId = component.get('v.modalId')
        var index = component.get('v.modalIndex')
        var callback = component.get('v.callback')
        var result ={
            'modalId':modalId,
            'index':index
        }
        callback(result)
    }
})