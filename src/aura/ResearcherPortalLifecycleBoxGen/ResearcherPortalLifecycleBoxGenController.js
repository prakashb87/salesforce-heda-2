({
    onInit: function(component, event, helper) {
        var contentAttrs = component.get('v.contentAttrs');
        var htmlContent = contentAttrs.htmlContent;

        var sitePrefix = window.location.pathname.split('/')[1];
        if (sitePrefix == 's' || !sitePrefix) {
            sitePrefix = '';
        }

        if (sitePrefix) {
            htmlContent = htmlContent.replace(new RegExp('"(\/servlet\/servlet\.FileDownload[^"]+)"', 'g'), '"/' + sitePrefix + '$1"');
            contentAttrs.htmlContent = htmlContent;
            component.set('v.contentAttrs', contentAttrs);
        }
    },
})