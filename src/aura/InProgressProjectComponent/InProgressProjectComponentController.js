/*******************************************
 Purpose: To Navigate to Blank Page
 History:
 Created by Esther Ethelbert on 20/08/2018
 *******************************************/
({
	// Method call to navigate to create new project page
	goToCreatePage: function(component,event) {
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
        "url": "/create-new-project"
		});

		urlEvent.fire();
	}

})