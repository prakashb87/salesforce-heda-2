({
	SearchHelper: function(component, event) {
        var action = component.get("c.fetchAccount");
        action.setParams({
            'searchKeyWord': component.get("v.searchText"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", true);
                } else {
                    component.set("v.Message", false);
                }
                component.set("v.numberOfRecord", storeResponse.length);
                console.log(storeResponse);
                component.set("v.searchResult", storeResponse);
            }
 
        });
        $A.enqueueAction(action);
 
    },
    deleteSelectedHelper: function(component, event, deleteRecordsIds) {
       	var action = component.get('c.updateAccount');
        action.setParams({
            "selectedIds": deleteRecordsIds,
            "campaignId": component.get("v.recordId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(state);
                if (response.getReturnValue() == 'true') {
                    $A.get('e.force:refreshView').fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                } else {
                    console.log('There was an error');
                    $A.get('e.force:refreshView').fire();
                }   
            }
        });
        $A.enqueueAction(action);
    },
})