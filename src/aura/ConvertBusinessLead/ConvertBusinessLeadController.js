({
    
    convertLead : function(component, event, helper) {
        
        var leadRecordObject = component.get("v.simpleLead");         
        if(leadRecordObject.Opportunity_Name__c == null && leadRecordObject.Don_t_create_a_new_opportunity_upon_Lead__c == false && leadRecordObject.Matched_Opportunity__c == null){
            component.set("v.message",'Error! Please enter an opportunity name to continue');
            component.set("v.processRender",false);
        }else{
        var action = component.get("c.convertBusinessLead");
        action.setParams({
                "idforConversion": leadRecordObject
        });
        
        var navEvent = $A.get("e.force:navigateToURL");
        action.setCallback(this, function(response) {
            var state = response.getState();
                console.log(state);
            if(state === "SUCCESS") {
                var returnValforLead = response.getReturnValue();
                    var result = returnValforLead.split(','); 
                    
                    if(result[0]=='true'){
                        component.set("v.processRender",true);
                navEvent.setParams({
                            "url": "/"+result[2]
                });
                var dismissActionPanel = $A.get("e.force:closeQuickAction").fire;
                navEvent.fire();
            } 
                }else{ 
                    var errors = response.getError(); 
                    component.set("v.message",errors[0].message);
                    component.set("v.processRender",false);
                }
        });    
        $A.enqueueAction(action);
        }
    }
})