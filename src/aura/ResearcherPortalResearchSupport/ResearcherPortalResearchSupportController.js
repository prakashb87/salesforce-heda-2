({
    onInit: function(component, event, helper) {

        var pathname = window.location.pathname.split("/s/")[1] = "portal-feedback";
        var url = window.location.protocol+"//"+window.location.hostname + window.location.pathname.split("/s/")[0]+'/s/'+pathname;
        component.set("v.envURL", url);
        helper.callApexServer(component,helper,"c.getResearcherLocation",helper.successLocationAction,helper.errorAction)
        helper.callApexServer(component,helper,"c.getAllTopicSubtopicInfo",helper.successSubtopicAction,helper.errorAction)
        helper.callApexServer(component,helper,"c.getIsQueryRelatedToProjectValues",helper.successQueryRelatedAction,helper.errorAction)
        helper.callApexServer(component,helper,"c.getProjectsAssigned",helper.successProjectsAction,helper.errorAction)
        setTimeout(function(){
            helper.callApexServer(component,helper,"c.getContractsAssigned",helper.successContractsAction,helper.errorAction)
            helper.callApexServer(component,helper,"c.getEthicsAssigned",helper.successEthicsAction,helper.errorAction)
            helper.callApexServer(component,helper,"c.getMilesthonesAssigned",helper.successMilesthonesAction,helper.errorAction)
            helper.callApexServer(component,helper,"c.getPublicationAssigned",helper.successPublicationAction,helper.errorAction)
        },2000)

        component.set("v.submittedSuccess",false);
        component.set("v.serverError", false);
    },

    dropdownCallback:function(component, event, helper){
        let params = event.getParam('arguments');
        let selectedValue = params.value
        if(params.dropdownId =='rp-rs-locationDropdown'){
            component.set("v.selectedLocation", selectedValue)
            if(selectedValue == 'Vietnam'){
                $("#supportTopicError").hide();
                $("#supportSubTopicError").hide();
                let subTopics = ['Support services for Vietnam Researchers']
                let supportTopics = ['Finding funding']
                component.set("v.supportTopics", supportTopics)
                component.set("v.subTopics",subTopics)
                component.set("v.selectedTopic",supportTopics[0])
                component.set("v.selectedSubtopic",subTopics[0])
                component.set("v.isLocationAustralia", false)
            } else if(selectedValue == 'Europe'){
                $("#supportTopicError").hide();
                $("#supportSubTopicError").hide();
                let subTopics = ['Support services through RMIT Europe']
                let supportTopics = ['Finding funding']
                component.set("v.supportTopics", supportTopics)
                component.set("v.subTopics",subTopics)
                component.set("v.selectedTopic",supportTopics[0])
                component.set("v.selectedSubtopic",subTopics[0])
                component.set("v.isLocationAustralia", false)
            }else {
                let actualSupportTopicList = component.get("v.actualSupportTopicList")
                helper.setSubTopic(component, actualSupportTopicList)
                component.set("v.selectedTopic",'')
                component.set("v.selectedSubtopic",'')
                component.set("v.subTopics",[])
                component.set("v.isLocationAustralia", true)
            }
        }
        if(params.dropdownId == 'rp-rs-topicDropdown'){
            let totalTopic = component.get("v.topicsDict")
            let selectedArr = totalTopic.filter(function(item){
                return item.key == selectedValue
            })
            if(selectedArr && selectedArr.length>0 && selectedArr[0].value){
                component.set("v.subTopics", [].concat(selectedArr[0].value))
            }
        }
        if(params.dropdownId == 'rp-rs-subtopicDropdown'){
            console.log(selectedValue)
        }

    },

    addHandler: function(component, event, helper) {

        /*$('#supportsubmitbtn').click(function(e){
            if($('#validation-form')[0].checkValidity() === false){
                e.preventDefault();
                e.stopPropagation();
            }else if($('#validation-form')[0].checkValidity() === true){
                helper.submitResearcherSupport(component, event, helper);
                component.set("v.submittedSuccess",true);
            }
            $('#validation-form')[0].classList.add('was-validated');
        });*/
    },

    goToPage: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var url = '';
        var targetId = targetCmp.id;

        if(targetId === 'logAnotherEnquriy'){
            url="/research-support";
        }else if(targetId === 'returnToDashboard'){
            url="/";
        }

        var urlEvent = $A.get("e.force:navigateToURL");

        urlEvent.setParams({
            "url" : url
        });

        urlEvent.fire();
    },



    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');

        if (openSections.length === 0) {
            cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },

    toggleDropdown: function(component, event, helper) {
        console.log(event)
        var targetCmp = event.currentTarget;

        var targetId = targetCmp.id;

        var type = targetCmp.dataset.type;

        $('#' + targetId).val('');

        //helper.restoreProjectList();
        if(type=='topic'){
            let actualsupportTopics = component.get("v.actualsupportTopics")
            component.set("v.supportTopics", actualsupportTopics)
        } else {
            let actualsubTopics = component.get("v.actualsubTopics")
            component.set("v.subTopics", actualsubTopics)
        }
        var topicDropdown = $('#' + type + 'Dropdown');
        if ((topicDropdown.css("display") === "none") || (topicDropdown.css("display") === "")) {
            topicDropdown.css("display", "block");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);

        } else {
            topicDropdown.css("display", "none");
            helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        }

        $(document).click(component, function(e) {
            if ($(e.target).closest('#supportTopics').length === 0 && $(e.target).closest('#topicDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#topicDropdown').is(":visible")) {
                $('#topicDropdown').toggle(false);
                helper.toggleArrowState('#topicDropdown', '#arrowtopic');
                var topic = $('#supportTopics').data('selected');
                $('#supportTopics').val(topic);
            }
            else if ($(e.target).closest('#subTopics').length === 0 && $(e.target).closest('#subtopicDropdown').length === 0 && $(e.target).closest('.input-group-btn').length === 0 && $('#subtopicDropdown').is(":visible")) {
                $('#subtopicDropdown').toggle(false);
                helper.toggleArrowState('#subtopicDropdown', '#arrowsubtopic');
                var topic = $('#subTopics').data('selected');
                $('#subTopics').val(topic);
            }

        });
    },

    toggleDropdownFromArrow: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var type = targetCmp.dataset.type;
        var inputId = (type === 'topic') ? 'supportTopics' : 'subTopics';
        $('#' + inputId).val('');
        helper.restoreProjectList();
        $('#' + type + 'Dropdown').toggle(true);
        helper.toggleArrowState('#' + type + 'Dropdown', '#arrow' + type);
        if (!$('#' + type + 'Dropdown').is(':visible')) {
            var topic = $('#' + inputId).data('selected');
            $('#' + inputId).val(topic);
        }
    },
    filterForFunction: function(component, event, helper) {
        var input, filter, ul, li, a, i, supportTopics, actualsupportTopics, subTopics, actualsubTopics;
        var targetCmp = event.currentTarget;
        var type = targetCmp.dataset.type;
        var dropdownId = "#" + type + "Dropdown";
        var inputId = (type === 'topic') ? 'supportTopics' : 'subTopics';
        $(dropdownId).css("display", "block");
        input = $('#' + inputId).val();
        filter = input.toUpperCase();
        if(type == 'topic'){
            actualsupportTopics = component.get("v.actualsupportTopics")
            let filteredData = actualsupportTopics.filter(function(item){
                let itemUpperCase =  item.toUpperCase()
                return itemUpperCase.includes(filter)
            })
            if(filter && filter.length>0){
                component.set("v.supportTopics", filteredData)
            } else {
                component.set("v.supportTopics", actualsupportTopics)
            }
        } else {
            subTopics = component.get("v.subTopics")
            actualsubTopics = component.get("v.actualsubTopics")
            let filteredData = actualsubTopics.filter(function(item){
                let itemUpperCase =  item.toUpperCase()
                return itemUpperCase.includes(filter)
            })
            if(filter && filter.length>0){
                component.set("v.subTopics", filteredData)
            } else {
                component.set("v.subTopics", actualsubTopics)
            }
        }
        /*a = $(dropdownId + " li");
        for (i = 0; i < a.length; i++) {
            if (a[i].attributes[1].value.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
            } else {
                a[i].style.display = "none";
            }
        }*/
    },
    selectTopic: function(component, event, helper) {
        var targetCmp = event.currentTarget;
        var topic = targetCmp.dataset.value;
        var targetId = targetCmp.parentElement.id;

        if (targetId === 'topicDropdown') {
            component.set("v.selectedTopic", topic);
            if(topic){
                $("#supportTopicError").hide();
            }

            $('#supportTopics').data('selected', topic);
            component.set("v.selectedSubtopic", '');
            $('#subTopics').data('selected','');
            $('#supportTopics').val(topic);
            $('#topicDropdown').toggle(false);
            helper.getSubTopicsFromTopic(component, event, helper,topic);
            helper.toggleArrowState('#topicDropdown', '#arrowtopic');
        } else if (targetId === 'subtopicDropdown') {
            component.set("v.selectedSubtopic", topic);
            if(topic){
                $("#supportSubTopicError").hide();
            }
            $('#subTopics').data('selected',topic);
            $('#subTopics').val(topic);
            $('#subtopicDropdown').toggle(false);
            helper.toggleArrowState('#subtopicDropdown', '#arrowsubtopic');
        }
    },
    cancelPopup:function(component, event, helper) {
        let modalDetails ={
            "headingText":"Do you wish to cancel your research support query?",
            "leftBtnText":"No - stay on this page",
            "rightBtnText":"Yes - cancel"
        }
        let modalBoxCmp= component.find("supportCancelPopup")
        modalBoxCmp.modalBox(component, 'supportCancelPopup', modalDetails, function(result){
            window.close()
        },'0', 'max600',function(){
            modalBoxCmp.hideModalBox()
        });

    },
    submitSupportPage:function(component, event, helper) {
        let supportTopics = $('#supportTopics').val()
        let subTopics = $('#subTopics').val()
        let projectQuery = $('input[name="projectQuery"]:checked').val()
        let querySubject = $('#rp-rs-querySubject').val()
        let queryDescription = $('#rp-rs-queryDescription').val()
        let selectedLocation = component.get("v.selectedLocation")
        let valid = true
        let accordionDataSelected = false
        let selectedProjectId =[]
        let contractsSelected=[]
        let ethicsSelected =[]
        let milestonesSelected =[]
        let publicationsSelected =[]
        if(projectQuery && projectQuery.toUpperCase() == 'YES'){
            let projectDDCmp= component.find("rp-ssdd-projectsListDropdown")
            projectDDCmp.getSingleSelectValue(component, 'rp-ssdd-projectsListDropdown', function(result){
                if(result && result.id){
                    selectedProjectId.push(result.id)
                } else {
                    $("#projectQueryError").show();
                    valid=false
                }
            })
        }
        if(!supportTopics){
            $("#supportTopicError").show();
            valid=false
        }
        if(!subTopics){
            $("#supportSubTopicError").show();
            valid=false
        }
        if(!querySubject || querySubject.trim().length==0){
            $("#supportSubjectError").show();
            valid=false
        }
        if(!queryDescription || queryDescription.trim().length==0){
            $("#supportQueryDetailsError").show();
            valid=false
        }
        let contractsListCmp = component.find('contractsList')
        contractsListCmp.getMultiSelectBoxSelectedIds('contractsList', function(selectedList){
            contractsSelected=selectedList
        })
        let ethicsListCmp = component.find('ethicsList')
        ethicsListCmp.getMultiSelectBoxSelectedIds('ethicsList', function(selectedList){
            ethicsSelected=selectedList
        })
        let milestonesListCmp = component.find('milestonesList')
        milestonesListCmp.getMultiSelectBoxSelectedIds('milestonesList', function(selectedList){
            milestonesSelected=selectedList
        })
        let publicationsListCmp = component.find('publicationsList')
        publicationsListCmp.getMultiSelectBoxSelectedIds('publicationsList', function(selectedList){
            publicationsSelected=selectedList
        })


        if(valid){
            let totalFiles
            let attachmentCmp= component.find("researchSupportAttachment")
            attachmentCmp.getAllAttachedFiles(component, 'researchSupportAttachment', function(result){
                totalFiles = result
            })
            console.log('totalFiles', totalFiles)
            component.set('v.submitLoader', true)
            var formData =  new Object();
            formData.researcherLocation = selectedLocation;
            formData.researcherCaseTopic = supportTopics;
            formData.researcherCaseSubTopic = subTopics;
            formData.Subject = querySubject;
            formData.Description = queryDescription;
            formData.projectQuery = projectQuery
            formData.caseJunction={}
            if(selectedProjectId && selectedProjectId.length>0){
                accordionDataSelected= true
                formData.projectSelected = selectedProjectId;
                formData.caseJunction.projectSelected=selectedProjectId
            }
            if(contractsSelected && contractsSelected.length>0){
                accordionDataSelected=true
                formData.contractsSelected = contractsSelected;
                formData.caseJunction.contractsSelected=contractsSelected
            }
            if(ethicsSelected && ethicsSelected.length>0){
                accordionDataSelected=true
                formData.ethicsSelected = ethicsSelected;
                formData.caseJunction.ethicsSelected=ethicsSelected
            }
            if(milestonesSelected && milestonesSelected.length>0){
                accordionDataSelected=true
                formData.milestonesSelected = milestonesSelected;
                formData.caseJunction.milestonesSelected=milestonesSelected
            }
            if(publicationsSelected && publicationsSelected.length>0){
                accordionDataSelected=true
                formData.publicationsSelected = publicationsSelected;
                formData.caseJunction.publicationsSelected=publicationsSelected
            }
            if(!accordionDataSelected){
                delete formData.caseJunction
            }

            formData.recordType = $A.get("$Label.c.RMITSupportRequestCase");
            formData.status = $A.get("$Label.c.Closed");
            console.log(formData)
            let params = {
                caseWrapperString: JSON.stringify(formData)
            }
            helper.callApexServer(component,helper,"c.createSupportCase",helper.successSupportPageAction,helper.errorAction, params, totalFiles)
            //component.set("v.submittedSuccess",true);

        }


    },
    subjectHandler:function(component, event, helper) {
        let subjectValue = event.target.value
        if(subjectValue.trim().length > 0){
            $("#supportSubjectError").hide();
        }
    },
    queryDescriptionHandler:function(component, event, helper) {
        let queryDescriptionValue = event.target.value
        if(queryDescriptionValue.trim().length > 0){
            $("#supportQueryDetailsError").hide();
        }
    },
    closeSupportPage: function(component, event, helper) {
        window.close()
    },
    resetSupportPage: function(component) {
        location.reload()
    },
    saveTotalFileHandler:function(component, event, helper){
        let attachmentCmp= component.find("researchSupportAttachment")
        attachmentCmp.getAllAttachedFiles(component, 'researchSupportAttachment', function(result){
            console.log(result)
        })
    },
    singleSearchDropdownCallback:function(component, event, helper){
        let params = event.getParam('arguments');
        let selectedValue = params.value
        if(selectedValue.title){
            $("#projectQueryError").hide();
        }
    },
    projectQueryHandler: function(component, event, helper) {
        console.log(event.target.value)
        let value = event.target.value
        if(value && value.toUpperCase() == 'YES'){
            component.set("v.doYouHaveAnyProject", true)
        } else {
            let projectDDCmp= component.find("rp-ssdd-projectsListDropdown")
            projectDDCmp.resetSingleSelectDropdown('rp-ssdd-projectsListDropdown')
            component.set("v.doYouHaveAnyProject", false)
            $('#projectQueryError').hide
        }
    },
    handleAccordionEvent:function(component, event, helper){
        let params = event.getParam('arguments');
        let accordionStatus =params.accordionStatus
        let accordionId =params.accordionId
        if(accordionId == 'contractsAccordion'){
            helper.setAccordionSubTextCount(component, accordionStatus, 'contracts', 'v.contractsAccordionSubText')
        }
        if(accordionId == 'ethicsAccordion'){
            helper.setAccordionSubTextCount(component, accordionStatus, 'ethics', 'v.ethicsAccordionSubText')
        }
        if(accordionId == 'milestonesAccordion'){
            helper.setAccordionSubTextCount(component, accordionStatus, 'milestones', 'v.milestonesAccordionSubText')
        }
        if(accordionId == 'publicationsAccordion'){
            helper.setAccordionSubTextCount(component, accordionStatus, 'publications', 'v.publicationsAccordionSubText')
        }
    },

})