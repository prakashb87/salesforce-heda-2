({
    getEnrollmentDetails : function(component) {
        
        var action = component.get('c.getProgramCourseEnrollDetails');
        
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            var state = response.getState();
            
            if(state ==='SUCCESS'){
                component.set('v.ProgramCourseWrapperList',result);
            }else{
                var errors = response.getError();
            }
            
        });
        $A.enqueueAction(action);        
        
    },
    
    closeDropDown : function(component) {
        var elem=document.body.parentElement;
        elem.addEventListener('click', function(e) { 
            e.stopPropagation(); 
            var elem=document.getElementById("opens");
            if(elem!=undefined)
            {
               if(elem.classList.contains('open'))
                {              
                    elem.classList.remove('open');
                } 
            }
            
            
        }) 
    },
   
    
    getCanvasRedirectionURL : function(component) {
        var action = component.get('c.geCanvasRedirectURL');
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() !=null){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    var CanvasRedirect = response.getReturnValue();
                    console.log('Url--->'+CanvasRedirect);
                    urlEvent.setParams({
                        "url": CanvasRedirect          
                    });
                    urlEvent.fire();  
                }
                
            }    
        });
        
        $A.enqueueAction(action); 
    },

    getCourseOfferings : function(component,event){
        
        var progName = event.currentTarget.getAttribute('data-progName');
        console.log('progName:::'+progName);  
        component.set("v.ProgramName",progName);
        
        var courseName = event.currentTarget.getAttribute('data-courseName');
        console.log('courseName:::'+courseName);
        component.set("v.CourseName",courseName);
        
        var courseId = event.currentTarget.getAttribute('data-courseId');
        console.log('courseId:::'+courseId);
        
        var serviceId = event.currentTarget.getAttribute('data-serviceId');
        console.log('serviceId:::'+serviceId);
        component.set("v.serviceId",serviceId);
        
        var progEnrollId = event.currentTarget.getAttribute('data-progenrollId');
        console.log('progEnrollId:::'+progEnrollId);
        component.set("v.progEnrollId",progEnrollId);
        
        var action = component.get('c.getCourseOfferingDetails');
        action.setParams({
            "courseId" : courseId
            
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
             console.log("response:::"+response.getReturnValue());
            if (state === "SUCCESS") {
                if(response.getReturnValue() !=null){
                    console.log("response:::"+response.getReturnValue());
                    component.set("v.courseOfferingList",response.getReturnValue());
                }else{
                    component.set("v.courseOfferingList",null);
                }
                
            }    
        });
        
        $A.enqueueAction(action); 
        
        
    },
    
    createCourseEnrollment : function(component){
        console.log('enroll');
        var sid = component.get("v.serviceId");
        var cofferid = component.get("v.courseOfferId");
        var peid = component.get("v.progEnrollId");
        console.log('sid:::'+sid+'cofferid:::'+cofferid+'peid:::'+peid);
        var action = component.get('c.createCourseConnectionForSelectedOffering');
        action.setParams({
            "serviceId" : sid,
            "courseOffId" : cofferid,
            "programEnrollId" : peid
            
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
             console.log("response:::"+response.getReturnValue());
            if (state === "SUCCESS") {
                debugger;
                if(response.getReturnValue() ==true){
                    console.log("response:::"+response.getReturnValue());
                   	var elem = document.querySelector('.success-enroll');
        			elem.style.display="block";
                    var elem = document.querySelector('.coffer-list');
        			elem.style.display="none";
                    var elem = document.querySelector('.coffer-enroll-list');
        			elem.style.display="none";
                    var elem = document.querySelector('.coffer-start');
        			elem.style.display="none";  
                    var successMsg = "Congratulations! You Have Been Successfully Enrolled In";
                    component.set("v.sucessenrollmessage",successMsg);
                }else{
                    component.set("v.sucessenrollmessage","Enrollment Failed");
                }
                
            }    
        });
        
        $A.enqueueAction(action); 
        
        
    },
    resetClass:function(component)
    {
        var elem=document.querySelectorAll('.badge-list');         
        for(var i=0;i<elem.length;i++)
        {
            var element=elem[i];
            element.classList.remove('open');
        }
    },
    
    //ECB-5379
    getTodaysDate:function(component){   
        var now = new Date(); 
        var da = $A.localizationService.formatDate(now, "YYYY-MM-DD");
        component.set('v.datet', da);
    },
    // New Code here
})