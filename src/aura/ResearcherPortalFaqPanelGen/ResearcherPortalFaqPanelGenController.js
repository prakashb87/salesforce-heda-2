({
    onRender: function(component, event, helper) {
        var isInitialized = component.get('v.initialized');
        if (!isInitialized) {
            var contentAttrs = component.get('v.contentAttrs');
            var htmlContent = contentAttrs.htmlContent;

            var sitePrefix = window.location.pathname.split('/')[1];
            if (sitePrefix == 's' || !sitePrefix) {
                sitePrefix = '';
            }

            if (sitePrefix) {
                htmlContent = htmlContent.replace(new RegExp('"(\/servlet\/servlet\.FileDownload[^"]+)"', 'g'), '"/' + sitePrefix + '$1"');
                contentAttrs.htmlContent = htmlContent;
                component.set('v.contentAttrs', contentAttrs);
            }

            component.set('v.initialized', true);
        }
    },

    toggleExpanded: function(component, event, helper) {
        var expandedClass = component.get('v.expandedClass');
        var heightTimeout = component.get('v.heightTimeout');
        var panelContent = component.find('faq_panel_content');
        var panelHeight = panelContent.getElement().clientHeight + 20;

        if (heightTimeout) {
            clearTimeout(heightTimeout);
        }

        if (expandedClass === 'expanded') {
            component.set('v.expandedClass', '');
            component.set('v.panelBodyStyle', 'max-height:' + panelHeight + 'px');

            component.set('v.heightTimeout', setTimeout($A.getCallback(function() {
                component.set('v.panelBodyStyle', '');
            }), 0));
        } else {
            component.set('v.expandedClass', 'expanded');
            component.set('v.panelBodyStyle', 'max-height:' + panelHeight + 'px');

            component.set('v.heightTimeout', setTimeout($A.getCallback(function() {
                component.set('v.panelBodyStyle', '');
            }), 200));
        }
    }
})