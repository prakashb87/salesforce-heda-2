/*******************************************
 Purpose: To Navigate to Blank Page
 History:
 Created by Esther Ethelbert on 20/08/2018
 *******************************************/
({
    //To navigate to default page
    goToNewPage: function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/defaultpage"
        });

        urlEvent.fire();
    }


})