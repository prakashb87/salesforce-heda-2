({
    doInit : function(component, event, helper) {
        
        var url = new URL(window.location);
        console.log('URL: '+url);
        var status;
        if(url.toString().includes("Status")) {
            status = url.searchParams.get("Status");
        }
        console.log('URL: '+status);
        if(status=='Failure'){
            console.log('URL: '+status);
            
            var modal = component.find("error-msg");
            $A.util.removeClass(modal, 'hideDiv');
            
            setTimeout(function(){$A.util.addClass(modal, 'hideDiv'); modal.click(); }, 4000)
            
            // window.scrollTo(0,document.body.scrollHeight);
            
        }
        console.log('final:::'+JSON.parse(localStorage.getItem('productBasket')));
        component.set("v.productBasket",JSON.parse(localStorage.getItem('productBasket')));
        
        var finalProdBasket = JSON.parse(localStorage.getItem('productBasket'));
             console.log(finalProdBasket.TotalPrice);
        if(finalProdBasket.TotalPrice == '0.00'){
            component.set("v.displayConfirmButton",true);
            component.set("v.displayPayNowButton",false);
        }else{
            component.set("v.displayConfirmButton",false);
            component.set("v.displayPayNowButton",true);
        } 
    
        //localStorage.getItem('productBasket'
        
        
    },
    
   
    ToTermsAndConditions : function(component, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/termsconditions"
        });
        urlEvent.fire();
        
    },
    
    navigate : function (component, event, helper) {  
        
        var finalProdBasket = JSON.parse(localStorage.getItem('productBasket'));
        var orderId = finalProdBasket.BasketNumber;
        var amount = finalProdBasket.TotalPrice;
        var action = component.get("c.getEmail");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                var email= response.getReturnValue();
                //ECB-3702 Open OneStop link in same window
                var urlEvent1 = $A.get("$Label.c.PAYNOW_ONESTOP_URL")+'&MPB='+orderId+'&EMAIL='+email+'&UNITAMOUNTINCTAX='+amount;
                window.open(urlEvent1,'_top');
            }else{
                console.log('Email Missing');
            }
        });	
        $A.enqueueAction(action);    
    },
    
   Enroll : function(component, event, helper) {
    
    var finalProdBasket = JSON.parse(localStorage.getItem('productBasket'));
    var basketNumber = finalProdBasket.BasketNumber;
       
    var action = component.get("c.confirmEnroll");
    action.setParams({
                "receiptNo" : "n/a",
                "basketNo"  : basketNumber
    });    
       
	action.setCallback(this, function(response){
    var result = response.getReturnValue();
        
    var flag = result.Status;    
    var state = response.getState();

        
    if (state === "SUCCESS") {
        console.log('Enroll');
        if(flag === "Pass"){
            var urlEvent = $A.get("e.force:navigateToURL");
        	urlEvent.setParams({
            "url": "/successpage",
             
        });
        urlEvent.fire(); 
        }else{
            var errors = response.getError();
        console.log('Enroll product error');
        console.log(errors);
        }
  		console.log(result);
    }
	});
    
	$A.enqueueAction(action);

	},
    
    closemodal : function(component, event, helper) {
        var modal = component.find("error-modal");
        $A.util.addClass(modal, 'hideDiv');
    },
})