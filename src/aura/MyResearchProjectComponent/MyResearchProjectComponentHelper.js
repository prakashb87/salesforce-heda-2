({
	addRows : function(tableId, dataset) {
		var table = $(tableId).ready().DataTable();
		if(!Array.isArray(dataset))
			dataset = [dataset];
			
		var data = [];
        dataset.forEach(function(project) {
        	var temp = [];

        	temp.push("");
            temp.push(project["projectTitle"] != undefined ? project["projectTitle"]:'');
            temp.push(project["projectStartDate"] != undefined ? project["projectStartDate"] :'');
            temp.push(project["projectEndDate"] != undefined ? project["projectEndDate"] :'');
            temp.push(project["projId"] != undefined ? project["projId"]:'');
            temp.push(project["isRMProject"] != undefined ? project["isRMProject"]:'');

            data.push(temp);
       });
       
       //table.order.neutral().draw();
       
       table.rows.add(data).draw();
	}
})