/*******************************************
 Purpose: To Navigate to Blank Page
 History:
 Created by Esther Ethelbert on 20/08/2018
 *******************************************/
({

    logData: function(component, event, helper) {

        var attr = 'data-value';
        var appEvent = $A.get("e.c:RecordEvent");
        appEvent.setParams({
            recordId: event.currentTarget.getAttribute("data-value")
        });
        appEvent.fire();

    },
	// Method call to get the list of projects associated to the user
    loadData: function(component, event, helper) {

    },
    
    enableDataTable: function(component, event, helper) {
    	//call apex class method
        var getProjectsAction = component.get('c.getMyResearchProjects');

        getProjectsAction.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in projectList attribute on component.
                var projectList = response.getReturnValue();
                component.set('v.projectList', projectList);
                
                var table = $('#myProjectList').DataTable({
                        "data": [],
                        "language": {
                            "emptyTable": $A.get("$Label.c.Youdonothaveanyprojectsavailableforviewing")
                        },
                        "paging": false,
                        "ordering": false,
                        "dom": '<"top"i>rt<"bottom"><"clear">',
                        "columns": [{
                            title: " "
                        }, {
                            title: $A.get("$Label.c.ProjectTitle")
                        }, {
                            title: $A.get("$Label.c.Start_Date")
                        }, {
                            title: $A.get("$Label.c.Project_End_Date")
                        }],
                        "info": false,
                        "aaSorting": [],
                        "searching": false,
                        "dom": 'frt',
                        "drawCallback": function() {
                            // If there is some more data
                        },
                        "columnDefs": [{
                            targets: 1,
                            className: "table-title-column",
                            "render": function(data, type, full, meta, row) {
                                return '<div class="truncate"><a class="project-title title-text" data-container="body" data-toggle="tooltip" data-placement="top" title="' + data + '" href="' + '/Researcherportal/s/project-details?recordId=' + full[4] + '">' + data + '</a>';

                            }
                        }, {
                            targets: 0,
                            width: "10px",
                            "contentPadding": "mmm",
                            bSortable: false,
                            className: "",
                            "render": function(data, type, full, meta, row) {
                                if (full[5] != true) {
                                    return '<div><span class="badge badge-pill rp-project-badge"><i class="fa fa-lightbulb-o" aria-hidden="true">' + data + '</i></span></div>';
                                } else {
                                    return '<div><span class="badge badge-pill badge-danger rm-project-badge">R' + data + '</span></div>';
                                }
                            }
                        }, {
                        	targets: [2,3],
                        	"render": function(data, type, full, meta, row) {
                        		if(type === 'display' || type === 'filter'){
                        			if(data !== '')
                        				return moment(data).format('DD/MM/YYYY') ;
                        			else
                        				return '';
                        		}
                        		
                        		return data;
                            }
                        }]

                    });
                    
                    helper.addRows("#myProjectList", projectList.lstResearchProject);
            }
        });
        $A.enqueueAction(getProjectsAction);
    }

})