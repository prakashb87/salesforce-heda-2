({
    // Method call to fetch project status list
    fetchProjectStatus: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.status", opts); 
            }
        });
        $A.enqueueAction(action);
    },
    //Method call to fetch Impact category list
    fetchImpactCategory: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.options1", opts); 
            }
        });
        $A.enqueueAction(action);
    },
    
    //Method call to fetch FOR codes list
    fetchFROcodes: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.ProjectClassificationObjInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.forCodes", opts); 
            }
        });
        $A.enqueueAction(action);
    },   
    
    // change arrow up when impact category dropdown is shown and arrow down when dropdown is closed
    toggleArrowState: function() {
        if ($('#myDropdown').is(':visible')) {
            $('#searchboxArrow').removeClass('fa-angle-down');
            $('#searchboxArrow').addClass('fa-angle-up');
        } else {
            $('#searchboxArrow').removeClass('fa-angle-up');
            $('#searchboxArrow').addClass('fa-angle-down');
        }
    },
    
    //Method call save the project created
    saveHelper: function(component,event,helper){        
        var updateRecordId;
        var urlString = window.location.href;
        if (urlString.indexOf("create-new-project") > -1) {
            var url = window.location;
            
            if (url.search) {
                updateRecordId = url.search.split("=")[1];
            } else {
                updateRecordId = null;
            }
        }
        var projectRecord = component.get("v.projectRecord")
        if(projectRecord && projectRecord.IsRMProject) {
            var prjTitle     = projectRecord.projectTitle;
            var prjSummary   = projectRecord.projectSummary;
            var prjStartdate = projectRecord.projectStartDate;
            var prjEnddate   = projectRecord.projectEndDate;
            var pjtApplStatus = projectRecord.status;           
        }  
        else if(projectRecord && !projectRecord.IsRMProject && updateRecordId != null){
            var prjTitle   = component.find("projecttitle").get("v.value");
            var prjSummary   = component.find("projectSummary").get("v.value");
            var prjStartdate = projectRecord.projectStartDate;
            var prjEnddate   = projectRecord.projectEndDate;
        }
            else if(updateRecordId == null){
                var prjTitle   = component.find("projecttitle").get("v.value");
                var prjSummary   = component.find("projectSummary").get("v.value");
                var prjStartdate = component.find("startDate").get("v.value");
                prjStartdate = prjStartdate=="" ? null :prjStartdate;
                var prjEnddate   = component.find("enddate").get("v.value"); 
                prjEnddate = prjEnddate=="" ? null :prjEnddate;   
            }
                else{
                    //var prjTitle     = $("#projecttitle").val();
                    var prjTitle   = component.find("projecttitle").get("v.value");
                    var prjSummary   = component.find("projectSummary").get("v.value");
                    var prjStartdate = component.find("startDate").get("v.value");
                    prjStartdate = prjStartdate=="" ? null :prjStartdate;
                    var prjEnddate   = component.find("enddate").get("v.value"); 
                    prjEnddate = prjEnddate=="" ? null :prjEnddate;             
                }
        
        /* if(updateRecordId != null && component.get("v.projectRecord.IsEdit") && !projectRecord.IsRMProject){
            pjtStatus = component.find("projectstatus").get("v.value");
        }
        else if(updateRecordId != null && component.get("v.projectRecord.IsEdit") && projectRecord.IsRMProject){
            pjtStatus = component.find("RMProjectstatus").get("v.value");
        }
            else{
                pjtStatus = "Idea";
            } */
        var selectedImpactCategories = component.get("v.selectedImpList");
        var approachedImpact = component.find("impactSummary").get("v.value");
        var selectedForCodesList = component.get("v.selectedForCodes");
        var radioSelectedValue =  $('input[name="options"]:checked').data('value');
        var selectedKeywordsList = component.get("v.pjtKeywords");
        var pjtExpectedImpact = component.find("expectedImpact").get("v.value");
        
        var projectData = new Object();
        projectData.projectTitle  = prjTitle;
        // projectData.currentStatus    = pjtStatus;
        projectData.summary   = prjSummary;
        projectData.startDate = prjStartdate;
        projectData.endDate   = prjEnddate;
        projectData.forCodes  = selectedForCodesList;
        projectData.access    = radioSelectedValue;
        projectData.impactCategory  = selectedImpactCategories;
        projectData.approachedImpact = approachedImpact;
        projectData.status = pjtApplStatus;
        projectData.keywords = selectedKeywordsList;
        projectData.expectedImpact = pjtExpectedImpact;
        console.log(projectData);
        var createProjectAction = component.get("c.createProject");
        createProjectAction.setParams({
            "jsonProjectData": JSON.stringify(projectData),
            recordIdfromcontroller: updateRecordId
            
        });
        
        createProjectAction.setCallback(this,function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
            
        });
        $A.enqueueAction(createProjectAction);          
        component.set('v.disabled',"true");
        
        if(updateRecordId != null && component.get("v.projectRecord.IsEdit")){
            var CI = projectRecord.projectMembers;
            var finalarr = [];
            CI.forEach(function(member) {
                if(member.RMPosition =="CI" && projectRecord.loggedInUser == member.contactName){
                    finalarr.push(member);
                }
            });
            console.log(finalarr.length)
            if((radioSelectedValue == $A.get("$Label.c.Private") && finalarr.length > 0) || (radioSelectedValue != $A.get("$Label.c.Private"))) {
                setTimeout(function(){ 
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/project-details?recordId="+updateRecordId
                    });
                    urlEvent.fire(); 
                }, 1000);
            }
            else{
                setTimeout(function(){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/view-all-project?tab=myprojects"
                    });
                    urlEvent.fire(); 
                }, 1000); 
            }            
            
        }
        else{
            setTimeout(function(){ 
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/view-all-project?tab=myprojects"
                });
                urlEvent.fire();
            }, 1000);
        }
    },
    // change arrow up when disciple area dropdown is shown and arrow down when dropdown is closed
    toggleforArrowState: function() {
        if ($('#forDropdown').is(':visible')) {
            $('#forsearchboxArrow').removeClass('fa-angle-down');
            $('#forsearchboxArrow').addClass('fa-angle-up');
        } else {
            $('#forsearchboxArrow').removeClass('fa-angle-up');
            $('#forsearchboxArrow').addClass('fa-angle-down');
        }
    },
    //method call to add keywords
    addKeywords:function(component){
        var totalKeywords = component.get("v.pjtKeywords")
        var keywrd = $(".keywordsBox13579").val();
        if(keywrd !=""){
            totalKeywords.push(keywrd);
            component.set("v.pjtKeywords", totalKeywords);
            $(".keywordsBox13579").val(""); 
        }
    }
    
})