({
    //initialization function to get account
    callAction: function(component, actionName) {
        var recordId = component.get("v.recordId");
        var action = component.get(actionName);
        action.setParams({
            'recrdId': recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('storeResponse===' + storeResponse);
                if (storeResponse != null) {
                    component.set("v.hasSchool", true);
                    component.set("v.accountName", storeResponse);
                } else {
                    component.set("v.hasNotSchool", true);
                    component.set("v.accountName", '');
                }

            } else {
                console.log('state' + state);
            }

        });
        $A.enqueueAction(action);
    },
    clickCreateLeadHelper : function(component,event,leadnew){
        var recordId = component.get("v.recordId");
        var accountName = component.get("v.accountName");
        var action = component.get('c.saveLead');
        action.setParams({
            "newLead": leadnew,
            "school": accountName,
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue == 'true') {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Thank You for your request. We will contact you soon!",
                        "type": "success"
                    });
                    toastEvent.fire();
                }
                $A.get("e.force:refreshView").fire();
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
        		dismissActionPanel.fire(); 
            } else {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Error Occured",
                    "message": "An error occured while creating Lead"
                });
                resultsToast.fire();
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action)
    }
})