({
    getErrorURL : function(component) {
        var action = component.get("c.getViewCartErrorURL");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() !=null){
                    var errorURL = response.getReturnValue();
                    component.set("v.ViewCartErrorURL",errorURL);
                }
                
            }    
        });
        
        $A.enqueueAction(action);
        
    },
    cartCreation : function(component) {
        
        var action = component.get('c.cartCreationForPendingTransaction');
        
        action.setCallback(this, function(res){
            console.log('Status-->'+res.getReturnValue().Status);
            
            if(res.getReturnValue().Status=='Pass'){
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/viewcart"
                });
                urlEvent.fire();
                component.set("v.Spinner", false);
                
            }else if(res.getReturnValue().Status === "Fail"){
                console.log('Error while Redirecting');
                var errorURL = component.get("v.ViewCartErrorURL")+'&Message='+msg;
                window.open(errorURL,'_top');
                component.set("v.Spinner", false);
            }
        });  $A.enqueueAction(action);
    }, 
    // New Code Here
})