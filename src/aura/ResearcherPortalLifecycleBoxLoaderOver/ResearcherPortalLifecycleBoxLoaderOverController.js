({
    doInit : function(component, event, helper) {
        var contentComponents = component.get('v.contentComponents');
        var rows = [];

        while (contentComponents.length > 0) {
            if (contentComponents.length === 4) {
                rows.push(contentComponents.splice(0, 2));
            } else {
                rows.push(contentComponents.splice(0, 3));
            }
        }

        component.set('v.firstRow', rows[0]);
        component.set('v.secondRow', rows[1]);
        component.set('v.thirdRow', rows[2]);
    }
})