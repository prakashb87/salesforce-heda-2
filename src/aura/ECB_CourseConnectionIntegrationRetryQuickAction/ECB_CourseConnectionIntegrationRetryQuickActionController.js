({
	doInit : function(component, event, helper) {
		console.log("ECB_CourseConnectionIntegrationRetryQuickActionController.doInit: entered");
		helper.processSAMSIntegration(component);
		console.log("ECB_CourseConnectionIntegrationRetryQuickActionController.doInit: exited");
	},

	closeQuickAction: function(component, event, helper) {
        //alert("aura:waiting");
        
        $A.get("e.force:closeQuickAction").fire();

    }

})