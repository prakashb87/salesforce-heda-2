/*********************************************************************************
*** @ClassName         : Batch_ActivatorLeadAutoConvert_helper
*** @Author            : Shubham Singh 
*** @Requirement       : This Class processes the Lead records match them with contacts based on the LastName and StudentID
                         and if matched then converts the lead
*** @Created date      : 2019-05-09
*** @Modified by       : -
*** @modified date     : -
*** @Reference         : RM-2720
**********************************************************************************/
public with sharing class Batch_ActivatorLeadAutoConvert_helper {
    //lead convert status
    static final String CONVERTMASTERLABEL = 'Qualified';
    //Lead registration type 
    static final String STAFFTYPEREGISTRATION = 'Staff';
    //Affiliation role
    static final String ACTIVATORROLE = 'Activator Member';
    //Affiliation status
    static final String CURRENTSTATUS = 'Current';
    /**
     * -----------------------------------------------------------------------------------------------+
     * This method process all the batch records to update lead owner to queue or convert them
     * ------------------------------------------------------------------------------------------------
     * @author    Shubham Singh 
     * @method    processActivatorLeads
     * @param     List<Lead> listOFLeadRecords
     * @return    -
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    public static void processActivatorLeads(List < Lead > listOFLeadRecords) {
        Map < String, Contact > mapOfContactByUniqueKey = new Map < String, Contact > ();
        Set < Lead > convertLeads = new Set < Lead > ();
        Set < Lead > activatorQueueLeads = new Set < Lead > ();
        ActivatorLeadAutoConvert wrapper = new ActivatorLeadAutoConvert();
        for (Lead record: listOFLeadRecords) {
            if (String.isNotBlank(record.Student_Number__c)) {
                //check if registration is staff to check contact with ENUMBER
                if (record.Type_of_registration__c == STAFFTYPEREGISTRATION) {
                    wrapper.setOfLastName.add(record.LastName);
                    wrapper.setOfStaffID.add(record.Student_Number__c);
                    wrapper.mapOfLeadByStudentOrStaffID.put(record.Student_Number__c, record);
                } else {
                    String studentorAluminiID = record.Student_Number__c;
                    studentorAluminiID = removeAlphabetS(studentorAluminiID);
                    wrapper.setOfStudentID.add(studentorAluminiID);
                    wrapper.setOfLastName.add(record.LastName);
                    wrapper.mapOfLeadByStudentOrStaffID.put(studentorAluminiID, record);
                }
            }
        }
        //query contact record based on lastname,uniqueLey(student or staff id)
        mapOfContactByUniqueKey = queryContactByLastNameandUniqueKey(wrapper);
        //separate leads for update and lead for conversion
        for (Lead leadRecord: listOFLeadRecords) {
            String studentorAluminiID = leadRecord.Student_Number__c + leadRecord.LastName;
            studentorAluminiID = removeAlphabetS(studentorAluminiID);
            studentorAluminiID = studentorAluminiID.toUpperCase();
            if (mapOfContactByUniqueKey.get(studentorAluminiID) == NULL) {
                activatorQueueLeads.add(leadRecord);
            } else {
                convertLeads.add(leadRecord);
            }
        }
        system.debug('activatorQueueLeads--->'+activatorQueueLeads);
        
        system.debug('convertLeads--->'+convertLeads);
        //Convert those leads which have matched contact
        list < Database.LeadConvertResult > leadConvertResult = convertLead(convertLeads, mapOfContactByUniqueKey);
        //create affiliation record based on lead success
        createAffiliationRecord(leadConvertResult);
        //Assign lead records to activator queue
        List < Group > activatorQueue;
        if (Schema.sObjectType.Group.fields.Id.isAccessible()) {
            activatorQueue = [Select Id from Group where Type = 'Queue'
                AND DeveloperName = 'Activator_Queue'
                Limit 1
            ];
        }
        List < Lead > updateOwnerOfLead = new List < Lead > ();
        //update the OWNER of the record to the activator queue
        for (Lead leadRecord: activatorQueueLeads) {
            leadRecord.OWNERID = activatorQueue[0].ID;
            updateOwnerOfLead.add(leadRecord);
        }
        Database.update(updateOwnerOfLead, false);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * This method remove the alphabet 's' from the student or staff id
     * ------------------------------------------------------------------------------------------------
     * @author    Shubham Singh 
     * @method    removeAlphabetS
     * @param     string studentorAluminiID
     * @return    string
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    public static string removeAlphabetS(string studentorAluminiID) {
        if (studentorAluminiID.startsWithIgnoreCase('S')) {
            studentorAluminiID = studentorAluminiID.removeStartIgnoreCase('S');
        }
        return studentorAluminiID;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query contact record based on last name,student or staff id
     * ------------------------------------------------------------------------------------------------
     * @author    Shubham Singh 
     * @method    removeAlphabetS
     * @param     string studentorAluminiID
     * @return    string
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    public static Map < String, Contact > queryContactByLastNameandUniqueKey(ActivatorLeadAutoConvert wrapper) {
        system.debug('wrapper---->'+wrapper);
        Map < String, Contact > mapOfContactByUniqueKey = new Map < String, Contact > ();
        //Query Contact Records based on LastName, Student/Staff ID and contact should have an account
        //map to get the contact record
        If(Schema.sObjectType.Contact.isCreateable()) {
            for (Contact contactRecord: [Select AccountID, LastName, Student_ID__c, Enumber__c from Contact where LastName IN: wrapper.setOfLastName AND(Student_ID__c =: wrapper.setOfStudentID OR Enumber__c =: wrapper.setOfStaffID) and AccountID != Null]) {
                if (String.isNotBlank(contactRecord.Student_ID__c) && wrapper.mapOfLeadByStudentOrStaffID.get(contactRecord.Student_ID__c) != null) {
                    String uniqueKey = contactRecord.Student_ID__c + contactRecord.LastName;
                    mapOfContactByUniqueKey.put(uniqueKey.toUpperCase(), contactRecord);
                } else if (String.isNotBlank(contactRecord.Enumber__c) && wrapper.mapOfLeadByStudentOrStaffID.get(contactRecord.Enumber__c) != null) {
                    String uniqueKey = contactRecord.Enumber__c + contactRecord.LastName;
                    mapOfContactByUniqueKey.put(uniqueKey.toUpperCase(), contactRecord);
                }
            }
        }
        system.debug('mapOfContactByUniqueKey---->'+mapOfContactByUniqueKey);
        return mapOfContactByUniqueKey;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * This method process the lead records to convert them into contact and account
     * ------------------------------------------------------------------------------------------------
     * @author    Shubham Singh 
     * @method    convertLead
     * @param     List<Lead> convertLeadRecords,Map< string, Contact > mapOfContactByUniqueKey
     * @return    list<Database.LeadConvertResult > leadConvertResult
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    public static list < Database.LeadConvertResult > convertLead(Set < Lead > convertLeadRecords, Map < string, Contact > mapOfContactByUniqueKey) {
        List < Database.LeadConvert > leadstobeconverted = new List < Database.LeadConvert > ();
        Database.LeadConvert leadToConvert;
        for (Lead leadRecord: convertLeadRecords) {
            String studentorAluminiID = leadRecord.Student_Number__c + leadRecord.LastName;
            studentorAluminiID = removeAlphabetS(studentorAluminiID);
            studentorAluminiID = studentorAluminiID.toUpperCase();
            leadToConvert = new Database.LeadConvert();
            leadToConvert.setLeadId(leadRecord.Id);
            leadToConvert.convertedStatus = CONVERTMASTERLABEL;
            //account should always be there
            leadToConvert.setAccountId(mapOfContactByUniqueKey.get(studentorAluminiID).AccountID);
            leadToConvert.setContactId(mapOfContactByUniqueKey.get(studentorAluminiID).ID);
            leadToConvert.setOwnerId(System.UserInfo.getUserId());
            leadToConvert.setDoNotCreateOpportunity(true);
            leadstobeconverted.add(leadToConvert);
        }
        system.debug('leadsToBeConverted ' + leadstobeconverted);
        return Database.convertLead(leadstobeconverted);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * This method creates affiliation records and associate them with activator account and contact
     * ------------------------------------------------------------------------------------------------
     * @author    Shubham Singh 
     * @method    createAffiliationRecord
     * @param     list<Database.LeadConvertResult > leadConvertResult
     * @return    -
     * @Reference RM-2720
     * -----------------------------------------------------------------------------------------------+
     */
    public static void createAffiliationRecord(list < Database.LeadConvertResult > leadConvertResult) {
        //Map<Id,Id> mapOfIdById = new Map<Id,Id>();
        //check for lead success and create affiliation
        Set < Id > setOfContactID = new Set < ID > ();
        for (Database.LeadConvertResult leadResult: leadConvertResult) {
            if (leadResult.isSuccess()) {
                setOfContactID.add(leadResult.getContactId());
                //mapOfIdById.put(leadResult.getContactId(),leadResult.getContactId());
            }
        }
        //Affiliation Activator recordType
        Id activatorRegistrationRTOfAffiliation = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByDeveloperName().get('Activator').getRecordTypeId();
        List < hed__Affiliation__c > afffiliationWithContact = [select id, hed__Contact__c, hed__Contact__r.ID from hed__Affiliation__c where hed__Contact__c IN: setOfContactID AND RecordTypeID =: activatorRegistrationRTOfAffiliation];
        If(Schema.sObjectType.hed__Affiliation__c.isCreateable()) {
            for (hed__Affiliation__c affparameter: afffiliationWithContact) {
                setOfContactID.remove(affparameter.hed__Contact__r.ID);
            }
        }
        system.debug('setOfContactID===>' + setOfContactID);

        //get Account record External ID
        String externalId = CustomSettingsService.getActivatorExternalId();
        List < Account > activatorAccount ;
        //get account recore with name and external id(as external id is unique for all org)
        if (Schema.sObjectType.Account.isCreateable() && Schema.sObjectType.Account.isUpdateable()) {
             activatorAccount = new List < Account > ([SELECT Id FROM Account Where Name = 'Activator'
                and External_Unique_ID__c =: externalId
            ]);
        }
        //get account reord type id
        Id activatorstartupRTOfAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Activator_Start_Up').getRecordTypeId();
        //create affiliation record
        List < hed__Affiliation__c > insertAffiliationRecords = new List < hed__Affiliation__c > ();
        hed__Affiliation__c hedaAffilliation;
        for (ID contactID: setOfContactID) {
            hedaAffilliation = new hed__Affiliation__c();
            hedaAffilliation.RecordTypeId = activatorRegistrationRTOfAffiliation;
            hedaAffilliation.hed__Contact__c = contactID;
            hedaAffilliation.hed__Account__c = activatorAccount[0].Id;
            hedaAffilliation.hed__Role__c = ACTIVATORROLE;
            hedaAffilliation.hed__Status__c = CURRENTSTATUS;
            hedaAffilliation.hed__StartDate__c = System.today();
            insertAffiliationRecords.add(hedaAffilliation);
        }
        system.debug('insertAffiliationRecords==>' + insertAffiliationRecords);
        Database.insert(insertAffiliationRecords, false);
    }
    //wrapper class with map and set
    public class ActivatorLeadAutoConvert {
        public Map < String, Lead > mapOfLeadByStudentOrStaffID = new Map < String, Lead > ();
        public Set < String > setOfStudentID = new Set < String > ();
        public Set < String > setOfStaffID = new Set < String > ();
        public Set < String > setOfLastName = new Set < String > ();
    }
} 