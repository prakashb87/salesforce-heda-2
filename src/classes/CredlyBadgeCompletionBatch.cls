/*****************************************************************************************************
 *** @Class             : CredlyBadgeCompletionBatch
 *** @Author            : Avinash /Resmi 
 *** @Requirement       : Batch Class to send the Course Connections 
 *** @Created date      : 12/07/2018
 *** @JIRA ID           : ECB-2446
 *****************************************************************************************************/ 
/*****************************************************************************************************
 *** @About Class
 *** This class is used to send Course Connection IDs to Ipass API. 
 *****************************************************************************************************/ 
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class CredlyBadgeCompletionBatch implements Database.Batchable<sObject>,Database.Stateful,Database.AllowsCallouts 
{
   global string  querystring;
   
   global Database.QueryLocator start(Database.BatchableContext bc) {
   
        String courseConnStatus = ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionStatus');
        String enrollmentStatus = ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionEnrolmentStatus');
        String courseRecordType = ConfigDataMapController.getCustomSettingValue('CredlyCourseConnectionType');
        
        querystring = 'SELECT Id, Name from Contact Where Id IN (Select '+
                                                                     'hed__Contact__c'+                                 
                                                                     ' FROM hed__Course_Enrollment__c'+
                                                                     ' WHERE hed__Status__c =: courseConnStatus '+
                                                                     ' AND Enrolment_Status__c =: enrollmentStatus '+
                                                                     ' AND hed__Course_Offering__r.hed__Course__r.RecordType.DeveloperName =: courseRecordType )';
                                                      
        return Database.getQueryLocator(querystring);
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> scope){
       
       Set<Id> conIdSet = new Set<Id>();
       if(!scope.isEmpty()){
           for(Contact con : scope){
               conIdSet.add(con.Id);
           }
           
           system.debug('conIdSet:::::'+conIdSet);
           
           try{
               CredlyBadgeCompletion.updateCourseConn(conIdSet);
           }catch(Exception ex){
               system.debug('Exception:::'+ex);
           }    
       }
          
    }
    
    global void finish(Database.BatchableContext bc) 
    {
        System.debug('CredlyBadgeCompletionBatch batch job completed.');
    }
    
}