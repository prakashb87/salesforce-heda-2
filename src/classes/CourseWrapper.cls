/*****************************************************************************************************
 *** @Class             : CourseWrapper
 *** @Author            : Praneet Vig
 *** @Requirement       : scheduler to sends newly created or updated courseIds to IPAAS API 
 *** @Created date      : 21/02/2018
 *** @JIRA ID           : ECB-30
 *** @LastModifiedBy	: Avinash Machineni
 *** @Created date      : 29/03/2018
 *** @JIRA ID           : ECB-290
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used as a scheduler to schedule Course ID to Ipass API. It works when we schedule using schedule apex.
 *****************************************************************************************************/ 
 
public class CourseWrapper {
    public String ObjectName;
    public Map<String, Object> key = new Map<String, Object>();
              
    public static CourseWrapper parse(String json) {
        return (CourseWrapper ) System.JSON.deserialize(json, CourseWrapper.class);
    }
}