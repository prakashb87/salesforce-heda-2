/**
 * Created by mmarsson on 1/03/19.
 */

public with sharing class WebserviceException extends Exception {

    public Enum WebservixceExceptionType{NOPAYLOAD,MANDATORYPARAMETERMISSING}


    public Blob payload;
    public WebservixceExceptionType exceptiontype;
    public String parameter;
    public String errorCode;
    public String businesString;


     public WebserviceException (String message, Blob payld){
        this(message);
        payload = payld;
    }

}