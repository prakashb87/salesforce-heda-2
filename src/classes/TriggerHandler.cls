/********************************************************************************************************/
/**Author : Capgemini                                  Date : 14/02/2019     ****************************/               
/* The interface dictates which methods every trigger handler must implement, 
   even if these methods have no code in them                                ****************************/ 
/********************************************************************************************************/
public interface TriggerHandler {
    
    void beforeInsert(List<SObject> newItems);
 
    void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
 
    void beforeDelete(Map<Id, SObject> oldItems);
 
    void afterInsert(Map<Id, SObject> newItems);
 
    void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
 
    void afterDelete(Map<Id, SObject> oldItems);
 
    void afterUndelete(Map<Id, SObject> oldItems);
    
}