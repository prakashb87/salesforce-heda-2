/**
 * Created by mmarsson on 1/03/19.
 */

public with sharing class WebserviceParametersHandler {

    public Type parameterType;
    private List<IInboundParameter> parameters;

    private WebserviceParametersHandler(){
        parameters=null;
        parameterType = null;
    }

    public List<IInboundParameter> getParameters(){
        return parameters;
    }
    
    public WebserviceParametersHandler(Type aType){
        this();
        this.parameterType = aType;
    }

    public void processParameters(){
        this.createParameterList();
        this.validateParametersInList();

    }

    private void validateParametersInList(){
        System.debug('Debug architecture='+parameters);

        for (IInboundParameter param : parameters){
            System.debug('Trying to call'+param);
            param.validateParameters();

        }
    }

    private void createParameterList(){


        //Validate Type. IF NOT IN LIST, ADD IT TO LIST
        try {
            String jsonString = RestContext.request.requestBody.toString();
            system.debug('jsonString===>'+jsonString);
            if(JSON.deserializeUntyped(jsonString) instanceof Map<String,Object>){
           		jsonString =  '[' + jsonString + ']';

            }
            parameters = (List<IInboundParameter>) JSON.deserializeStrict(jsonString, parameterType);
            system.debug('parameters===>'+parameters);
        } catch (Exception e){
           // System.debug('parameters='+parameters);
            WebserviceException ex = new WebserviceException(e);
            ex.setMessage('Invalid payload. Could not continue processing.');
            ex.payload = RestContext.request.requestBody;
            ex.exceptiontype = WebserviceException.WebservixceExceptionType.NOPAYLOAD;
            ex.errorCode = '01';
            throw ex;
        }

    }

    //USE WITH CARE. SOME ASSUMPTIONS. FIELDS MUST BE UNIQUE AND STRING.
    public Map <String,IInboundParameter> getInboundParametersByMandatoryStringField(String field) {

		system.debug('field==== > '+field);
        Map <String,IInboundParameter> parameterByString = new Map <String,IInboundParameter>();
        for (IInboundParameter parameter : parameters){
            system.debug('parameter==== > '+parameter);
            parameterByString.put((String)parameter.getFieldValueByName(field),parameter);
        }
		system.debug('parameterByString===> '+parameterByString);
        return parameterByString;
    }




}