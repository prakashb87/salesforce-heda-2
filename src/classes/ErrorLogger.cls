/*****************************************************************************************************
 *** @Class             : ErrorLogger
 *** @Author            : Ming Ma
 *** @Purpose 	        : Log error into RMErrLog__c object
 *** @Created date      : 18/05/2018
 *** @JIRA ID           : ECB-2955
 *****************************************************************************************************/

public class ErrorLogger
{

	/*public static void logError(Id recordID, string errorType, string errorPayload, String errorCode, String errorMessage) {
		system.debug('ErrorLogger.logError entered');
		RMErrLog__c newError = New RMErrLog__c();
		newError.Record_Id__c = recordID;
		newError.Error_Type__c = errorType;
		newError.Error_Payload__c = errorPayload;
		newError.Error_Code__c = errorCode;
		newError.Error_Message__c = errorMessage;


		try {
			Insert newError;
			system.debug(newError);

		} catch (Exception e) {
			system.debug('ErrorLogger.logError exception occured: ' + e.getMessage());
		}


		system.debug('ErrorLogger.logError exited');


	}*/

	public static void sendErrorEmailAlert(Id courseConnectionRecordID) {

		Config_Data_Map__c email = Config_Data_Map__c.getInstance('ECB Support Email');
		String supportEmailAddress = email.Config_Value__c;
		system.debug(supportEmailAddress);

		// Now create a new single email message object
		// that will send out a single email to the addresses in the To, CC & BCC list.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		// Strings to hold the email addresses to which you are sending the email.
		String[] toAddresses = new String[] {supportEmailAddress};


		// Assign the addresses for the To and CC lists to the mail object.
		mail.setToAddresses(toAddresses);


		// Specify the name used as the display name.
		mail.setSenderDisplayName('Salesforce Support');

		// Specify the subject line for your email address.
		mail.setSubject('SAMS Integration Error Alert');

		String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/lightning/r/hed__Course_Enrollment__c/' + courseConnectionRecordID + '/view';

		// Specify the text content of the email.
		mail.setHtmlBody('SAMS Integration Error<p>' +
		                 'To view this error <a href=' + fullRecordURL + '>click here.</a>');

		// Send the email you have created.
		if (!isSandbox()) {
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	}


	// Next method tells us if we're in a sandbox
    private Static Boolean isSandbox(){
 
        String host = URL.getSalesforceBaseUrl().getHost();
        String server = host.substring(0,host.indexOf('.'));
 
        // It's easiest to check for 'my domain' sandboxes first 
        // even though that will be rare
        if (server.contains('--')) {
            return true;
        }
        // tapp0 is a unique "non-cs" server so we check it now
        if (server == 'tapp0') {
            return true;
        }
        // If server is 'cs' followed by a number it's a sandbox
        if(server.length()>2){
            if(server.substring(0,2)=='cs'){
                try{
                    Integer.valueOf(server.substring(2,server.length()));
                }
                catch(exception e){
                    //started with cs, but not followed by a number
                    return false;
                }
 
                //cs followed by a number, that's a hit
                return true;
            }
        }
 
        // If we made it here it's a production box
        return false;
    }

}