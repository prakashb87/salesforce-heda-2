/*********************************************************************************
 *** @Class Name        : ProgramEnrollmentHelper
 *** @Author            : Shubham Singh
 *** @Requirement       : To update values Of ProgramEnrollment on Contact
 *** @Created date      : 13/11/2017
 **********************************************************************************/
/**********************************************************************************
 *** @About Class
 *** This class is a helper class for ProgramEnrollTrigger and works on four events
 *** (after insert,after update, after delete, after undelete) of ProgramEnrollment. 
 *** It is used to update the values on Contact object.
 ***********************************************************************************/
@SuppressWarnings('PMD')
public with sharing class ProgramEnrollmentHelper extends TriggerHandlerExt{

    
    public override void afterInsert(){
        ProgramEnrollmentService pehh = new ProgramEnrollmentService();
        pehh.updateExistingContactAffiliationOnInsert((List<hed__Program_Enrollment__c>) trigger.new);
        countofPE((List<hed__Program_Enrollment__c>) trigger.new,null);
        PEValuesOnContact(trigger.new,null);
        
    }
    
    public override void afterUpdate(){
        ProgramEnrollmentHelperHandler pehh = new ProgramEnrollmentHelperHandler();
        pehh.updateExistingContact(trigger.new);
        countofPE((List<hed__Program_Enrollment__c>)trigger.new,(Map<Id,hed__Program_Enrollment__c>)trigger.oldmap);
        PEValuesOnContact((List<hed__Program_Enrollment__c>)trigger.new,(Map<Id,hed__Program_Enrollment__c>)trigger.oldmap);
        
    }
    
    public override void afterDelete(){
        countofPE(null,(Map<Id,hed__Program_Enrollment__c>)trigger.oldmap);
        PEValuesOnContact(null,(Map<Id,hed__Program_Enrollment__c>)trigger.oldmap);
    }
    
    public override void afterUndelete(){
        countofPE(trigger.new,null);
        PEValuesOnContact(trigger.new,null);
    }
    
   
    //To update the NumberOfActiveProgramEnrollments on Contact by calculating the sum of the related ProgramActionValue of ProgramEnrollment.
    public void countofPE(List < hed__Program_Enrollment__c > vLstProgEnroll, Map < Id, hed__Program_Enrollment__c > oldProgEnroll) {
        set < String > vContId = new set < String > ();
        if (vLstProgEnroll != null) {
            for (hed__Program_Enrollment__c vPE: vLstProgEnroll) {
                if (vPE.hed__Contact__c != null) {
                    vContId.add(vPE.hed__Contact__c);
                }
            }
        }
        if (oldProgEnroll != null && vLstProgEnroll != null) {
            for (hed__Program_Enrollment__c vNewPE: vLstProgEnroll) {
                hed__Program_Enrollment__c oldPE = oldProgEnroll.get(vNewPE.Id);
                if ((vNewPE.hed__Contact__c != oldPE.hed__Contact__c) || (vNewPE.Program_Action__c != oldPE.Program_Action__c) && vNewPE.hed__Contact__c != null ) {
                   vContId.add(vNewPE.hed__Contact__c);
                }
            }
        }

        if (oldProgEnroll != null && vLstProgEnroll == null) {
            for (hed__Program_Enrollment__c oldPE: oldProgEnroll.values()) {
                if (OldPE.hed__Contact__c != null) {
                    vContId.add(oldPE.hed__Contact__c);
                }
            }
        }
        List < AggregateResult > vLstAggr;
        IF(vContId != null && Schema.sObjectType.hed__Program_Enrollment__c.fields.Program_Action_Value__c.isAccessible()) {
            vLstAggr = [SELECT Sum(Program_Action_Value__c) NPAV, hed__Contact__c
                FROM hed__Program_Enrollment__c
                WHERE hed__Contact__c In: vContId
                GROUP BY hed__Contact__c
            ];
        }


        Contact vContact;
        Map < id, Contact > vLstContacts = new Map < id, Contact > ();
        If(vLstAggr != null && vLstAggr.size() > 0) {
            for (AggregateResult vAggr: vLstAggr) {
                string conId = (string) vAggr.get('hed__Contact__c');
                decimal countOfChild = (decimal) vAggr.get('NPAV');
                vContact = new Contact(Id = conId, Number_of_active_program_enrollments__c = countOfChild);
                vLstContacts.put(vContact.Id, vContact);
            }
        }
        else {
            For(Id cId: vContId) {
                string conId = (string) cId;
                decimal countOfChild = null;
                vContact = new Contact(Id = conId, Number_of_active_program_enrollments__c = countOfChild);
                vLstContacts.put(vContact.Id, vContact);
            }
        }
        if (vLstContacts.size() > 0) {
            Database.update(vLstContacts.values(), false);
        }
    }
    //To update the ProgramAction,ActionDate,GraduationYear,Program values Of ProgramEnrollment on contact 
    public void pEValuesOnContact(List < hed__Program_Enrollment__c > vLstProgEnroll, Map < Id, hed__Program_Enrollment__c > oldProgEnroll) {
        
    //RSTP Change
        Id affRecordTypeId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get(System.Label.AffiliationRecordTypeHDRCandidate).getRecordTypeId();
        List<hed__Affiliation__c> affsResearcherStudents=new List<hed__Affiliation__c>();
            
        Map < Id, Contact > mapContact = new Map < Id, Contact > ();
        set<Id> setContact = new set<Id>();
        if (vLstProgEnroll != null) {
            for (hed__Program_Enrollment__c hPE: vLstProgEnroll){
                if(hPE.hed__Contact__c !=null)
                {
                    setContact.add(hPE.hed__Contact__c);
                }
            }
        }
        //if(setContact.size()>0)
        //{
            //list<hed__Program_Enrollment__c> lstPE = new list<hed__Program_Enrollment__c>([select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c,hed__Contact__c, career__c, program_status__c, academic_program__c, hed__Account__c from hed__Program_Enrollment__c where hed__Contact__c in: setContact Order by LastModifiedDate Asc]);
            //if (lstPE != null) {
                for (hed__Program_Enrollment__c oPE: [select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c,hed__Contact__c, career__c, program_status__c, academic_program__c, hed__Account__c from hed__Program_Enrollment__c where hed__Contact__c in: setContact Order by LastModifiedDate Asc] ) {
                    Contact c = new contact(Id = oPE.hed__Contact__c);
                        c.IsHDRCandidate__c=false;  //RSTP Change
                    //for (hed__Program_Enrollment__c newPE: [select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c from hed__Program_Enrollment__c where hed__Contact__c =: hPE.hed__Contact__c Order by LastModifiedDate Asc]) {
                        c.Action_Date__c = oPE.Action_Date__c;
                        c.Program__c = oPE.Program_Value__c;
                        c.Program_Action__c = oPE.Program_Action__c;
                        c.Class_Year__c = oPE.hed__Graduation_Year__c;
                        //RSTP Change:
                        if (isEnrollmentofResearcherStudent(oPE)) {
                           c.IsHDRCandidate__c=true;
                           if ((oldProgEnroll==null) || (oldProgEnroll!=null && oldProgEnroll.get(oPE.id)!=null && (!isEnrollmentofResearcherStudent(oldProgEnroll.get(oPE.id)) || Label.DataFixForRSTPAffiliations=='true'))){
                               affsResearcherStudents.add(createAffiliationForStudentResearcher(c.id, oPE, affRecordTypeId));
                            }
                         }
                    //}
                    mapContact.put(c.id, c);
                }
            //}
        //}
        /*if (vLstProgEnroll != null) {
            for (hed__Program_Enrollment__c hPE: vLstProgEnroll) {
                Contact c = new contact(Id = hPE.hed__Contact__c);
                for (hed__Program_Enrollment__c newPE: [select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c from hed__Program_Enrollment__c where hed__Contact__c =: hPE.hed__Contact__c Order by LastModifiedDate Asc]) {
                    c.Action_Date__c = newPE.Action_Date__c;
                    c.Program__c = newPE.Program_Value__c;
                    c.Program_Action__c = newPE.Program_Action__c;
                    c.Class_Year__c = newPE.hed__Graduation_Year__c;
                }
                mapContact.put(c.id, c);
            }
        }*/
        if (vLstProgEnroll == null && oldProgEnroll != null) {
            set < Id > cID = new set < Id > ();
            map < Id, List < hed__Program_Enrollment__c >> mapId = new map < Id, List < hed__Program_Enrollment__c >> ();
            for (hed__Program_Enrollment__c hPE: oldProgEnroll.values()) {
                cID.add(hPE.hed__Contact__c);
            }
            List < hed__Program_Enrollment__c > lstProgEnroll = new List < hed__Program_Enrollment__c > ([select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c, hed__Contact__c, career__c, program_status__c, academic_program__c, hed__Account__c from hed__Program_Enrollment__c where hed__Contact__c in: CID Order By LastModifiedDate Asc]); //RSTP Change: Added fields to query: career__c, program_status__c, academic_program__c, hed__Account__c
            if (lstProgEnroll != null && lstProgEnroll.size() > 0) {
                for (hed__Program_Enrollment__c hPE: lstProgEnroll) {
                    if (mapId.containsKey(hPE.hed__Contact__c)) {
                        List < hed__Program_Enrollment__c > lsthPE = mapId.get(hPE.hed__Contact__c);
                        lsthPE.add(hPE);
                        mapId.put(hPE.hed__Contact__c, lsthPE);
                    } else {
                        mapId.put(hPE.hed__Contact__c, new List < hed__Program_Enrollment__c > {
                            hPE
                        });
                    }
                }
            } else {
                for (Id cI: cID) {
                    Contact c = new Contact(Id = cI);
                    c.Action_Date__c = null;
                    c.Program__c = null;
                    c.Program_Action__c = null;
                    c.Class_Year__c = null;
                    c.IsHDRCandidate__c=false;  //RSTP Change
                    mapContact.put(c.Id, c);
                }
            }
            if (mapId.size() > 0) {
                for (hed__Program_Enrollment__c hPE: lstProgEnroll) {
                    Contact c = new contact(Id = hPE.hed__Contact__c);
                    c.IsHDRCandidate__c=false; //RSTP Change
                    if(mapId.get(hPE.hed__Contact__c) != null && mapId.get(hPE.hed__Contact__c).size() > 0) {
                        for (hed__Program_Enrollment__c newPE: mapId.get(hPE.hed__Contact__c)) {
                            System.debug('Test3=== ');
                            c.Action_Date__c = newPE.Action_Date__c;
                            c.Program__c = newPE.Program_Value__c;
                            c.Program_Action__c = newPE.Program_Action__c;
                            c.Class_Year__c = newPE.hed__Graduation_Year__c;

                            //RSTP Change:
                            if (isEnrollmentofResearcherStudent(newPE)) {
                                c.IsHDRCandidate__c=true;
                                if ((oldProgEnroll==null && newPE.id==hPE.id) || (newPE.id==hPE.id && oldProgEnroll!=null && oldProgEnroll.get(newPE.id)!=null && (!isEnrollmentofResearcherStudent(oldProgEnroll.get(newPE.id)) || Label.DataFixForRSTPAffiliations=='true'))){
                                    affsResearcherStudents.add(createAffiliationForStudentResearcher(c.id, newPE, affRecordTypeId));
                                }
                            }
                        }
                    } else {
                        c.Action_Date__c = null;
                        c.Program__c = null;
                        c.Program_Action__c = null;
                        c.Class_Year__c = null;
                        c.IsHDRCandidate__c=false;                        //RSTP Change
                        mapContact.put(c.Id, c);
                    }
                    mapContact.put(c.id, c);
                }
            }
        }
        if (mapContact.size() > 0) {
            database.update(mapContact.values(), false);
        }
        
        //RSTP Change: Code added to create Researcher Student Affiliation
        if (affsResearcherStudents.size()>0){
            database.insert(affsResearcherStudents, false);
        }

    }

    //RSTP Change: method to create HDR Candidate Affiliation
    hed__Affiliation__c createAffiliationForStudentResearcher(Id contId, hed__Program_Enrollment__c pe, Id affRecordTypeId){
        return new hed__Affiliation__c(hed__Account__c=pe.hed__Account__c,hed__Contact__c=contId, recordTypeId=affRecordTypeId, hed__Role__c=System.Label.AffiliationRoleHDRCandidate);
    }
/*
    List<hed__Affiliation__c> createAffiliationsForStudentResearchers(Map<Id, Contact> mapContact){
        Map<id, Contact> oldContacts=new Map<Id, Contact>([select id, IsHDRCandidate__c from Contact where id IN :mapContact.keySet()]);
        List<hed__Affiliation__c> researcherAffs = new List<hed__Affiliation__c>();
        for (Contact cont: mapContact.values()){
            if (cont.IsHDRCandidate__c!=oldContacts.get(cont.Id).IsHDRCandidate__c){
                if (cont.IsHDRCandidate__c){
                    researcherAffs.add(new hed__Affiliation__c(hed__Account__c=,hed__Contact__c=cont.Id, recordTypeId=ContactTriggerHandler.getAffRecordTypeId(System.Label.AffiliationRecordTypeHDRCandidate), hed__Role__c=System.Label.AffiliationRoleHDRCandidate));
                }
            }
        }
        return researcherAffs;
    }
*/

    //RSTP Change: method to check if Program Enrollment satisfies condition of HDR Candidate (If HRD candidate is currently enrolled in researcher program)
    public static boolean isEnrollmentofResearcherStudent(Hed__Program_Enrollment__c pe){
        return ( pe.Career__c==System.Label.ResearcherAcademicCareerCode
                 && System.Label.ProgramStatusesforcurrentPE.split(',').contains(pe.program_status__c)
                 && System.Label.ResearcherAcademicProgramCodes.split(',').contains(pe.academic_program__c.substring(0,2)) ) ? true:false;
    }

}