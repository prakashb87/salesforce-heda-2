/*******************************************
Purpose: Test class ProjectSAPFundingTrigger
History:
Created by Ankit Bhagat on 03/10/2018
Clean up and modification done by Subhajit 
*******************************************/
@isTest
public class TestProjectSAPFundingTrigger {
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
              
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
               
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
            
            Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
            con.hed__UniversityEmail__c='raj@rmit.edu.au';
            con.Enumber__c='e100';
            update con;
            system.assert(con!=null,'Records Created');
        }
       
    }    
    

/************************************************************************************
// Purpose      :  Test functionality of Positive for Trigger of Project Member Sharing
// Developer    :  Subhajit
// Created Date :  10/27/2018                 
//***********************************************************************************/
@isTest
    static void testResearcherProjectTriggerHandlerInsertEvent() {
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        
        system.runAs(usr)
        {
            
            /*List<Account> accts = RSTP_TestDataFactoryUtils.createAccounts(1);
            system.assert(accts!=null,'Records Created');
            List<Contact> cons = RSTP_TestDataFactoryUtils.createContactWithAccount(1,accts);
            cons[0].hed__UniversityEmail__c='abc@rmit.edu.au';
            cons[0].Enumber__c='e100';
            update cons;
            system.assert(cons!=null,'Records Created');*/
            
            List<ResearcherPortalProject__c> researchProjects = RSTP_TestDataFactoryUtils.createResearchProj(1,
                                                                                                             Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId(),
                                                                                                             'Private'); 
            researchProjects[0].SAP_WBS_NUM__c='100';
            update researchProjects;
            system.assert(researchProjects!=null,'Records Created');
            
            List<ProjectSAPFunding__c> projectSAPFundingRecs = RSTP_TestDataFactoryUtils.createSAPFunding(51); 
            insert projectSAPFundingRecs;
            system.assert(projectSAPFundingRecs!=null,'Records Created');
        }
    }
/************************************************************************************
// Purpose      :  Test functionality of Positive for Trigger of Project Member Sharing
// Developer    :  Subhajit
// Created Date :  10/27/2018                 
//***********************************************************************************/
    @isTest
    static void testResearcherProjectTriggerNegativeMethod() {
    
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];
        
        system.runAs(usr)
        {
           /* List<Account> accts = RSTP_TestDataFactoryUtils.createAccounts(1);
            system.assert(accts!=null,'Records Created');
            List<Contact> cons = RSTP_TestDataFactoryUtils.createContactWithAccount(1,accts);
            cons[0].hed__UniversityEmail__c='abc@rmit.edu.au';
            cons[0].Enumber__c='e100';
            update cons;
 			system.assert(cons!=null,'Records Created');	*/
            
            Boolean result = false;
            Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
            con.hed__UniversityEmail__c='raj@rmit.edu.au';
            con.lastname='';
            //update con; 
            
            try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
                
            }
            system.assert(result);
            
           
            List<ResearcherPortalProject__c> researchProjects = RSTP_TestDataFactoryUtils.createResearchProj(1,
                                                                                                             Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId(),
                                                                                                             'Private'); 
            update researchProjects;
            system.assert(researchProjects!=null,'Records Created');
            
            List<ProjectSAPFunding__c> projectSAPFundingRecs=new List<ProjectSAPFunding__c>{new ProjectSAPFunding__c(ProjectCodeSAPWBSNum__c='100' )};                                                                                   
                //insert projectSAPFundingRecs;
                system.assert(projectSAPFundingRecs!=null,'Records Created');
            
        }
    
    }
}