@isTest
private class HEDA2CSBatchSchedulerTest {

	public static final String CRON_EXP = '0 0 0 15 3 ? 2022';

	@isTest static void test() {

		// Create test data
		Id courseRecordTypeId = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
		system.debug('21CC recordTypeId ' + courseRecordTypeId);
		
		Account department = New Account(name = 'RMITU');
		Insert department;

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = department.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = department.Id;
		course.recordTypeId = courseRecordTypeId;
		Insert course;

		system.debug('**** debug course creation ' + course);

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.hed__course__r = course;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		hed__Course_Offering__c courseOffering2 = New hed__Course_Offering__c();
		courseOffering2.name = 'Course Offering ID 2';
		courseOffering2.hed__course__c = course.Id;
		courseOffering2.Status__c = 'Inactive';
		courseOffering2.hed__Term__c = term.Id;
		courseOffering2.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering2.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering2;

		hed__Course_Offering__c courseOffering3 = New hed__Course_Offering__c();
		courseOffering3.name = 'Course Offering ID 3';
		courseOffering3.hed__course__c = course.Id;
		courseOffering3.Status__c = 'Active';
		courseOffering3.hed__Term__c = term.Id;
		courseOffering3.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering3.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering3;

		// Read data from DB
		list<hed__Course_Offering__c> testDataFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c,
		                              hed__End_Date__c,
		                              hed__course__r.name,
		                              //hed__course__r.price__c,
		                              hed__course__r.recordTypeId,
		                              status__c 
		                              FROM hed__Course_Offering__c 
		                              WHERE hed__course__r.recordTypeId = :courseRecordTypeId];


		Test.startTest();
		// Schedule the test job
		HEDA2CSBatchScheduler job= new HEDA2CSBatchScheduler();
		String jobId = System.schedule('ScheduledApexTest',
		                               CRON_EXP,
		                               job);
		job.execute(null);
		// Verify the scheduled job has not run yet.
		List<cspmb__Price_Item__c> priceItems = [SELECT Id
		                 FROM cspmb__Price_Item__c];
		System.assertEquals(0, priceItems.size());
		// Stopping the test will run the job synchronously
		Test.stopTest();


		// Verify price items after sheduled job
		List<cspmb__Price_Item__c> priceItemsAfter = [SELECT Id
		                 FROM cspmb__Price_Item__c];
		System.assertEquals(3, priceItemsAfter.size());

		
	}


}