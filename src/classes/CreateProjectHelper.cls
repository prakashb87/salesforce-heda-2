/*******************************************
 Purpose: Helper class for the Create Researcher Project Controller
 History:
 Created by Ankit Bhagat on 05/09/2018
 Update: Removed the ProjectDetail method from this helper and moved to projecthelper.
 *******************************************/
public without sharing class CreateProjectHelper {
    
    
    /*****************************************************************************************
    // Purpose      :  To create Researcher Project
    // Parameters   :  String jsonProjectData,id recordIdfromcontroller
    // Developer    :  Ankit
    // Created Date :  09/05/2018               
    //****************************************************************************************/
    
    public static void createResearcherProject(String jsonProjectData,id recordIdfromcontroller) {
        
        projectDetailsWrapper projectdata =(projectDetailsWrapper)JSON.deserialize(jsonProjectData,projectDetailsWrapper.class);
        system.debug('@@@projectdata ---->' + JSON.serializePretty(projectdata));
        //  system.debug('**Recordid ' + recordId);
        String commaSepratedList = '';
        String forCodesdata = '';
        String keywordsList = '';
        
        //String recordId;
        if(null !=projectdata.impactCategory){
            for(String str : projectdata.impactCategory)
            {
                commaSepratedList += str + ';' ;
            }
       
            // remove last additional comma from string
            commaSepratedList = commaSepratedList.removeEnd(';');
        }
        
         if(null !=projectdata.keywords){
            for(String str : projectdata.keywords)
            {
                keywordsList += str + ';' ;
            }
       
            // remove last additional comma from string
            keywordsList = keywordsList.removeEnd(';');
        } 
        
		/*
        if(null!=projectdata.forCodes){
            for(String str : projectdata.forCodes)
            {
                forCodesdata += str + ';' ;
            }
            
            forCodesdata = forCodesdata.removeEnd(';');
        }
        system.debug('@@@forCodesdata'+forCodesdata); */
		
     
        Id recordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
        
        ResearcherPortalProject__c projectObj = new ResearcherPortalProject__c();
      
        projectObj.Title__c            = projectdata.projectTitle;
        projectObj.Description__c      = projectdata.summary;
        projectObj.Start_Date__c       = projectdata.startDate;
        projectObj.End_Date__c         = projectdata.endDate;
        projectObj.Status__c           = projectdata.status;
        projectObj.Portal_Project_Status__c = projectdata.currentStatus;// Added by Subhajit :: 10.09.18 :: RPORW-264 
        projectObj.Access__c           = projectdata.access;
        projectObj.Approach_to_impact__c    = projectdata.approachedImpact;
        projectObj.Impact_Category__c  = commaSepratedList;
        projectObj.Discipline_area__c  = forCodesdata;  
                                                        
        projectObj.Keywords__c  = keywordsList;  // Added by Ankit :: 11.08.18 :: RPORW-229
        projectObj.Expected_Impact__c = projectdata.expectedImpact; // Added by Ankit :: 11.08.18 :: RPORW-229

        projectObj.id = recordIdfromcontroller;
        if(recordIdfromcontroller == null){
            projectObj.RecordtypeId        = recordTypeId;
            projectObj.Project_Type__c     = 'Researcher Portal';				
        } 
		
		performDMLOperations(projectObj,projectdata, recordIdfromcontroller); //Khushman: For PMD Fix: Below code moved to this method
		/*        
        system.debug('@@@projectObj'+JSON.serializePretty(projectObj));
        if (ResearcherPortalProject__c.sObjectType.getDescribe().isUpdateable() &&
        	ResearcherPortalProject__c.sObjectType.getDescribe().IsCreateable()){
        	upsert projectObj;
        }
        //On create project, create sharing member records as CI and Primary Contact.
        if(recordIdfromcontroller == null ) {
		    ResearcherMemberSharingUtils.createResearcherProjectMemberSharing(projectObj.ID,userInfo.getUserId(), Label.CI, true );
		    if(null!=projectdata.forCodes){
                createProjectClassifications(projectdata.forCodes, projectObj.id);
            }
        }
        
        if(recordIdfromcontroller != null){
           deleteProjectClassifications(projectObj.id); 
           if(null!=projectdata.forCodes){
               createProjectClassifications(projectdata.forCodes, projectObj.id);
           }
        }*/ 
    } 


    //Khushman: Method created for PMD Fix
    private static void performDMLOperations(ResearcherPortalProject__c projectObj,projectDetailsWrapper projectdata, Id recordIdfromcontroller){
    	System.debug('@@@projectObj'+JSON.serializePretty(projectObj));
		if(!Schema.sObjectType.ResearcherPortalProject__c.isUpdateable() ||
				!Schema.sObjectType.ResearcherPortalProject__c.isCreateable()){   
			System.debug('Check PMD');
			throw new DMLException();
		}
        upsert projectObj;
        //On create project, create sharing member records as CI and Primary Contact.
        if(recordIdfromcontroller == null ) {
		    ResearcherMemberSharingUtils.createResearcherProjectMemberSharing(projectObj.ID,userInfo.getUserId(), Label.CI, true );
		    if(null!=projectdata.forCodes){
                createProjectClassifications(projectdata.forCodes, projectObj.id);
            }
        }
        
        if(recordIdfromcontroller != null){
           deleteProjectClassifications(projectObj.id); 
           if(null!=projectdata.forCodes){
               createProjectClassifications(projectdata.forCodes, projectObj.id);
           }
        }
    }
    
    /*****************************************************************************************
	// Purpose      :  Method for creating Project Classificatons records 
	// Developer    :  Ankit Bhagat	
	// Created Date :  06/11/2018                 
	//************************************************************************************/      
    public static void createProjectClassifications(List<String> fORCodes, Id projectId) {
        
        System.debug('@@@FORCodes'+fORCodes);
        System.debug('@@@ProjectId'+projectId);
        // Added by Ankit for 1127 Defect 
        Id rmProjectRecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Master Project').getRecordTypeId();
        Id rppProjectRecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
        Id projectRecordTypeId = [Select id, RecordTypeId From ResearcherPortalProject__c where id =: projectId].RecordTypeId;
    	System.debug('@@@projectObjREcordTYpe'+projectRecordTypeId);
  		List<Research_Project_Classification__c> projectClassificationList = new List<Research_Project_Classification__c>();
		
		for(String eachFORcode : fORCodes) {
			
			Research_Project_Classification__c projectClassification = new Research_Project_Classification__c();
			if(projectRecordTypeId == rppProjectRecordTypeId){
		        projectClassification.For_Four_Digit_Detail__c = eachFORcode;
			}
			else if(projectRecordTypeId == rmProjectRecordTypeId){
		        projectClassification.For_Six_Digit_Detail__c = eachFORcode;
			}
			projectClassification.ResearcherPortalProject__c = projectId;   			
			projectClassificationList.add(projectClassification);	
		}
		
		if(!Schema.sObjectType.Research_Project_Classification__c.isCreateable()){   
			System.debug('Check PMD');
			throw new DMLException();
		}

        insert projectClassificationList;	
    }
	
	
    /*****************************************************************************************
	// Purpose      :  Method for deleting Project Classificatons records 
	// Developer    :  Ankit Bhagat	
	// Created Date :  06/11/2018                 
	//************************************************************************************/      
    private static void deleteProjectClassifications(Id projectId) {
        
		List<Research_Project_Classification__c> projectClassificationList = new List<Research_Project_Classification__c>();
		
		if(!Schema.sObjectType.Research_Project_Classification__c.isAccessible()){   
			System.debug('Check PMD');
			throw new DMLException();
		}
		
		projectClassificationList = [SELECT id, ResearcherPortalProject__c
	                                         		From Research_Project_Classification__c
			                                        WHERE ResearcherPortalProject__c =: projectId];
			                                        
		if(!Schema.sObjectType.Research_Project_Classification__c.isDeletable()){   
			System.debug('Check PMD');
			throw new DMLException();
		}
		delete projectClassificationList;
        
    }
	
    
    /*****************************************************************************************
	// Purpose      :  Wrapper of ResearcherProject 
	// Developer    :  Ankit Bhagat	
	// Created Date :  09/05/2018                 
	//************************************************************************************/      
    public class projectDetailsWrapper{
        @AuraEnabled public String recordId;
        @AuraEnabled public String projectId;
        @AuraEnabled public String projectTitle;
        @AuraEnabled public String status;
        @AuraEnabled public String currentStatus; // Added by Subhajit :: 10.09.18 :: RPORW-264 
        @AuraEnabled public String summary ;
        @AuraEnabled public date startDate ;
        @AuraEnabled public date endDate ;
        @AuraEnabled public String access;
       // @AuraEnabled public String projectStatus;
        @AuraEnabled public List<String> impactCategory;
        @AuraEnabled public List<String> forCodes;
        @AuraEnabled public String approachedImpact;
        @AuraEnabled public List<Id> removedFORcodes;
        @AuraEnabled public List<string> keywords {get;set;} // Added by Ankit :: 11.08.18 :: RPORW-229 
        @AuraEnabled public string expectedImpact {get;set;} // Added by Ankit :: 11.08.18 :: RPORW-229 

    }
}