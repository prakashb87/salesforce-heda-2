@isTest
public class PassportIdentityProviderLMSUserReqTest {

    @TestSetup
    static void makeData(){
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();

        Config_Data_Map__c endpointConfig = new Config_Data_Map__c();
        endpointConfig.name = 'IDAMProvisonEndPoint';
        endpointConfig.Config_Value__c = 'https://test.rmit.edu.au/api/idp';
        configurations.add(endpointConfig);

        Config_Data_Map__c clientIdConfig = new Config_Data_Map__c();
        clientIdConfig.name = 'IDAMIpaaSClientID';
        clientIdConfig.Config_Value__c = 'testadmin123';
        configurations.add(clientIdConfig);

        Config_Data_Map__c clientSecret = new Config_Data_Map__c();
        clientSecret.name = 'IDAMIpaaSClientSecret';
        clientSecret.Config_Value__c = 'dfsdkfdslkm34543lkmldkvmlfd';
        configurations.add(clientSecret);

        insert configurations;
    }

    @isTest
    static void constuctorTest() {
        PassportIdentityUserValidator userValidator = new PassportIdentityUserValidator();
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(userValidator);
        System.assertNotEquals(null, req.validator);
    }

    @isTest
    static void bastardConstructorTest() {
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest();
        System.assertNotEquals(null, req.validator);
    }

    @isTest
    static void successfulCallout() {

        Test.setMock(HttpCalloutMock.class, new IDAMProvisionSuccessMock());
        Test.startTest();
        Set<LearningManagementSystem> systems = new Set<LearningManagementSystem>{ 
            LearningManagementSystem.OPENEDX, LearningManagementSystem.CANVAS };
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser('test@example.com', systems);
            System.assert(true);
        } catch(HttpCalloutException e) {
            System.assert(false);
        }
        Test.stopTest();
    }

    @isTest
    static void errorResponseCallout() {

        Test.setMock(HttpCalloutMock.class, new IDAMProvisionErrorMock());
        Test.startTest();
        Set<LearningManagementSystem> systems = new Set<LearningManagementSystem>{
            LearningManagementSystem.OPENEDX, LearningManagementSystem.CANVAS
        };
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser('test@example.com', systems);
            System.assert(false);
        } catch(HttpCalloutException e) {
            System.assert(true);
        }
        Test.stopTest();
    }

    @isTest
    static void nullSystemsParameterTest() {
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser('test@example.com', null);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        }
    }

    @isTest
    static void blankSystemsParameterTest() {
        Set<LearningManagementSystem> systems = new Set<LearningManagementSystem>();
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser('test@example.com', systems);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        }
    }

    @isTest
    static void blankEmailParameterTest() {
        Set<LearningManagementSystem> systems = new Set<LearningManagementSystem>{
            LearningManagementSystem.OPENEDX
        };
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser('', systems);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        }
    }

    @isTest
    static void blankParametersTest() {
        Test.startTest();
        PassportIdentityProviderLMSUserRequest req = new PassportIdentityProviderLMSUserRequest(new MockUserValidator());
        try{
            req.createLearningMgmtSystemUser(null, null);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        }
        Test.stopTest();
    }


    private class IDAMProvisionSuccessMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"id": "ID-dev-6r87t87588","result": "success","code": "200","application": "rmit-system-api-idam","provider": "idam","payload":{"success": true,"message": ""}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    private class IDAMProvisionErrorMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"id": "ID-dev-6r87t87588","result": "Error","code": "400","application": "rmit-system-api-idam","provider": "idam","payload": {"Error": true,"message": "abcdefghi"}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
        
    }
}