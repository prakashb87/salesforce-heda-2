/*
  ******************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 18/05/2018 
  * @description  : TestClass for CreateRecordsOnCourseConnectionLifeCycle class
  ******************************************************************************************************
*/
@isTest
public class CreateRecordOnCCLifeCycleTest
{
    
    
    static testMethod void testCourseConnectionLifeCycleObjCreation()
    {
        List<hed__Course_Enrollment__c> courseConnectionObjList = TestDataFactoryUtil.createCourseConnectionRecords();
        system.debug('LLLLL=' + courseConnectionObjList.size());
        TestDataFactoryUtil.createLifeCycleRecords();
        TestDataFactoryUtil.createConfigDataMapRecords();
        TestDataFactoryUtil.dummycustomsetting();
        Set<Id> courseConnectionIds = new Set<Id>();
        for( hed__Course_Enrollment__c ccObj : courseConnectionObjList)            
        {
            courseConnectionIds.add(ccObj.id);
        }
        
        
        Test.startTest();
      
        CreateRecordsOnCourseConnectionLifeCycle.createCourseConnection(courseConnectionIds);
        
        Test.stopTest(); 
        
        
        List<Course_Connection_Life_Cycle__c> courseConnectionLClist = new List<Course_Connection_Life_Cycle__c>();
        

        CreateRecordsOnCourseConnectionLifeCycle.getFederationId ((new list<Id>(courseConnectionIds) )[0] );
        Id firstElement = null;
        for (Id setElement : courseConnectionIds)
        {
                firstElement = setElement;
                break;
        }
        System.debug('FFF' + firstElement);
        courseConnectionLClist = [SELECT id from Course_Connection_Life_Cycle__c where Course_Connection__c =: firstElement];
        System.debug('FFFFF=' + courseConnectionLClist.size());
        System.assert(courseConnectionLClist.size()==3);
        
        List<Course_Connection_Life_Cycle__c> courseConnectionLCSAMS = new List<Course_Connection_Life_Cycle__c>();
        courseConnectionLCSAMS = [SELECT id from Course_Connection_Life_Cycle__c where Course_Connection__c =: firstElement and Stage__c =: ConstantUtil.COURSECONNECTION_LIFECYCLE_POSTTOSAMS];
        System.assert(courseConnectionLCSAMS.size() == 1);
        
        List<Course_Connection_Life_Cycle__c> courseConnectionLCCanvas= new List<Course_Connection_Life_Cycle__c>();
        courseConnectionLCCanvas = [SELECT id from Course_Connection_Life_Cycle__c where Course_Connection__c =: firstElement and Stage__c =: ConstantUtil.COURSECONNECTION_LIFECYCLE_POSTTOCANVAS];
        System.assert(courseConnectionLCCanvas.size() == 1); 
        
        List<Course_Connection_Life_Cycle__c> courseConnectionLCCreated = new List<Course_Connection_Life_Cycle__c>();
        courseConnectionLCCreated = [SELECT id from Course_Connection_Life_Cycle__c where Course_Connection__c =: firstElement and Stage__c =: ConstantUtil.COURSECONNECTION_LIFECYCLE_STAGE_CREATED];
        System.assert(courseConnectionLCCreated.size() == 1); 
    }
    
	
}