/**
* -------------------------------------------------------------------------------------------------+
* @description This Test class(ActivatorLeadConversionControllerTest) to get code coverage of 
                main apex class (ActivatorLeadConversionController)
* --------------------------------------------------------------------------------------------------
* @author         Dinesh Kumar
* @version        0.1
* @created        05-10-2019
* @modified       05-14-2019
* ------------------------------------------------------------------------------------------------+
*/

@isTest
public class ActivatorLeadConversionControllerTest {
    
    private static final String CONTACTSTUDENT = 'Validation error on Contact: This contact has a student affiliation, therefore, the following fields cannot be amended; First Name, Last Name, Middle Name, Preferred Name, Gender, Mailing Address, Fax.';
    private static Id affStudentRecordTypeId= Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
    private static Id leadActivatorRecordTypeId= Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Activator Registration').getRecordTypeId();
    
    @isTest
    static void testConvertActivatorLead(){
        
        Id accRecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        String externalId = CustomSettingsService.getActivatorExternalId();
        //Add Activator account
        Account activator = new Account();
        activator.Name = 'Activator';
        activator.External_Unique_ID__c = externalId;
        insert activator;
        Account acc = new Account(Name='Test' , RecordTypeId=accRecordTypeId);
        insert acc;
        
        Contact con = new Contact(LastName='Test', AccountId=acc.Id);
        insert con;
        
        Group testGroup = new Group(Name='RMITO Queue', type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId())){
            QueueSObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Lead');
            insert testQueue;
        }
        
        Lead lRecord7 = new Lead(LastName = 'Fry1', Company='Fry And Sons1',RecordTypeId=leadActivatorRecordTypeId, OwnerId = testGroup.Id);
        insert lRecord7;
        
        String[] result = ActivatorLeadConversionController.convertActivatorLead(lRecord7).split(',');
        System.assertEquals('true',result[0]);
  
    }


    @isTest
    static void testThroughAurException(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='RMIT Activator User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com.rmdev2');
        insert u;

        Contact lCon8 = new Contact(LastName = 'FryCon');
        insert lCon8;
        hed__Affiliation__c lAff8 = new hed__Affiliation__c(hed__Contact__c = lCon8.Id,RecordTypeId = AffStudentRecordTypeId);
        insert lAff8;
        Lead lRecord8 = new Lead(FirstName='FryCon ',LastName = 'Fry1', Company='Fry And Sons1',Matched_Contact__c = lCon8.Id,RecordTypeId=leadActivatorRecordTypeId, ownerId = u.Id);
        insert lRecord8;

        System.runAs(u) {

            try{
                ActivatorLeadConversionController.convertActivatorLead(lRecord8);   
            }catch(Exception e){
                List<String> errorMsg = CONTACTSTUDENT.split(':');
                System.assertEquals(errorMsg[1], e.getMessage());
            }
        }        
        
    }


}