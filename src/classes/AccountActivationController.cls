/***************************************************************************************************************
 * ECB-4587 Maria Luisa Esmero 01/11/2018
 * Description: Where the user clicks on the activation Url link in the user registration email,  
 *    validate the registration, update the User Registration record and redirect to the cart page.
 * Parameter  : Id parameter from the Url link in the email
 * To run using Developer Console, comment out idParam statement and run this script:
 *    AccountActivationController accountactivate = new AccountActivationController();
 *    accountactivate.idParam = 'copy paste the id parameter from the activation Url link here'; 
 *    accountactivate.updateRegistrationRedirect();
 * PMD checks: Removed object permissions for guest users
 **************************************************************************************************************/
public without sharing class AccountActivationController{
    public string idParam {get;set;}
    public boolean success {get;set;}    
    public boolean isActivated {get;set;}
    public boolean isExpired {get;set;}
    public boolean isUpdated {get;set;}
    public string userRegistrationName {get;set;}
    public id userRegId {get;set;}
    public datetime expiry {get;set;}
    private string contactId {get;set;}
    private String userEmail;
    
    @testVisible private IdentityProviderUserVerificationReq verificationReq;

    public AccountActivationController() { 
        // Get User Registration record
        idParam = ApexPages.currentPage().getParameters().get('id');   
        success = true;
        verificationReq = new PassportIdpUserVerificationReq();
    }
    
    public PageReference updateRegistrationRedirect() {  
        try {
            string err = '';
            User_Registration__c [] userReg = [SELECT id, Name, Contact__c, Contact__r.email, Activated__c, Expiry__c FROM User_Registration__c where Registration_id__c = :idParam LIMIT 1];
            system.debug('userReg value ::: ' +userReg);
            
            if (userReg.size() > 0) {
                system.debug('User Registration record Id selected ::: '+userReg[0].Id);
                expiry = userReg[0].Expiry__c;
                isActivated = userReg[0].Activated__c;
                contactId = userReg[0].Contact__c;
                userRegistrationName = userReg[0].Name;
                userRegId = userReg[0].id;
                userEmail = userReg[0].Contact__r.email;
                success = true;      
                // Check if the User Registration record is activated. 
                if (isActivated == true) {
                    success = true;
                    system.debug('User Registration record is activated ::: '+isActivated);
                } 
                // Ensure that the User Registration record is not yet expired.
                if (expiry < datetime.now()) {
                    isExpired = true;
                    success = false;
                    err='1';
                    system.debug('User Registration record is expired ::: '+isExpired);
                }         
                system.debug('The User Registration record is not activated nor expired ::: '+success); 
            } else {
                success = false;
                system.debug('Unable to find the User Registration with Id ::: ' +userRegId);
            }    
            
            // Update the User Registration record only if not yet activated.
            if ((success == true) && (isActivated != true)) {

                // Update status with Passport IDAM
                verificationReq.updateUserToVerified(this.userEmail);
                system.debug('UPDATING User Registration record as activated with Id ::: '+userRegId);
                
                // Create the community user from the Contact 
                createCommunityUser();

                // Update User Registration record 
                User_Registration__c uRegObj = [SELECT id, Activated__c, Activated_on__c FROM User_Registration__c where id = :userRegId  LIMIT 1];
                if (uRegObj != null) {
                    system.debug('User Registration record Id for update ::: '+userRegId);
                    uRegObj.Activated__c = true;
                    system.debug('Set Activated to ::: '+uRegObj.Activated__c);
                    uRegObj.Activated_on__c = datetime.now();
                    system.debug('Set Activation date to ::: '+uRegObj.Activated_on__c); 
                    update uRegObj;
                    isUpdated = true;
                    system.debug('Successful update of User Registration record as activated with Id ::: '+userRegId); 
                }          
            }

            system.debug('Continue to redirect? ::: '+success); 
            
            // Redirect        
            if (success) {             
                PageReference pageRef = new PageReference(ConfigDataMapController.getCustomSettingValue('RMITAccountActivationSuccessUrl'));
                system.debug('Successfully redirected to account successful activation Url ::: '+pageref.getUrl());
                return pageRef;
            } else {
                PageReference pageRef = new PageReference(ConfigDataMapController.getCustomSettingValue('RMITAccountActivationFailedUrl') + '?err=' + err);
                system.debug('Successfully redirected to error Url ::: '+pageref.getUrl());
                return pageRef;  
            }
        } catch (Exception e) {
            PageReference pageRef = new PageReference(ConfigDataMapController.getCustomSettingValue('RMITAccountActivationFailedUrl') + '?err=1');
            system.debug('Successfully redirected to error Url ::: '+pageref.getUrl());
            return pageRef;
        }
    }

    private void createCommunityUser() {
        Contact con = [SELECT Id, FirstName, LastName, Title, Email, IDAM_ECB_Federated_Id__c FROM Contact WHERE Id = :this.contactId LIMIT 1];

        Id profileId = [SELECT Id FROM Profile WHERE Name =:ConfigDataMapController.getCustomSettingValue('ProfileName')].Id;
        User usr = new User();
        
        String aliasFName = con.FirstName.substring(0,1);
        String aliasLName;
        if (con.LastName.length() > 4){
            aliasLName = con.LastName.substring(0,4);
        } else {
            aliasLName = con.LastName;
        } 
        
        usr.FirstName = con.FirstName;
        usr.LastName = con.LastName;
        usr.Email=con.Email;
        if(!runningInASandbox()){
            usr.Username=con.Email+'.marketplace';
            usr.Title = con.Title;
            usr.CommunityNickname=con.FirstName.left(1)+con.LastName+'.' + con.IDAM_ECB_Federated_Id__c;
        }else{
            usr.Username=con.Email+'.marketplace.'+ConfigDataMapController.getCustomSettingValue('SandboxName'); 
            usr.Title = con.Title+'.'+ConfigDataMapController.getCustomSettingValue('SandboxName'); 
            usr.CommunityNickname=con.FirstName.left(1)+con.LastName+'.' + con.IDAM_ECB_Federated_Id__c+'.' +ConfigDataMapController.getCustomSettingValue('SandboxName'); 
        }
        usr.ProfileId = profileId;
        usr.ContactId = con.Id;
        usr.IsActive = true;
        usr.IsPortalSelfRegistered = true;
        String userFedId = con.IDAM_ECB_Federated_Id__c;
        userFedId = userFedId.toLowerCase();
        usr.FederationIdentifier = userFedId;
        usr.Alias = aliasFName+aliasLName;
        usr.TimeZoneSidKey = ConfigDataMapController.getCustomSettingValue('Region'); 
        usr.EmailEncodingKey = ConfigDataMapController.getCustomSettingValue('EmailFormat'); 
        usr.LanguageLocaleKey = ConfigDataMapController.getCustomSettingValue('Language'); 
        usr.LocaleSidKey = ConfigDataMapController.getCustomSettingValue('Locale');
        DataBase.insert(usr);
        
        con.Community_User_Provisioned__c = true;
        String conId = con.Id;
        System.debug('conId------>'+conId);
        Database.update(con);
    }

    private static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    
}