/*******************************************
Purpose: Test class ViewMyPublicationHelper
History:
Created by Ankit Bhagat on 31/10/2018
*******************************************/
@isTest
public class TestViewMyPublicationHelper {
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    
    /************************************************************************************
// Purpose      :  Test functionality for View MyEthicsList Controller
// Developer    :  Ankit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    
    @isTest
    public static void viewMyEthicsListUtilMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            List<Publication__c> publicationList = new List<Publication__c>();            
            Publication__c publication = new Publication__c();
            publication.Year_Published__c = 2017;
            publication.Publication_Category__c = ' A1 - Book';
            publication.Publication_Title__c = 'Test Title' ;
            publication.Outlet__c = 'Test';
            publication.Audit_Result__c   = '39 active';
            publication.Conference_Location__c='india';
            publication.Conference_Name__c='salesforce';
            publication.Copyright__c='testcopyright';
            publication.Creative_Work_Extent__c='creative';
            publication.Creative_Work_Type__c='dynamic';
            publication.Digital_Object_Identifier__c='DTO';
            publication.Description__c='testDisc';
            publication.Edition__c='First';
            publication.Editors__c='Ali';
            publication.End_Page__c='salesEnd';
            publication.Event_Date__c=  '2019';
            publication.ISSN__c='123124435';
            publication.Publication_Id__c = 123;
            publicationList.add(publication);
            
            
            Publication__c publication1 = new Publication__c();
            publication1.Year_Published__c = 2017;
            publication1.Publication_Category__c = ' A1 - Book';
            publication1.Publication_Title__c = 'Test Title1';
            publication1.Outlet__c = 'Test';
            publication1.Audit_Result__c   = '39 active';
            publication1.Publication_Id__c = 1234;
            publicationList.add(publication1);
            insert publicationList;
            System.Assert(publicationList.size()>0);      
            
            Publication_Classification__c pubclass = new Publication_Classification__c();
            pubclass.For_Six_Digit_Detail__c='3213yergsdfasdf';
            pubclass.For_Six_Digit_Code__c='12342345';
            pubclass.Publication__c=publicationList[0].id;
            insert pubclass;
                
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Ethics__c = ethicTest1.id;
            ethicsMemberSharing.User__c   = u.id;
            ethicsMemberSharing.Currently_Linked_Flag__c=true;
            ethicsMemberSharing.AccessLevel__c='RMIT Public';
            ethicsMemberSharing.RecordTypeId= Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
            memberResearchList.add(ethicsMemberSharing);
            
            Researcher_Member_Sharing__c projectMemberSharing = new Researcher_Member_Sharing__c();
            projectMemberSharing.Researcher_Portal_Project__c = researchProject.id;
            projectMemberSharing.User__c   = u.id;
            projectMemberSharing.Currently_Linked_Flag__c=true;
            projectMemberSharing.AccessLevel__c='RMIT Public';
            projectMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            memberResearchList.add(projectMemberSharing);
            
            Researcher_Member_Sharing__c publicationMemberSharing = new Researcher_Member_Sharing__c();
            publicationMemberSharing.Publication__c = publicationList[0].id;
            publicationMemberSharing.User__c   = u.id;
            publicationMemberSharing.Currently_Linked_Flag__c=true;
            publicationMemberSharing.AccessLevel__c='RMIT Public';
            publicationMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
            memberResearchList.add(publicationMemberSharing);
            insert memberResearchList;
                       
            List<PublicationDetailsWrapper.publicationWrapper> publicationWrapperList = new List<PublicationDetailsWrapper.publicationWrapper>();
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper = new PublicationDetailsWrapper.publicationWrapper();
            publicationWrapper.year       = 2016;
            publicationWrapper.category   = 'TestData';
            publicationWrapper.title      = 'Test';
            publicationWrapper.outlet     = 'Test';
            publicationWrapper.status     = 'Test';
            publicationWrapper.authors   ='';
            publicationWrapperList.add(publicationWrapper);
            
            
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper1 = new PublicationDetailsWrapper.publicationWrapper(publicationList[0]);
            
            PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper totalwrapper = new PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper();
            totalwrapper.totalPublications = 1;
            totalwrapper.publicationWrapperList = publicationWrapperList; 
            
            ViewMyPublicationHelper.getPublicationWrapperList();
            ViewMyPublicationHelper.getPublicationDetailsHelper(publicationList[0].id);
         	//ViewMyPublicationHelper.getPublicationStatusValue();
           
            
        }
    }
            
        /************************************************************************************
        // Purpose      :  Test functionality for View MyEthicsList Controller
        // Developer    :  Ankit
        // Created Date :  12/05/2018                 
        //***********************************************************************************/
        
     /*   @isTest
        public static void viewMyEthicsListUtilNegativeMethod(){
        
            User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
            
            System.runAs(u) {
            
            PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper returnvalue = ViewMyPublicationHelper.getPublicationWrapperList();
            System.assert(returnvalue!= null);
            }
            }*/
        
}