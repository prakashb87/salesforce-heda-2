/*****************************************************************************************************
*** @Class             : IDAMResponseWrapperTest
*** @Author            : Rishabh Anand
*** @Requirement       : Request Wrapper Class For IDAM Integration 
*** @Created date      : 12/09/2018
*** @JIRA ID           : ECB-4352
*** @LastModifiedBy	: Rishabh Anand
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** Test Class For IDAMResponseWrapper
*****************************************************************************************************/
@isTest
public class IDAMResponseWrapperTest {
    
    static testMethod void testParse() {
        
        String json = '{'+
            '    \"id\": \"ID-dev-6r87t87588\",'+
            '    \"result\": \"success\",'+
            '    \"code\": \"200\",'+
            '    \"application\": \"rmit-system-api-idam\",'+
            '    \"provider\": \"idam\",'+
            '    \"payload\": {'+
            '        \"success\": true,'+
            '        \"message\": \"\"'+
            '    }'+
            '}';
        IDAMResponseWrapper obj = IDAMResponseWrapper.parse(json);
        System.assert(obj != null);
    }    
}