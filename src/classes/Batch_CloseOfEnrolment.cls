/**
* -------------------------------------------------------------------------------------------------+
* This batch updates all RMITO Leads that have not been converted, and have RMITO courses, 
* in order to update the Lead Close of Enrolment.
* --------------------------------------------------------------------------------------------------
* @author         Sajitha Deivasikamani
* @version        0.3
* @created        2019-03-12
* @modified       2012-04-10
* @modified by    Ajay Kumar P.
* @Reference      RM-2539,RM-2791
* -------------------------------------------------------------------------------------------------+
*/

public class Batch_CloseOfEnrolment implements Database.Batchable<sObject> {
    private static final String RMITO = 'RMIT Online';
    public Database.QueryLocator start(Database.BatchableContext leadContext){   
        String leadQuery = 'SELECT Id,FirstName,LastName,Company,Course__c,Close_of_Enrolment__c,Lead.RecordTypeId FROM Lead WHERE RecordType.DeveloperName = \'Prospective_Student_RMIT_Online\' AND isConverted = false AND Course__c != null';
        return Database.getQueryLocator(leadQuery);
    }
    
    public void execute(Database.BatchableContext leadContext,List <Lead> scope ){        
        updateCloseofEnrolment(scope);
    }
    
    public void finish(Database.BatchableContext leadContext){
        RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('COEBatchConfiguration');
        AsyncApexJob jobInfo = [SELECT Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:leadContext.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = csRmito.BatchFinishEmailAddresses__c.split(',');
        mail.setToAddresses(toAddresses);
        mail.setSubject('Close of Enrolment update ' + jobInfo.Status);
        mail.setPlainTextBody('Records processed ' + jobInfo.TotalJobItems +   'with '+ jobInfo.NumberOfErrors + ' failures.');
        if(jobInfo.NumberOfErrors > 0 && csRmito.EnableSendEmailBatchFinish__c){            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }        
    }
    
    private void updateCloseofEnrolment(List<Lead> leadRecord){
        
        RMErrLogHandlerClassException re = new RMErrLogHandlerClassException();
        List< Lead > leadToUpdate= new List< Lead >();
        Map< Id, Date > courseIDSet = new Map< Id, Date >();
        
        for(hed__Course__c courseRecord: [Select Id, 
                                          (select Id,hed__Start_Date__c from hed__Course_Offerings__r  
                                           where Last_Day_to_Add__c >= TODAY 
                                           AND hed__Start_Date__c != NULL ORDER BY hed__Start_Date__c limit 1) 
                                          from hed__Course__c 
                                          where Id IN (Select Course__c FROM Lead 
                                                       WHERE RecordType.DeveloperName = 'Prospective_Student_RMIT_Online' 
                                                       AND isConverted = false 
                                                       AND Course__c != null)  
                                          AND Product_Line__c =: 'RMIT Online']){ 
                                              if(courseRecord.hed__Course_Offerings__r.size()>0) {
                                                  courseIDSet.put(courseRecord.id,courseRecord.hed__Course_Offerings__r[0].hed__Start_Date__c);	
                                              }
                                          }
        for (Lead newLead: leadRecord) {  
            if(courseIDSet.containskey(newLead.Course__c)){                
                if(newLead.Close_of_Enrolment__c !=courseIDSet.get(newLead.Course__c)){
                    newLead.Close_of_Enrolment__c = Null;
                    leadtoUpdate.add(newLead);  
                }
            }else{              
                if(String.IsNotBlank(String.Valueof(newLead.Close_of_Enrolment__c))){
                    newLead.Close_of_Enrolment__c = Null;
                    leadtoUpdate.add(newLead);
                }
            }
        }
        try {
            if(!leadToUpdate.isEmpty()) {
                Database.update(leadToUpdate,false);
            }
            testthrowCustomException();
        }catch (Exception e) {
            re.RMErrLogRecHandler(UserInfo.getUserId(), e, e.getCause(), e.getLineNumber(), e.getMessage(), DateTime.now(), e.getTypeName());
        }        
    }
    
    private void testthrowCustomException(){
    	if(Test.isRunningTest()){
                CalloutException e = new CalloutException();
                e.setMessage('This is a constructed exception for testing and code coverage');
                throw e;
            }
    }
    
}