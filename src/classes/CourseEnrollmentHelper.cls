/* 
   @Author <Pawan Srivastava>
   @name <CourseEnrollmentHelper>
   @CreateDate <11/10/2018>
   @Description 
   @Version <1.0>
   */
public without sharing class CourseEnrollmentHelper {

    /**
    * @Author       Pawan Srivastava
    * @Name         iPassOpenEdxApiEnrollCall
    * @Date         11/10/2018
    * @Description  This method will call OpenEdxApi
    * @Param        string, string
    * @return       NA
    */    

    @Future(callout=true)
    public static void iPassOpenEdxApiEnrollCall(String userId, Set<String> courseIds) {   
            HttpResponse response = iPassOpenEdxApiEnroll(userId, courseIds);
            //Add code here to handle the response and process it update the Course Lifecycle object
            handleOpenEdxEnrollResposne(response,userId, courseIds);
    }  
      
    @Future(callout=true)
    public static void iPassOpenEdxApiUnEnroll(String userId, Set<String> courseIds) {              
        HttpResponse response = iPassOpenEdxApiUnEnrollCall(userId, courseIds);
        //Add code here to handle the response and process it and update the Course Lifecycle object
        handleOpenEdxUnEnrollResposne(response,userId, courseIds);
    }
    //@Future(callout=true)
    //public static void iPassOpenEdxApiEnrollCall(String userId, Set<String> courseIds) {               
    public static HttpResponse iPassOpenEdxApiEnroll(String userId, Set<String> courseIds) {               
        
        HttpResponse response = new HttpResponse();
        List<enrollmentToOpenEdx_Courses> courseWrapperList = CourseEnrollmentHandler.getcourseWrapperList(courseIds); 
                  
        String requestJSONString = JSON.serialize(new enrollmentToOpenEdxRequest(userId, courseWrapperList));        
        System.debug('requestJSONString -->'+requestJSONString);
        
        try {      
            string methodName = 'iPassOpenEdxApiEnroll';
            response = CourseEnrollmentHandler.setHttpReqResAndInsertRMErrorLogger(requestJSONString,methodName);
        }
        catch (Exception ex) {
           System.debug('response---------------'+ex.getMessage()); 
           throw ex;
        }
        return response;
    }

    //ECB-4635 - Process the Open EDX response on enrolment
    public static void handleOpenEdxEnrollResposne(HttpResponse response,String userId, Set<String> courseIds)
    {       
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c();
        
        try{
        	
            List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList = CourseEnrollmentHandler.getOpenEdxEnrollCClifeCycleList(userId,courseIds);
             
            OpenEdxResponseWrapper jsonResponse = (OpenEdxResponseWrapper) JSON.deserialize(response.getBody(), OpenEdxResponseWrapper.class);
            system.debug('jsonResponse::::::'+jsonResponse);
            
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'OpenEDX_Enroll',response.getBody(), 'Acknowledgement','',false);
            CourseEnrollmentHandler.insertResponseLogObject(responseLogObjectToInsert);
                       
            if(response.getStatusCode() == 200)
            {
            	CourseEnrollmentHandler.getOfferSetsAndUpdateCClifeCycle(jsonResponse,courseConnectionLifeCycleList);
            }        
        }
        catch(Exception ex)
        {
            system.debug('Exception:::::'+ ex);
            ApplicationErrorLogger.logError(ex);
        }
    }
          
     /**
    * @Author       Pawan Srivastava
    * @Name         iPassOpenEdxApiUnEnrollCall
    * @Date         11/10/2018
    * @Description  This method will call OpenEdxApi
    * @Param        string, string
    * @return       NA
    */    
    
    //@Future(callout=true)
    //public static void iPassOpenEdxApiUnEnrollCall(String userId, Set<String> courseIds) {               
    public static HttpResponse iPassOpenEdxApiUnEnrollCall(String userId, Set<String> courseIds) {              

        HttpResponse response = new HttpResponse();
        List<enrollmentToOpenEdx_Courses> courseWrapperList = CourseEnrollmentHandler.getcourseWrapperList(courseIds);
                  
        String requestJSONString = JSON.serialize(new enrollmentToOpenEdxRequest(userId, courseWrapperList));        
        System.debug('requestJSONString -->'+requestJSONString);
        
        try { 
        	string methodName = 'iPassOpenEdxApiUnEnrollCall';
        	response = CourseEnrollmentHandler.setHttpReqResAndInsertRMErrorLogger(requestJSONString,methodName);
            return response;
        }
        Catch (Exception ex) {
           System.debug('response---------------'+ex.getMessage()); 
           throw ex;
        }
    }
    
    //ECB-4635 - Process the Open EDX response on Unenrolment
    public static void handleOpenEdxUnEnrollResposne(HttpResponse response,String userId, Set<String> courseIds){
        
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c();
        
        try{
        	List<Course_Connection_Life_Cycle__c> courseConnectionLifeCycleList = CourseEnrollmentHandler.getOpenEdxUnEnrollCClifeCycleList(userId,courseIds);
            OpenEdxResponseWrapper jsonResponse = (OpenEdxResponseWrapper) JSON.deserialize(response.getBody(), OpenEdxResponseWrapper.class);
            system.debug('jsonResponse::::::'+jsonResponse);
            
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'OpenEDX_Unenroll',response.getBody(), 'Acknowledgement','',false);
            CourseEnrollmentHandler.insertResponseLogObject(responseLogObjectToInsert); 
            
            
            if(response.getStatusCode() == 200)
            {
            	CourseEnrollmentHandler.getOfferSetsAndUpdateCClifeCycle(jsonResponse,courseConnectionLifeCycleList);
            }
        
        }
        catch(Exception ex)
        {
            system.debug('Exception:::::'+ ex);
             ApplicationErrorLogger.logError(ex);
        }
    }
    
    public class enrollmentToOpenEdxRequest {
        
        @AuraEnabled
        public String userId { get; set; }  
       
        @AuraEnabled
        public enrollmentToOpenEdx_Courses[] courses{ get; set; } 
        
        public enrollmentToOpenEdxRequest (String userId , enrollmentToOpenEdx_Courses[] corses) {
            this.userId = userId;
            courses = corses;
        }
        
   }
    
    public class enrollmentToOpenEdx_Courses {                
        @AuraEnabled
        public String courseId; 
        
        public enrollmentToOpenEdx_Courses(String course) {
            courseId = course;
        }
    }
    
}