/*****************************************************************
Name: EmbeddedPdfAndIntegrationController
Author: Capgemini [Shreya]
Purpose: Handle PDF generation,SAMS and Canvas Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900 
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           23/06/2018         Initial Version
********************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class EmbeddedPdfAndIntegrationController{

    /********************************************************************
    // Purpose              : Query all the related Services                        
    // Author               : Capgemini [Shreya]
    // Parameters           : List < Id > serviceIds
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 

    
    global static void getServices(List < Id > serviceIds) {

        system.debug('serviceId:::'+serviceIds);
                  
        List <csord__Service__c> serviceList = [SELECT csordtelcoa__Product_Configuration__r.Id,
            csordtelcoa__Product_Basket__r.Basket_Number__c,
            csordtelcoa__Product_Basket__r.cscfga__Total_Price__c,
            csordtelcoa__Main_Contact__r.Name,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__Formula_MailingAddress__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c,
            csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c,
            csordtelcoa__Main_Contact__r.Student_ID__c,
            csordtelcoa__Main_Contact__r.Email,
            csordtelcoa__Main_Contact__r.id
            FROM csord__Service__c WHERE Id IN: serviceIds
        ];
                     
        system.debug('serviceList:::'+serviceList);

       
        Map<String,List<csord__Service__c>> contactServiceMap = new Map<String,List<csord__Service__c>>();
        Set<Id> productBasketIdSet = new Set<Id>();
        
        if(!serviceList.isEmpty()){
            for(csord__Service__c ser : serviceList){
                productBasketIdSet.add(ser.csordtelcoa__Product_Basket__c);
                String mapKeyString = ser.csordtelcoa__Main_Contact__r.id + ' ' + ser.csordtelcoa__Product_Basket__r.Basket_Number__c;
                
                if(contactServiceMap.containsKey(mapKeyString)){
                    contactServiceMap.get(mapKeyString).add(ser);
                }else{
                    List<csord__Service__c> contactServiceList = new List<csord__Service__c>();
                    contactServiceList.add(ser);
                    contactServiceMap.put(mapKeyString,contactServiceList);
                }
                
            }
        } 
        
        List<cscfga__Product_Basket__c> prodBasketList = [Select Id,Basket_Number__c,cscfga__Total_Price__c from cscfga__Product_Basket__c where Id IN :productBasketIdSet ];
        
        
        System.debug('prodBasketList ----------------->'+prodBasketList );
        List<Payment__c> paymentRecordList = new List<Payment__c>();
        
        for(cscfga__Product_Basket__c pb : prodBasketList ){
            //Creating Payment Record
            Payment__c paymentObj = new Payment__c();
            paymentObj.Receipt_Number__c = 'n/a';  
            paymentObj.Product_Basket__c = pb.Id;
            paymentObj.Amount__c = pb.cscfga__Total_Price__c;
            paymentRecordList.add(paymentObj);
            
         }   
        
        System.debug('paymentRecordList------------------>'+paymentRecordList);
        
        
        
        try {
            Insert paymentRecordList;
            if(Test.isRunningTest()){
            throw new DMLException();
        }
        } catch (Exception ex) {
            system.debug('Exception:::' + ex);
        }  
        
        system.debug('contactServiceMap>>>>'+contactServiceMap);
        
        if(!contactServiceMap.isEmpty()){
            for(String keyVal : contactServiceMap.KeySet()){                
                generatePDF(contactServiceMap.get(keyVal));
            }   
        }
    }

    /********************************************************************
    // Purpose              : Generate PDF for all the course connection records which are created via embedded batch credential job.                          
    // Author               : Capgemini [Shreya]
    // Parameters           : List <csord__Service__c> serviceList
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 
        
    global static void generatePDF(List <csord__Service__c> serviceList) {
    
        system.debug('serviceList>>>>'+serviceList);
        
        csord__Service__c service = new csord__Service__c();
        String prodConfigIdString = ''; //To store all the prod configuration Id as a string and pass it to the vf page 
        String address;
        
      /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/
        String street;
        String city;
        String cityStatePOCode;
        String country;
        
        if(!serviceList.isEmpty()){
            //Creating Contact Address
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c !=null){
                street = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c + ' ';
            }
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c !=null){
                street += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c + ' ';
            }
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c !=null){
                cityStatePOCode = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c + ' ';
            }   
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c !=null){
                cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c + ' ';
            }   
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c !=null){
                cityStatePOCode += serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c + ' ';
            }   
            if(serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c != '' && serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c !=null){
                country = serviceList[0].csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c + ' ';
            }   
         }                   
    
       /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
        
        if (!serviceList.isEmpty()) {
            service = serviceList[0];
            for (csord__Service__c ser: serviceList) {

                if (ser.csordtelcoa__Product_Configuration__r.Id != null) {
                    prodConfigIdString = prodConfigIdString + ser.csordtelcoa__Product_Configuration__r.Id + ',';

                }
            }
        }
        prodConfigIdString.removeEnd(',');
        system.debug('prodConfigIdString:::::::::' + prodConfigIdString);
        
        //Querying to get the payment record related to the service basket number
        Payment__c paymentRec; 
        try{ 
            paymentRec = [Select Id,Product_Basket__c,Receipt_Number__c from Payment__c where Product_Basket__c = :serviceList[0].csordtelcoa__Product_Basket__c];
            system.debug('paymentObj::::' + paymentRec.Id);
        }catch(QueryException ex){
            system.debug('Exception:::'+ex);
        }
        

        
        //Passing parameters to the InvoicePDF vf page
        PageReference pdf = Page.InvoicePDF;
        
        if(!serviceList.isEmpty()){
            pdf.getParameters().put('basketNumber', serviceList[0].csordtelcoa__Product_Basket__r.Basket_Number__c);
            pdf.getParameters().put('studentName', serviceList[0].csordtelcoa__Main_Contact__r.Name);
            pdf.getParameters().put('studentId', serviceList[0].csordtelcoa__Main_Contact__r.Student_ID__c);
            pdf.getParameters().put('totalPrice', String.valueOf(serviceList[0].csordtelcoa__Product_Basket__r.cscfga__Total_Price__c));
        }
        
     /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/
        if(street!='' && street!=null){
            pdf.getParameters().put('street', street );
        } 
        if(cityStatePOCode!='' && cityStatePOCode!=null){
            pdf.getParameters().put('cityStatePOCode', cityStatePOCode);
        } 
        if(country!='' && country!=null){
            pdf.getParameters().put('country', country );
        } 
        
        /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
              
        if(prodConfigIdString!='' && prodConfigIdString!=null){
            pdf.getParameters().put('prodConfigIds', prodConfigIdString);
        }
        if(paymentRec!=null){
            pdf.getParameters().put('receiptNo', paymentRec.Receipt_Number__c);
        }   
        
        Blob body;
        //getting the dynamic content of pdf
        try {
            body = pdf.getContent();
        } catch (VisualforceException e) {
            body = Blob.valueOf('No Pdf');
        }

        system.debug('body:::' + body);
        
       
        //Code to save the generated pdf in Notes & Attachment
        if(paymentRec != null){       
            Attachment att = new Attachment();
            att.Body = body;
            att.ParentId = paymentRec.Id;
           // att.Name = System.Label.InvoiceAttName;
            att.Name = ConfigDataMapController.getCustomSettingValue('InvoiceAttName'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            
            try{
                Insert att;                   
            }catch(DmlException ex){
                system.debug('Exception::'+ex);
            }   
        }     
        
        //Invoking send pdf method to send email to the related contact with the pdf attached
        EmailSending obj = new EmailSending();
        obj.sendPdf(service, body);
             

    }
    
    /********************************************************************
    // Purpose              : SAMS Integration for all the course connection records which are created via embedded batch credential job.                          
    // Author               : Capgemini [Shreya]
    // Parameters           : Set<Id> incomingCourseEnrolments
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 
    
    global static void sendCourseConn(Set<Id> incomingCourseEnrolments) {
      
      system.debug('Ids::::'+incomingCourseEnrolments);
     
    //Start: 06-06-2018 creating  SAMS request log to Integration log object.
    Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
    Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c(); 
    //Ends: 06-06-2018 creating  SAMS request log to Integration log object.
      
    List<hed__Course_Enrollment__c> listCrsconn = [Select Id,
                                    Enrolment_Status__c,
                                    hed__Course_Offering__r.Name,
                                    hed__Contact__r.Id,
                                    hed__Contact__r.Student_ID__c,
                                    hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c
                                    FROM hed__Course_Enrollment__c
                                    WHERE Id IN: incomingCourseEnrolments];
    List<CourseConnectionWrapper> crsconns = new List<CourseConnectionWrapper>();
    if (listCrsconn.size() > 0) {
      for (hed__Course_Enrollment__c crsenr : listCrsconn) 
      {          
        CourseConnectionWrapper cw = New CourseConnectionWrapper();
        cw.courseConnectionId = crsenr.Id;
          
        String subStringWithoutS ='';
        if( ! String.isBlank(crsenr.hed__Contact__r.Student_ID__c))
        {
            if( crsenr.hed__Contact__r.Student_ID__c.startsWith('S'))
            {
                subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c.substringAfter('S');
            }
            else if(crsenr.hed__Contact__r.Student_ID__c.startsWith('s'))
            {
                subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c.substringAfter('s');
            }
            else
            {
                subStringWithoutS = crsenr.hed__Contact__r.Student_ID__c;
            }
        }
        else
        {
            subStringWithoutS = '';
        }
        cw.studentId = subStringWithoutS;
        cw.courseId = crsenr.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c;
        cw.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
        cw.enrolStatus = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusR');
        crsconns.add(cw);
        
      }
    }
    // HTTP Request Creation
    String jSONString = JSON.serialize(crsconns);
    String endpoint = ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasEndPoint');
    HttpRequest req = new HttpRequest();
    req.setEndpoint(endpoint);
    req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientId'));
    req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CourseConnectionIPaasClientSecretId'));
    req.setHeader('Content-Type', 'application/json');
    req.setMethod('POST');
    req.setbody(jSONString);
    Http http = new Http();
    HTTPResponse response;
    try
    {
        //Start: 06-06-2018 creating  SAMS request log to Integration log object.
        requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('SAMS_IPaaS',jSONString, 'Outbound Service','',false);
        //Ends: 06-06-2018 creating SAMS request log to Integration log object.
        
        response = http.send(req); 
        System.debug('Http Response+++++++'+response);        
        //Start: 06-06-2018 creating SAMS response log to Integration log object
        responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('SAMS_IPaaS',response.getBody(), 'Acknowledgement','',false);
        //Ends: 06-06-2018 creating SAMS response log to Integration log object
    }
    catch(Exception e)
    {
        System.debug('Exception++++++'+e);
    }    
     
    System.debug('Response Code ---------->' + response.getStatusCode() + '\nResponse Code ---------->' + response.getStatus());

    List<Course_Connection_Life_Cycle__c> cclifecycles = [SELECT Id,
                                          Name,
                                          Status__c,
                                          Course_Connection__c
                                          FROM Course_Connection_Life_Cycle__c
                                          WHERE Course_Connection__c in : listCrsconn AND Stage__c = : ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStage')];

    if (cclifecycles.size() > 0) {
       
      if (response.getStatusCode() == 200) {
          for (Course_Connection_Life_Cycle__c cclyfcycle : cclifecycles) {
              cclyfcycle.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated');
          }
      } else {
        for (Course_Connection_Life_Cycle__c cclyfcycle : cclifecycles) {
          cclyfcycle.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');

        }
      }
        try {
            
            update cclifecycles;
        } catch (exception e) {
        ApplicationErrorLogger.logError(e);
      }

    }

    //Response from IPaas
    System.debug('response body ---->' + response.getBody());

    JsonCourseWrapper jstr = (JsonCourseWrapper) JSON.deserializeStrict(response.getBody(), JsonCourseWrapper.class);

    Map<String, JsonCourseWrapper.IpaasResponseWrapper> ipaasResWrp = new Map<String, JsonCourseWrapper.IpaasResponseWrapper>();

    for (JsonCourseWrapper.IpaasResponseWrapper jsoncrswrp : jstr.payload) {
      ipaasResWrp.put(jsoncrswrp.courseConnectionId, jsoncrswrp);
    }
    List<Course_Connection_Life_Cycle__c> cclifecycless = [SELECT Id,
                                                                  Name,
                                                                  Status__c,
                                                                  Course_Connection__c,
                                                                  Course_Connection__r.Enrolment_Status__c,
                                                                  Course_Connection__r.Name
                                                                  FROM Course_Connection_Life_Cycle__c
                                                                  WHERE Course_Connection__c in :ipaasResWrp.keyset() AND Stage__c = : ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStage')];

    Set<id> ccId = new Set<id>();
    System.debug('Response from IPaas+++'+ipaasResWrp.keyset());
    List<hed__Course_Enrollment__c> crsconnections = new List<hed__Course_Enrollment__c>();

    if (cclifecycless.size() > 0) {

      for (Course_Connection_Life_Cycle__c cclyf : cclifecycless) {
          if (ipaasResWrp.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess')) {
              cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
              crsconnections.add(new hed__Course_Enrollment__c(id = cclyf.Course_Connection__c, Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE')));
          } else {
          cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
          crsconnections.add(new hed__Course_Enrollment__c(id = cclyf.Course_Connection__c, Enrolment_Status__c = ''));
          ApplicationErrorLogger.logError(cclyf, 'SAMS Integration', ipaasResWrp);
            if(!Test.isRunningTest()){
                ErrorLogger.sendErrorEmailAlert(cclyf.Course_Connection__c);
            }else{
                System.debug('Test is Running');
            }
        }
      }
      try 
      {
        update cclifecycless;
        update crsconnections;
         // Starts: 06-06-2018 Below code is inserting  log records on integration log object.
        if( requestLogObjectToInsert != null)
        {
            insert requestLogObjectToInsert;   
        }
        if( responseLogObjectToInsert != null )
        {
            insert responseLogObjectToInsert;
        }
        // Ends:06-06-2018 Below code is inserting  log records on integration log object.
      } 
      catch (exception e) 
      {
        ApplicationErrorLogger.logError(e);
      }

    }
    System.debug(ipaasResWrp.keySet());
     
  }
  
   /********************************************************************
    // Purpose              : Canvas Integration for all the course connection records which are created via embedded batch credential job.                          
    // Author               : Capgemini [Shreya]
    // Parameters           : Set<Id> incomingCourseEnrolments
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 
  
  //Sending Request from Salesforce to IPaas
    global static void sendCourseConnIds(Set<Id> incomingCourseEnrolments)
    {
       
        
       // User usr = [Select Id, name, ContactId from user WHERE id =: UserInfo.getUserId() LIMIT 1];
       List<hed__Course_Enrollment__c> courseConnList =  [Select hed__Contact__c from hed__Course_Enrollment__c where Id IN :incomingCourseEnrolments];
       Set<Id> contactIdSet = new Set<Id>();
       
       for(hed__Course_Enrollment__c course : courseConnList ){
           contactIdSet.add(course.hed__Contact__c);
       }
        
        
       /* if(Test.isRunningTest())
        {
            usr = TestDataFactoryUtil.usr4;
        }*/
        
        List<Contact> conList = [SELECT id,
                              Name,
                              Student_ID__c,
                              hed__UniversityEmail__c,
                              (SELECT ID, hed__Course_Offering__r.Name, hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c FROM hed__Student_Course_Enrollments__r WHERE Id IN: incomingCourseEnrolments)
                              FROM Contact WHERE id IN :contactIdSet 
                                     ];
        system.debug('conList ::::'+conList);
                                       
        Map<String,List<hed__Course_Enrollment__c>>  contactCourseConnMap = new Map<String,List<hed__Course_Enrollment__c>>();                          
        
        List<hed__Course_Enrollment__c> enrollList = new List<hed__Course_Enrollment__c>();
        
        if(!conList.isEmpty()){
            for(Contact c : conList){
                List<hed__Course_Enrollment__c> cousrConnList = new List<hed__Course_Enrollment__c>();
                cousrConnList.addAll(c.hed__Student_Course_Enrollments__r); 
                enrollList.addAll(c.hed__Student_Course_Enrollments__r);                
                contactCourseConnMap.put(c.Id,cousrConnList);
            } 
            
             system.debug('contactCourseConnMap::::'+contactCourseConnMap);
            
         }                               
        
        
        
        
        CourseConnCanvasRequestWrapper crsconnwrp;
        if(conList != null)
        {
            for(Contact con : conList){
                crsconnwrp = New CourseConnCanvasRequestWrapper();
                crsconnwrp.studentId = con.Student_ID__c;
                crsconnwrp.studentName = con.Name;
                crsconnwrp.studentEmail = con.hed__UniversityEmail__c;
                crsconnwrp.accountId = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasAccountId');
                crsconnwrp.type = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvastype');
                crsconnwrp.enrollment_state = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasenrolmentstate');
                crsconnwrp.notify = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasnotify');
                crsconnwrp.enrolmentDetails.addAll(createCourseEnrollmentList(contactCourseConnMap.get(con.Id)));
                
                system.debug('crsconnwrp:::'+crsconnwrp);   
                 
                sendCourseDetailsToCanvas(crsconnwrp,contactCourseConnMap.get(con.Id));
                 
                
        
            }   
            
          
          
        } 
            
     
        
     
    }
      
  
  public class CourseConnectionWrapper {
    Id courseConnectionId;
    String studentId;
    String courseId;
    String courseOfferingId;
    String enrolStatus;
  }
  
   /********************************************************************
    // Purpose              : Create Contact Course Connection List
    // Author               : Capgemini [Shreya]
    // Parameters           : List<hed__Course_Enrollment__c> courseEnrollList
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 
  
  public static List<CourseConnCanvasRequestWrapper.EnrolDetails> createCourseEnrollmentList(List<hed__Course_Enrollment__c> courseEnrollList){
    
    system.debug('courseEnrollList::::'+courseEnrollList);
    List<CourseConnCanvasRequestWrapper.EnrolDetails> enrollDetailList = new List<CourseConnCanvasRequestWrapper.EnrolDetails>();
    
    for(hed__Course_Enrollment__c crsenr : courseEnrollList)
    {
        //crsconnids.add(crsenr.id);
        CourseConnCanvasRequestWrapper.EnrolDetails enrdet = new CourseConnCanvasRequestWrapper.EnrolDetails();
        enrdet.courseConnectionId = crsenr.id;
        enrdet.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
        enrdet.sisCourseId = crsenr.hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
        enrollDetailList.add(enrdet);
        //crsconnwrp.enrolmentDetails.add(enrdet);
    }
    
    system.debug('enrollDetailList::::'+enrollDetailList);
    
    if(!enrollDetailList.isEmpty()){
        return enrollDetailList;
    }else{
        return null;
    }
    
  }
  
    /********************************************************************
    // Purpose              : Send the Course Details to Canvas
    // Author               : Capgemini [Shreya]
    // Parameters           : CourseConnCanvasRequestWrapper crsconnwrp,List<hed__Course_Enrollment__c> enrollList
    //  Returns             : void
    //JIRA Reference        : ECB-3900
    //********************************************************************/ 
  public static void sendCourseDetailsToCanvas(CourseConnCanvasRequestWrapper crsconnwrp,List<hed__Course_Enrollment__c> enrollList){
        
        Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c();
        // HTTP Request Creation
        String jSONString = JSON.serialize(crsconnwrp);
        
        system.debug('Request crsconnwrp:::'+jSONString);   
        system.debug('endpoint:::'+ConfigDataMapController.getCustomSettingValue('CanvasEndPointURL')); 
        
        String endpoint = ConfigDataMapController.getCustomSettingValue('CanvasEndPointURL');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CanvasClientId'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CanvasClientSecretId'));
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        req.setbody(jSONString);
        req.setTimeout(30000);
        Http http = new Http();
        HTTPResponse response;
        try
        {
            requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('Canvas_IPaaS',jSONString, 'Outbound Service','',false);
            response = http.send(req);
           
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Canvas_IPaaS',response.getBody(), 'Acknowledgement','',false);
        }
        catch (exception e) 
        {
             ApplicationErrorLogger.logError(e);
        }
        
        System.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
        set<id> crsconnids = new set<id>();
        for(hed__Course_Enrollment__c crsenr : enrollList)
        {
            crsconnids.add(crsenr.id);
        }
        
        //system.debug('crsconnids :::'+crsconnids );
        List<Course_Connection_Life_Cycle__c> cclyfcycles = [SELECT Id,
                                                                    Name,
                                                                    Status__c,
                                                                    Course_Connection__c
                                                                    FROM Course_Connection_Life_Cycle__c 
                                                                    WHERE Course_Connection__c in : crsconnids AND Stage__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStageCanvas')];
        
        if(cclyfcycles.size() > 0)
        {
            
            for(Course_Connection_Life_Cycle__c cclyfcyc : cclyfcycles)
            {
                if (response.getStatusCode() == 200){
                    cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated'); 
                }
                else{
                    
                    cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                }
            }
            try
            {
                if(Test.isRunningTest()){
                    
                    throw new DMLException();
                }
                update cclyfcycles;
            } 
            catch(Exception e)
            {
               ApplicationErrorLogger.logError(e); 
            }
        }
        System.debug('Response body------'+response.getBody());
        
        CourseConnCanvasResponseWrapper jstr = (CourseConnCanvasResponseWrapper) JSON.deserializeStrict(response.getBody(), CourseConnCanvasResponseWrapper.class);
        
        System.debug('Json String---------->'+jstr);
        
        Map<Id,CourseConnCanvasResponseWrapper.ResponseInfo> canvasResWrp = new Map<Id,CourseConnCanvasResponseWrapper.ResponseInfo>();
        
        CourseConnCanvasResponseWrapper.ResponseInfo crsconreswrp;
        
        Map<Id, CourseConnCanvasResponseWrapper.Response> canvasres = new  Map<Id, CourseConnCanvasResponseWrapper.Response>();
        
        for(CourseConnCanvasResponseWrapper.Response responsefromcanvas : jstr.payload.enrolmentResponse)
        {
            canvasres.put(responsefromcanvas.courseConnectionId, responsefromcanvas);
        }
        
        //List of Course Connections life cycles in course connections coming from Ipaas
        List<Course_Connection_Life_Cycle__c> cclifecycless = [SELECT Id,
                                                               Name,
                                                               Status__c,
                                                               Course_Connection__c, 
                                                               Course_Connection__r.Enrolment_Status__c,
                                                               Course_Connection__r.Name
                                                               FROM Course_Connection_Life_Cycle__c 
                                                               WHERE Course_Connection__c in :canvasres.keyset() AND Stage__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStageCanvas')];
        
        List<hed__Course_Enrollment__c> crsconnections = new List<hed__Course_Enrollment__c>();
        if(cclifecycless.size()>0)
        {
            
            for(Course_Connection_Life_Cycle__c cclyf : cclifecycless)
            {
                if(canvasres.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess'))
                {
                    cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
                    
                }
                else
                {
                    cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                    crsconnections.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Enrolment_Status__c=''));
                    
                    ApplicationErrorLogger.logError(cclyf, 'Canvas Integration',canvasres);
                    
                    if(!Test.isRunningTest()){
                         ErrorLogger.sendErrorEmailAlert(cclyf.Course_Connection__c);
                    }else{
                        System.debug('Test is Running');
                    }
                   
                }
            }
            
            try{
                
                update cclifecycless;
                update crsconnections;
                System.debug('Life Cycles after updated'+cclifecycless);
                
                if( requestLogObjectToInsert != null)
                {
                    insert requestLogObjectToInsert;        
                }
                if( responseLogObjectToInsert != null )
                {
                    insert responseLogObjectToInsert;
                }
                
            }
            catch (exception e) 
            {
                ApplicationErrorLogger.logError(e);
            }
            
        }                                        
        System.debug(canvasResWrp.keySet());

      
    

    }   
}