/*********************************************************************************
*** @ClassName         : EnrolmentInboundService
*** @Author            : Shubham Singh 
*** @Requirement       : Getting records from external system through json structure
                         and upsert records into salesforce object(Course Connection).
*** @Created date      : 13/03/2019
*** @Modified by       : Shubham Singh 
*** @modified date     : 07/05/2019
**********************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
@RestResource(urlMapping = '/HEDA/v1.1/Enrolment')
global with sharing class EnrolmentInboundService {
    //created these variable as we have restriction for number of parameter to be passed in methods and other criterias
    //Change the error line number if you add any line in this
    //To get account record from account service
    private static AccountService.AccountServiceWrapper wrapperAccountService;
    //To get Term object record from TermService
    private static TermService.TermServiceWrapper wrapperTermService;
    //To get Course offering object from CourseOfferingService
    private static CourseOfferingService.CourseOfferingServiceWrapper wrapperCourseOfferingService;
    //To get Program Enrolment object record from ProgramEnrolmentService
    private static ProgramEnrolmentService.ProgramEnrolmentServiceWrapper wrapperProgramEnrolmentService;
    //To get Course connection object record from CourseConnectionService
    private static CourseConnectionService.CourseConnectionServiceWrapper wrapperCourseConnectionService;
    //To create the collection of sets to be used in queries to fetch records EnrolmentInboundServiceHelper
    public static EnrolmentInboundServiceHelper.wrapperEnrolmentInboundServiceHelper helperWrapper;
    //To get HECS object record from HecsService
    private static Map < String, HECS__c > hecsNameIdMap;
    //Map to store values of contact with student id
    private static Map < String, ID > studentContactMap;
    //Map to store values of fundsource with its name
    private static Map < String, Fund_Source__c > fundSourceByName;
    
    @HttpPost
    global static Map < String, String > handlePostInboundEnrolmentCall() {
        //To check mandatory parameters contain values
        WebserviceParametersHandler enrolmentHandler = new WebserviceParametersHandler(List < InboundParameterImplementations.EnrolmentParameters > .class);
        try {
            //proccess and validate the parameters 
            enrolmentHandler.processParameters();
        } catch (WebserviceException exptn) {
            //Create Error Log.
            exptn.businesString = 'EnrolmentInboundService';
            RMErrLog__c errorLog = ErrorService.LogFromWebserviceErrorWithAttachment(exptn);
            return WebServiceHelper.returnErrorForWebServiceException(exptn, errorLog);

        }
        //****************************************Business Logic Implementation*********************************************//

        Map < String, IInboundParameter > allParametersByStudentID = (Map < String, IInboundParameter > ) enrolmentHandler.getInboundParametersByMandatoryStringField('studentId');

        //Assign values to the respective parameters
        List < InboundParameterImplementations.EnrolmentParameters > parameterList = (List < InboundParameterImplementations.EnrolmentParameters > ) enrolmentHandler.getParameters();
        //assign values to the respective SET 
        helperWrapper = EnrolmentInboundServiceHelper.createSetOfWrapper(parameterList);

        //Get fund source from fundsource service
        fundSourceByName = FundSourceService.getFundsourceMapByName(FundSourceService.getFundSourceByName(helperWrapper.fundSource));

        //Process business logic and give error 
        Map < String, String > returnError = processBusinessLogic(helperWrapper, allParametersByStudentID);
        if (returnError != null) {
            return returnError;
        }
        //Course connection to upsert
        List < hed__Course_Enrollment__c > courseConnectionToUpsert = new List < hed__Course_Enrollment__c > ();
        for (InboundParameterImplementations.EnrolmentParameters parameter : parameterList) {
            courseConnectionToUpsert.add(createUpdateCourseConnectionFromParameter(parameter));
        }
        system.debug('courseConnectionToUpsert' + courseConnectionToUpsert);
        Map < string, hed__Course_Enrollment__c > mapOfCourseConnection = new Map < string, hed__Course_Enrollment__c > ();
        //As list can contain multiple records with same unique key. So, we need to create a map that will contain values w.r.t its unique key
        mapOfCourseConnection = createCourseConnectionMap(courseConnectionToUpsert);
        //Try catch block to upsert list could be in a service class if needed... here for now.
        try {
            if (Schema.sObjectType.hed__Course_Enrollment__c.isCreateable() && Schema.sObjectType.hed__Course_Enrollment__c.isUpdateable()) {
                upsert mapOfCourseConnection.values() Course_Connection_Unique_Key__c;
                system.debug('courseConnectionToUpsert' + mapOfCourseConnection);

            } else {
                throw new WebserviceException('Current user has no Write/Update Access to Course Enrollment');

            }

        } catch (Exception e) {            
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            resp.message = 'Error Inserting / Updating Student enrolment';
            resp.statusCode = 500; 
            RMErrLog__c error = ErrorService.logError(e, '08');
            ErrorService.saveError(error);
            ErrorService.addPayloadToError(error, RestContext.request.requestBody);
            resp.code = '08';
            resp.value = e.getMessage();
            return WebServiceHelper.generateResponseFromWrapper(resp);
        }

        return WebServiceHelper.setReturnOKStatus();

    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * create map to insert/update course connection record
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    createCourseConnectionMap
     * @param     List < hed__Course_Enrollment__c > courseConnectionToUpsert
     * @return    Map<string,hed__Course_Enrollment__c>
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static Map < string, hed__Course_Enrollment__c > createCourseConnectionMap(List < hed__Course_Enrollment__c > courseConnectionToUpsert) {
        Map < string, hed__Course_Enrollment__c > mapOfCourseConnection = new Map < string, hed__Course_Enrollment__c > ();
        for (hed__Course_Enrollment__c hecCourseEnroll: courseConnectionToUpsert) {
            string uniquekey = String.valueOf(hecCourseEnroll.Academic_Career__c) + String.valueOf(hecCourseEnroll.Class_Number__c) + String.valueOf(hecCourseEnroll.Term__c) + String.valueOf(hecCourseEnroll.hed__Contact__c);
            if (String.isNotBlank(uniquekey)) {
                mapOfCourseConnection.put(uniquekey, hecCourseEnroll);
            }
        }
        return mapOfCourseConnection;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Assigning values to the respective variables of course connection
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    createUpdateCourseConnectionFromParameter
     * @param     InboundParameterImplementations.EnrolmentParameters parameter
     * @return    hed__Course_Enrollment__c
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static hed__Course_Enrollment__c createUpdateCourseConnectionFromParameter(InboundParameterImplementations.EnrolmentParameters parameter) {
        Id studentrecodTypeID = Schema.SObjectType.hed__Course_Enrollment__c.getrecordtypeinfosbydevelopername().get('Student').getRecordTypeId();
        hed__Course_Enrollment__c courseConnection = new hed__Course_Enrollment__c();
        string courseOffKey = parameter.classNumber + parameter.term;
        string key = parameter.academicCareer + parameter.classNumber + parameter.term + parameter.studentId + parameter.institution;
        string accKey = parameter.academicProgram + parameter.institution;
        string programEnrollKey = parameter.institution + parameter.studentId + parameter.academicProgram + parameter.academicCareer + parameter.studentCareerNumber;
        courseConnection.Course_Connection_Unique_Key__c = key;
        courseConnection.RecordTypeId = studentrecodTypeID;
        courseConnection = EnrolmentInboundServiceHelper.processConnectionRecords(courseConnection, parameter);
        courseConnection = getCourseFundSourceAndContact(courseConnection, parameter, key);
        courseConnection.hed__Account__c = !wrapperAccountService.institutionAccountNumberMap.isEmpty() && wrapperAccountService.institutionAccountNumberMap.get(accKey) != null ? wrapperAccountService.institutionAccountNumberMap.get(accKey) : null;
        //As there will be only one institution RMITU
        courseConnection.Institution__c = wrapperAccountService.institution[0].ID;
        courseConnection.hed__Course_Offering__c = !wrapperCourseOfferingService.courseOfferingClassNumberTermMap.isEmpty() && wrapperCourseOfferingService.courseOfferingClassNumberTermMap.get(courseOffKey) != null ? wrapperCourseOfferingService.courseOfferingClassNumberTermMap.get(courseOffKey) : null;
        courseConnection.hed__Program_Enrollment__c = !wrapperProgramEnrolmentService.programEnrolStringMap.isEmpty() && wrapperProgramEnrolmentService.programEnrolStringMap.get(programEnrollKey) != null ? wrapperProgramEnrolmentService.programEnrolStringMap.get(programEnrollKey) : null;
        courseConnection.HECS_Exempt_Status_Code__c = !hecsNameIdMap.isEmpty() && hecsNameIdMap.get(parameter.hecsExemptionStatusCode) != null ? hecsNameIdMap.get(parameter.hecsExemptionStatusCode).ID : null;

        return courseConnection;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Get values of course connection, contact and fundsource from their respective maps
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    getFundSourceAndContact
     * @param     hed__Course_Enrollment__c courseConnection, InboundParameterImplementations.EnrolmentParameters parameter,String key
     * @return    hed__Course_Enrollment__c
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static hed__Course_Enrollment__c getCourseFundSourceAndContact(hed__Course_Enrollment__c courseConnection, InboundParameterImplementations.EnrolmentParameters parameter, String key) {
        courseConnection.hed__Contact__c = !studentContactMap.isEmpty() ? studentContactMap.get(parameter.studentId) : null;
        Fund_Source__c fundSourceVar = !fundSourceByName.isEmpty() && fundSourceByName.get(parameter.fundingSourceCode) != null ? fundSourceByName.get(parameter.fundingSourceCode) : null;
        if (fundSourceVar != null) {
            courseConnection.Fund_Source_Description__c = fundSourceVar.Description__c;

            courseConnection.Fund_Source__c = fundSourceVar.Id;
        }
        if (!wrapperCourseConnectionService.mapOfCourseConnectionToUpdate.isEmpty()) {
            courseConnection.Id = wrapperCourseConnectionService.mapOfCourseConnectionToUpdate.get(key) != null ? wrapperCourseConnectionService.mapOfCourseConnectionToUpdate.get(key) : null;
        }
        system.debug('courseConnection====> ' + courseConnection);
        return courseConnection;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * To process business logic and return error if required values are found respectively
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    processBusinessLogic
     * @param     EnrolmentInboundServiceHelper.wrapperEnrolmentInboundServiceHelper helperWrapper, Map < String, IInboundParameter > allParametersByStudentID
     * @return    Map < String, String >
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static Map < String, String > processBusinessLogic(EnrolmentInboundServiceHelper.wrapperEnrolmentInboundServiceHelper helperWrapper, Map < String, IInboundParameter > allParametersByStudentID) {
        //Error log object
        RMErrLog__c error = new RMErrLog__c();
        //we can use the studentId in place of allParametersByStudentID.keySet()
        ContactService.validationAllObjectsExistWrapper wrapperContactService = ContactService.getContactsByStudentIdEnsureAllExists(allParametersByStudentID.keySet());
        system.debug('wrapperContactService== ' + wrapperContactService);
        // Return error if any student ID is not found
        // If one of the Students ID do not exist, return an error.
        if (wrapperContactService.allExist == false) {

            String errorString = 'The following Student Ids were not found in Salesforce: ' + String.join(new List < String > (wrapperContactService.notFound), ',');
            String message = 'Inexistent student Id ';
            error.Error_Message__c = errorString;
            error.Error_Line_Number__c = 192;
            error.Class_Name__c = 'EnrolmentInboundService';
            error.Method_Name__c = 'processBusinessLogic';
            error.Business_Function_Name__c = 'EnrolmentInboundService';
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            resp.value = String.join(new List < String > (wrapperContactService.notFound), ',');
            resp.code = '03';
            return processErrorLogic(error, message, resp);
        }
        studentContactMap = new Map < string, ID > ();

        for (contact studentContact: wrapperContactService.contacts) {
            studentContactMap.put(studentContact.Student_Id__c, studentContact.ID);
        }

        wrapperAccountService = AccountService.getAccountMap(helperWrapper.academicProgram, helperWrapper.institution);
        system.debug('wrapperAccountService== ' + wrapperAccountService);
        //Return error if institution is not found
        if (wrapperAccountService.allExist == false) {

            String errorString = 'The following institution were not found in Salesforce: ' + String.join(new List < String > (wrapperAccountService.notFound), ',');
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            String message = 'Inexistent Institution Id ';
            error.Error_Message__c = errorString;
            error.Method_Name__c = 'processBusinessLogic';
            error.Error_Line_Number__c = 216;
            error.Class_Name__c = 'EnrolmentInboundService';
            error.Business_Function_Name__c = 'EnrolmentInboundService';
            resp.value = String.join(new List < String > (wrapperAccountService.notFound), ',');
            resp.code = '04';
            return processErrorLogic(error, message, resp);

        }

        Map < String, String > returnIfError = new Map < String, String > ();
        returnIfError = courseOfferingAndProgramCheck(helperWrapper,error);
        if (returnIfError != null) {
            return returnIfError;
        }
        wrapperCourseConnectionService = CourseConnectionService.getCourseConnectionByUniqueKey(helperWrapper.courseConnectionUniqueKey);
        hecsNameIdMap = HecsService.getHecsbyNameMap(helperWrapper.hecsName);
        return null;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * To process error logic if key values are not found
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    courseOfferingAndProgramCheck
     * @param     
     * @return    Map < String, String >
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */

    //Should be thought over. Exception should be thrown in case there are issues...
    public static Map < String, String > courseOfferingAndProgramCheck(EnrolmentInboundServiceHelper.wrapperEnrolmentInboundServiceHelper helperWrapper,RMErrLog__c error) {
        ContactService.validationAllObjectsExistWrapper wrapperContactService = new ContactService.validationAllObjectsExistWrapper();
        wrapperTermService = TermService.getTermByTermCode(helperWrapper.termSet);
        system.debug('wrapperTermService== ' + wrapperTermService);
        //Return error if Term is not found
        if (wrapperTermService.allExist == false) {

            String errorString = 'The following Term Ids were not found in Salesforce: ' + String.join(new List < String > (wrapperTermService.notFound), ',');
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            String message = 'Inexistent Term Id ';
            error.Method_Name__c = 'courseOfferingAndProgramCheck';
            error.Error_Message__c = errorString;
            error.Error_Line_Number__c = 258;
            error.Class_Name__c = 'EnrolmentInboundService';
            error.Business_Function_Name__c = 'EnrolmentInboundService';
            resp.value = String.join(new List < String > (wrapperTermService.notFound), ',');
            resp.code = '05';
            return processErrorLogic(error, message, resp);

        }
        wrapperCourseOfferingService = CourseOfferingService.getCourseOffering(helperWrapper.classNumber, helperWrapper.termSet);
        system.debug('wrapperCourseOfferingService== ' + wrapperCourseOfferingService);
        //Return error if Course Offering is not found
        if (wrapperCourseOfferingService.allExist == false) {

            String errorString = 'The following classNumber Ids were not found in Salesforce: ' + String.join(new List < String > (wrapperCourseOfferingService.notFound), ',');
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            String message = 'Inexistent Class Id ';
            error.Method_Name__c = 'courseOfferingAndProgramCheck';
            error.Error_Message__c = errorString;
            error.Error_Line_Number__c = 277;
            error.Class_Name__c = 'EnrolmentInboundService';
            error.Business_Function_Name__c = 'EnrolmentInboundService';
            resp.value = String.join(new List < String > (wrapperCourseOfferingService.notFound), ',');
            resp.code = '06';
            return processErrorLogic(error, message, resp);

        }
        ProgramEnrolmentService.ProgramEnrolmentServiceWrapper wrapper = new ProgramEnrolmentService.ProgramEnrolmentServiceWrapper();
        wrapper.acdemicCareer =  helperWrapper.acdemicCareer;
        wrapper.studentCareerNumber =  helperWrapper.studentCareerNumber;
        wrapper.academicProgram =  helperWrapper.academicProgram;
        wrapper.studentId =  helperWrapper.studentId;
        wrapperProgramEnrolmentService = ProgramEnrolmentService.getProgramEnrolment(wrapper);
        system.debug('wrapperProgramEnrolmentService== ' + wrapperProgramEnrolmentService);
        //Return error if Program Enrolment is not found
        if (wrapperProgramEnrolmentService.allExist == false) {

            String errorString = 'The following Carrer Ids were not found in Salesforce: ' + String.join(new List < String > (wrapperProgramEnrolmentService.notFound), ',');
            WebServiceHelper.StrategicIntegrationResponse resp = new WebServiceHelper.StrategicIntegrationResponse();
            String message = 'Inexistent Academic Carreer Id ';
            error.Method_Name__c = 'courseOfferingAndProgramCheck';
            error.Error_Message__c = errorString;
            error.Error_Line_Number__c = 300;
            error.Class_Name__c = 'EnrolmentInboundService';
            error.Business_Function_Name__c = 'EnrolmentInboundService';
            resp.value = String.join(new List < String > (wrapperProgramEnrolmentService.notFound), ',');
            resp.code = '07';
            return processErrorLogic(error, message, resp);

        }
        return null;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * to create error record and then return error
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    processErrorLogic
     * @param     String errorString, String message, WebServiceHelper.StrategicIntegrationResponse resp
     * @return    Map < String, String >
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static Map < String, String > processErrorLogic(RMErrLog__c error, String message, WebServiceHelper.StrategicIntegrationResponse resp) {
		RMErrLog__c errorNew = new RMErrLog__c();
        errorNew = ErrorService.logWebserviceError('05', error.Error_Message__c);
        errorNew.Error_Line_Number__c = error.Error_Line_Number__c;
        errorNew.Class_Name__c = error.Class_Name__c;
        errorNew.Business_Function_Name__c = error.Business_Function_Name__c;
        errorNew.Method_Name__c = error.Method_Name__c;
        ErrorService.saveError(errorNew);
        ErrorService.addPayloadToError(errorNew, RestContext.request.requestBody);
        //resp = new WebServiceHelper.StrategicIntegrationResponse();
        resp.message = message;
        //resp.code = '03';
        resp.errorId = errorNew.Id;
        RestContext.response.statusCode = 500;
        return WebServiceHelper.generateResponseFromWrapper(resp);
    }
}