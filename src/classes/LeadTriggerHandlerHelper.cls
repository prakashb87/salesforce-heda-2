public with sharing class LeadTriggerHandlerHelper {
    private static final Map < ID, Schema.RecordTypeInfo > LEAD_RT = Lead.sObjectType.getDescribe().getRecordTypeInfosById();
    private static final String RMIT_ONLINE_RECORDTYPE = 'Prospective Student - RMIT Online';
    public set<Id> convertedLeadIds = new set<ID>();
    
    public void updateMobileTextFields(List<Lead> allLeads){
        for(Lead leadRec: allLeads){
            leadRec.Mobile_Text1__c = leadRec.Mobile_formatted__c;
            leadRec.Mobile_Text2__c = leadRec.Mobile_Formatted2__c;
        }
    }
    
    public void admitTermFields(List<Lead> prospectiveStudentRTLeads){
        for(Lead leadRec: prospectiveStudentRTLeads){
            leadRec.Admit_Term__c = leadRec.Admit_Term_Calc__c;
            leadRec.Admit_Term_desc__c = leadRec.Admit_Term_Desc_Calc__c;
        }
    }
    
    public void updateDuplicateCountOnInsert(List<Lead> leadRecords){
            
    	List<Lead> idlist;
    	set<ID> leadIDs = new set<ID>();
    	list<Lead> updateLeads= new List<Lead>();
    	for(Lead leadRec:leadRecords){
    		leadIDs.add(leadRec.Id);
    		leadIDs.addAll(matchRecordIDs(leadRec)); 
        }
    	for(Lead relatedLead: [SELECT Id, 
    	 						Number_Of_Potential_Duplicate__c 
    	 						FROM Lead WHERE Id In: leadIDs]){
    	 	relatedLead.Number_Of_Potential_Duplicate__c= duplicateCount(relatedLead);
    	 	updateLeads.add(relatedLead);
        }
    	 if(!updateLeads.isEmpty()){
    	 	Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
    	 	database.update(updateLeads,dml);
    }
    
    }
    private set<ID> matchRecordIDs(Lead record){
   		set<ID> result= new set<ID>();
        List<Lead> idlist;
   		idlist = new List<Lead>{record};
                Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(idlist);
                for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                    for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                        for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {  
                            for(Datacloud.MatchRecord duplicateMatchRecord : matchResult.getMatchRecords()) {
	    	 			result.add(duplicateMatchRecord.getRecord().Id);
                            }
                        }
                    }
                }
    	return result;
            }
    private integer duplicateCount(Lead record){
        List<Lead> idlist;
    	Integer dupCount=0;
    	idlist = new List<Lead>{record};
                    Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(idlist);
                for (Datacloud.FindDuplicatesResult findDupeResult : results) {
                    for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                        for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {
    	 			 	 dupCount = matchResult.getSize();
    	 			 }
                            }
                        }
    	 return dupCount;				
                    }
   public void updateDuplicateCountOnUpdate(list<Lead> leadRecords){
   	List<Lead> idlist;
    set<ID> leadIDs = new set<ID>();
    list<Lead> updateLeads= new List<Lead>();
   	for(Lead leadRec : leadRecords){  
       Lead oldLead = (Lead)Trigger.oldMap.get(leadRec.Id);
      
       if(leadRec.Mobile_Text1__c != oldLead.Mobile_Text1__c || leadRec.Mobile_Text2__c != oldLead.Mobile_Text2__c || leadRec.Email != oldLead.Email ){
       		leadIDs.add(leadRec.Id);
      		leadIDs.addAll(matchRecordIDs(leadRec)); 
       		leadIDs.addAll(matchRecordIDs(oldLead));
                }
       if(leadRec.isConverted){
       		leadIDs.addAll(matchRecordIDs(oldLead));
            }
        }
   	 for(Lead relatedLead: [SELECT Id, 
    	 						Number_Of_Potential_Duplicate__c 
    	 						FROM Lead WHERE Id In: leadIDs]){
    	 	relatedLead.Number_Of_Potential_Duplicate__c= duplicateCount(relatedLead);
    	 	updateLeads.add(relatedLead);
    	 }
    	 if(!updateLeads.isEmpty()){
    	 	Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
    	 	database.update(updateLeads,dml);
        }
    } 
    
}