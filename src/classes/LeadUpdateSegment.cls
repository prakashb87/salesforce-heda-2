public with sharing class LeadUpdateSegment {
    public LeadUpdateSegment(){
        System.debug('This will be deleted on the netxt release');
        
    }
 /*   private static final String PROSPECTIVE_STUDENT_RECORDTYPE = 'Prospective Student';
    private static final map < ID, Schema.RecordTypeInfo > RECORDTYPEINFOBYID = Lead.sObjectType.getDescribe().getRecordTypeInfosById();
    /**
* -----------------------------------------------------------------------------------------------+
* @description
* Method to update the segment based on level of study values which can be found in below label
* Custom Label: 
* ------------------------------------------------------------------------------------------------
* @author	 RM Squad
* @param     leadRecord
* @Reference RM-2436
* -----------------------------------------------------------------------------------------------+

    
    public static void updateSegment(List<Lead> leadRecord){
        
        for (Lead newLead: leadRecord) {
            if (RECORDTYPEINFOBYID.get(newLead.recordTypeID).getName().equalsIgnoreCase(PROSPECTIVE_STUDENT_RECORDTYPE) && String.isNotEmpty(newLead.Level_of_study__c)) {
                if (newLead.Level_of_study__c == Label.Postgraduate_by_coursework || newLead.Level_of_study__c == Label.Postgraduate_by_research) {
                    newLead.Segment__c = Label.Postgrad;
                } else {
                    newLead.Segment__c = validatePicklist(newLead.I_am_currently__c);
                }
            }
        }
        
    }
    
    /*
* -----------------------------------------------------------------------------------------------+
* @description
* helper method for updateSegment to validate the picklist values
* ------------------------------------------------------------------------------------------------
* @author	 RM Squad
* @method    validatePicklist
* @param     String value of picklist
* @return    String-> Custom Label
* @Reference RM-2436
* -----------------------------------------------------------------------------------------------+
*
    private static String validatePicklist(String fieldValue){
        List<String> listofValues = new List<String>{'Studying Year 10',
            'Studying Year 11',
            'Studying Year 12',
            'Studying Year 9',
            'Parent/Guardian',
            'Not studying',
            'Parent/Guardian',
            'Careers/employment advisor',
            'Studying at another uni/TAFE',
            'Studying at RMIT',
            'Parent of Year 10',
            'Parent of Year 11',
            'Parent of Year 12',
            'Parent of Year 9'};
                string result = Label.Non_School_Leaver;
        if(String.isNotEmpty(fieldValue) && !listofValues.contains(fieldValue)){
            result = '';
        }
        return result;
    }*/
    
}