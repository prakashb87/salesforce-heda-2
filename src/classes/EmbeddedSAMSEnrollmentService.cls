/*****************************************************************
Name: EmbeddedSAMSEnrollmentController
Author: Capgemini [Shreya]
Purpose: Handle SAMS Integration  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-5545,ECB-5549
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           02/04/2019         Initial Version
********************************************************************/

public without sharing class EmbeddedSAMSEnrollmentService{
    
    private ISAMSEnrollmentRequest samsRequest;
    @testVisible private List<Course_Connection_Life_Cycle__c> ccLifeCycleList;
    @testVisible private List<hed__Course_Enrollment__c> updatedCourseConnList ;
    @testVisible private Integer failedCount;
    
    public EmbeddedSAMSEnrollmentService(ISAMSEnrollmentRequest samsRequest) {
        this.samsRequest = samsRequest;
        this.failedCount = 0;
        updatedCourseConnList = new List<hed__Course_Enrollment__c>();
        ccLifeCycleList = new List<Course_Connection_Life_Cycle__c>();
    }
    
    public Integer getFailedCount() {
        return this.failedCount;
    }
    
    public void sendCourseConn(Set<Id> incomingCourseEnrolments) {
      
        system.debug('Ids::::'+incomingCourseEnrolments);
        if (incomingCourseEnrolments == null) {
            throw new QueryException('Missing course enrollment Ids. Value cannot be null.');
        }
        
        //querying the course enrollment details
        List<hed__Course_Enrollment__c> listCrsconn = getCourseEnrollmentDetails(incomingCourseEnrolments);   
        
        //creating the request wrapper for SAMS
        List<CourseConnectionWrapper> samsReqWrap = createCourseConnectionWrapper(listCrsconn,incomingCourseEnrolments);   
        
        //Querying the related life cycle records
        queryCCLifeCycleRec(incomingCourseEnrolments);
        
        try{
            samsRequest.sendEnrollDetailToSAMS(samsReqWrap);
            //In case of response 200 update life cycle status to Initated
            updateLifeCycleStatusToInitiated();
            
        }catch(SAMSRequestCalloutException ex){
            //In case of response other than 200 update life cycle status to Error
            updateLifeCycleStatusToError();
            ApplicationErrorLogger.logError(ex);
            //counting the integration failure count
            failedCount = failedCount+1;
        }
        
    }
    
    @testVisible
    private List<hed__Course_Enrollment__c> getCourseEnrollmentDetails(Set<Id> courseEnrollmentIds) {
        if (courseEnrollmentIds == null) {
            throw new QueryException('Missing course enrollment ids');
        }
        String courseEnrollQuery = 'SELECT id,Enrolment_Status__c,hed__Course_Offering__r.Name,hed__Contact__r.Id,';
        courseEnrollQuery+='hed__Contact__r.Student_ID__c, hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c ';
        courseEnrollQuery+='FROM hed__Course_Enrollment__c WHERE Id IN: courseEnrollmentIds';
        
        return Database.query(courseEnrollQuery);
    }
    
    //Creating the request wrapper
    private List<CourseConnectionWrapper> createCourseConnectionWrapper(List<hed__Course_Enrollment__c> listCrsconn, Set<Id> incomingCourseEnrolments){
        List<CourseConnectionWrapper> crsconns = new List<CourseConnectionWrapper>();
        
        
        if (listCrsconn.size() > 0) {
          for (hed__Course_Enrollment__c crsenr : listCrsconn) 
          {          
            CourseConnectionWrapper cw = New CourseConnectionWrapper();
            cw.courseConnectionId = crsenr.Id;
              
            String subStringWithoutS ='';
            string studentId = crsenr.hed__Contact__r.Student_ID__c;
            if(!String.isEmpty(studentId) && studentId.startsWithIgnoreCase('S'))
            {
                subStringWithoutS = studentId.substring(1,studentId.length());
            }
            else
            {
                subStringWithoutS = studentId;
            }        
            cw.studentId = subStringWithoutS;
            cw.courseId = crsenr.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c;
            cw.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
            cw.enrolStatus = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusR');
            crsconns.add(cw);
            
          }
        }
        return crsconns;
    }
    
     //Method to query related life cycle records
     @testVisible
    private void queryCCLifeCycleRec(Set<Id> incomingCourseEnrolments){
        
        if (incomingCourseEnrolments == null) {
            throw new QueryException('Missing course enrollment Ids.');
        }
        String lifeCycStage = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStage');
        
        String lifecycQuery = 'SELECT Id,Name,Status__c,Course_Connection__c FROM Course_Connection_Life_Cycle__c ';
        lifecycQuery+='WHERE Course_Connection__c in : incomingCourseEnrolments AND Stage__c =: lifeCycStage';
        system.debug('lifecycQuery:::'+lifecycQuery);
        ccLifeCycleList = Database.query(lifecycQuery);        
        
    
    }
    
    //Method to update lifecycle record to initiated,if Ipass returns 200 response code
    @testVisible
    private void updateLifeCycleStatusToInitiated(){
        for(Course_Connection_Life_Cycle__c cclyfcyc : ccLifeCycleList){
                
            cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated'); 
               
        }
        
        Map<Id, JsonCourseWrapper.IpaasResponseWrapper> samsEnrollResByCourseConnIdMap = samsRequest.getSAMSEnrollResByCourseConnId();
        //Based on Canvas response updating life cycle record to Success or Error
        updateLifeCycleStatToSuccessorError(samsEnrollResByCourseConnIdMap);
         
    }
    
    //Method to update lifecycle record to error,if Ipass returns any response code other than 200
    private void updateLifeCycleStatusToError(){
        for(Course_Connection_Life_Cycle__c cclyfcyc : ccLifeCycleList){
                
            cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError'); 
               
        }
        
        updateCouseConnAndLifeCycRec();
    }
    
     //Method to update lifecycle records to success or error,based on response from canvas
    private void updateLifeCycleStatToSuccessorError(Map<Id, JsonCourseWrapper.IpaasResponseWrapper> samsEnrollResByCourseConnIdMap){
        
        for(Course_Connection_Life_Cycle__c cclyf : ccLifeCycleList){
                
            if(samsEnrollResByCourseConnIdMap.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess')){
                cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
                //ECB-5545: If enrollment to Canvas is successful,setting the flag Canvas_Integration_Processing_Complete__c to true : Shreya
                updatedCourseConnList.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Enrolment_Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusE'),SAMS_Integration_Processing_Complete__c=true));
            }else{
                cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                //ECB-5545: If enrollment to Canvas is successful,setting the flag Canvas_Integration_Processing_Complete__c to false : Shreya
                updatedCourseConnList.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Enrolment_Status__c='',SAMS_Integration_Processing_Complete__c=false));
                
                ApplicationErrorLogger.logError('SAMS Integration', String.ValueOf(cclyf.Course_Connection__c), 
                    'Lifecycle error in EmbeddedSAMSEnrollmentService class, method updateLifeCycleStatToSuccessorError');
                failedCount = failedCount+1;
            }
               
        }
        
        updateCouseConnAndLifeCycRec();
    }
    
     //Method to update database
    private void updateCouseConnAndLifeCycRec(){
        if(!ccLifeCycleList.isEmpty()){             
            Database.update(ccLifeCycleList,false); 
        }     
        system.debug('updatedCourseConnList:::'+updatedCourseConnList);
        if(!updatedCourseConnList.isEmpty()){           
            Database.update(updatedCourseConnList,false);
        }    
                                                    
        
    }

}