/*********************************************************************************
*** @TriggerHelperHandlerClass      : ProgramEnrollmentHelperHandler
*** @Author		    			 : BJIRA Squad
*** @LastModifiedDate               : 5 Aug 2019
**********************************************************************************/
public with sharing class ProgramEnrollmentHelperHandler {
    private static set<string> programStatus = new Set<String> {'AD','AC'};
        public static Map < String, Id > segAndRtId = new Map < String, Id > {
            'International' => Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('International Applicant Opportunities').getRecordTypeId(),
                'School Leaver' => Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('School Leaver Applicant Opportunities').getRecordTypeId(),
                'Non-School Leaver' => Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Non-School Leaver Applicant Opportunities').getRecordTypeId()
                };
                    public static Id inquiryRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Inquiry Opportunities').getRecordTypeId();
    
    public class ProgramEnrollmentHelperHandlerWrapper {
        
        public List < Opportunity > opportunityList = new List < Opportunity > ();
        public Map < Id, hed__Program_Enrollment__c > conIdAndProgEnrollment = new Map < Id, hed__Program_Enrollment__c > ();
        public Map < Id, hed__Program_Enrollment__c > conIdAndProgEnrAffiliation = new Map < Id, hed__Program_Enrollment__c > ();
        public Map < Id, List < Opportunity >> contactIdAndIAOpportunity = new Map < Id, List < Opportunity >> ();
        public Map < Id, List < Opportunity >> contactIdAndInquiryOpportunity = new Map < Id, List < Opportunity >> ();
    }
    public static ProgramEnrollmentHelperHandler.ProgramEnrollmentHelperHandlerWrapper wrapper = new ProgramEnrollmentHelperHandler.ProgramEnrollmentHelperHandlerWrapper();
    
    public void updateExistingContact(List<hed__Program_Enrollment__c> programEnrollments) {
        
        if(programEnrollments!=null){
            for (hed__Program_Enrollment__c progEnroll: programEnrollments) {
                hed__Program_Enrollment__c oldPELead = (hed__Program_Enrollment__c)Trigger.oldMap.get(progEnroll.Id);
                system.debug('Program Enrolment Insert Rec:'+ json.serializePretty(progEnroll));
                system.debug('Program Enrolment Old Rec:'+ json.serializePretty(oldPELead));
                if(progEnroll.Enrolment_Date__c!=null && oldPELead.Enrolment_Date__c == null){
                    wrapper.conIdAndProgEnrollment.put(progEnroll.hed__Contact__c, progEnroll);
                }
            }
        }
        processProgramEnrolment(wrapper.conIdAndProgEnrollment);
        
        if (wrapper.contactIdAndIAOpportunity.size() > 0) {
            //to update the affiliation rec and to create applicant opportunity.
            contactWithApplicationOpportunityOnly(wrapper.contactIdAndIAOpportunity, wrapper.conIdAndProgEnrollment);
        } else if (wrapper.contactIdAndInquiryOpportunity.size() > 0) {
            //to update the affiliation rec and to create Inquiry opportunity.
            contactWithInquiryOpportunityOnly(wrapper.contactIdAndInquiryOpportunity, wrapper.conIdAndProgEnrollment);
        }
        
    }
    
    private static void processProgramEnrolment( Map < Id, hed__Program_Enrollment__c >  contactProgram){
        
        for (Opportunity opp: [SELECT Admit_Term__c,
                               Assign_To_Contact__c,
                               Assign_To_Contact__r.OwnerId,
                               Assign_To_Contact__r.AccountId,
                               Assign_To_Contact__r.Name,
                               Affiliation__c,
                               RecordType.DeveloperName,
                               StageName,
                               Segment__c,
                               CloseDate,
                               Name,
                               OwnerId,
                               Inquiry_Date__c,
                               Level_of_Study__c
                               FROM Opportunity
                               Where Assign_To_Contact__c IN: contactProgram.keySet()
                               AND(RecordTypeId IN: segAndRtId.values() OR RecordTypeId =: inquiryRecordTypeId) Order by CreatedDate Asc]) {
                                   
                                   if (wrapper.contactIdAndIAOpportunity.containsKey(opp.Assign_To_Contact__c)) {
                                       wrapper.contactIdAndIAOpportunity.get(opp.Assign_To_Contact__c).add(opp);
                                   } else {
                                       wrapper.contactIdAndIAOpportunity.put(opp.Assign_To_Contact__c, new List < Opportunity > {
                                           opp
                                               });
                                   }
                                   if (!segAndRtId.values().contains(opp.RecordType.DeveloperName)) {
                                       if (wrapper.contactIdAndInquiryOpportunity.containsKey(opp.Assign_To_Contact__c)) {
                                           wrapper.contactIdAndInquiryOpportunity.get(opp.Assign_To_Contact__c).add(opp);
                                       } else {
                                           wrapper.contactIdAndInquiryOpportunity.put(opp.Assign_To_Contact__c, new List < Opportunity > {
                                               opp
                                                   });
                                       }
                                       if (wrapper.contactIdAndIAOpportunity.containsKey(opp.Assign_To_Contact__c)) {
                                           wrapper.contactIdAndIAOpportunity.remove(opp.Assign_To_Contact__c);
                                       }
                                       
                                   }
                               }		
    }
    
    private static void contactWithApplicationOpportunityOnly( Map < Id, List < Opportunity >> appOpportunityList, Map < Id, hed__Program_Enrollment__c > conIdAndProgEnrollmentRec) {
        
        for (Id cntId: conIdAndProgEnrollmentRec.keyset()) {
            for (Opportunity oppRec: appOpportunityList.get(cntId)) {
                if (ProgramEnrollmentService.checkProgramStatus(conIdAndProgEnrollmentRec.get(cntId), oppRec)) {
                    wrapper.opportunityList.add(ProgramEnrollmentService.createApplicantforInquiryOpp(oppRec, conIdAndProgEnrollmentRec.get(cntId), segAndRtId.get(oppRec.Segment__c)));
                    break;
                }
            }
        }
        ProgramEnrollmentService.methodtoUpdate(wrapper.opportunityList);
    }
    private static void validCnd1Opportunity(List < Opportunity > oppList) {
        List < Opportunity > oppRec = new List < Opportunity > ();
        String affStatus;
        for (Opportunity opp: oppList) {
            if (ProgramEnrollmentService.checkInquiryOppStatus(wrapper.conIdAndProgEnrollment.get(opp.Assign_To_Contact__c), opp.Admit_Term__c) == 1 && opp.StageName == 'Confirmed Interest') {
                opp.RecordTypeId = segAndRtId.get(opp.Segment__c);
                opp.StageName = 'Enrolled';
                opp.Admit_Term__c = wrapper.conIdAndProgEnrollment.get(opp.Assign_To_Contact__c).Admit_Term__c;
                wrapper.opportunityList.add(opp);
                oppRec.add(opp);
                break;
            }
        }
        if (oppRec.size() == 0) {
            validCnd2Opportunity(oppList);
        }
    }
    
    private static void validCnd2Opportunity(List < Opportunity > oppList) {
        List < Opportunity > oppRec = new List < Opportunity > ();
        String affStatus;
        for (Opportunity opp: oppList) {
            if (ProgramEnrollmentService.checkInquiryOppStatus(wrapper.conIdAndProgEnrollment.get(opp.Assign_To_Contact__c), opp.Admit_Term__c) == 2 && opp.StageName == 'Confirmed Interest') {
                
                oppRec.add(opp);
                break;
            }
        }
        if (oppRec.size() == 0) {
            validCnd3Opportunity(oppList);
        }
    }
    private static void validCnd3Opportunity(List < Opportunity > oppList) {
        List < Opportunity > oppRec = new List < Opportunity > ();
        String affStatus;
        for (Opportunity opp: oppList) {
            if (ProgramEnrollmentService.checkInquiryOppStatus(wrapper.conIdAndProgEnrollment.get(opp.Assign_To_Contact__c), opp.Admit_Term__c) == 3 && opp.StageName == 'Confirmed Interest') {
                opp.StageName = 'Closed Lost';
                wrapper.opportunityList.add(opp);
                oppRec.add(opp);
            }
        }
        if (oppRec.size() == 0) {
            validCnd4Opportunity(oppList);
        } else {
            wrapper.opportunityList.add(ProgramEnrollmentService.createApplicantforInquiryOpp(oppRec[0], wrapper.conIdAndProgEnrollment.get(oppRec[0].Assign_To_Contact__c),segAndRtId.get(oppRec[0].Segment__c) ));
        }
    }
    private static void validCnd4Opportunity(List < Opportunity > oppList) {
        List < Opportunity > oppRec = new List < Opportunity > ();
        String affStatus;
        for (Opportunity opp: oppList) {
            if (ProgramEnrollmentService.checkInquiryOppStatus(wrapper.conIdAndProgEnrollment.get(opp.Assign_To_Contact__c), opp.Admit_Term__c) == 4 && opp.StageName == 'Confirmed Interest') {
                
                oppRec.add(opp);
            }
        }
        Opportunity appOpptoCreate = new Opportunity();
        appOpptoCreate = oppList[0]; 
        if (oppRec.size() == 0) {
            wrapper.opportunityList.add(ProgramEnrollmentService.createApplicantforInquiryOpp(appOpptoCreate, wrapper.conIdAndProgEnrollment.get(appOpptoCreate.Assign_To_Contact__c),segAndRtId.get( appOpptoCreate.Segment__c)));
        }
    }
    private static void contactWithInquiryOpportunityOnly(Map < Id, List < Opportunity >> appOpportunityList, Map < Id, hed__Program_Enrollment__c > conIdAndProgEnrollmentRec) {
        List < Opportunity > oppRecList = new List < Opportunity > ();
        for (Id cntId: conIdAndProgEnrollmentRec.keyset()) {
            validCnd1Opportunity(appOpportunityList.get(cntId));
        }
        ProgramEnrollmentService.methodtoUpdate(wrapper.opportunityList);
    }
    
}