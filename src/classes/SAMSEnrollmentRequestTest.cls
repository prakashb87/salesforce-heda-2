/*****************************************************************
Name: SAMSEnrollmentRequestTest 
Author: Capgemini [Shreya]
Purpose: Test Class of SAMSEnrollmentRequest 
*****************************************************************/

@isTest
public class SAMSEnrollmentRequestTest {
     
   
    static testmethod void testBlankRequestJSON(){
        EmbedSamsDataFactoryUtil.testEmbeds();
        Test.setMock(HttpCalloutMock.class, new samsCalloutMockFailure());
        try {
            Test.startTest();
                List<CourseConnectionWrapper> samsEnrollmentRequestList = new List<CourseConnectionWrapper>();
                SAMSEnrollmentRequest req = new SAMSEnrollmentRequest();
                req.sendEnrollDetailToSAMS(samsEnrollmentRequestList);
                req.getSAMSEnrollResByCourseConnId();
                System.assert(false);
             Test.stopTest();    
        } catch (Exception e) {
            System.assert(true);
        }
    }
    
     static testmethod void testSAMSSuccess(){
         EmbedSamsDataFactoryUtil.testEmbeds();
        Test.setMock(HttpCalloutMock.class, new samsCalloutmock());
        try {

            List<hed__Course_Enrollment__c> cenrollRecList = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.Name,hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c];
            List<CourseConnectionWrapper> samsEnrolDetailList = new List<CourseConnectionWrapper>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            for (hed__Course_Enrollment__c crsenr : cenrollRecList ) {          
                CourseConnectionWrapper cw = New CourseConnectionWrapper();
                cw.courseConnectionId = crsenr.Id;
                  
                String subStringWithoutS ='';
                string studentId = crsenr.hed__Contact__r.Student_ID__c;
                if(!String.isEmpty(studentId) && studentId.startsWithIgnoreCase('S'))
                {
                    subStringWithoutS = studentId.substring(1,studentId.length());
                }
                else
                {
                    subStringWithoutS = studentId;
                }        
                cw.studentId = subStringWithoutS;
                cw.courseId = crsenr.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c;
                cw.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
                cw.enrolStatus = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusR');
                samsEnrolDetailList.add(cw);
           }
       
            
            Test.startTest();
                SAMSEnrollmentRequest req = new SAMSEnrollmentRequest();
                req.sendEnrollDetailToSAMS(samsEnrolDetailList);
                Map<Id, JsonCourseWrapper.IpaasResponseWrapper> responseMap = req.getSAMSEnrollResByCourseConnId();
                System.assert(true);
                System.assert(responseMap.size()>0,true);
            Test.stopTest();     
        } catch (CanvasRequestCalloutException e) {
            // If callout was not successful, an exception would be thrown
            System.assert(false);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testSamsBlankResponse1(){
        EmbedSamsDataFactoryUtil.testEmbeds();
        try {
            Test.startTest();
                SAMSEnrollmentRequest req = new SAMSEnrollmentRequest();
                req.getSAMSEnrollResByCourseConnId();
            Test.stopTest();     
        } catch (ArgumentException e) {
            // If canvas response is null
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }
    
    static testmethod void testSAMSError(){
        EmbedSamsDataFactoryUtil.testEmbeds();
        Test.setMock(HttpCalloutMock.class, new samsCalloutMockError());
        try {

            List<hed__Course_Enrollment__c> cenrollRecList = [SELECT Id,hed__Course_Offering__c,hed__Course_Offering__r.Name,hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c,hed__Contact__r.Student_ID__c,hed__Contact__r.Name,hed__Contact__r.Email FROM hed__Course_Enrollment__c LIMIT 1];
            List<CourseConnectionWrapper> samsEnrolDetailList = new List<CourseConnectionWrapper>();
            CourseConnCanvasRequestWrapper.EnrolDetails enroll = new CourseConnCanvasRequestWrapper.EnrolDetails();
            for (hed__Course_Enrollment__c crsenr : cenrollRecList ) {          
                CourseConnectionWrapper cw = New CourseConnectionWrapper();
                cw.courseConnectionId = crsenr.Id;              
                cw.studentId = '12345';
                cw.courseId = crsenr.hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c;
                cw.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
                cw.enrolStatus = ConfigDataMapController.getCustomSettingValue('CourseConnectionEnrolmentStatusR');
                samsEnrolDetailList.add(cw);
           }
       
            
            Test.startTest();
                SAMSEnrollmentRequest req = new SAMSEnrollmentRequest();
                req.sendEnrollDetailToSAMS(samsEnrolDetailList);
            Test.stopTest();     
        } catch (CanvasRequestCalloutException e) {
            // If callout was not successful, an exception would be thrown
            System.assert(false);
        } catch (Exception e) {
            System.assert(true);
        }
    }
    
    private class samsCalloutMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            List<hed__Course_Enrollment__c> cenroll = [Select Id,hed__Contact__r.Student_ID__c from hed__Course_Enrollment__c];
            String jsonBody ='{"payload": [{"courseConnectionId": "'+cenroll[0].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[1].Id+'","result": "Error","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[2].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[3].Id+'","result": "Success","errorCode": "","errorText": ""},';
            jsonBody+='{"courseConnectionId": "'+cenroll[4].Id+'","result": "Error","errorCode": "","errorText": ""}';
            jsonBody+=']}';
           
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
     private class samsCalloutMockFailure implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {              
            HttpResponse response = new HttpResponse();           
            response.setHeader('Content-Type', 'application/json');
            response.setStatus('Bad Request');
            response.setStatusCode(404);
            return response;
        }
        
    }
    
    private class samsCalloutMockError implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {              
            HttpResponse response = new HttpResponse();
            String jsonBody = '{"id": "ID-devqa-e503a961-6dc9-4d8f-b4ee-c2d723cafd4d","result": "ERROR","code": "500", "payload": "Error occured while getting User details from Canvas. (User not present)"}';
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatus('Success');
            response.setStatusCode(200);
            return response;
        }
        
    }
}