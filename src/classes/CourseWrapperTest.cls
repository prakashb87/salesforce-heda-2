/*****************************************************************************************************
 *** @Class             : CourseWrapperTest
 *** @Author            : Praneet Vig
 *** @Requirement       : To test the courseWrapper class
 *** @Created date      : 21/02/2018
 *** @JIRA ID           : ECB-30
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to cover the code coverage for CourseWrapper Class
 *****************************************************************************************************/ 

@IsTest
public class CourseWrapperTest {
    
    static testMethod void testParse() {
        String json = '{'+
        '        \"ObjectName\": \"Course\",'+
        '        \"course_id\": \"ci102\"'+
        '    },'+
        '    {'+
        '        \"ObjectName\": \"Course Offering\",'+
        '        \"External_Unique_ID__c\": \"7890\"'+
        '        \"Course External_Unique_ID__c\": \"98128\"'+
        '    }'+
        '    {'+
        '        \"ObjectName\": \"Course Offering Fee\",'+
        '        \"ID\": \"ci102\"'+
        '        \"Course External_Unique_ID__c\": \"csic1012\"'+
        '    }'+
        '    {'+
        '        \"ObjectName\": \"course-session\",'+
        '        \"course_id\": \"ci102\"'+
        '        \"course_sessionid\": \"csi1012\"'+
        '    }'+
        ']';
        CourseWrapper obj = CourseWrapper.parse(json);
        System.assert(obj != null);
    }
}