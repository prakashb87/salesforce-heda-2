@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class HEDAProgram2CSBatch implements Database.Batchable<sObject> {
	private String query;

	Id recordTypeId21CC = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
	Id recordTypeIdRMITOnline = Schema.SObjectType.hed__course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();


	global HEDAProgram2CSBatch() {
		Id accountID21CC = HEDA2CSUtil.get21CCAccountId();
		Id accountIDRMITOnline = HEDA2CSUtil.getRMITOnlineAccountId();

		query = 'SELECT Id, hed__Account__c, hed__Is_Primary__c, ' +
		        'hed__Account__r.name, hed__Account__r.ParentId ' +
		        'FROM hed__Program_Plan__c ' +
		        'WHERE hed__Is_Primary__c = true ' +
		        'AND (hed__Account__r.ParentId = \'' + accountID21CC + '\'  OR hed__Account__r.ParentId = \'' + accountIDRMITOnline + '\' )';
	}

	global Database.QueryLocator start(Database.BatchableContext context) {
		system.debug('HEDAProgram2CSBatch.start entered');
		system.debug('HEDAProgram2CSBatch.start query ' + query);
		system.debug(Database.getQueryLocator(query));
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext context, List<sObject> scope) {
		//System.debug('HEDAProgram2CSBatch scope' + scope);
		system.debug('HEDAProgram2CSBatch.execute entered');

		HEDA2CSUtil.transformProgram2PriceItems(scope);
	}

	global void finish(Database.BatchableContext context) {
		system.debug('HEDAProgram2CSBatch.finish entered');

	}

}