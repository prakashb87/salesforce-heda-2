/*********************************************************************************
*** @TestClassName     : PROGRAM_REST_TEST
*** @Author            : Anupam Singhal
*** @Requirement       : To Test the PROGRAM_REST web service apex class.
*** @Created date      : 30/08/2018
*** @Modified date     : 24/09/2018
**********************************************************************************/
@isTest
public class PROGRAM_REST_TEST {
    static testMethod void myUnitTest() {
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
        
        List<Campus__c> lstCampus = new List<Campus__c>();
        
        Campus__c campusRecord = new Campus__c();
        campusRecord.Name = 'Test Campus';
        campusRecord.Effective_Date__c = system.today();
        campusRecord.Descrshort__c = 'Test Description';
        insert campusRecord;
        
        lstCampus.add(campusRecord);
        
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Program';
        
        //creating test data
        String JsonMsg = '{"institution":"RMITU","academicProgram":"FS004","effectiveDate":"2001-01-01","effectiveStatus":"Active","description":"Cert Found Stud (App Sci/Eng)","shortDescription":"821100","academicCareer":"PREP","academicCalendarCode":"PREP","academicGroup":"APSCI","academicOrganisation":"821","defaultAcademicPlan":"FS000","defaultCampus":"Test Campus","firstTermValid":"0005","academicLevelRule":"1/96","gradingScheme":"PRE","gradingBasis":"FDS","transcriptLevel":"20","honorAndAwardDate":"2001-02-02"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = PROGRAM_REST.upsertProgram();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void testDescriptionNull() {
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
                
        List<Campus__c> lstCampus = new List<Campus__c>();
        
        Campus__c campusRecord = new Campus__c();
        campusRecord.Name = 'Test Campus';
        campusRecord.Effective_Date__c = system.today();
        campusRecord.Descrshort__c = 'Test Description';
        insert campusRecord;
        
        lstCampus.add(campusRecord);
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Program';
        
        //creating test data
        String JsonMsg = '{"institution":"RMITU","academicProgram":"FS004","effectiveDate":"2001-01-01","effectiveStatus":"Active","description":"","shortDescription":"821100","academicCareer":"PREP","academicCalendarCode":"PREP","academicGroup":"APSCI","academicOrganisation":"821","defaultAcademicPlan":"FS000","defaultCampus":"Test Campus","firstTermValid":"0005","academicLevelRule":"1/96","gradingScheme":"PRE","gradingBasis":"FDS","transcriptLevel":"20","honorAndAwardDate":"2001-02-02"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = PROGRAM_REST.upsertProgram();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void effectiveDateGreater() {
        
       Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
        
        List<Campus__c> lstCampus = new List<Campus__c>();
        
        Campus__c campusRecord = new Campus__c();
        campusRecord.Name = 'Test Campus';
        campusRecord.Effective_Date__c = system.today();
        campusRecord.Descrshort__c = 'Test Description';
        insert campusRecord;
        
        Campus__c campusRec = new Campus__c();
        campusRec.Name = 'Test Campus1';
        insert campusRec;
        
        lstCampus.add(campusRecord);
        
        Future_Change__c fuchangeRec = new Future_Change__c();
        fuchangeRec.API_Field_Name__c = 'TEST API TEST';
        fuchangeRec.Value__c = campusRecord.Id;
        insert fuchangeRec;
        
        Account program = new Account(Name='FS001', Academic_Institution__c=institution.Id, Academic_Organisation__c = organisation.Id, Academic_Program__c = 'FS001', Default_Campus__c = campusRecord.Id);
        insert program;
        Date todaysDate = Date.today();
        Date futureDate = todaysDate.addDays(1);
        

        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"institution":"RMITU","academicProgram":"FS001","effectiveDate":"'+futureDate+'","effectiveStatus":"Active","description":"Cert Found Stud (App Sci/Eng)","shortDescription":"821100","academicCareer":"PREP","academicCalendarCode":"PREP","academicGroup":"APSCI","academicOrganisation":"821","defaultAcademicPlan":"FS000","defaultCampus":"Test Campus1","firstTermValid":"0005","academicLevelRule":"1/96","gradingScheme":"PRE","gradingBasis":"FDS","transcriptLevel":"20","honorAndAwardDate":"2001-02-02"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = PROGRAM_REST.upsertProgram();
        system.debug('------Test class check---------'+check);
        List < String > str = check.values();
        system.debug('------Test class str---------'+str);
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Program';
        
        //creating test data
        String JsonMsg = '{"institution":"RMITU","academicProgram":"","effectiveDate":"2001-01-01","effectiveStatus":"Active","description":"Cert Found Stud (App Sci/Eng)","shortDescription":"821100","academicCareer":"PREP","academicCalendarCode":"PREP","academicGroup":"APSCI","academicOrganisation":"821","defaultAcademicPlan":"FS000","defaultCampus":"","firstTermValid":"0005","academicLevelRule":"1/96","gradingScheme":"PRE","gradingBasis":"FDS","transcriptLevel":"20","honorAndAwardDate":"2001-02-02"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = PROGRAM_REST.upsertProgram();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = PROGRAM_REST.upsertProgram();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
               
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        Account organisation = new Account();
        organisation.Name = '821';
        insert organisation;
        
        Account program = new Account(Name='FS001', Academic_Institution__c=institution.Id, Academic_Organisation__c = organisation.Id, Academic_Program__c = 'FS001');
        insert program;
                
        List<Campus__c> lstCampus = new List<Campus__c>();
        
        Campus__c campusRecord = new Campus__c();
        campusRecord.Name = 'Test Campus';
        campusRecord.Effective_Date__c = system.today();
        campusRecord.Descrshort__c = 'Test Description';
        insert campusRecord;
        
        lstCampus.add(campusRecord);
                    
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"institution":"RMITU","academicProgram":"FS001","effectiveDate":"2002-01-02","effectiveStatus":"Active","description":"Cert Found Stud (App Sci/Eng)","shortDescription":"821100","academicCareer":"PREP","academicCalendarCode":"PREP","academicGroup":"APSCI","academicOrganisation":"821","defaultAcademicPlan":"FS000","defaultCampus":"Test Campus","firstTermValid":"0005","academicLevelRule":"1/96","gradingScheme":"PRE","gradingBasis":"FDS","transcriptLevel":"20","honorAndAwardDate":"2001-02-02"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = PROGRAM_REST.upsertProgram();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}