/********************************************************************
// Purpose              : Test Class for the getAddressDetails() method Positive Scenario
// Author               : Capgemini 
// Parameters           : null
// Returns              : void 
//JIRA Reference        : ECB-4360 : Retrieve and Update Address Details
//********************************************************************/     

@isTest
public class AdditionalInformationControllerTest {

    @isTest    
    public static void testGetAddressDetails(){
        TestDataFactoryUtil.setupAddress();
        Test.startTest();
        system.runAs(TestDataFactoryUtil.testUser){
            Contact con = AdditionalInformationController.getContactDetails();
            
            con.hed__Gender__c = 'Male';
            
          //  AdditionalInformationController.upsertContactDetails(con);
            
            hed__Address__c homeAdd = AdditionalInformationController.getAddressDetails();
            hed__Address__c billingAdd = AdditionalInformationController.getBillingAddressDetails();
            
            hed__Address__c updateHomeAdd = [select id,hed__MailingStreet__c, hed__Parent_Contact__c from hed__Address__c where hed__Address_Type__c = 'Home' and hed__Parent_Contact__c =: con.id limit 1];
            updateHomeAdd.hed__MailingState__c = 'Updated Home';
            hed__Address__c homeUpdatedAddress = AdditionalInformationController.upsertHomeAddressDetails(updateHomeAdd);
            
            hed__Address__c updateBillingAdd = [select id,hed__MailingStreet__c, hed__Parent_Contact__c from hed__Address__c where hed__Address_Type__c = 'Billing' and hed__Parent_Contact__c =: con.id limit 1];
            updateBillingAdd.hed__MailingState__c = 'Updated Billing';
          //  hed__Address__c billingUpdatedAddress = AdditionalInformationController.upsertBillingAddressDetails(updateBillingAdd);
            

            
          //  System.assertEquals(updateHomeAdd.hed__MailingState__c, 'Updated Home');
          //  System.assertEquals(updateBillingAdd.hed__MailingState__c, 'Updated Billing');
            
        }
        Test.stopTest();
    }
    
    @isTest    
    public static void getJobAreaTest(){
        AdditionalInformationController.getJobArea();
    }
    /*
    @isTest
    public static void getCitizenshipTest(){
        AdditionalInformationController.getCitizenship();
        TestDataFactoryUtil.deltaTestSuccess();
        List<String> courseNames = new List<String>();
        courseNames.add('test Name');
        AdditionalInformationController.getCitizenshipStatus();
    }
    */
    @isTest    
    public static void testNegativeGetAddressDetails(){
        
        TestDataFactoryUtil.setupAddress();
        List<hed__Address__c> addresses = [select id from hed__Address__c];
        
        Test.startTest();
        Database.Delete(addresses);
        system.runAs(TestDataFactoryUtil.testUser){
            hed__Address__c homeAdd = AdditionalInformationController.getAddressDetails();
            hed__Address__c billingAdd = AdditionalInformationController.getBillingAddressDetails();
        }
        Test.stopTest();
    }
   /*
    @isTest
    public static void getresultTest(){
        
        Test.startTest();
        TestDataFactoryUtil.testCreateData();
        
        csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
        details.CourseName = TestDataFactoryUtil.course.Name;
        details.CourseType = 'Course';
        details.ProductReference = 'Test definition';
        details.Fees = '100.0';
        details.CourseOfferingId = TestDataFactoryUtil.courseOffering.Id;
        details.SessionStartDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__Start_Date__c);
        details.SessionEndDate = string.valueOf(TestDataFactoryUtil.courseOffering.hed__End_Date__c);
        
        csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
        request.Course = details;
        request.IsBatchProcessed = false;
        
        csapi_BasketRequestWrapper.AddProductResponse response = csapi_RestBasketExtension_AddProduct.doPost(request);
        system.debug('response >> ' + response);
        
        system.assertEquals(response.Status, 'Pass');
        
        //get basket info
        csapi_BasketRequestWrapper.BasketInfoRequest request2 = new csapi_BasketRequestWrapper.BasketInfoRequest();
        csapi_BasketRequestWrapper.BasketInfoResponse response2 = AdditionalInformationController.getresult(); 
        
        //assert its created
        system.assertEquals(response2.Status, 'Pass');
        
        Test.stopTest();       
    }
  */
    
    @isTest
    public static void getEmailTest(){
        
        Test.startTest();
        String Useremail=UserInfo.getUserEmail();
        String email = AdditionalInformationController.getEmail();
        system.assertEquals(email, Useremail);
        Test.stopTest();
    }

    @isTest
    public static void confirmEnrollTest(){

        Test.startTest();
        TestDataFactoryUtil.test();
        
        TestDataFactoryUtil.testCreateData();
        
        cscfga__Product_Basket__c prodBasket = [Select Id,Basket_Number__c from cscfga__Product_Basket__c where Name = 'New Basket'];
        
        csapi_BasketRequestWrapper.SyncBasketResponse response = AdditionalInformationController.confirmEnroll('R1234', prodBasket.Basket_Number__c);
        system.assertEquals(response.Status, 'Pass');
        
        Test.stopTest();
    }
}