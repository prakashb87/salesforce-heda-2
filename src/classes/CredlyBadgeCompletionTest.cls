/*****************************************************************************************************
*** @Class             : CredlyBadgeCompletionTest
*** @Author            : Rishabh Anand/ Avinash Machineni
*** @Requirement       : Test Class for  CredlyBadgeCompletion
*** @Created date      : 16/06/2018
*** @Updated date      : 02/08/2018 - Rishabh
*** @JIRA ID           : ECB-2446
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to test BadgeCompletion
*****************************************************************************************************/
@isTest
public class CredlyBadgeCompletionTest {
    
    public static void schedulerTest() 
    {
        String cronExp = '0 15 17 ? * MON-FRI';
        
        CredlyCatalogueScheduler p = new CredlyCatalogueScheduler();
        
        String jobId = System.schedule('Credly Data sync',  cronExp, p);
        CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(cronExp, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
    }
    
    @isTest
    public static void testPositiveResponse()
    {
        schedulerTest();
        
        TestDataFactoryUtilRefOne.testCreateDataforCredlySuccess();
        
        
        //Test JSON Structure
        String jsonBody = '{"email":"abc@test.com","result":"OK","badgeResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","badgeId":"'+TestDataFactoryUtil.crs10.Credly_Badge_Id__c+'","issueDate":"2018-05-05"}]}';
        
        Test.setMock(HttpCalloutMock.class, new credlyCalloutMockPositive());
        /* 
        Test.startTest();
        CredlyBadgeCompletionBatch obj = new CredlyBadgeCompletionBatch();
        DataBase.executeBatch(obj, 5);
        Test.stopTest(); 
        
        Course_Connection_Life_Cycle__c lifeCycle = [SELECT  Id, Stage__c, Status__c FROM Course_Connection_Life_Cycle__c ];
        system.assertEquals('Badge Awarded',lifeCycle.Stage__c);
        
		List<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
        system.assertEquals(1, dbResults.size());
        system.assertEquals(jsonBody, dbResults[0].Request_Response__c);
        */
        
    }
    
    @isTest
    public static void negativeResponseTest(){
		schedulerTest();
        
        TestDataFactoryUtilRefOne.testCreateDataforCredlyError();
        
        
        //Test JSON Structure
        String jsonBody ='{"email":"def@test.com","result":"EMAIL_NOT_FOUND","message":"The email address was not found.","badgeResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","badgeId":"'+TestDataFactoryUtil.crs10.Credly_Badge_Id__c+'","issueDate":"","message":"The email address was not found."}]}';
        
        Test.setMock(HttpCalloutMock.class, new credlyCalloutMockNegative());
        /*
        Test.startTest();
        CredlyBadgeCompletionBatch obj = new CredlyBadgeCompletionBatch();
        DataBase.executeBatch(obj);
        Test.stopTest(); 
        
       	Course_Connection_Life_Cycle__c lifeCycle = [SELECT  Id, Stage__c, Status__c FROM Course_Connection_Life_Cycle__c ];
        system.assertEquals('Not Started',lifeCycle.Status__c);
        
        List<Integration_Logs__c> integrationLogList = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Outbound Service'];
        system.assertEquals(1, integrationLogList.size());
        */
    }
    
    
    
    public class credlyCalloutMockPositive implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
         
            String jsonBody ='{"email":"abc@test.com","result":"OK","badgeResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","badgeId":"'+TestDataFactoryUtil.crs10.Credly_Badge_Id__c+'","issueDate":"2018-05-05"}]}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    
    public class credlyCalloutMockNegative implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            
            String jsonBody ='{"email":"def@test.com","result":"EMAIL_NOT_FOUND","message":"The email address was not found.","badgeResponse":[{"courseConnectionId":"'+TestDataFactoryUtil.cern2.id+'","badgeId":"'+TestDataFactoryUtil.crs10.Credly_Badge_Id__c+'","issueDate":"","message":"The email address was not found."}]}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }
        
    }
}