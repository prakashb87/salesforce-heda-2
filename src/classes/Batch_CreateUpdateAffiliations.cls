/*********************************************************************************
*** @Class			    : Batch_CreateUpdateAffiliations
*** @Author		        : Mehreen Mansoor
*** @Requirement     	: Batch for Creating Alumni Affiliations for existing students
*** @Created date    	: 01/03/2019
**********************************************************************************/
/*****************************************************************************************
*** @About Class
*** This is a class for Batch_CreateUpdateAffiliations batch job to create Alumni affiliations
*** This batch job will run once to create Alumni Affiliations
*****************************************************************************************/
public class Batch_CreateUpdateAffiliations implements Database.Batchable<sObject>, Database.Stateful {
    
    static final String QUERYSTRING = 	'SELECT Id '
            + 'FROM Contact '
            + 'WHERE Student_ID__c != Null';
    
    
   public Database.QueryLocator start(Database.BatchableContext context){ 
        
        
        system.debug('QUERYSTRING'+QUERYSTRING);
        return Database.getQueryLocator(QUERYSTRING);
        
        
        
    }    
    public void execute(Database.BatchableContext context,List <Contact> scope )
    {
        
        Batch_CreateUpdateAffiliations_Helper cleanUpHelper = new Batch_CreateUpdateAffiliations_Helper(scope);
        cleanUpHelper.affiliations();
        
        
    }
    public void finish(Database.BatchableContext context){ 
        
        System.debug('Batch job finished');
    } 
    
}