/************************************************************************************************
 *** @Class  			: UpdateEmailOnContactBatch
 *** @Author		    : Shubham Singh
 *** @Requirement     	: Batch to updated the Email value on Contact
 *** @Created date    	: 18/12/2017
 ************************************************************************************************/
/*************************************************************************************************
 *** @About Class
 *** This class is a Batch class that is used to update the previous records of contact. Depending
 *** on the Preferred Email value it will update the email values on the standard Email field.
 *************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class UpdateEmailOnContactBatch implements Database.Batchable < sObject > , Database.Stateful {
	// Method to pass the records of ProgramEnrollment to the execute Method.
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT ID,hed__Preferred_Email__c,hed__UniversityEmail__c,Home_Email__c,hed__WorkEmail__c,hed__AlternateEmail__c,Email FROM Contact'
        );
    }
	//Method to process the related values of ProgramEnrollment On contact
    global void execute(Database.BatchableContext bc, List < contact > scope) {
        // process each batch of records and assign the preffered email value in the standard email field
        // if it is null assign null to it.
        Map < id, Contact > mapContacts = new Map < id, Contact > ();
        for(Contact c : scope){
            if(c.hed__Preferred_Email__c != null){
                if(c.hed__Preferred_Email__c == 'University')
                {
                     c.Email = c.hed__UniversityEmail__c;
                }
                else if(c.hed__Preferred_Email__c == 'Home')
                {
                     c.Email = c.Home_Email__c; 
                }
                else if(c.hed__Preferred_Email__c == 'Work')
                {
                     c.Email = c.hed__WorkEmail__c;
                }
                else if(c.hed__Preferred_Email__c == 'Alternate')
                {
                     c.Email = c.hed__AlternateEmail__c;
                }
                mapContacts.put(c.Id,c);
            }
        }
        //Update if Contacts and associated values are added
        if (mapContacts.size() > 0) {
            Database.update(mapContacts.values(), false);
        }
    }
	//Method of Impemented class
    global void finish(Database.BatchableContext bc) {
         system.debug('UpdateEmailOnContactBatch/finish');
    }
}