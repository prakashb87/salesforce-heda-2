/*******************************************
Purpose: Test class ResearcherMemberSharingUtils
History:
Created by Ankit Bhagat on 21/09/2018
Modified By Subhajit on 27/10/2018 for more coverage
*******************************************/
@isTest
public class TestResearcherMemberSharingUtils {
    
    /************************************************************************************
// Purpose      :  Creating Test data setup for RSTP for this test class
// Developer    :  Subhajit
// Created Date :  10/24/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    
    /************************************************************************************
// Purpose      :  Testing unit cases for Project funding Expense Utility
// Developer    :  Subhajit
// Created Date :  10/26/2018                 
//***********************************************************************************/
    @isTest
    public static void testResearcherMemberSharingUtilsMethods(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];  
       
        Contact con =[SELECT name, hed__UniversityEmail__c FROM Contact LIMIT 1];
        con.hed__UniversityEmail__c='abc@rmit.edu.au';
        update con;
        
        system.runAs(usr)
        {
            List<ResearcherPortalProject__c> portalProjectList = new List<ResearcherPortalProject__c>();
            
            ResearcherPortalProject__c portalProject = new ResearcherPortalProject__c();
            portalProject.Status__c='Idea';
            portalProject.Access__c='Members';
            portalProject.Approach_to_impact__c='test1';
            portalProject.Discipline_area__c='';
            portalProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            portalProject.End_Date__c=date.newInstance(2018, 12, 14);
            portalProject.Impact_Category__c='Social';
            portalProject.Title__c='projectTest1';
            portalProject.RecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
            portalProjectList.add(portalProject);
            insert portalProjectList; 
            String projectId = portalProjectList[0].Id;
            system.assert(portalProjectList!=null);
        
            List<Researcher_Member_Sharing__c> memberResearchList1 = new List<Researcher_Member_Sharing__c>();
           
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Position__c = 'Chief Investigator';
            memberResearch.Contact__c = usr.ContactId;
            memberResearch.Researcher_Portal_Project__c=projectId;
            memberResearch.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId(); 
            memberResearchList1.add(memberResearch);
            insert memberResearchList1;
            String researcherMemberID = memberResearchList1[0].Id;
            system.assert(memberResearchList1!=null);
   

            Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            Id ethicsRecordTypeId  = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
            Id contractRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
            Id publicationRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
            
            Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId, ethicsRecordTypeId, contractRecordTypeId, publicationRecordTypeId};

            List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'testSchoolVarName');
            conDetailList.add(conDetail);        
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics';
            ethicTest1.Application_Title__c ='Test Ethics';
            ethicTest1.Approved_Date__c = system.today();
            ethicTest1.Expiry_Date__c=system.today() +2;  
            insert ethicTest1;     
            system.assert(ethicTest1!=null);
            
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Position__c = 'Student';
            ethicsMemberSharing.Contact__c = usr.ContactId;
            ethicsMemberSharing.Ethics__c = ethicTest1.Id; 
            ethicsMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId(); 
            Insert ethicsMemberSharing;
            system.assert(ethicsMemberSharing!=null);
          
            Ethics_Researcher_Portal_Project__c ethicsResearchPortal = new Ethics_Researcher_Portal_Project__c();
            ethicsResearchPortal.Ethics__c=ethicTest1.Id;
            ethicsResearchPortal.Researcher_Portal_Project__c=projectId;
            ethicsResearchPortal.Name='test7';           
            insert ethicsResearchPortal;
            
            System.debug('@@@@ethicsResearchPortal =>'+ JSON.serializePretty(ethicsResearchPortal));
            
            Test.StartTest();
            
            ResearcherMemberSharingUtils.addResearcherMemberSharing(portalProject.Id,usr.ContactId,'HDR');
            //ResearcherMemberSharingUtils.createResearchEthicsMemberSharing(usr.ContactId,usr.id,ethicTest1.Id,'CI');
            ResearcherMemberSharingUtils.createResearcherMemberSharing(conDetailList,portalProject.Id);
            ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, usr.id);
            
            Test.StopTest();
        /*    // Delete Research member sharing use Case1  
            List<ProjectDetailsWrapper.ContactDetail> conDetailList1 = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail1 = new ProjectDetailsWrapper.ContactDetail(memberResearch);
            conDetailList1.add(conDetail1);
            system.debug('@@@@@JSON conDetailList1 ==>'+JSON.serializePretty(conDetailList1));   
           
            ResearcherMemberSharingUtils.deleteResearcherMemberSharing(conDetailList1,portalProject.Id);   
            
            //UseCase 1 member to private
            ResearcherMemberSharingUtils.operatingProjectAccess(portalProjectList);
            //UseCase 2 private to member
            ResearcherPortalProject__c proj= [SELECT Id, Access__c FROM ResearcherPortalProject__c WHERE Id =:projectId];
            proj.Access__c='Private';  
            update proj;
      
            /*List<ResearcherPortalProject__c> newPortalProjectList = new List<ResearcherPortalProject__c>();
            newPortalProjectList.add(proj);
            ResearcherMemberSharingUtils.operatingProjectAccess(newPortalProjectList);*/
            /*
            ResearcherMemberSharingUtils.createResearcherProjectMemberSharing(portalProject.Id,usr.id,'Chief Investigator',true);
            
            //Test.StopTest();*/
        }   
    }
    
    @isTest
    public static void testResearcherMemberSharingUtilsMethods2(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];  
       
        Contact con =[SELECT name, hed__UniversityEmail__c FROM Contact LIMIT 1];
        con.hed__UniversityEmail__c='abc@rmit.edu.au';
        update con;
        
        system.runAs(usr)
        {
            List<ResearcherPortalProject__c> portalProjectList = new List<ResearcherPortalProject__c>();
            
            ResearcherPortalProject__c portalProject = new ResearcherPortalProject__c();
            portalProject.Status__c='Idea';
            portalProject.Access__c='Members';
            portalProject.Approach_to_impact__c='test1';
            portalProject.Discipline_area__c='';
            portalProject.Start_Date__c= date.newInstance(2018, 10, 08) ;
            portalProject.End_Date__c=date.newInstance(2018, 12, 14);
            portalProject.Impact_Category__c='Social';
            portalProject.Title__c='projectTest1';
            portalProject.RecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
            portalProjectList.add(portalProject);
            insert portalProjectList; 
            String projectId = portalProjectList[0].Id;
            system.assert(portalProjectList!=null);
        
            List<Researcher_Member_Sharing__c> memberResearchList1 = new List<Researcher_Member_Sharing__c>();
           
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Position__c = 'Chief Investigator';           
            memberResearch.Contact__c = usr.ContactId;
            memberResearch.Researcher_Portal_Project__c=projectId;
            memberResearch.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId(); 
            memberResearchList1.add(memberResearch);
            insert memberResearchList1;
            String researcherMemberID = memberResearchList1[0].Id;
            system.assert(memberResearchList1!=null);
   

            Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            Id ethicsRecordTypeId  = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
            Id contractRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
            Id publicationRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
            
            Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId, ethicsRecordTypeId, contractRecordTypeId, publicationRecordTypeId};

            List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'testSchoolVarName');
            conDetailList.add(conDetail);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics';
            ethicTest1.Application_Title__c ='Test Ethics';
            ethicTest1.Approved_Date__c = system.today();
            ethicTest1.Expiry_Date__c=system.today() +2;  
            insert ethicTest1;     
            system.assert(ethicTest1!=null);
            
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Position__c = 'Student';
            ethicsMemberSharing.Contact__c = usr.ContactId;
            ethicsMemberSharing.Ethics__c = ethicTest1.Id; 
            ethicsMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId(); 
            Insert ethicsMemberSharing;
            system.assert(ethicsMemberSharing!=null);
          
            Ethics_Researcher_Portal_Project__c ethicsResearchPortal = new Ethics_Researcher_Portal_Project__c();
            ethicsResearchPortal.Ethics__c=ethicTest1.Id;
            ethicsResearchPortal.Researcher_Portal_Project__c=portalProjectList[0].id;
            ethicsResearchPortal.Name='test7';           
            insert ethicsResearchPortal;
            
            System.debug('@@@@ethicsResearchPortal =>'+ JSON.serializePretty(ethicsResearchPortal));
            
            Test.startTest();
            // Delete Research member sharing use Case1  
            List<ProjectDetailsWrapper.ContactDetail> conDetailList1 = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail1 = new ProjectDetailsWrapper.ContactDetail(memberResearch);
            conDetailList1.add(conDetail1);
            system.debug('@@@@@JSON conDetailList1 ==>'+JSON.serializePretty(conDetailList1));   
           
            ResearcherMemberSharingUtils.deleteResearcherMemberSharing(conDetailList1,portalProject.Id);   
            
            //UseCase 1 member to private
            ResearcherMemberSharingUtils.operatingProjectAccess(portalProjectList);
            //UseCase 2 private to member
            ResearcherPortalProject__c proj= [SELECT Id, Access__c FROM ResearcherPortalProject__c WHERE Id =:projectId];
            proj.Access__c='Private';  
            update proj;
      
            /*List<ResearcherPortalProject__c> newPortalProjectList = new List<ResearcherPortalProject__c>();
            newPortalProjectList.add(proj);
            ResearcherMemberSharingUtils.operatingProjectAccess(newPortalProjectList);*/
            
            ResearcherMemberSharingUtils.createResearcherProjectMemberSharing(portalProject.Id,usr.id,'Chief Investigator',true);
            
            ResearchProjectContract__c newContractRec = new ResearchProjectContract__c();
            newContractRec.Ecode__c ='0000123456';
            newContractRec.ContractTitle__c='test Contract';
            newContractRec.Status__c='Active';
            newContractRec.Contract_Logged_Date__c = date.today().addDays(-2);
            newContractRec.Current_Active_Action__c='With Researcher (12/05/15)';
            newContractRec.Fully_Executed_Date__c = date.today().addDays(+1);
            newContractRec.Contact_Preferred_Full_Name__c ='Dr test researcher';
            newContractRec.Activity_Type__c='Contract Research';
            newContractRec.Contract_Type__c ='RA-Variation';
            insert newContractRec;
            System.debug('@@@@@newContractRec==>'+JSON.serializePretty(newContractRec));
            System.assert(newContractRec!=null);
            
            //Researcher_Member_Sharing__c ethicsMemberSharing2 = ResearcherMemberSharingUtils.createResearchContractMemberSharing(con.id,usr.id,newContractRec.id,'Chief Investigator');
            Test.StopTest();
        }   
    }
        /************************************************************************************
// Purpose      :  Testing unit cases negative for Project funding Expense Utility
// Developer    :  Subhajit
// Created Date :  10/26/2018                 
//***********************************************************************************/
    @isTest
    public static void testResearcherMemberSharingUtilsNegativeMethods(){
        
        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        Contact con =[SELECT name, hed__UniversityEmail__c FROM Contact LIMIT 1];
        con.hed__UniversityEmail__c='abc@rmit.edu.au';
        update con;
        system.runAs(usr)
        {
            List<ResearcherPortalProject__c> portalProjectList = new List<ResearcherPortalProject__c>();
            
            ResearcherPortalProject__c portalProject = new ResearcherPortalProject__c();
            portalProject.Status__c='Idea';
            portalProject.Access__c='Members';
            portalProjectList.add(portalProject);
            insert portalProjectList; 
            String projectId = portalProjectList[0].Id;
            system.assert(portalProjectList!=null);
        
            List<Researcher_Member_Sharing__c> memberResearchList1 = new List<Researcher_Member_Sharing__c>();
           
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Position__c = 'Chief Investigator';
            memberResearch.Contact__c = usr.ContactId;
            memberResearchList1.add(memberResearch);
            
            //String researcherMemberID = memberResearchList1[0].Id;
            //system.assert(memberResearchList1!=null);
   

            Id projectRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
            Id ethicsRecordTypeId  = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_EthicsRecordtype).getRecordTypeId();
            Id contractRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_MilestoneContractRecordtype).getRecordTypeId();
            Id publicationRecordTypeId = Schema.SObjectType.SignificantEvent__c.getRecordTypeInfosByName().get(Label.RSTP_PublicationRecordtype).getRecordTypeId();
            
            Set<Id> recordTypeIdSet = new Set<Id>{projectRecordTypeId, ethicsRecordTypeId, contractRecordTypeId, publicationRecordTypeId};

            List<ProjectDetailsWrapper.ContactDetail> conDetailList = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail = new ProjectDetailsWrapper.ContactDetail(con,'testSchoolVarName');
            conDetailList.add(conDetail);           
                       
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics';
            ethicTest1.Application_Title__c ='Test Ethics';
            ethicTest1.Approved_Date__c = system.today();
            ethicTest1.Expiry_Date__c=system.today() +2;  
            insert ethicTest1;     
            system.assert(ethicTest1!=null);
            
            Researcher_Member_Sharing__c ethicsMemberSharing = new Researcher_Member_Sharing__c();
            ethicsMemberSharing.Position__c = 'Student';
            ethicsMemberSharing.Contact__c = usr.ContactId;
            ethicsMemberSharing.Ethics__c = ethicTest1.Id; 
            ethicsMemberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId(); 
            memberResearchList1.add(ethicsMemberSharing);
            insert memberResearchList1;
            
          
            Ethics_Researcher_Portal_Project__c ethicsResearchPortal = new Ethics_Researcher_Portal_Project__c();
            ethicsResearchPortal.Ethics__c=ethicTest1.Id;
            ethicsResearchPortal.Researcher_Portal_Project__c=portalProjectList[0].id;
            ethicsResearchPortal.Name='test7';           
            insert ethicsResearchPortal;
            
            System.debug('@@@@ethicsResearchPortal =>'+ JSON.serializePretty(ethicsResearchPortal));
            
            Test.StartTest();
            
            ResearcherMemberSharingUtils.addResearcherMemberSharing(portalProject.Id,usr.ContactId,'HDR');
            //ResearcherMemberSharingUtils.createResearchEthicsMemberSharing(usr.ContactId,usr.id,ethicTest1.Id,'CI');
            ResearcherMemberSharingUtils.createResearcherMemberSharing(conDetailList,portalProject.Id);
            ResearcherMemberSharingUtils.geResearcherMemberSharingList(recordTypeIdSet, usr.id);
     
            // Delete Research member sharing use Case1  
            List<ProjectDetailsWrapper.ContactDetail> conDetailList1 = new List<ProjectDetailsWrapper.ContactDetail>();
            ProjectDetailsWrapper.ContactDetail conDetail1 = new ProjectDetailsWrapper.ContactDetail(memberResearch);
            conDetailList1.add(conDetail1);
            system.debug('@@@@@JSON conDetailList1 ==>'+JSON.serializePretty(conDetailList1));   
            ResearcherMemberSharingUtils.deleteResearcherMemberSharing(conDetailList1,portalProject.Id);   
            
            //UseCase 1 member to private
            ResearcherMemberSharingUtils.operatingProjectAccess(portalProjectList);
            //UseCase 2 private to member
            /*  ResearcherPortalProject__c proj= [SELECT Id, Access__c FROM ResearcherPortalProject__c WHERE Id =:projectId];
            proj.Access__c='Private';  
            update proj;
            
            */
           // ResearcherMemberSharingUtils.createResearcherProjectMemberSharing(portalProject.Id,usr.id,'Chief Investigator',true);
            
            Test.StopTest();
        }   
    }
}