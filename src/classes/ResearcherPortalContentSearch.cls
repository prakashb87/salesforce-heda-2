public class ResearcherPortalContentSearch implements Database.Batchable<sObject> {
    String sname;

    public ResearcherPortalContentSearch(String sname) {
        this.sname = sname;
    }

    public static void executeAsBatch(String sname) {
        ResearcherPortalContentSearch btch = new ResearcherPortalContentSearch(sname);
        Database.executeBatch(btch, 20);
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String sname = this.sname;
        return Database.getQueryLocator('SELECT Id FROM cms__Content__c WHERE cms__Version_Number__c = 1 AND cms__Revision_Number__c = 0 AND cms__Site_Name__c = :sname');
    }

    public void execute(Database.BatchableContext bc, List<cms__Content__c> scope) {
        Id[] originIds = new Id[] {};
        for (cms__Content__c c : scope) {
            originIds.add(c.Id);
        }

        ContentBundle[] bundles = new ContentBundle[] {};

        RenderingApiRequest apiRequest = new RenderingApiRequest();
        apiRequest.parameters = new Map<String, String> { 'renderType' => 'originId' };
        apiRequest.listParameters = new Map<String, String[]> { 'originIds' => originIds };
        apiRequest.requestFlags = new Map<String, Boolean> {
                'noRendering' => true,
                'withContentBundle' => true
        };

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
                'service' => 'OrchestraRenderingApi',
                'action' => 'getRenderedContent',
                'renderingRequest' => Json.serialize(apiRequest),
                'sname' => this.sname,
                'apiVersion' => '5.1'
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        RenderResultBundle renderResult = (RenderResultBundle)Json.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        for (RenderResultBundle.RenderedContent content : renderResult.renderings) {
            bundles.add(content.contentBundle);
        }

        ResearcherPortalContentSearch.upsertSearchObjectsForContent(bundles);
    }

    public void finish(Database.BatchableContext bc) {
        return; // Redundant, but satisfies PMD
    }

    public static void processLifecycleEvents(cms__LifeCycleEvent__e[] events) {
        if (Trigger.isAfter && Trigger.isInsert) {
            ContentBundle[] publishedBundles = new ContentBundle[] {};
            for (cms__LifeCycleEvent__e recordNew : events) {
                if (recordNew.cms__EventName__c == 'CONTENT_PUBLISHED') {
                    publishedBundles.add((ContentBundle) Json.deserialize(recordNew.cms__JsonObject__c, ContentBundle.class));
                }
            }

            ResearcherPortalContentSearch.upsertSearchObjectsForContent(publishedBundles);
        }
    }

    public static void upsertSearchObjectsForContent(ContentBundle[] bundles) {
        Map<Id, ContentBundle> originToBundle = new Map<Id, ContentBundle>();
        Map<Id, OCMS_Search__c> originToSearchObject = new Map<Id, OCMS_Search__c>();

        for(ContentBundle bundle : bundles) {
            originToBundle.put(bundle.originId, bundle);
        }

        for (OCMS_Search__c searchObject : [
                SELECT Content__c, Content_Type__c, Importance__c
                FROM OCMS_Search__c
                WHERE Content__c IN :originToBundle.keySet()
        ]) {
            originToSearchObject.put(searchObject.Content__c, searchObject);
        }

        Map<Id, String> originToContentType = new Map<Id, String>();
        for (cms__Content__c content : [SELECT cms__Content_Type__r.cms__Name__c FROM cms__Content__c WHERE Id IN :originToBundle.keySet() AND cms__Content_Type__c != null]) {
            originToContentType.put(content.Id, content.cms__Content_Type__r.cms__Name__c);
        }

        for (Id originId : originToBundle.keySet()) {
            ContentBundle bundle = originToBundle.get(originId);
            OCMS_Search__c searchObject = originToSearchObject.get(originId);

            if (searchObject == null) {
                searchObject = new OCMS_Search__c(
                        Content__c = originId
                );
            }

            searchObject.Content_Type__c = originToContentType.get(originId);
            searchObject.Importance__c = getIntegerAttribute(bundle, 'importance', 0);

            originToSearchObject.put(originId, searchObject);
        }

        upsert originToSearchObject.values();
    }

    private static Integer getIntegerAttribute(ContentBundle bundle, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            for (String langCode : bundle.contentAttributes.keySet()) {
                for (AttributeBundle.ContentAttribute attribute : bundle.contentAttributes.get(langCode)) {
                    if (attribute.name == name) {
                        value = Integer.valueOf(attribute.value);
                    }
                }
            }
        } catch(TypeException ex) {
            /* No issue - we'll just return the default */
            value = defaultValue; // Redundant, but this satisfies PMD
        }

        return value;
    }
}