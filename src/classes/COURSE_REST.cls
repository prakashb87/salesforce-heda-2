/*********************************************************************************
*** @ClassName         : COURSE_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 30/08/2018
*** @Modified by       : Anupam Singhal 
*** @modified date     : 13/11/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Course')
global with sharing class COURSE_REST {
    
    public String courseCode;
    public String effectiveDate;
    public String status;
    public String title;
    public String description;
    public String longDescription;
    public String minimumUnits;
    public String maximumUnits;
    public String academicProgressUnits;
    public String scheduledContactHours;
    public String gradingbasis;

    @HttpPost
    global static Map < String, String > upsertCourse() {
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Course offering
        list < Course_List__c > upsertCourse = new list < Course_List__c > ();
        //getting course Id
        list < String > addCourseID = new list < String > ();
        
        Course_List__c course;

   try {

        //request and response variable
        RestRequest request = RestContext.request;

        RestResponse response = RestContext.response;

        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        system.debug('Post from IPaaS------->' + reqStr);
        response.addHeader('Content-Type', 'application/json');

        //getting the data from JSON and Deserialize it
        List < COURSE_REST > postString = (List < COURSE_REST > ) System.JSON.deserialize(reqStr, List < COURSE_REST > .class);

        //store institution
        Account institution;
        
        if (postString != null) {
            for (COURSE_REST courseRest: postString) {
                if (courseRest.courseCode != null && (courseRest.courseCode).length() > 0) {
                    addCourseID.add(courseRest.courseCode);
                    if(courseRest.effectiveDate != null && (courseRest.effectiveDate).length() > 0)
                    {
                        updateEffectiveDate.put(courseRest.courseCode,Date.ValueOf(courseRest.effectiveDate));
                    }
                } else {
                    //throw error
                    responsetoSend = throwError();
                    return responsetoSend;
                }

            }
        }
        
        //getting the old course for update
        list < Course_List__c > previousCourse = new list < Course_List__c > ();
        if (addCourseID != null && addCourseID.size() > 0) {
            previousCourse = [select id,Name,Effective_Date__c,Status__c,Description__c,Long_Course_Title__c,Long_Description__c,Minimum_Units__c,Maximum_Units__c,Academic_Progress_Units__c,Scheduled_Contact_Hours__c,Grading_Basis__c from Course_List__c where Name =: addCourseID[0] Limit 1
            ];
        }

        //map to update the previous Translation service
        map < String, Course_List__c > oldCourse = new map < String, Course_List__c > ();
        for (Course_List__c courseVar : previousCourse) {
            if(courseVar.Name != null ){
                String uniqKey = courseVar.Name + '';
                if (updateEffectiveDate != null && updateEffectiveDate.get(uniqKey) != null) {
                    if( updateEffectiveDate.get(uniqKey) <= System.today() && courseVar.Effective_Date__c <= updateEffectiveDate.get(uniqKey)){
                    oldCourse.put(uniqKey, courseVar);
                }else if(courseVar.Effective_Date__c > updateEffectiveDate.get(uniqKey)){
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;
                }
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;
                }
            }
        }

        //insert or update the course record
        for (COURSE_REST restCourse: postString) {
            String key = restCourse.courseCode + '';
            course = new Course_List__c();
            course.Name =   restCourse.courseCode;
            course.Effective_Date__c    = restCourse.effectiveDate != null && ( restCourse.effectiveDate).length () > 0 ? Date.ValueOf( restCourse.effectiveDate) : null;
            course.Status__c    =  restCourse.status == 'A'?'Active':restCourse.status == 'I'?'Inactive': null;
            course.Description__c   =   restCourse.title;
            course.Long_Course_Title__c =   restCourse.description;
            course.Long_Description__c  =   restCourse.longDescription;
                course.Minimum_Units__c =   restCourse.minimumUnits != null && (restCourse.minimumUnits).length() > 0 ? Decimal.valueOf(restCourse.minimumUnits) : null;
	        course.Maximum_Units__c =   restCourse.maximumUnits != null && (restCourse.maximumUnits).length() > 0 ? Decimal.valueOf(restCourse.maximumUnits) : null;
	        course.Academic_Progress_Units__c   =   restCourse.academicProgressUnits != null && (restCourse.academicProgressUnits).length() > 0 ? Decimal.valueOf(restCourse.academicProgressUnits) : null ;
	        course.Scheduled_Contact_Hours__c   =   restCourse.scheduledContactHours != null && (restCourse.scheduledContactHours).length() > 0 ? Decimal.valueOf(restCourse.scheduledContactHours) : null;
	        course.Grading_Basis__c =   restCourse.gradingbasis;
                if(oldCourse.size() > 0 && oldCourse.get(restCourse.courseCode) != null){
                    if(oldCourse.get(key).Effective_Date__c <= course.Effective_Date__c)
                    {
                        course.Id = oldCourse.get(key).Id;
                    }else{
                        oldCourse= null;
                    }
             }

            //course.Field_of_Education_Code__c = 
           if(course!= null)
           {
                upsertCourse.add(course);
           }
        }
        //upsert course
            if (upsertCourse != null && upsertCourse.size() > 0 && Date.ValueOf( postString[0].effectiveDate) <= System.today() ) {
            upsert upsertCourse Name;
        }
        responsetoSend.put('message', 'OK');
        responsetoSend.put('status', '200');
        return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }

    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Course was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}