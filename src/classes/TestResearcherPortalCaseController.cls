/*******************************************
Purpose: Test class ResearcherPortalCaseController
History:
Created by Md Ali on 12/11/2018
*******************************************/
@isTest
public class TestResearcherPortalCaseController {
    
    /************************************************************************************
// Purpose      :  Creating Test data for Community User for all test methods 
// Developer    :  Md Ali
// Created Date :  12/01/2018                 
//***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
// Purpose      :  Test functionality for ResearcherPortalSupportController
// Developer    :  Md Ali
// Created Date :  11/12/2018                 
//***********************************************************************************/
    
    @isTest
    public static void testPositiveUseCaseForResearcherPortalCaseControllerMethods(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.assert(u!=null);
        String attachmentString = '{"fileName":"dashboard.PNG","fileContent":"AAAABJRU5ErkJggg==","contentType":"png"}';
        String serailizedString = '{"researcherCaseTopic":"Enabling Capability Platform (ECPs)","researcherCaseSubTopic":"Funding options, application, deadlines and management","Subject":"test subject","Description":"test description","recordType":"Researcher Portal Support Case","status":"Closed"}';
        //JSON.serialize(portalWrapper); 
        String portalSerailizedString='{"portalFeedback":"Researcher Portal experience","Subject":"test subject","Description":"test feedback","recordType":"Researcher Portal Feedback","status":"Closed"}';        
        System.runAs(u) {
            
            ResearcherPortalCaseController.getAllTopicSubtopicInfo();
            ResearcherPortalCaseController.createCase(serailizedString);
            String caseId = ResearcherPortalCaseController.createCase(portalSerailizedString);
            ResearcherPortalCaseController.getAllfeedbackTopicInfo();
            ResearcherPortalCaseController.attachFilesToCaseRecord(attachmentString,caseId);
            
        } 
    }    
    
    /************************************************************************************
// Purpose      :  Test functionality for ResearcherPortalSupportController
// Developer    :  Md Ali
// Created Date :  12/05/2018                 
//***********************************************************************************/
    
    @isTest
    public static void testNegativeUseCaseForResearcherPortalCaseControllerMethods(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(u!=null);
        
        String serailizedString = '{"researcherCaseTopic":"Enabling Capability Platform (ECPs)","researcherCaseSubTopic":"Funding options, application, deadlines and management","Subject":"test subject","Description":"test description","recordType":"Researcher Portal Support Case","status":"Closed"}';
        String portalSerailizedString='{"portalFeedback":"Researcher Portal experience","Subject":"test subject","Description":"test feedback","recordType":"Researcher Portal Feedback","status":"Closed"}';        
        System.runAs(u) {
            
            ResearcherPortalCaseController.getAllTopicSubtopicInfo();
            ResearcherPortalCaseController.createCase(serailizedString);
            ResearcherPortalCaseController.createCase(portalSerailizedString);
            ResearcherPortalCaseController.getAllfeedbackTopicInfo();
            //usecase for Exception handling catch block
            String testString = '{"researcherCaseTopic":"Enabling Capability Platform (ECPs)","researcherCaseSubTopic":"Funding options, application, deadlines and management","Subject":"test subject","Description":"test description","recordType": null,"status":"Closed"}';
            ResearcherPortalCaseController.createCase(testString);   
           
            
        } 
    }    
       
    /************************************************************************************
// Purpose      :  Test functionality for ResearcherPortalSupportController
// Developer    :  Md Ali
// Created Date :  01/04/2018                 
//***********************************************************************************/
  @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(u!=null);
        
        System.runAs(u) {      
            
            ResearcherPortalCaseController.isForceExceptionRequired=true;
            ResearcherPortalCaseController.getAllTopicSubtopicInfo();
            ResearcherPortalCaseController.getAllfeedbackTopicInfo();
            
        }
    }
}