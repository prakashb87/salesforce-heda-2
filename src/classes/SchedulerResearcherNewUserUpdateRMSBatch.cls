/*******************************************
Name: SchedulerResearcherNewUserUpdateRMSBatch 

Description : Scheduler class to schedule to batch class to update the new users on the RMS records.
              
Created by : Prakash B for RPORW-897
Modified by Subhajit to fix 3 PMD issues

History:
*******************************************/

public without sharing class SchedulerResearcherNewUserUpdateRMSBatch implements Schedulable {
   public void execute(SchedulableContext sc) {
   
       ResearcherNewUserUpdateRMSBatch rmsBatch = new ResearcherNewUserUpdateRMSBatch();
       Database.executeBatch(rmsBatch); 
   }
}