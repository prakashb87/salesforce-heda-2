public without sharing class ExpenseRollupToContact{

    private static final String EXPENSE_TYPE_ENTERTAINMENT_VOUCHER = 'Entertainment Voucher (mentor)';
    //private static final Id ACC_MENTOR_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mentor').getRecordTypeId();
    
    public static void rollupExpensesTotalHoursCompletedToContact(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : mNewExpensesList){
                if(mExpensesItem.Service_Provider_Contact__c != NULL){
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                }

            }
            system.debug('----------mServiceProviderIdSet----------------'+mServiceProviderIdSet);
        
            //Aggregate Expenses records to Contact
            if(mServiceProviderIdSet.size() > 0){
                //Set<Id> mAccRecordTypeIDSet = new Set<Id>{ACC_INDUSTRY_PARTNER_RECORDTYPEID, ACC_COACH_RECORDTYPEID, ACC_MENTOR_RECORDTYPEID};
                List<AggregateResult> mExpenseAggList = [SELECT SUM(Hours__c) Sum_TotalHours, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         GROUP BY Service_Provider_Contact__c];
              
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                system.debug('----------mExpenseAggList----------------'+mExpenseAggList);
                
                if(mExpenseAggList.size() > 0){
                    for(AggregateResult mAggExpenseItem : mExpenseAggList){
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Hours_Completed__c = (Decimal)mAggExpenseItem.get('Sum_TotalHours');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0){
                    update mUpdateRollupToContactList;
                }
              
            }
        
        
        }catch(System.Exception ex){
            System.debug('>>>>> Error Message from ExpenseRollupTotalHoursCompletedToContact: '+ex.getMessage());
        }
    }
    public static void rollupExpensesTotalHoursCompletedToContact_ForDelete(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            Set<Id> mDeleteExpenseIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : mNewExpensesList){
                if(mExpensesItem.Service_Provider_Contact__c != NULL){
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                    mDeleteExpenseIdSet.add(mExpensesItem.Id);
                }
            }
            
            //Aggregate Expense records to Contact
            if(mServiceProviderIdSet.size() > 0){
                //Set<Id> mAccRecordTypeIDSet = new Set<Id>{ACC_INDUSTRY_PARTNER_RECORDTYPEID, ACC_COACH_RECORDTYPEID, ACC_MENTOR_RECORDTYPEID};
                List<AggregateResult> mExpenseAggList = [SELECT SUM(Hours__c) Sum_TotalHours, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Id NOT IN: mDeleteExpenseIdSet  
                                                         AND Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         GROUP BY Service_Provider_Contact__c];
                
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                if(mExpenseAggList.size() > 0){
                    for(AggregateResult mAggExpenseItem : mExpenseAggList){
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Hours_Completed__c = (Decimal)mAggExpenseItem.get('Sum_TotalHours');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }else{
                    //In case delete all expense record that related to Service Provider
                    for(Expenses__c mExpenseItem : mNewExpensesList){
                        Contact mContact = new Contact(Id = mExpenseItem.Service_Provider_Contact__c);
                        mContact.Total_Hours_Completed__c = 0;
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0){
                    update mUpdateRollupToContactList;
                }
            }
        }
        catch(System.Exception ex){
            System.debug('>>>>> Error Message from ExpenseRollupTotalHoursCompletedToContact For Delete: '+ex.getMessage());
        }
    }
    
    public static void rollupExpensesTotalPaymentToContact(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : [SELECT Id, Name, Service_Provider_Contact__c FROM Expenses__c WHERE Id =: mNewExpensesList]){
                if(mExpensesItem.Service_Provider_Contact__c != NULL) {  
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                }
            }
            
            //Aggregate Expense records to Contact
            if(mServiceProviderIdSet.size() > 0){
            
                List<AggregateResult> mExpenseAggList = [SELECT SUM(Amount__c) Sum_TotalPayment, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         GROUP BY Service_Provider_Contact__c];
                
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                if(mExpenseAggList.size() > 0){
                    for(AggregateResult mAggExpenseItem : mExpenseAggList){
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Payment__c = (Decimal)mAggExpenseItem.get('Sum_TotalPayment');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0){
                    update mUpdateRollupToContactList;
                }
            }
            
        }catch(System.Exception ex){
            System.debug('>>>>> Error Message from ExpenseRollupTotalPaymentToContact: '+ex.getMessage());
        }
    }
    public static void rollupExpensesTotalPaymentToContact_ForDelete(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            Set<Id> mDeleteExpenseIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : [SELECT Id, Name, Service_Provider_Contact__c FROM Expenses__c WHERE Id =: mNewExpensesList]){
                if(mExpensesItem.Service_Provider_Contact__c != NULL){
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                    mDeleteExpenseIdSet.add(mExpensesItem.Id);
                }
            }
            
            //Aggregate Expense records to Contact
            if(mServiceProviderIdSet.size() > 0){
                
                List<AggregateResult> mExpenseAggList = [SELECT SUM(Amount__c) Sum_TotalPayment, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Id NOT IN: mDeleteExpenseIdSet  
                                                         AND Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         GROUP BY Service_Provider_Contact__c];
                
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                if(mExpenseAggList.size() > 0){
                    for(AggregateResult mAggExpenseItem : mExpenseAggList)
                    {
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Payment__c = (Decimal)mAggExpenseItem.get('Sum_TotalPayment');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                    
                }else{
                    //In case delete all expense record that related to Service Provider
                    for(Expenses__c mExpenseItem : mNewExpensesList){
                        Contact mContact = new Contact(Id = mExpenseItem.Service_Provider_Contact__c);
                        mContact.Total_Payment__c = 0;
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0){
                    update mUpdateRollupToContactList;
                }
            }
        }
        catch(System.Exception ex){
            System.debug('>>>>> Error Message from ExpenseRollupTotalPaymentToContact For Delete: '+ex.getMessage());
        }
    }
    
    public static void rollupExpensesTotalVouchersToContact(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : [SELECT Id, Name, Service_Provider_Contact__c FROM Expenses__c WHERE Id =: mNewExpensesList]){
                if(mExpensesItem.Service_Provider_Contact__c != NULL){
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                }
            }
            
            //Aggregate Expense records to Contact
            if(mServiceProviderIdSet.size() > 0){

                List<AggregateResult> mExpenseAggList = [SELECT COUNT(Id) Count_TotalVouchers, SUM(Amount__c) Sum_Amount, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         AND Type__c =: EXPENSE_TYPE_ENTERTAINMENT_VOUCHER 
                                                         GROUP BY Service_Provider_Contact__c];
                
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                if(mExpenseAggList.size() > 0){
                    for(AggregateResult mAggExpenseItem : mExpenseAggList){
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Vouchers_Provided__c = (Decimal)mAggExpenseItem.get('Count_TotalVouchers');
                        mContact.Total_Value_of_Vouchers_Provided__c = (Decimal)mAggExpenseItem.get('Sum_Amount');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0){
                    update mUpdateRollupToContactList;
                }
            }
        }
        catch(System.Exception ex){
            System.debug('>>>>> Error Message from ExpenseRollupTotalVouchersToAccCtr: '+ex.getMessage());
        }
    }
    
    public static void rollupExpensesTotalVouchersToContact_ForDelete(List<Expenses__c> mNewExpensesList){
        try{
            Set<Id> mServiceProviderIdSet = new Set<Id>();
            Set<Id> mDeleteExpenseIdSet = new Set<Id>();
            for(Expenses__c mExpensesItem : [SELECT Id, Name, Service_Provider_Contact__c FROM Expenses__c WHERE Id =: mNewExpensesList]){
                if(mExpensesItem.Service_Provider_Contact__c != NULL){
    
                    mServiceProviderIdSet.add(mExpensesItem.Service_Provider_Contact__c);
                    mDeleteExpenseIdSet.add(mExpensesItem.Id);
                    
                }
            }
            
            //Aggregate Expense records to Contact
            if(mServiceProviderIdSet.size() > 0){
                //Set<Id> mAccRecordTypeIDSet = new Set<Id>{ACC_MENTOR_RECORDTYPEID};
                List<AggregateResult> mExpenseAggList = [SELECT COUNT(Id) Count_TotalVouchers, SUM(Amount__c) Sum_Amount, Service_Provider_Contact__c mServiceProvider
                                                         FROM Expenses__c 
                                                         WHERE Id NOT IN: mDeleteExpenseIdSet  
                                                         AND Service_Provider_Contact__c IN: mServiceProviderIdSet 
                                                         //AND Service_Provider_Account__r.RecordTypeId IN: mAccRecordTypeIDSet
                                                         AND Type__c =: EXPENSE_TYPE_ENTERTAINMENT_VOUCHER
                                                         GROUP BY Service_Provider_Contact__c];
                
                List<Contact> mUpdateRollupToContactList = new List<Contact>();
                if(mExpenseAggList.size() > 0){
                
                    for(AggregateResult mAggExpenseItem : mExpenseAggList){
                    
                        Contact mContact = new Contact(Id = (Id)mAggExpenseItem.get('mServiceProvider'));
                        mContact.Total_Vouchers_Provided__c = (Decimal)mAggExpenseItem.get('Count_TotalVouchers');
                        mContact.Total_Value_of_Vouchers_Provided__c = (Decimal)mAggExpenseItem.get('Sum_Amount');
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }else{
                    //In case delete all expense record that related to Service Provider
                    for(Expenses__c mExpenseItem : mNewExpensesList){
                        Contact mContact = new Contact(Id = mExpenseItem.Service_Provider_Contact__c);
                        mContact.Total_Vouchers_Provided__c = 0;
                        mContact.Total_Value_of_Vouchers_Provided__c = 0;
                        
                        mUpdateRollupToContactList.add(mContact);
                    }
                }
                
                if(mUpdateRollupToContactList.size() > 0)
                {
                    update mUpdateRollupToContactList;
                }
            }
        }
        catch(System.Exception ex)
        {
            System.debug('>>>>> Error Message from ExpenseRollupTotalVouchersToAccCtr For Delete: '+ex.getMessage());
        }
    }
}