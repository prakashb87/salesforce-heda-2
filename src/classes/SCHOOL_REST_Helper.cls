/*********************************************************************************
*** @ClassName         : SCHOOL_REST_HELPER
*** @Author            : Dinesh Kumar 
*** @Requirement       : Moved some code from SCHOOL_REST to this class
*** @Created date      : 04/02/2019
*** @Modified by       : Khushman Deomurari
*** @modified date     : 04/02/2019
**********************************************************************************/
public with sharing class SCHOOL_REST_Helper {


	public static Map<String, String> upsertSchool(Account school, List <Account> upsertSchool, List < SCHOOL_REST > postString){
			Map <String, String> responsetoSend = new Map <String, String>();
		
            //add account to the list
           if(school !=null){
                upsertSchool.add(school);    
           }
           // add account to upsert
           if (upsertSchool != null && upsertSchool.size() > 0 && Date.valueOf(postString[0].effectiveDate) <= system.today()) {
           	if (Schema.sObjectType.Account.isUpdateable() && Schema.sObjectType.Account.isCreateable()){
            	upsert upsertSchool AccountNumber__c;
           	}
           }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
	}

    public static List<Account> queryAcademicInstitutionRecord(List<String> listAcademicInstitution){
    	List<Account> academicInstitutionRecord=new List<Account>();
    	If(listAcademicInstitution != null && listAcademicInstitution.size() > 0){
        	if (Schema.sObjectType.Account.isAccessible()){
            	academicInstitutionRecord = [SELECT Id FROM Account WHERE Name =: listAcademicInstitution[0] Limit 1];
        	}    
        }
    	return academicInstitutionRecord;
    }

    public static List<AOU_Code__c> queryAouCodeRecord(List<String> listAOUCode){
    	List<AOU_Code__c> aouCodeRecord=new List<AOU_Code__c>();
        If(listAOUCode != null && listAOUCode.size() > 0){
        	if (Schema.sObjectType.AOU_Code__c.isAccessible()){
            	aouCodeRecord = [SELECT Id,Description__c FROM AOU_Code__c WHERE Name =: listAOUCode[0] Limit 1];
        	}    
        }
        return aouCodeRecord;
    }

    public static List<Campus__c> queryCampusRecord(List<String> listCampus){
    	List<Campus__c> campusRecord=new List<Campus__c>();
        If(listCampus != null && listCampus.size() > 0){
        	if (Schema.sObjectType.Campus__c.isAccessible()){
            	campusRecord = [SELECT Id FROM Campus__c WHERE Name =: listCampus[0] Limit 1];
        	}    
        }
    	return campusRecord;
    }    
    
	public static List<Account> queryCheckExisAccountRecord(List<String> lstSchool, String uniqueKey){
		List<Account> checkExisAccountRecord=new List<Account>();
		if(lstSchool[0] != null && lstSchool.size() > 0){
            checkExisAccountRecord = [SELECT Id, AccountNumber__c, Effective_Date__c, Academic_Institution__r.Name,
                                    Status__c, Description, Short_Description__c, Formal_Description__c, 
                                    Academic_Institution__c, AOUCode__c, Campus__c, ParentId                                      
                                    FROM Account WHERE AccountNumber__c =: lstSchool[0] limit 1];
            uniqueKey = lstSchool[0];
        }
		return checkExisAccountRecord;
	}

	public static List<Account> queryParentAcadOrgaRecord(List<String> listParAcadOrg){
        List<Account> parentAcadOrgaRecord = new List<Account>();
        
        If(listParAcadOrg != null && listParAcadOrg.size() > 0){
            parentAcadOrgaRecord = [SELECT Id FROM Account WHERE AccountNumber__c =: listParAcadOrg[0] Limit 1];    
        }
        return parentAcadOrgaRecord;
	}
	
}