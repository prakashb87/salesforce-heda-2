/**
 * @description Abstract Factory interface for creating dependencies required to
 * process a new customer registration
 */
// Global state is required as the page referencing this class
// will be inside an iframe
@SuppressWarnings('PMD.AvoidGlobalModifier')
global interface RegistrationAbstractFactory {

    /**
     * @description Creates the captcha instance
     */
    CaptchaVerificationRequest createCaptcha();


    /**
     * @description Creates the IdentityProviderUserStatusReq instance
     */
    IdentityProviderUserStatusReq createIdpUserStatusReq(String email);


    /**
     * @description Creates the IdentityProviderUserRequest instance
     */
    IdentityProviderUserRequest createIdpUserCreateReq();


    /**
     * @description Creates the IdentityProviderUserDeleteRequest instance
     */
    IdentityProviderUserDeleteRequest createIdpUserDeleteReq();
}