/***************************************************************************************
 *** @Class             : EmailToCaseHandlerUtility 
 *** @Author            : Shubham Singh
 *** @Requirement       : To handle the incoming mails
 *** @Created date      : 19/01/2018
  ***@Modified By       : Praneet Vig
  ***@Modified Date     : 21/03/2018

 ***********************************************************************************************/
/************************************************************************************************
 *** @About Class
 *** This class is utility class for EmailtoCaseHandler class 
 *** 
 ***********************************************************************************************/
public without sharing class EmailToCaseHandlerUtility {
    public void createCaseOrTask(List < Contact > vCon, List <case > c, Messaging.inboundEmail email) {
        try {
               Map < Id, Id > contentmap = new Map < Id, Id > ();  //Creating Map for attachment
            //if there is a contact but no case, then create case or if there is no contact and no case
            list<string> recidlist = new list<string>();
            if (c != null){
            If(c.size() == 0){
                case newC = new case ();
                if (vCon.size() > 0 && vCon != null && c.size() == 0){
                    newC.ContactId = vCon.isempty() ? null : vCon[0].id;
                    newC.SuppliedEmail = email.fromAddress;
                }
                if (c != null && vCon != null ){
                if(vCon.size() == 0 && c.size() == 0){
                   newC.SuppliedEmail = email.fromAddress; 
                }
                }
                if (email.plainTextBody != null)
                {
                    newC.Description = email.plainTextBody;
                }
                else if (email.htmlBody != null)
                {
                    newC.Description = email.htmlBody;
                }
                newC.Priority = Label.CasePriority;
                newC.Status =  Label.CaseStatus;
                newC.Subject = email.subject;
                newC.Origin = Label.CaseOrigin;
                insert newC;
                List < ContentVersion > lstContentVersionID1 = new List < ContentVersion > ();
                List < ContentDocumentLink > lstContentDocumentLink1 = new List < ContentDocumentLink > ();
                if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                    ContentVersion cv = new ContentVersion();
                    for (integer i = 0; i < email.binaryAttachments.size(); i++) {
                        cv = new ContentVersion();
                        cv.ContentLocation = 'S';
                        cv.PathOnClient = email.binaryAttachments[i].filename;
                        cv.Origin = 'H';
                        cv.Title = email.binaryAttachments[i].filename;
                        cv.VersionData = email.binaryAttachments[i].body;
                        lstContentVersionID1.add(cv);
                    }
                    if (lstContentVersionID1.size() > 0 && lstContentVersionID1 != null) {
                        list<database.saveresult> srList = Database.insert(lstContentVersionID1,false);
                            
                        for (Database.SaveResult sr : srList)   
                        {
                            if (sr.isSuccess())
                            {
                                recidlist.add(string.valueof(sr.getId()));
                            }
                            
                        }
                    }          
                    for (contentversion cvc : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id in:recidlist]){
                        contentmap.put(cvc.id,cvc.contentDocumentId);
                    }

                    ContentDocumentLink cdl = new ContentDocumentLink();
                    for (Integer i = 0; i < recidlist.size(); i++) {
                        cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = contentmap.get(recidlist[i]);
                        cdl.LinkedEntityId = newC.Id;
                        cdl.ShareType = 'V';
                        lstContentDocumentLink1.add(cdl);
                    }
                }
                if (lstContentDocumentLink1.size() > 0 && lstContentDocumentLink1 != null) {
                    Database.insert(lstContentDocumentLink1,false);
                }
                List < ContentVersion > lstContentVersionID2 = new List < ContentVersion > ();
                List < ContentDocumentLink > lstContentDocumentLink2 = new List < ContentDocumentLink > ();
                if (email.textAttachments != null && email.textAttachments.size() > 0) {
                    ContentVersion cv = new ContentVersion();
                    for (integer i = 0; i < email.textAttachments.size(); i++) {
                        cv = new ContentVersion();
                        cv.ContentLocation = 'S';
                        cv.PathOnClient = email.textAttachments[i].filename;
                        cv.Origin = 'H';
                        //cv.OwnerId = attachment.OwnerId;
                        cv.Title = email.textAttachments[i].filename;
                        cv.VersionData = Blob.valueOf(email.textAttachments[i].body);
                        lstContentVersionID2.add(cv);
                    }
                    List<string> recidlistnew = new list<String>();
                    if (lstContentVersionID2.size() > 0 && lstContentVersionID2 != null) {
                        list<database.saveresult> saveList = Database.insert(lstContentVersionID2,false);  
                        for (Database.SaveResult sr : saveList)   
                        {
                            if (sr.isSuccess())
                            {
                            recidlistnew.add(string.valueof(sr.getId()));
                            }

                        }
                    }
                    for (contentversion cvc : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id in:recidlist]){
                        contentmap.put(cvc.id,cvc.contentDocumentId);   //Adding attachment to Map
                    }
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    for (Integer i = 0; i < lstContentVersionID2.size(); i++) {
                        cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = contentmap.get(recidlistnew[i]);   //Inserting attachment to case
                        cdl.LinkedEntityId = newC.Id;
                        cdl.ShareType = 'V';
                        lstContentDocumentLink2.add(cdl);
                    }
                }
                if (lstContentDocumentLink2.size() > 0 && lstContentDocumentLink2 != null) {
                    Database.insert(lstContentDocumentLink2,false);
                }
            }
            }
            if ((vCon.size() > 0 && vCon != null && c.size() > 0 && c != null) || (vCon.size() == 0 && c.size() > 0 && c != null)) {
                Task newTask = new Task();
                if (email.plainTextBody != null)
                {
                    newTask.Description = email.plainTextBody;
                }
                else if (email.htmlBody != null)
                {
                    newTask.Description = email.htmlBody;
                }
                newTask.subject = email.subject;
                newTask.status = Label.TaskStatus; //labels
                newTask.whatId = c.isEmpty() ? null :c[0].id;
                insert newTask;
                List < ContentVersion > lstContentVersionID5 = new List < ContentVersion > ();
                List < ContentDocumentLink > lstContentDocumentLink5 = new List < ContentDocumentLink > ();
                if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                    ContentVersion cv = new ContentVersion();
                    for (integer i = 0; i < email.binaryAttachments.size(); i++) {
                        cv = new ContentVersion();
                        cv.ContentLocation = 'S';
                        cv.PathOnClient = email.binaryAttachments[i].filename;
                        cv.Origin = 'H';
                        cv.Title = email.binaryAttachments[i].filename;
                        cv.VersionData = email.binaryAttachments[i].body;
                        lstContentVersionID5.add(cv);
                    }
                    List<String> recidlist2 = new List<String>();
                    if (lstContentVersionID5.size() > 0 && lstContentVersionID5 != null) {
                        list<database.saveresult> saveList5 = Database.insert(lstContentVersionID5,false);  
                        for (Database.SaveResult sr : saveList5)   
                        {
                            if (sr.isSuccess())
                            {
                            recidlist2.add(string.valueof(sr.getId()));
                            }
                        }
                    }
                    for (contentversion cvc : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id in:recidlist]){
                        contentmap.put(cvc.id,cvc.contentDocumentId);
                    }
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    for (Integer i = 0; i < lstContentVersionID5.size(); i++) {
                        cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = contentmap.get(recidlist2[i]);  //Inserting attachment to case
                        cdl.LinkedEntityId = c.isEmpty() ? null :c[0].id;
                        cdl.ShareType = 'V';
                        lstContentDocumentLink5.add(cdl);
                    }
                }
                if (lstContentDocumentLink5.size() > 0 && lstContentDocumentLink5 != null) {
                    Database.insert(lstContentDocumentLink5,false);
                }
                List < ContentVersion > lstContentVersionID6 = new List < ContentVersion > ();
                List < ContentDocumentLink > lstContentDocumentLink6 = new List < ContentDocumentLink > ();
                if (email.textAttachments != null && email.textAttachments.size() > 0) {
                    ContentVersion cv = new ContentVersion();
                    for (integer i = 0; i < email.textAttachments.size(); i++) {
                        cv = new ContentVersion();
                        cv.ContentLocation = 'S';
                        cv.PathOnClient = email.textAttachments[i].filename;
                        cv.Origin = 'H';
                        //cv.OwnerId = attachment.OwnerId;
                        cv.Title = email.textAttachments[i].filename;
                        cv.VersionData = Blob.valueOf(email.textAttachments[i].body);
                        lstContentVersionID6.add(cv);
                    }
                    List<String> recidlist4 = new List<String>();
                    if (lstContentVersionID6.size() > 0 && lstContentVersionID6 != null) {
                        Database.insert(lstContentVersionID6,false);
                        list<database.saveresult> saveList6 = Database.insert(lstContentVersionID6,false);  
                        for (Database.SaveResult sr : saveList6)   
                        {
                            if (sr.isSuccess())
                            {
                            recidlist4.add(string.valueof(sr.getId()));
                            }
                        }
                    }
                    for (contentversion cvc : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id in:recidlist]){
                        contentmap.put(cvc.id,cvc.contentDocumentId);
                    }
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    for (Integer i = 0; i < lstContentVersionID6.size(); i++) {
                        cdl = new ContentDocumentLink();
                        if(recidlist4.size()>i){                        
                        cdl.ContentDocumentId = contentmap.get(recidlist4[i]);//Inserting attachment to case
                       }
                        cdl.LinkedEntityId = c.isEmpty() ? null :c[0].id;
                        cdl.ShareType = 'V';
                        lstContentDocumentLink6.add(cdl);
                    }
                    
                }
                if (lstContentDocumentLink6.size() > 0 && lstContentDocumentLink6 != null) {
                    Database.insert(lstContentDocumentLink6,false);                    
                }
            }
        } catch (QueryException e) {
        	
          system.debug('Exception:'+e.getMessage());
        }
    }

}