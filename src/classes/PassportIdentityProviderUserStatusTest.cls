/*
  *****************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 05/11/2018 
  * @description  : Test calss for PassportIdentityProviderUserStatusReq
  ******************************************************************************************************************
*/

@isTest
public class PassportIdentityProviderUserStatusTest {

    @TestSetup
    static void makeData(){
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c configDataMapObj1 = new Config_Data_Map__c();
        configDataMapObj1.Name = 'IDAMIpaaSEndPoint';
        configDataMapObj1.Config_Value__c = 'https://sys-idm-v1-dev-qa.npe.integration.rmit.edu.au/api/user';
        configurations.add(configDataMapObj1);
        
        Config_Data_Map__c configDataMapObj2 = new Config_Data_Map__c();
        configDataMapObj2.Name = 'IDAMIpaaSClientID';
        configDataMapObj2.Config_Value__c = 'testadmin123';
        configurations.add(configDataMapObj2);
        
        Config_Data_Map__c configDataMapObj3 = new Config_Data_Map__c();
        configDataMapObj3.Name = 'IDAMIpaaSClientSecret';
        configDataMapObj3.Config_Value__c = '32534cjsjdoslk4234324dscclcp';
        configurations.add(configDataMapObj3);

        insert configurations;
    }

    @isTest
    static void constructorNullParameterTest() {
        try {
            PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('');
            System.assert(false, 'Expecting null parameter exception to be caught');
        } catch (ArgumentException e) {
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }

    @isTest
    static void constructorNonNullParameterTest() {
        try {
            PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }


    @isTest
    static void loggingSetterTest() {
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
        passport.setIntegrationLogging(false);
        System.assertEquals(false, passport.isCalloutLogged);
    }


    @isTest
    static void loggingOffTest() {
        Integer logCount = [SELECT COUNT() FROM Integration_Logs__c];
        Test.setMock(HttpCalloutMock.class, new UserExistsAndVerifiedMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('exists@test.com');
        passport.setIntegrationLogging(false);
        passport.isExistingUser();
        Test.stopTest();
        Integer logCountAfterCallout = [SELECT COUNT() FROM Integration_Logs__c];
        System.assertEquals(logCount, logCountAfterCallout);
    }


    @isTest
    static void loggingOnTest() {

		Config_Data_Map__c logSetting = new Config_Data_Map__c();
        logSetting.Name = 'LogIntegrationRequestToIntegrationLog';
        logSetting.Config_Value__c = 'true';
        insert logSetting;

        Integer logCount = [SELECT COUNT() FROM Integration_Logs__c];
        Test.setMock(HttpCalloutMock.class, new UserExistsAndVerifiedMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('exists@test.com');
        passport.isExistingUser();
        Test.stopTest();
        Integer logCountAfterCallout = [SELECT COUNT() FROM Integration_Logs__c];
        System.assert(logCountAfterCallout > logCount);
    }


    @isTest
    static void userDoesNotExistResponseTest() {
        Test.setMock(HttpCalloutMock.class, new UserDoesNotExistMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('dnf@test.com');
        passport.setIntegrationLogging(false);
        Boolean result = passport.isExistingUser();

        Test.stopTest();

        System.assertEquals(false, result);
    }


    @isTest
    static void userDoesNotExistVerifiedResponseTest() {
        Test.setMock(HttpCalloutMock.class, new UserDoesNotExistMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('dnf@test.com');
        passport.setIntegrationLogging(false);
        Boolean result = passport.isExistingVerifiedUser();

        Test.stopTest();

        System.assertEquals(false, result);
    }


    @isTest
    static void userExistsResponseTest() {
        Test.setMock(HttpCalloutMock.class, new UserExistsAndVerifiedMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('exists@test.com');
        passport.setIntegrationLogging(false);
        Boolean result = passport.isExistingUser();
        Test.stopTest();
        System.assertEquals(true, result);
    }


    @isTest
    static void userVerifiedResponseTest() {
        Test.setMock(HttpCalloutMock.class, new UserExistsAndVerifiedMockResponse());
        Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('exists@test.com');
        passport.setIntegrationLogging(false);
        Boolean result = passport.isExistingVerifiedUser();
        Test.stopTest();
        System.assertEquals(true, result);
    }


	static testMethod void testEmailCheckExistVerifiedCondition() 
     {

        String json = '{"id": "ID-devqa-e9e9361d-eabc-42eb-b807-41b857efe682","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","exists": true,"isverified": true}}';
        Test.setMock(HttpCalloutMock.class, new UserExistsAndVerifiedMockResponse());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');
		
		Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;
		
		Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
		Boolean bResul = passport.isExistingVerifiedUser();
		Test.stopTest();

		//verify outbound request record in SF
		list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, dbResults.size());
        System.debug('dbResults='+dbResults);

		system.assertEquals(json, dbResults[0].Request_Response__c);
		system.assertEquals(bResul, true);

    }
    static testMethod void testEmailCheckExistNotVerifiedCondition() 
    {

        String json = '{"id": "ID-devqa-e9e9361d-eabc-42eb-b807-41b857efe682","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","exists": true,"isverified": false}}';
        Test.setMock(HttpCalloutMock.class, new PassportEmailCheckExistNotVerifiedMock());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');
		
		Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;
		
		Test.startTest();
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
		Boolean bResul = passport.isExistingVerifiedUser();
		Test.stopTest();

		//verify outbound request record in SF
		list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, dbResults.size());
        System.debug('dbResults='+dbResults);

		system.assertEquals(json, dbResults[0].Request_Response__c);
		system.assertEquals(bResul, false);

    }
    static testMethod void testEmailCheckErrorCondition() 
    {
        String json = '{id": "ID-dev-6r87t87587","result": "error","code": "500","application": "rmit-system-api-idam","provider": "idam","payload":{ "success": false, "message": "iDAM is down / ipaas is not able to call IDAM" }}';
        Test.setMock(HttpCalloutMock.class, new PassportEmailCheckExistErrorMock());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');
		
		Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;
        
		try {
			Test.startTest();
            PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
		    Boolean bResul = passport.isExistingVerifiedUser();
			Test.stopTest();
            System.assert(false);
		} catch (HttpCalloutException e){
            // Expecting an exception due to the server returning 500 response
            System.assert(true);
        } catch(Exception e ) {
            System.assert(false);
	    	system.debug('Error:There is an exception');
	    }
    }
    static testMethod void testEmailCheckEmptyEmailCondition() 
    {
    	try {
			Test.startTest();
            PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('');
			Test.stopTest();
            System.assert(false);
		} catch (ArgumentException e) {
            // Expecting the exception to be thrown when there is a null parameter
            System.assert(true);
        } catch(Exception e ){
            System.assert(false);
            system.debug('Error:There is an exception');
	    }

    }

    // COMMENTING OUT TEST - doesn't not seem to be supported by API
    /*
    static testMethod void testEmailCheckDuplicateEmailCondition() 
    {

        String json = '{"id": "ID-dev-6r87t87587","result": "success","code": "200","application": "rmit-system-api-idam","provider": "idam","payload":{ "success": false, "message": "Multiple matches were found." }}';
        Test.setMock(HttpCalloutMock.class, new PassportEmailCheckDuplicateMock());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');
		
		Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;
		
		Test.startTest();        
        PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
		Boolean bResul = passport.isExistingVerifiedUser();
		Test.stopTest();

		//verify outbound request record in SF
		list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
		system.assertEquals(1, dbResults.size());
        System.debug('dbResults='+dbResults);

		system.assertEquals(json, dbResults[0].Request_Response__c);
		system.assertEquals(bResul, false);

    }
    */
    static testMethod void testEmailCheckNullCondition() 
    {

        String json = '{"id": "ID-dev-6r87t87587","result": "success","code": "200","application": "rmit-system-api-idam","provider": "idam","payload":null}';
        Test.setMock(HttpCalloutMock.class, new PassportEmailCheckPayLoadNull());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');
		
		Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;
		
		Test.startTest();
        try {
            PassportIdentityProviderUserStatusReq passport = new PassportIdentityProviderUserStatusReq('test@test.com');
            Boolean bResul = passport.isExistingVerifiedUser();
        } catch(HttpCalloutException e) {
            // invalid payload from IPaaS
            System.assert(true);
        } finally {
            //verify outbound request record in SF
            list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
            system.assertEquals(1, dbResults.size());
            System.debug('dbResults='+dbResults);

            system.assertEquals(json, dbResults[0].Request_Response__c);

        }
		Test.stopTest();

    }
    //*************************************************************************************************//

    // Need to confirm with Prashant what the proper response is
    // This is the response IPaaS is currently returning
    private class UserDoesNotExistMockResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"id": "ID-devqa-e9e9361d-eabc-42eb-b807-41b857efe682","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","exists": false}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }

    private class UserExistsAndVerifiedMockResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"id": "ID-devqa-e9e9361d-eabc-42eb-b807-41b857efe682","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","exists": true,"isverified": true}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    private class PassportEmailCheckExistNotVerifiedMock implements HttpCalloutMock 
	{
        public HTTPResponse respond(HTTPRequest request)
        {
            // Create a fake response
            String jsonBody = '{"id": "ID-devqa-e9e9361d-eabc-42eb-b807-41b857efe682","result": "SUCCESS","code": "200","application": "rmit-system-api-idm","provider": "idm","payload": {"success": true,"message": "","exists": true,"isverified": false}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
        
    }
    private class PassportEmailCheckExistErrorMock implements HttpCalloutMock 
	{
        public HTTPResponse respond(HTTPRequest request)
        {
            // Create a fake response
            String jsonBody = '{id": "ID-dev-6r87t87587","result": "error","code": "500","application": "rmit-system-api-idam","provider": "idam","payload":{ "success": false, "message": "iDAM is down / ipaas is not able to call IDAM" }}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(500);
            return response;
        }
        
    }

    private class PassportEmailCheckDuplicateMock implements HttpCalloutMock 
	{
        public HTTPResponse respond(HTTPRequest request)
        {
            // Create a fake response
            String jsonBody = '{"id": "ID-dev-6r87t87587","result": "success","code": "200","application": "rmit-system-api-idam","provider": "idam","payload":{ "success": false, "message": "Multiple matches were found." }}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
            
    }

    private class PassportEmailCheckPayLoadNull implements HttpCalloutMock 
	{
        public HTTPResponse respond(HTTPRequest request)
        {
        // Create a fake response
        String jsonBody = '{"id": "ID-dev-6r87t87587","result": "success","code": "200","application": "rmit-system-api-idam","provider": "idam","payload":null}';
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(jsonBody);
        response.setStatusCode(200);
        return response;
        }
        
    }

}