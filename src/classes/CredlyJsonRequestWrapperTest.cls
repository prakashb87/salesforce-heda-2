@IsTest
public class CredlyJsonRequestWrapperTest {
	
	static testMethod void testParse() {
		String json = '{  '+
		'   \"email\":\"test@test.com\",'+
		'   \"badgeDetails\":[  '+
		'      {  '+
		'         \"courseConnectionId\":\"\",'+
		'         \"badgeId\":\"\"'+
		'      },'+
		'      {  '+
		'         \"courseConnectionId\":\"\",'+
		'         \"badgeId\":\"\"'+
		'      }'+
		'   ]'+
		'}';
		CredlyJsonRequestWrapper obj = CredlyJsonRequestWrapper.parse(json);
		System.assert(obj != null);
	}
}