/*********************************************************************************
 *** @Class  			: PEValuesOnContactBatch
 *** @Author		    : Shubham Singh
 *** @Requirement     	: Batch to updated the NumberOfActiveProgramEnrollment on Contact
 *** @Created date    	: 14/11/2017
 **********************************************************************************/
/*****************************************************************************************
 *** @About Class
 *** This class is a Batch class that is used to update the previous records of contact
 *** according to the related ProgramEnrollment.
 *****************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global class PEValuesOnContactBatch implements Database.Batchable < sObject > , Database.Stateful {
	// Method to pass the records of ProgramEnrollment to the execute Method.
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT ID,Program_Action_Value__c,Action_Date__c,Program_Value__c,Program_Action__c,hed__Graduation_Year__c,hed__Contact__c FROM hed__Program_Enrollment__c'
        );
    }
	//Method to process the related values of ProgramEnrollment On contact
    global void execute(Database.BatchableContext bc, List < hed__Program_Enrollment__c > scope) {
        // process each batch of records
        set < Id > contactId = new set < Id > ();
        Map < id, Contact > mapContacts = new Map < id, Contact > ();
        for (hed__Program_Enrollment__c qPE: scope) {
            if (qPE.hed__Contact__c != null)
            {
                contactId.add(qPE.hed__Contact__c);
            }
        }
        for (hed__Program_Enrollment__c hPE: scope) {
            Contact c = new contact(Id = hPE.hed__Contact__c);
            for (hed__Program_Enrollment__c newPE: [select id, Action_Date__c, Program_Value__c, Program_Action__c, hed__Graduation_Year__c from hed__Program_Enrollment__c where hed__Contact__c =: hPE.hed__Contact__c Order by CreatedDate Desc]) {
                c.Action_Date__c = newPE.Action_Date__c;
                c.Program__c = newPE.Program_Value__c;
                c.Program_Action__c = newPE.Program_Action__c;
                c.Class_Year__c = newPE.hed__Graduation_Year__c;

            }
            mapContacts.put(c.Id, c);
        }
        //Update if Contacts and associated values are added
        if (mapContacts.size() > 0) {
            Database.update(mapContacts.values(), false);
        }
    }
	//Method of Impemented class
    global void finish(Database.BatchableContext bc) {
         system.debug('PEValuesOnContactBatch--finish');
    }
}