/*******************************************
Purpose: To create wrapper class for Ethics Details
History:
Created by Ankit Bhagat on 20/09/2018
*******************************************/

public class EthicsDetailWrapper {
    
    /*****************************************************************************************
	// Purpose      :  Viewing Ethics
	// Developer    :  Ankit Bhagat	
	// Created Date :  09/20/2018                 
	//************************************************************************************/ 
    public class ethicsWrapper{
        
        @AuraEnabled public string ethicsTitle;
        @AuraEnabled public string type;
        @AuraEnabled public string status;
        @AuraEnabled public string projectTitle;
        @AuraEnabled public string projectId;
        @AuraEnabled public date approvedDate;
        @AuraEnabled public date expiryDate;
        @AuraEnabled public string ethicsId;
        @AuraEnabled public String position;
        
        public ethicsWrapper(){
            
            ethicsTitle  = '';
            type         = '';
            status       = '';
            projectTitle = '';
            projectId    = '';
            ethicsId     = '';
            position	 = '';
        }
        
        public ethicsWrapper(Ethics__c ethics){
            
            ethicsTitle  = ethics.Application_Title__c;
            type         = ethics.Application_Type__c;
            status       = ethics.Status__c;
            approvedDate = ethics.Approved_Date__c;
            expiryDate   = ethics.Expiry_Date__c;
            ethicsId     = ethics.Id;
            
        }
        
        public ethicsWrapper(Ethics_Researcher_Portal_Project__c ethicsproject){
            
            ethicsTitle   = ethicsproject.Ethics__r.Application_Title__c;
            type          = ethicsproject.Ethics__r.Application_Type__c;
            status        = ethicsproject.Ethics__r.Status__c;
            projectTitle  = ethicsproject.Researcher_Portal_Project__r.Title__c;
            projectId     = ethicsproject.Researcher_Portal_Project__r.Id;
            approvedDate  = ethicsproject.Ethics__r.Approved_Date__c;
            expiryDate    = ethicsproject.Ethics__r.Expiry_Date__c;
            ethicsId      = ethicsproject.Ethics__c;
            
        }
        
    }
    
}