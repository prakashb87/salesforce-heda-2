/*********************************************************************************
*** @TestClassName     : Admission_Rest_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Admission_REST web service apex class.
*** @Created date      : 01/06/2018
*** @Modified by       : DINESH KUMAR
*** @modified date     : 06/10/2018
**********************************************************************************/
@isTest
public class Admission_Rest_Test {
    
  static testMethod void testUpsertAdmission() {
        //creating test data
        //insert contacts
        Contact con1 = new Contact(FirstName = 'TestCon', LastName = 'Test', Student_ID__c = '88002P');
        insert con1;
        Contact con2 = new Contact(FirstName = 'Testing', LastName = 'TestNew', Student_ID__c = '88003Q');
        insert con2;
        //insert rmitu account
        //id, AccountNumber, ParentID, Academic_Institution__c, , Effective_Date__c,
        Account rmit = new Account(Name = 'rmitu');
        insert rmit;
      	system.debug('---TEST-----RMIT Account------'+rmit);
      
      Account acc = new Account();
      acc.Name = 'test';
      acc.Academic_Institution__c = rmit.Id;
      acc.Academic_Career__c = 'TULIP';
      acc.Effective_Date__c = Date.ValueOf('2002-12-28 00:00:00');
      acc.Academic_Program__c = '9873A';
      insert acc;
      
        //insert parent account
        Account ac = new Account(Name = 'Parent');
        insert ac;
        //insert accounts
        Account acc1 = new Account(Name = 'Admission Test', Academic_Institution__c = rmit.ID, AccountNumber = '9873A', Effective_Date__c = Date.ValueOf('2000-12-28'), ParentId = ac.ID, Academic_Career__c = 'TULIP');
        insert acc1;
        Account acc2 = new Account(Name = 'Testing', Academic_Institution__c = rmit.ID, AccountNumber = '9874B', Effective_Date__c = Date.ValueOf('2002-12-28'), ParentId = ac.ID, Academic_Career__c = 'PULIP');
        insert acc2;
        Account acc3 = new Account(Name = 'Admission Three', Academic_Institution__c = rmit.ID, AccountNumber = '9874B', Effective_Date__c = Date.ValueOf('2002-06-28'), ParentId = ac.ID, Academic_Career__c = 'PULIP');
        insert acc3;
        //insert affiliation for already present record
        hed__Affiliation__c hedAff = new hed__Affiliation__c();
        hedAff.hed__Contact__c = con2.Id;
        hedAff.hed__Account__c = ac.Id;
        hedAff.RecordTypeId = Schema.SObjectType.hed__Affiliation__c.getrecordtypeinfosbydevelopername().get('Student').getRecordTypeId();
        insert hedAff;
        //insert Program Enrollment for update
        hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c();
        pe.Academic_Plan__c = 'AE08AB';
        pe.Career__c = 'TULIP';
        pe.Student_Career_Number__c = '12345';
        pe.Plan_Sequence__c = '10';
        pe.Effective_Date__c = Date.ValueOf('2002-12-28');
        pe.Effective_Sequence__c = '0';
        insert pe;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        String JsonMsg = '[{"studentId": "88002P","academicCareer": "TULIP","studentCareerNumber": "12345","effectiveDate": "2002-12-28","effectiveSequence": 0,"institution": "rmitu","academicProgram": "9873A","programStatus": "AC","programAction": "ACTV","actionDate": "2000-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" },{ "studentId": "88003Q","academicCareer": "PULIP","studentCareerNumber": 1,"effectiveDate": "2000-12-28","effectiveSequence": 1,"institution": "RMITU","academicProgram": "9874B","programStatus": "CN","programAction": "WADM","actionDate": "2001-10-21","actionReason": "CONV","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AIOB","planSequence": "123","cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-01","enrolmentDate": ""} ]';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Admission_REST.upsertAdmission();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(true, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        //creating test data
        String JsonMsg = '{"studentId": "","academicCareer": "TULIP","studentCareerNumber": "12345","effectiveDate": "2000-12-28","effectiveSequence": 0,"institution": "RMITU","academicProgram": "9873A","programStatus": "AC","programAction": "ACTV","actionDate": "2000-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" } ';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Admission_REST.upsertAdmission();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = Admission_REST.upsertAdmission();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void coverElse1() {
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        //creating test data
        String JsonMsg = '{"studentId": "hduishi","academicCareer": "TULIP","studentCareerNumber": "12345","effectiveDate": "2000-12-28","effectiveSequence": 0,"institution": "RMITU","academicProgram": "NJUIOY","programStatus": "AC","programAction": "ACTV","actionDate": "2000-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" } ';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Admission_REST.upsertAdmission();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverElse2() {
        //insert institution
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert parent account
        Account ac = new Account(Name = 'Parent');
        insert ac;
        //insert accounts
        Account acc1 = new Account(Name = 'Admission Test', Academic_Institution__c = rmit.ID, AccountNumber = '9873A', Effective_Date__c = Date.ValueOf('2000-12-28'), ParentId = ac.ID, Academic_Career__c = 'TULIP');
        insert acc1;
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        //creating test data
        String JsonMsg = '{"studentId": "hduishi","academicCareer": "TULIP","studentCareerNumber": "12345","effectiveDate": "2000-12-28","effectiveSequence": 0,"institution": "RMITU","academicProgram": "9873A","programStatus": "AC","programAction": "ACTV","actionDate": "2000-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" } ';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = Admission_REST.upsertAdmission();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
       
    //static testMethod void testCreateFutChange() {
		//Admission_REST.createFutChange('Enrolment_Date__c');
    //}
    
    static testMethod void testCaseMatching() {
        
       	Fund_Source__c newfs = new Fund_Source__c();
        newfs.Name = '02';
        insert newfs;
        
        hed__Program_Enrollment__c proEnrollRecord = new hed__Program_Enrollment__c();
        proEnrollRecord.hed__Start_Date__c = system.today();
        proEnrollRecord.Student_Career_Number__c = '12345';
        proEnrollRecord.Effective_Date__c = Date.Valueof('2000-12-28');
        proEnrollRecord.Effective_Sequence__c = '0';
        proEnrollRecord.Program_Status__c = 'ACT';
        proEnrollRecord.Program_Action__c = 'ACTVV';
        proEnrollRecord.Action_Date__c = Date.Valueof('2003-12-28');
        proEnrollRecord.Action_Reason__c = 'action Reason';
        proEnrollRecord.Admit_Term__c = '0084';
        proEnrollRecord.Approved_Academic_Load__c = 'FF';
        proEnrollRecord.Campus__c = 'AUSC';
        proEnrollRecord.Degree_Checkout_Status__c = 'T';
        proEnrollRecord.Completion_Term__c = 'T';
        proEnrollRecord.Dual_Academic_Program__c = 'T';
        proEnrollRecord.Plan_Sequence__c = '11';
        proEnrollRecord.Cohort_Year__c = '0001';
        proEnrollRecord.Funding_Source__c = newfs.id;
        proEnrollRecord.Research_Commencement_Date__c = Date.Valueof('1997-02-01');
        proEnrollRecord.Enrolment_Date__c = Date.Valueof('2001-01-29');
        
        insert proEnrollRecord;
        //lstProEnroll.add(proEnrollRecord);
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        String JsonMsg = '{"studentId": "hduishi","academicCareer": "TULIP","studentCareerNumber": "12345","effectiveDate": "2001-12-28","effectiveSequence": 0,"institution": "RMITU","academicProgram": "9873A","programStatus": "AC","programAction": "ACTV","actionDate": "2001-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" } ';
        
        Admission_REST postString = (Admission_REST) System.JSON.deserialize(JsonMsg , Admission_REST.class);
        
        Admission_REST.createFutChange('Effective_Date__c');
        
        Admission_REST.caseMatching(postString, proEnrollRecord);    
    }
        
    static testMethod void testLeaveOfAbsenceRecord() {
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        
        RestRequest req1 = new RestRequest();
        
        RestResponse res1 = new RestResponse();
        // adding header to the request
        req1.addHeader('httpMethod', 'POST');
        
        req1.requestUri = '/services/apexrest/HEDA/v1/Admission';
        
        String JsonMsg = '{"studentId": "128791","academicCareer": "TULIP1","studentCareerNumber": "128345","effectiveDate": "2001-12-28","effectiveSequence": 0,"institution": "RMITU","academicProgram": "9873A","programStatus": "LA","programAction": "ACTV","actionDate": "2001-12-28","actionReason": "","admitTerm": "0083","approvedAcademicLoad": "F","campus": "AUSCY","degreeCheckoutStatus": "","completionTerm": "","dualAcademicProgram": "","plan": "AE08AB","planSequence": 10,"cohortYear": "0000","fundingSource": "03","researchCommencementDate": "1996-02-29","enrolmentDate": "2000-01-29" }';
        req1.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req1;
        
        RestContext.response = res1;
        
        Admission_REST postString = (Admission_REST) System.JSON.deserialize(JsonMsg , Admission_REST.class);
        Map < String, String > check = Admission_REST.upsertAdmission();
    }
}