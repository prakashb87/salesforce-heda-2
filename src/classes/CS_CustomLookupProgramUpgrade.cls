@SuppressWarnings('PMD.ApexSharingViolations,PMD.AvoidGlobalModifier')
global class CS_CustomLookupProgramUpgrade extends cscfga.ALookupSearch {

    public static map<Id, cspmb__Price_Item__c> mapResults;
    
    public override String getRequiredAttributes() {
        return '["DeliveryMode","ProductLine","PriceItemId","ContactId","CourseType"]'; 
    }

    private Object[] doSearch(Map<String, String> searchFields, String productDefinitionID) {
        
        List<cspmb__Price_Item__c> results = new List<cspmb__Price_Item__c>();
        
        string statusCompleted = Config_Data_Map__c.getInstance('CourseConnectionStatus_Completed').Config_Value__c;
        string statusEnrolled = Config_Data_Map__c.getInstance('CourseConnectionStatus_Enrolled').Config_Value__c;
        
        string attPriceItemId = searchFields.get('PriceItemId');
        string attContactId = searchFields.get('ContactId');
        string attDeliveryMode = searchFields.get('DeliveryMode');
        string courseType = searchFields.get('CourseType');
        
        system.debug('CS_CustomLookupPriceItem: attPriceItemId>> ' + attPriceItemId);
        system.debug('CS_CustomLookupPriceItem: attContactId >> ' + attContactId);
        system.debug('CS_CustomLookupPriceItem: attDeliveryMode >> ' + attDeliveryMode);
        
        List<cspmb__Price_Item__c> priceItems = [select Id 
                , Name     
                , cspmb__Effective_Start_Date__c   
                , cspmb__Effective_End_Date__c     
                , cspmb__Price_Item_Code__c    
                , cspmb__Product_Definition_Name__c    
                , Course_ID__c     
                , Course_Offering_ID__c    
                , Fees_ID__c   
                , Session_ID2__c   
                , Course_Program__c  
                , Program__r.Name
                , GST_Applicable__c    
                , Program_Plan__c  
                , Product_Line__c  
                , Overhead_Price__c    
                , cspmb__Is_Active__c
                , cspmb__One_Off_Charge__c
                , cspmb__One_Off_Cost__c
                , List_Price__c
                , ( select cspmb__Add_On_Price_Item__r.Id
                        , List_Price__c
                        , cspmb__One_Off_Charge__c
                        , cspmb__Add_On_Price_Item__r.Name
                        , cspmb__Add_On_Price_Item__r.cspmb__One_Off_Charge__c
                        , cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c
                    from 
                        cspmb__Price_Item_Add_On_Price_Item_Association__r 
                    where 
                        cspmb__Add_On_Price_Item__r.Name = :attDeliveryMode)
            from 
                cspmb__Price_Item__c
            where
                Id = :attPriceItemId];
        
        system.debug('priceItems >> ' + priceItems);
        
        if (priceItems.size() > 0 && courseType == 'Program') { 
            
            cspmb__Price_Item__c priceItem = priceItems[0];
            cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAssoc = priceItem.cspmb__Price_Item_Add_On_Price_Item_Association__r[0];
            
            if (String.isNotBlank(priceItem.Program__r.Name)) {
                
                Decimal programListPrice = priceItemAssoc.List_Price__c; //priceItem.cspmb__One_Off_Cost__c;
                Decimal programSalePrice = priceItemAssoc.cspmb__One_Off_Charge__c;
                Decimal upgradeDiscount = ((programListPrice - programSalePrice) / programListPrice) * 100;
                
                // collect courses within the program
                set<id> setProgCourseId = new set<id>();
                for (hed__Plan_Requirement__c pr : [select hed__Course__c 
                    from hed__Plan_Requirement__c
                    where hed__Program_Plan__c = :priceItem.Program_Plan__c]) {
                    setProgCourseId.add(pr.hed__Course__c);
                }
                
                // collect coursee within the program that are not enrolled or completed
                map<Id, Id>  mapEnrolledCourseIdToCourseOfferingId = new map<Id, Id>(); //setProgEnrolledCourseId = new set<id>();
                for (hed__Course_Enrollment__c enr: [select hed__Course_Offering__r.hed__Course__c
                    from 
                        hed__Course_Enrollment__c
                    where 
                        hed__Contact__c = :attContactId and                     
                        hed__Course_Offering__r.hed__Course__c in :setProgCourseId and
                        (hed__Status__c = :statusCompleted or hed__Status__c = :statusEnrolled )]) {
                    mapEnrolledCourseIdToCourseOfferingId.put(enr.hed__Course_Offering__r.hed__Course__c, enr.hed__Course_Offering__c);
                }
                
                system.debug('mapEnrolledCourseIdToCourseOfferingId >> ' + mapEnrolledCourseIdToCourseOfferingId);
                
                // calculate the sum of the list prices of the courses that are "enrolled" or "completed". 
                Decimal sumCourseEnrolledListPrice = 0.0;
                set<Id> setProgEnrolledCourseId = mapEnrolledCourseIdToCourseOfferingId.keySet();
                for (cspmb__Price_Item_Add_On_Price_Item_Association__c assoc : [select Id, Name
                        , List_Price__c
                        , cspmb__One_Off_Charge__c
                        , cspmb__Price_Item__r.Course_Offering_ID__c
                        , cspmb__Price_Item__r.Course_Offering_ID__r.hed__Course__c
                        , cspmb__Add_On_Price_Item__r.cspmb__One_Off_Charge__c
                        , cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c
                    from 
                        cspmb__Price_Item_Add_On_Price_Item_Association__c 
                    where 
                        cspmb__Add_On_Price_Item__r.Name = :attDeliveryMode and 
                        cspmb__Price_Item__r.Course_Offering_ID__r.hed__Course__c in :setProgEnrolledCourseId]) {
                    if (mapEnrolledCourseIdToCourseOfferingId.get(assoc.cspmb__Price_Item__r.Course_Offering_ID__r.hed__Course__c) == assoc.cspmb__Price_Item__r.Course_Offering_ID__c){
                        sumCourseEnrolledListPrice += assoc.List_Price__c; 
                    }
                }
                
                system.debug('sumCourseEnrolledListPrice >> ' + sumCourseEnrolledListPrice);
                
                Boolean isUpgrade = setProgEnrolledCourseId.size() > 0;
                
                Decimal upgradeCourseCount = isUpgrade ? (setProgCourseId.size() - setProgEnrolledCourseId.size()) : 0;
                
                // calculate the difference between program's list price and the sum of all the list prices for the courses done
                Decimal upgradePrice = programListPrice - sumCourseEnrolledListPrice;
                
                // apply the same upgrade discount percentage on the program to course enrolled sum difference
                Decimal upgradeDiscountedPrice = upgradePrice - (upgradePrice * (upgradeDiscount/100));
                
                // create new result price item
                results.add(
                    new cspmb__Price_Item__c (Upgrade_Discount__c = upgradeDiscount
                        , Upgrade_Price__c = upgradePrice
                        , Upgrade_Price_Discounted__c = upgradeDiscountedPrice
                        , Is_Upgrade__c = isUpgrade
                        , Upgrade_Course_Count__c = upgradeCourseCount));
            }
        }
        else{
            results.add(
                    new cspmb__Price_Item__c (Upgrade_Discount__c = 0
                        , Upgrade_Price__c = 0
                        , Upgrade_Price_Discounted__c = 0
                        , Is_Upgrade__c = false
                        , Upgrade_Course_Count__c = 0));
        }
        
        system.debug('results 2 >> ' + results);
        
        return results;
    }

    public override Object[] doDynamicLookupsearch(Map<String, String> searchFields, String productDefinitionID) {
        return doSearch(searchFields, productDefinitionID);
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
        return doSearch(searchFields, productDefinitionId);
    }
}