/**
 * @description Exception for ECB Registration failure
 */
public with sharing class ECBRegistrationException extends Exception {}