/*******************************************
Purpose: To create different Utility Class to move simple frequent checks
History:
Created by Khushman Deomurari on 05/02/2019
*******************************************/

public with sharing class QuickSobjectUtils {

		public static Id getFirstIDElseNull(List<SObject> sObjList){
			if (sObjList.size()>0){
				return sObjList[0].Id;
			}
			return null;
		}
		
		/* Not In Use as of now:
		
		*/

//------------------------------------Divider----------------


}