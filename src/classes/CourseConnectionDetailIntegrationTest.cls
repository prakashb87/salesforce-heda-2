/*****************************************************************************************************
*** @Class             : CourseConnectionDetailIntegrationTest
*** @Author            : Avinash Machineni 
*** @Requirement       : Test Class for  Integration between Salesforce and IPaaS for sending course connection details
*** @Created date      : 15/05/2018
*** @JIRA ID           : ECB-2954,ECB-3900
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to test integrating the records from Salesforce and IPaas for sending course connection details
*****************************************************************************************************/
@isTest
public class CourseConnectionDetailIntegrationTest {

    
    class EchoHttpMock implements HttpCalloutMock {
        HttpResponse res;
        EchoHttpMock(HttpResponse r) {
            res = r;
        }
        // This is the HttpCalloutMock interface method
        public HttpResponse respond(HttpRequest req) {
            return res;
        }
    }
    
    Static testmethod void positiveResponseTest(){
        
        TestDataFactoryUtil.test(); 
        List<hed__Course_Enrollment__c> lstcrsenr = [Select Id, Name, Enrolment_Status__c,
                                                    hed__Course_Offering__r.Name,
                                                    hed__Contact__r.Id,
                                                    hed__Contact__r.Student_ID__c,
                                                    hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c 
                                                    from hed__Course_Enrollment__c];
        Set<id> crsenrId =new Set<id>();
        for(hed__Course_Enrollment__c cenr : lstcrsenr){
            crsenrId.add(cenr.id);
        }
        HttpResponse res = new HttpResponse();
        res.setBody('{"payload": [{"courseConnectionId": "'+lstcrsenr[0].Id+'","result": "Success","errorCode": "","errorText": ""}]}');
        res.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, new EchoHttpMock(res));
        
        Test.startTest();  
        	system.assert(lstcrsenr.size() > 0, 'true');
            CourseConnectionDetailIntegration.sendCourseConn(crsenrId);
            EmbeddedPdfAndIntegrationController.sendCourseConn(crsenrId);
            system.assert(crsenrId!=null, 'true');
        Test.stopTest(); 
    }
 /*   
    Static testmethod void negativeResponseTest(){
        
        TestDataFactoryUtil.test(); 
        List<hed__Course_Enrollment__c> lstcrsenr = [Select Id, Name, Enrolment_Status__c,
                                                    hed__Course_Offering__r.Name,
                                                    hed__Contact__r.Id,
                                                    hed__Contact__r.Student_ID__c,
                                                    hed__Course_Offering__r.hed__Course__r.hed__Course_ID__c 
                                                    from hed__Course_Enrollment__c];
        Set<id> crsenrId =new Set<id>();
        for(hed__Course_Enrollment__c cenr : lstcrsenr){
            crsenrId.add(cenr.id);
        }
       
        HttpResponse res1 = new HttpResponse();
        res1.setBody('{"payload": [{"courseConnectionId": "'+lstcrsenr[1].Id+'","result": "Error","errorCode": "","errorText": ""}]}');
        res1.setStatusCode(400);
        Test.setMock(HttpCalloutMock.class, new EchoHttpMock(res1));
        
        Test.startTest();   
        	system.assert(lstcrsenr.size() > 0, 'true');
         	CourseConnectionDetailIntegration.sendCourseConn(crsenrId);
         	EmbeddedPdfAndIntegrationController.sendCourseConn(crsenrId);
         	system.assert(crsenrId!=null, 'true');
        Test.stopTest(); 
       } 
*/	   
}