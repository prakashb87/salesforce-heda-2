/*********************************************************************************
*** @Class			    : CustomSettingService
*** @Author		        : Marcelo Marsson
*** @Requirement     	: Service Class for Custom Settings
*** @Created date    	: 06/05/2019
**********************************************************************************/
/*****************************************************************************************
*** @About Class
*** Class responsible for Handling Custom Settings
*****************************************************************************************/
public with sharing class CustomSettingsService {

    private static SystemConfiguration__c systemConfiguration {

        get {
            systemConfiguration = SystemConfiguration__c.getInstance();
            //Creates the Cust. Setting if not exists
            if(systemConfiguration.ActivatorExternalId__c==null) {
                insert new SystemConfiguration__c();
                systemConfiguration = SystemConfiguration__c.getInstance();
            }

            return systemConfiguration;
    }
        private set;}


    //Returns ActivatorExternalId value for Logged In User from Custom Setting
    public static String getActivatorExternalId (){
        return systemConfiguration.ActivatorExternalId__c;
            }
}