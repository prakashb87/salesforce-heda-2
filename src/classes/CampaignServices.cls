public without sharing class CampaignServices 
{
	
    public static void populateContact(List<Campaign> newCampaign)
    {
        Set<Id> accSetID = new Set<Id>();
        for(Campaign mCampaign : newCampaign)
        {
            accSetID.add(mCampaign.School__c);
        }
        List<Contact> accContactList = new List<Contact>([SELECT Id, Name, AccountId 
                                                                FROM Contact 
                                                                WHERE AccountId =:accSetID]);
        if(accContactList.size() > 0)
        {
            Map<Id, Contact> contactMap = new Map<Id, Contact>();
            for(Contact con : accContactList)
            {
                contactMap.put(con.AccountId, con);
            }
            
            for(Campaign mCampaign : newCampaign)
            {
                if(mCampaign.School_Contact__c == NULL && contactMap.get(mCampaign.School__c) != NULL)
                {
                    mCampaign.School_Contact__c = contactMap.get(mCampaign.School__c).Id;
                }
            }
        }
    }
}