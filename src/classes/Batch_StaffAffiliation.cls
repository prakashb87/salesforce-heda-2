public class Batch_StaffAffiliation implements Database.Batchable<sObject> {
    Batch_StaffAffiliation_helper helperUpdateMethod;
    
     public Batch_StaffAffiliation(){
        helperUpdateMethod = new Batch_StaffAffiliation_helper();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC1){ 
        
        String queryString = 'SELECT Id,hed__Contact__c,hed__Contact__r.Enumber__c, hed__Contact__r.Position_Description__c, hed__Contact__r.Hire_Date__c,'+
            ' hed__Account__c, hed__Contact__r.hed__Primary_Organization__c, hed__Contact__r.Finish_Date__c '+
            'from hed__Affiliation__c WHERE hed__StartDate__c = null ';
        return Database.getQueryLocator(queryString);
    }    
    public void execute(Database.BatchableContext BC1,List <hed__Affiliation__c> scope )
    {
        helperUpdateMethod.updateAffiliation(scope);
    }
    public void finish(Database.BatchableContext BC1){  
    	system.debug('Batch_StaffAffiliation/finish');
    } 
    
}