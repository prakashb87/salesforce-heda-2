public class MockUnverifiedIdentityProviderUserStatus implements IdentityProviderUserStatusReq
{
	public void setIntegrationLogging(Boolean isLogging)
	{
	 	system.debug('Not Required');
	 }

    public boolean isExistingUser()
    {
        return true;
    }

    public boolean isExistingVerifiedUser()
    {
    	return false;
    }
}