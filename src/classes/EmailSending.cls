/*****************************************************************
Name: EmailSending
Author: Capgemini 
Purpose: To create dynamic content for Invoice pdf and send an email with the pdf attached 
ECB-3737:Do not send an email to the customer on auto purchase
*****************************************************************/
/*==================================================================
History
--------
Version   Author                   Date              Detail
1.0       Shreya/Praneet           08/05/2018       Initial Version
2.0       Rishabh                  11/06/2018       ECB-3737
********************************************************************/
public without sharing class EmailSending {
    /********************************************************************
    // Purpose              : To send the generated pdf                             
    // Author               : Capgemini [Praneet]
    // Parameters           : csord__Service__c ser,Blob body
    //  Returns             : void
    //JIRA Reference        : ECB-2451 : Send email with invoice
    //********************************************************************/

    List <case>caseList = new list <case >();

    public void sendPdf(csord__Service__c ser, Blob body) 
    {
        //--------------Fix for 4257-------------------------//
        String owEmailAddressId = generateOWEmailId();
        system.debug('owEmailAddressId=' + owEmailAddressId);
        //--------------Fix for 4257-------------------------//
        system.debug('ser:::::'+ser);
        system.debug('body:::::'+body);
        
       
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setContentType('application/pdf');
            attach.setFileName('Payment Invoice.pdf');
            attach.setInline(false);
            attach.Body = body;
    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            String[] toAddresses = new String[] {
                ser.csordtelcoa__Main_Contact__r.Email
            };
            mail.setToAddresses(toAddresses);
            //--------------Fix for 4257-------------------------//
             mail.setOrgWideEmailAddressId(owEmailAddressId);
            //--------------Fix For 4257 ------------------------//
            system.debug(toAddresses.get(0));
            mail.setTargetObjectId(ser.csordtelcoa__Main_Contact__r.id);
            mail.setTemplateId(ConfigDataMapController.getCustomSettingValue('invoiceEmailTemplate')); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {
                attach
            });
            
            mail.setSaveAsActivity(true);
    
            // Send the email 
            //ECB-3737 Do not send an email to the customer on auto purchase 
            if(!System.isBatch()){ 
            try{  
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                   mail
                });
            }catch(exception ex){
                 system.debug('Exception:::'+ex);
            }   
            }
        
        caseList = [select id, contactId, recordtypeID from
            case where recordtypeId = :ConfigDataMapController.getCustomSettingValue('PaymentCaseRecordID')
            AND ContactId = :ser.csordtelcoa__Main_Contact__r.Id
        ];
        //Updated code for JIRA 3217 for storing email into case
        Attachment attachment1 = new Attachment();
        // Creating a case for the email
        case c = new case ();
        if (caseList.size() == 0) {
            c.RecordTypeId = ConfigDataMapController.getCustomSettingValue('PaymentCaseRecordID'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.Origin = ConfigDataMapController.getCustomSettingValue('PaymentCaseEmail'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.Status = ConfigDataMapController.getCustomSettingValue('PaymentCaseStatus'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            c.description = mail.getPlainTextBody(); 
            c.subject = mail.subject;
            c.ContactId = ser.csordtelcoa__Main_Contact__r.Id;
            // Case Type Fix - ECB-4269
            c.Type = 'MarketPlace';
            // 
            
            try{
                insert c;
            }catch(DmlException ex){
                system.debug('Exception:::'+ex);
            }    

           // Creating a attachment for the email
            attachment1.Body = body;
            attachment1.Name = 'invoice';
            attachment1.ParentId = c.id;
            attachment1.ContentType = '.pdf';
            
             try{
                insert attachment1;
             }catch(DmlException ex){
                system.debug('Exception:::'+ex);
             }      
                
        } else {
            attachment1.Body = body;
            attachment1.Name = 'invoice';
            attachment1.ParentId = caseList[0].id;
            attachment1.ContentType = '.pdf';
            
            try{
                insert attachment1;
            }catch(DmlException ex){
                system.debug('Exception:::'+ex);
            }   
        }
        // Creating a task for the email
        Task t = new Task();
        t.subject = mail.subject;
        t.Status = ConfigDataMapController.getCustomSettingValue('PaymentTaskStatus'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
        t.description = mail.getPlainTextBody();
        if(caseList.size() > 0)
        {
            t.WhatId = caseList[0].Id;
        }
        else
        {
           t.WhatId = C.Id;
        t.WhoId=ser.csordtelcoa__Main_Contact__r.Id;
        }
        try{
            insert t; 
        }catch(DmlException ex){
            system.debug('Exception:::'+ex);
        } 
              
    }

    Public static String generateOWEmailId()
    {
        string owEmailAddressId;
        OrgWideEmailAddress owea = [SELECT Address,DisplayName FROM OrgWideEmailAddress where DisplayName = 'PaymentInvoiceEmail' LIMIT 1];
        if ( owea != null ) 
        {
            owEmailAddressId = owea.Id;
        }
        else
        {
            owEmailAddressId ='';
        }
        return owEmailAddressId;
    }
}