public without sharing class AllProductBasketTriggersHandler {

    public static void handleBeforeInsert(List<cscfga__Product_Basket__c> newList) {
        updateBasketOwner(newList);
    }

    public static void updateBasketOwner(List<cscfga__Product_Basket__c> newList) {
        
        set<Id> setOpptyIds = new set<Id>();
        for (cscfga__Product_Basket__c basket: newList) {
            setOpptyIds.add(basket.cscfga__Opportunity__c);
        }
        
        map<Id, Id> mapOpptyIdToContactId = new map<Id, Id>();
        for (Opportunity oppty: [select Id, Assign_To_Contact__c 
            from Opportunity 
            where id in: setOpptyIds]) {
            mapOpptyIdToContactId.put(oppty.id, oppty.Assign_To_Contact__c);
        }
        
        map<Id, Id> mapContactIdUserId = new map<Id, Id>();
        for (User user: [select Id, ContactId 
            from User 
            where IsActive = true 
                and ContactId <> '' 
                and ContactId in: mapOpptyIdToContactId.values()]) { 
            mapContactIdUserId.put(user.ContactId, user.Id);
        }
        
        for (cscfga__Product_Basket__c basket: newList) {
            if (mapOpptyIdToContactId.get(basket.cscfga__Opportunity__c) != null) {
                basket.OwnerId = mapContactIdUserId.get(mapOpptyIdToContactId.get(basket.cscfga__Opportunity__c));  
            }
        }
    }
}