/**
* -------------------------------------------------------------------------------------------------+
* This class wil schedule the batch after each hour
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        0.0
* @created        2019-02-04
* @modified       -
* @modified by    -
* @Reference      - RM-2397
* -------------------------------------------------------------------------------------------------+
*/
public with sharing class Batch_LeadAutoConvert_Scheduler implements Schedulable {
    public void execute(SchedulableContext ctx){
        Batch_LeadAutoConvert bLeadAutoConverter = new Batch_LeadAutoConvert();
        RMITO_Lead_Auto_Convert__c csRmito = RMITO_Lead_Auto_Convert__c.getValues('BatchNotificationConfig');
        Database.executeBatch(bLeadAutoConverter,200);
        Batch_LeadAutoConvert_Scheduler scheduleBatch = new Batch_LeadAutoConvert_Scheduler();
        DateTime sysTime = System.now();
        Integer addMorehours = Integer.valueOf(csRmito.BatchRunFrequency__c);
        sysTime = sysTime.addHours(addMorehours);
        String scheduleInterval = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        String scheduledJobId = System.schedule('RMITO Converting Leads Job'+scheduleInterval, scheduleInterval, scheduleBatch);
    }

}