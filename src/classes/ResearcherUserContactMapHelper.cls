public without sharing class ResearcherUserContactMapHelper {
	
	
	 //This method to get the contact id for community user and dual access user and associate the values.  
   public static Id getContactId(id userId){
    	
    	Id conId;
    	//Checking Accessibility of Case Object
        if((Schema.sObjectType.User.isAccessible()) && (Schema.sObjectType.User.fields.contactId.isAccessible())){
            System.debug('Not Accessible');
        }
        User userDetails = [Select id,contactId, FederationIdentifier, Profile.UserLicense.Name from User where Id =:userId LIMIT 1];
    	
    	if(userDetails.contactId != null) {
    		conId = userDetails.contactId;
    	}
    	else {
    		conId = getSfdcContactId(userDetails);
    	}
    	return conId;
    }
     //This method to get the contact id for dual access user and logic if duplicate contact for same enumber.. 
   public static Id getSfdcContactId(User userDetails){
   	   
   	   Id conId;
   	   List<Contact> contactList = new List<Contact>();
   	   
   	   //RPORW-1707
   	   if(Schema.sObjectType.Contact.fields.Enumber__c.isAccessible()){
   	   	system.debug('No Access');
   	   }
   	   
   	   if( userDetails != null && userDetails.FederationIdentifier != null && userDetails.FederationIdentifier.startsWithIgnoreCase('e')) {
   	   	
   	   	 contactList = [SELECT Id, Enumber__c, Student_ID__c ,hed__UniversityEmail__c, hed__WorkEmail__c                                            
                             FROM Contact  
                             WHERE Enumber__c =: userDetails.FederationIdentifier];
   	     	
   	    }else
   	     if (userDetails != null && userDetails.FederationIdentifier != null && userDetails.FederationIdentifier.startsWithIgnoreCase('s')){
   	     	 //1638
             String studentFedrationId = (userDetails.FederationIdentifier).removeStartIgnoreCase('S');
   	    	 contactList = [SELECT Id, Enumber__c, Student_ID__c ,hed__UniversityEmail__c, hed__WorkEmail__c                                            
                             FROM Contact  
                             WHERE Student_ID__c =: studentFedrationId];  // 1638
   	     	 
   	    }
   	        
       
        System.debug('contactList'+contactList);
        
        if(contactList.size() >= 1) {
        	conId = contactList[0].id;
        } 
        //else {
        //	conId = getContactIdForDublicateContacts(contactList,userDetails);
        //}
        
        return conId;
        
     }  

	public static Id getContactIdForDublicateContacts(List<Contact> contactList,User userDetails){
    
        Id contactId; 
        Set<Id> contactids = new Set<Id>();
        for(Contact eachCon:contactList){
            contactids.add(eachCon.Id);
        }

    
           for(hed__Affiliation__c eachAffiliation : 
           [Select id,RecordType.Name ,hed__Primary__c, hed__Contact__c from hed__Affiliation__c where hed__Contact__c IN :contactids] ){
            
               if(userDetails.FederationIdentifier.startsWithIgnoreCase('e') &&
                  eachAffiliation.RecordType.Name.equalsIgnoreCase('RMIT Staff') &&
                  eachAffiliation.hed__Primary__c == true ){                    
                    
                     contactId = eachAffiliation.hed__Contact__c; 
                     break;
                    
               } else
                   
               if(userDetails.FederationIdentifier.startsWithIgnoreCase('s') &&
                   eachAffiliation.RecordType.Name.equalsIgnoreCase('Student') &&
                   eachAffiliation.hed__Primary__c == true ){
                    
                    contactId = eachAffiliation.hed__Contact__c; 
                    break;
                    }
                }
   
           return contactId ; 
  
    }
	
	
    
}