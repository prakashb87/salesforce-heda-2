@isTest
public with sharing class TestResearcherPortalOcmsSearchLoader {
    private static final String CONTENT_TYPE = 'TestContentType';
    private static final String CONTENT_NAME = 'TestContent';
    private static final String SITE_NAME = 'TestSite';
    private static final String TAXONOMY_PATH = 'Test/Taxonomy/Path';
    private static final String SEARCH_TERM = 'term';

    static cms__Content__c[] testContents;

    static ApiRequest buildBasicApiRequest() {
        ApiRequest req = new ApiRequest();
        req.parameters = new Map<String, String> {
                'depth' => '6',
                'limitCount' => '5',
                'searchTerm' => ''
        };

        req.listParameters = new Map<String, String[]> {
                'contentTypes' => new String[] { CONTENT_TYPE },
                'tagPaths' => new String[] {}
        };

        req.requestFlags = new Map<String, Boolean> {
                'targeted' => true
        };

        return req;
    }

    static void buildTestContent() {
        // Normally we wouldn't touch any cms__ objects, even in a test
        // Here we need a valid cms__Content__c ID to insert an OCMS_Search__c record
        cms__Content__c testContent = new cms__Content__c(
                cms__Name__c = CONTENT_NAME,
                cms__Site_Name__c = SITE_NAME
        );

        insert testContent;

        testContents = new cms__Content__c[] { testContent };

        OCMS_Search__c ocmsSearch = new OCMS_Search__c(
                Content__c = testContent.Id,
                Importance__c = 50
        );

        insert ocmsSearch;
    }

    static RenderResultBundle.RenderedContent buildRenderedContent(Id originId) {
        RenderResultBundle.RenderedContent renderedContent = new RenderResultBundle.RenderedContent();
        renderedContent.originId = originId;
        renderedContent.renderMap = new Map<String, String> {
                'JsonDataTemplate' => JSON.serialize(buildContentBundle(originId))
        };

        return renderedContent;
    }

    static ContentBundle buildContentBundle(Id originId) {
        // ResearcherPortalOcmsSearchLoader only uses bundle.originId
        ContentBundle bundle = new ContentBundle();
        bundle.originId = originId;
        return bundle;
    }

    static RenderResultBundle buildRenderResultBundle() {
        RenderResultBundle bundle = new RenderResultBundle();
        bundle.renderings = new RenderResultBundle.RenderedContent[] {};
        for (cms__Content__c testContent : testContents) {
            bundle.renderings.add(buildRenderedContent(testContent.Id));
        }
        return bundle;
    }

    static FilteringBundle buildFilteringBundle() {
        FilteringBundle bundle = new FilteringBundle();
        bundle.filters = new Filter[] { new Filter() };
        bundle.filters[0].filtertype = 'TaxonomyItem';
        bundle.filters[0].children = new FilterItem[] { new FilterItem() };
        bundle.filters[0].children[0].filter = TAXONOMY_PATH;
        bundle.filters[0].children[0].originIds = new Set<Id>();

        bundle.contentBundles = new Map<Id, ContentBundle>();

        for(cms__Content__c testContent : testContents) {
            bundle.filters[0].children[0].originIds.add(testContent.Id);
            bundle.contentBundles.put(testContent.Id, buildContentBundle(testContent.Id));
        }

        return bundle;
    }

    static JsonMessage.ApiResponse buildApiResponse(Object responseObject) {
        JsonMessage.ApiResponse response = new JsonMessage.ApiResponse();
        response.isSuccess = true;
        response.responseObject = Json.serialize(responseObject);
        return response;
    }

    static testmethod void testGetContentNoSearchNoTaxonomy() {
        buildTestContent();
        ApiRequest req = buildBasicApiRequest();
        RenderResultBundle resultBundle = buildRenderResultBundle();
        JsonMessage.ApiResponse response = buildApiResponse(resultBundle);

        // 2 mock API responses needed - first one for the "get all content" call,
        // second one to fetch content after ordering/pagination is complete
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(response));
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(response));

        ResearcherPortalOcmsSearchLoader loader = new ResearcherPortalOcmsSearchLoader();

        String bundleListJson = loader.getContent(JSON.serialize(req));
        ContentBundleList bundleList = (ContentBundleList)JSON.deserialize(bundleListJson, ContentBundleList.class);

        System.assertEquals(testContents.size(), bundleList.contentBundles.size());
    }

    static testmethod void testGetContentNoSearchWithTaxonomy() {
        buildTestContent();
        ApiRequest req = buildBasicApiRequest();
        req.listParameters.put('tagPaths', new String[] { TAXONOMY_PATH });
        RenderResultBundle resultBundle = buildRenderResultBundle();
        JsonMessage.ApiResponse response = buildApiResponse(resultBundle);

        // 2 mock API responses needed - first one for the "get all content" call,
        // second one to fetch content after ordering/pagination is complete
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(response));
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(response));

        ResearcherPortalOcmsSearchLoader loader = new ResearcherPortalOcmsSearchLoader();

        String bundleListJson = loader.getContent(JSON.serialize(req));
        ContentBundleList bundleList = (ContentBundleList)JSON.deserialize(bundleListJson, ContentBundleList.class);

        System.assertEquals(testContents.size(), bundleList.contentBundles.size());
    }

    static testmethod void testGetContentWithSearchNoTaxonomy() {
        buildTestContent();
        ApiRequest req = buildBasicApiRequest();
        req.parameters.put('searchTerm', SEARCH_TERM);

        RenderResultBundle renderBundle = buildRenderResultBundle();
        JsonMessage.ApiResponse renderResponse = buildApiResponse(renderBundle);

        FilteringBundle filterBundle = buildFilteringBundle();
        JsonMessage.ApiResponse filterResponse = buildApiResponse(filterBundle);

        cms.ServiceEndPoint.addMockResponse('FilteringApi', Json.serialize(filterResponse));
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(renderResponse));

        ResearcherPortalOcmsSearchLoader loader = new ResearcherPortalOcmsSearchLoader();

        String bundleListJson = loader.getContent(JSON.serialize(req));
        ContentBundleList bundleList = (ContentBundleList)JSON.deserialize(bundleListJson, ContentBundleList.class);

        System.assertEquals(testContents.size(), bundleList.contentBundles.size());
    }

    static testmethod void testGetContentWithSearchWithTaxonomy() {
        buildTestContent();
        ApiRequest req = buildBasicApiRequest();
        req.listParameters.put('tagPaths', new String[] { TAXONOMY_PATH });
        req.parameters.put('searchTerm', SEARCH_TERM);

        RenderResultBundle renderBundle = buildRenderResultBundle();
        JsonMessage.ApiResponse renderResponse = buildApiResponse(renderBundle);

        FilteringBundle filterBundle = buildFilteringBundle();
        JsonMessage.ApiResponse filterResponse = buildApiResponse(filterBundle);

        cms.ServiceEndPoint.addMockResponse('FilteringApi', Json.serialize(filterResponse));
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(renderResponse));

        ResearcherPortalOcmsSearchLoader loader = new ResearcherPortalOcmsSearchLoader();

        String bundleListJson = loader.getContent(JSON.serialize(req));
        ContentBundleList bundleList = (ContentBundleList)JSON.deserialize(bundleListJson, ContentBundleList.class);

        System.assertEquals(testContents.size(), bundleList.contentBundles.size());
    }
}