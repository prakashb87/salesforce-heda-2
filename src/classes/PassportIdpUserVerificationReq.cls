/**
 * @description This class is using to send user verification request to IDAM 
 * @group Identity Management
 * @author Resmi Ramakrishnan  
 */
//@JIRA ID: ECB-4585 
public with sharing class PassportIdpUserVerificationReq implements IdentityProviderUserVerificationReq 
{
	/**
	 * @description This method makes a callout to Passport IDAM to update the user's 
	 * verified status to true. This method should only be called once Salesforce has 
	 * performed the necessary user verification (e.g. verifying the email provided is
	 * correct)
	 * @param emailAddress The email address of the user to be verified
	 */
	public void updateUserToVerified(String emailAddress) 
	{

		if (!String.isBlank(emailAddress))
		{
			UserVerificationRequest requestStructure = new UserVerificationRequest();
			UserVerificationRequest.Isverified innerObj = new UserVerificationRequest.Isverified();
			innerObj.add = 'true';

			requestStructure.email = emailAddress;
			List<UserVerificationRequest.Isverified> newList = new List<UserVerificationRequest.Isverified>();
			newList.add(innerObj);

			requestStructure.isverified = newList;
			String userVerificationJSON = JSON.serialize(requestStructure);	

			String endpoint = ConfigDataMapController.getCustomSettingValue('IDAMIPaaSActivateEndPoint');
			system.debug('Activate EndPoint is' + endpoint);
	        HttpRequest req = new HttpRequest();
	        req.setEndpoint(endpoint);
	        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientID'));
	        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientSecret'));
	        req.setHeader('Content-Type', 'application/json');
	        req.setMethod('PUT');
	        req.setbody(userVerificationJSON);
	        System.debug('User Verify: Request=' + userVerificationJSON);

			Http http = new Http();
       		HTTPResponse response = http.send(req);
       		System.debug('User verify : Response=' + response.getBody());

       		String responseBody = response.getBody();
			// Due to inconsistent response payload structure, the below is required for JSON deserialization
       		if (responseBody.contains('"payload": ""'))
       		{
       			responseBody = responseBody.replace('"payload": ""', '"payload": {"success":true, "message": "" }');
       		}
       		system.debug('User verify : Replaced Response=' + responseBody);

            UserVerificationResponse userCreationResponse = UserVerificationResponse.parse(responseBody);
            if (response.getStatusCode() == 200 && userCreationResponse.payload.success == true )
            {
            	System.debug('PASSPORT User Verification:Successfully Verified');
            }
            else 
            {
            	System.debug('PASSPORT User Verification:Error in verification. Status Code=' + response.getStatusCode() );
            	throw new HttpCalloutException('Unable to verify user in IDAM. getStatusCode =' + response.getStatusCode() );

            }
            
            logInegrationlog(userVerificationJSON,response.getBody());
            
		}
		else
		{
			throw new ArgumentException('Email address cannot be blank.');
			System.debug('PASSPORT VERIFY: Email Address is Blank');
		}
		
	}

	@testVisible
	private static void logInegrationlog(String userVerificationJSON, String response)
	{
		    IntegrationLog requestLog = new IntegrationLog();
	        requestLog.setRequestBody(userVerificationJSON);
	        requestLog.setType('IDAM_IPaaS_VerifyUser');
	        requestLog.setServiceType('Outbound Service');
	        requestLog.log();

	        IntegrationLog responseLog = new IntegrationLog();
	        responseLog.setRequestBody(response);
	        responseLog.setType('IDAM_IPaaS_VerifyUser');
	        responseLog.setServiceType('Acknowledgement');
	        responseLog.log();
	}

}