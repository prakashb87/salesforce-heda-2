/*******************************************
Purpose: Test class MilestonesDetailsWrapper
History:
Created by Ankit Bhagat on 31/10/2018
*******************************************/

@isTest
public class TestPublicationDetailsWrapper {

     /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Shalu
    // Created Date :  11/1/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
    
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName); 
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
        
     /************************************************************************************
    // Purpose      :  Test functionality for View PublicationDetailsWrapper 
    // Developer    :  Shalu
    // Created Date :  11/01/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void publicationDetailsWrapperMethod(){
        
       User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            
            Ethics__c ethicObj = new Ethics__c();
            ethicObj.Ethics_Id__c='TestEthics';
            insert ethicObj ;
            List<id> ethicsId = new List<id>();
            ethicsId.add(ethicObj.id);
            system.assert(ethicsId!=null);
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearchList.add(memberResearch1);
            insert memberResearchList;
                        
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            Publication__c publication = new Publication__c();
            publication.Publication_Title__c = 'Test';
            publication.Publication_Id__c = 123;
            insert publication;
            System.Assert(publication.Publication_Title__c == 'Test');          
            
            List<Publication__c> publicationsList = new List<Publication__c>();
           
                Publication__c publicationEthics = new Publication__c();
                publicationEthics.Year_Published__c       = 2018;
                publicationEthics.Publication_Category__c   = 'TestData1';
                publicationEthics.Publication_Title__c      = 'Test1';
                publicationEthics.Outlet__c     = 'Test1';
                publicationEthics.Audit_Result__c     = 'Test1';
                publicationEthics.Publication_Id__c = 1234;
                publicationsList.add(publicationEthics);   
                
                Publication__c publicationProject = new Publication__c();
                publicationProject.Year_Published__c       = 2019;
                publicationProject.Publication_Category__c   = 'TestData2';
                publicationProject.Publication_Title__c      = 'Test2';
                publicationProject.Outlet__c     = 'Test2';
                publicationProject.Audit_Result__c     = 'Test2';
                publicationProject.Publication_Id__c = 12345;
                publicationsList.add(publicationProject);  
                
                Publication__c publicationDetails = new Publication__c();
                publicationDetails.Year_Published__c       = 2017;
                publicationDetails.Publication_Category__c   = 'TestData3';
                publicationDetails.Publication_Title__c      = 'Test3';
                publicationDetails.Outlet__c     = 'Test3';
                publicationDetails.Audit_Result__c     = 'Test3';
                publicationDetails.Publication_Id__c = 12356;
                publicationsList.add(publicationDetails);          
                
            insert publicationsList;
            System.Assert(publicationsList.size()== 3);
            
            //ViewMyMilestonesHelper.getMilestonesWrapperList();
            
           
        
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper = new PublicationDetailsWrapper.publicationWrapper();
            publicationWrapper.year       = 2016;
            publicationWrapper.category   = 'TestData';
            publicationWrapper.title      = 'Test';
            publicationWrapper.outlet     = 'Test';
            publicationWrapper.status     = 'Test';
        
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper1 = new PublicationDetailsWrapper.publicationWrapper();
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper2 = new PublicationDetailsWrapper.publicationWrapper(publicationsList[0]);
        }        
    }
     /************************************************************************************
    // Purpose      :  Test functionality for View PublicationDetailsWrapper 
    // Developer    :  Shalu
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void publicationDetailsWrapperNegativeMethod(){
        
       User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 

        System.runAs(u) {            
            
            Ethics__c ethicTest1 = new Ethics__c();
            ethicTest1.Ethics_Id__c='TestEthics1';
            ethicTest1.Application_Title__c = 'Test';
            ethicTest1.Application_Type__c = 'HUMAN = Human Ethics';
            ethicTest1.Status__c = 'Amended';
            ethicTest1.Approved_Date__c = date.today();
            ethicTest1.Expiry_Date__c   = date.today().addDays(10);
            insert ethicTest1;
            
            List<Researcher_Member_Sharing__c> memberResearchList = new List<Researcher_Member_Sharing__c>();
            Researcher_Member_Sharing__c memberResearch = new Researcher_Member_Sharing__c();
            memberResearch.Ethics__c = ethicTest1.id;
            memberResearch.User__c   = u.id;
            memberResearchList.add(memberResearch);
            
            Researcher_Member_Sharing__c memberResearch1 = new Researcher_Member_Sharing__c();
            memberResearch1.Ethics__c = ethicTest1.id;
            memberResearch1.User__c   = u.id;
            memberResearchList.add(memberResearch1);
            insert memberResearchList;
                        
            ResearcherPortalProject__c researchProject = new ResearcherPortalProject__c();
            researchProject.Title__c = 'Test';
            insert researchProject;
            System.Assert(researchProject.Title__c == 'Test');
            
            Publication__c publication = new Publication__c();
            publication.Publication_Title__c = 'Test';
            publication.Publication_Id__c = 123;
            insert publication;
            System.Assert(publication.Publication_Title__c == 'Test');          
            
            List<Publication__c> publicationsList = new List<Publication__c>();
           
                Publication__c publicationEthics = new Publication__c();
                publicationEthics.Year_Published__c       = 2018;
                publicationEthics.Publication_Category__c   = 'TestData1';
                publicationEthics.Publication_Title__c      = 'Test1';
                publicationEthics.Outlet__c     = 'Test1';
                publicationEthics.Audit_Result__c     = 'Test1';
                publicationEthics.Publication_Id__c = 1234;
                publicationsList.add(publicationEthics);   
                
            insert publicationsList;
            System.Assert(publicationsList.size()== 1);
            
            //ViewMyMilestonesHelper.getMilestonesWrapperList();
            
           
        
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper = new PublicationDetailsWrapper.publicationWrapper();
            publicationWrapper.year       = 2016;
            publicationWrapper.category   = 'TestData';
            publicationWrapper.title      = 'Test';
            publicationWrapper.outlet     = 'Test';
            publicationWrapper.status     = 'Test';
        
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper1 = new PublicationDetailsWrapper.publicationWrapper();
            PublicationDetailsWrapper.publicationWrapper  publicationWrapper2 = new PublicationDetailsWrapper.publicationWrapper(publicationsList[0]);
        }
        
        
    }
}