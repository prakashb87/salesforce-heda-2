/*****************************************************************************************************
*** @Class             : BatchJobUserCreationTest
*** @Author            : Rishabh Anand
*** @Requirement       : test class for BatchJobUserCreation
*** @Created date      : 31/05/2018
*** @JIRA ID           : ECB-3249
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is a test class for BatchJobUserCreation 
*****************************************************************************************************/ 
@isTest
public class BatchJobUserCreationTest {

     @isTest
    public static void test(){
        
       TestDataFactoryUtil.testUserdata();
        
         Test.startTest();
            BatchJobUserCreation obj = new BatchJobUserCreation();
            DataBase.executeBatch(obj); 
        Test.stopTest();
        
        Contact Contacts = [SELECT FirstName, LastName, Community_User_Provisioned__c FROM Contact limit 1];
        //System.assertEquals(true,Contacts.Community_User_Provisioned__c);
    }
}