/*********************************************************************************
 *** @ClassName         : CourseOfferingService
 *** @Author            : Shubham Singh 
 *** @Requirement       : CourseOfferingService class for methods related to web service version 1.1
 *** @Created date      : 17/03/2019
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 01/04/2019
 **********************************************************************************/
public with sharing class CourseOfferingService {
    //wrapper class to create variables to store the respective values 
    public class CourseOfferingServiceWrapper {
        public Boolean allExist = true;
        public Set < String > notFound = new Set < String > ();
        public Map < String, Id > courseOfferingClassNumberTermMap = new Map < String, Id > ();
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check for course offering , if yes then create map of course offering
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    getCourseOffering
     * @param     Set<String> classNumber , Set<String> term
     * @return    CourseOfferingServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static CourseOfferingServiceWrapper getCourseOffering(Set < String > classNumber, Set < String > term) {
        CourseOfferingServiceWrapper wrapper = new CourseOfferingServiceWrapper();
        System.debug('classNumberclassNumber ' + classNumber);
        System.debug('term ' + term);
        wrapper = checkAllExistClassNummber(classNumber);
        if (wrapper.allExist) {
            List < hed__Course_Offering__c > listCourseOffering = new List < hed__Course_Offering__c > ();
            listCourseOffering = courseOfferingBasedOnNameAndTerm(classNumber, term);
            for (hed__Course_Offering__c loopVar: listCourseOffering) {
                wrapper.courseOfferingClassNumberTermMap.put(String.valueOf(loopVar.name) + loopVar.hed__Term__r.Term_Code__r.Term_Code__c, loopVar.Id);
            }
            return wrapper;
        }
        return wrapper;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query course offering records
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    courseOfferingBasedOnNameAndTerm
     * @param     Set<String> classNumber , Set<String> term
     * @return    List<hed__Course_Offering__c> 
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static List < hed__Course_Offering__c > courseOfferingBasedOnNameAndTerm(Set < String > classNumber, Set < String > term) {
        if (Schema.sObjectType.hed__Course_Offering__c.fields.hed__Term__c.isAccessible()) {
            return [Select Id, Name, hed__Term__r.Term_Code__r.Term_Code__c FROM hed__Course_Offering__c WHERE Name IN: classNumber AND hed__Term__r.Term_Code__r.Term_Code__c IN: term AND Name != null AND hed__Term__c != null];
        }
        return null;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check for course offering exist with their respective classNumber otherwise add in notFound set
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    checkAllExistClassNummber
     * @param     Set<String> classNumber
     * @return    CourseOfferingServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static CourseOfferingServiceWrapper checkAllExistClassNummber(Set < String > classNumber) {
        CourseOfferingServiceWrapper wrapper = new CourseOfferingServiceWrapper();
        System.debug('classNumberclassNumber' + classNumber);
        List < hed__Course_Offering__c > listOfCourseOffering = new List < hed__Course_Offering__c > ();
        if (Schema.sObjectType.hed__Course_Offering__c.fields.hed__Term__c.isAccessible()) {
            listOfCourseOffering = [Select Id, Name FROM hed__Course_Offering__c WHERE Name IN: classNumber];
        }
        wrapper.notFound = classNumber.clone();
        for (hed__Course_Offering__c loopCourse: listOfCourseOffering) {
            wrapper.notFound.remove(loopCourse.Name);
        }
        if (!wrapper.notFound.isEmpty()) {
            wrapper.allExist = false;
        }
        return wrapper;
    }
}