/*******************************************
Purpose: To create wrapper class for Milestones Details
History:
Created by Ankit Bhagat on 30/10/2018
*******************************************/

public class MilestonesDetailsWrapper {
    
    /*****************************************************************************************
	// Purpose      :  Milestones wrapper with total Notifications and milestoneWrapperList
	// Developer    :  Ankit Bhagat	
	// Created Date :  30/10/2018                 
	//************************************************************************************/ 
    public class milestonesListWithNotificationWrapper{
        
        @AuraEnabled public Integer totalMilestones=0;
		@AuraEnabled public List<milestonesWrapper> milestoneWrapperList;
    }
	
    /*****************************************************************************************
	// Purpose      :  Milestones wrapper with milestoneWrapper
	// Developer    :  Ankit Bhagat	
	// Created Date :  30/10/2018                 
	//************************************************************************************/ 
    public class milestonesWrapper{
        
        @AuraEnabled public date dueDate;
        @AuraEnabled public string type;
        @AuraEnabled public string name;
        @AuraEnabled public string description;
        @AuraEnabled public string details;
        @AuraEnabled public date dateCompleted;
        @AuraEnabled public boolean isDueWithin30Days;
        @AuraEnabled public boolean isOverDue;
	@AuraEnabled public String milestoneId;//RPORW-1640 - Ali
        @AuraEnabled public string milestoneTypeRecordId; //added by Ankit,1512
		
        
	public milestonesWrapper(){
            
            type         = '';
            name       = '';
            description = '';
            details    = '';
            milestoneTypeRecordId = ''; //added by Ankit,1512
        }
        
        public milestonesWrapper(SignificantEvent__c milestone){
            
            dueDate       = milestone.DueDate__c;
            milestoneId	  = milestone.Id;//RPORW-1640 - Ali
            description   = milestone.SignificantEventType__c;
            details       = milestone.EventDescription__c ;
            dateCompleted = milestone.ActualCompletionDate__c;
            IsDueWithin30Days = false;
			IsOverDue = false;
        }
        
    }
	
}