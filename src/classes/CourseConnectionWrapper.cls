//Wrapper class for SAMS request structure
public class CourseConnectionWrapper {
    public Id courseConnectionId;
    public String studentId;
    public String courseId;
    public String courseOfferingId;
    public String enrolStatus;
}