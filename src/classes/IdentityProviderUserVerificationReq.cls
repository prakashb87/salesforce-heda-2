/*
 * @description This interface defines the methods for Salesforce to activating 
 * or verifying a user on behalf of the Identity Provider
 * @group Identity Management
 */
public interface IdentityProviderUserVerificationReq {
    
    /**
     * @description This method tells the Identity Provider that Salesforce
     * has verified the user
     * @param emailAddress The email address of the user to be verified
     */
    void updateUserToVerified( String emailAddress);

}