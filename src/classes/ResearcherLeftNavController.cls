/******************************************
 Purpose: To get the variables from ResearcherLeftNavWrapper for Left Navigation bar
 History:
 Created by Ankit Bhagat on 11/11/2018
 *******************************************/
public with sharing class ResearcherLeftNavController {
   
        /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired = false;//added by Ali
    
    /************************************************************************************
    // Purpose      : getting Left Navigation Wrapper details from Helper class
    // Parameters   : Void          
    // Developer    : Ankit  
    // Created Date : 11/11/2018                  
    //***********************************************************************************/ 
    @AuraEnabled    
    public static ResearcherLeftNavWrapper.leftNavWrapper getLeftNavWrapper(){
   
       ResearcherLeftNavWrapper.leftNavWrapper leftNavWrapperDetails = new ResearcherLeftNavWrapper.leftNavWrapper();
       try{
           leftNavWrapperDetails = ResearcherLeftNavHelper.getLeftNavWrapperDetails();
           system.debug('Istrue==>'+isForceExceptionRequired);
           
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {     
           //Exception handling Error Log captured :: Starts
          ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       System.debug('@@@leftNavWrapperDetails'+JSON.serializePretty(leftNavWrapperDetails));
       return leftNavWrapperDetails;
    }
    
    /************************************************************************************
    // JIRA         : 245/223
    // SPRINT       : 7
    // Purpose      : checking Organisation is Sandbox
    // Parameters   : Boolean       
    // Developer    : Subhajit  
    // Created Date : 11/14/2018                  
    //***********************************************************************************/ 
    @AuraEnabled    
    public static Boolean checkIsSandbox(){
       Boolean isSandbox =false; 
     
       try{
           isSandbox = SobjectUtils.checkOrgType();
           system.debug('Istrue==>'+isForceExceptionRequired);
         
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {    
           //Exception handling Error Log captured :: Starts
          ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       return isSandbox;
    }
    /************************************************************************************
   // Purpose      : To check current login user is community user or salesforce user
                  
   //***********************************************************************************/ 
     
  @AuraEnabled     
   public static Boolean checkInternalDualAccessUser(){
           Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
           return isInternalDualAccessUser;
   }    

}