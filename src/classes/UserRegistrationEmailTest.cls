/***************************************************************************************************************
 * ECB-4584 Maria Luisa Esmero 26/10/2018
 * Test class for UserRegistrationEmail
 * Description: Inserts a User Registration record of the Contact, and sends an activation email to the Contact.
 **************************************************************************************************************/
@isTest
public class UserRegistrationEmailTest {
    
    @testSetup
    static void createTestData() {
        TestDataFactoryUtilRefOne.testUserRegistrationEmail();  
    }
    
    // postive test
    static testmethod void testUserRegistrationEmail() {
        // create test data
        Contact usercontact = [select id from Contact LIMIT 1];
        System.assert(usercontact.id!=null,true);
        System.debug('Contact id ::: '+usercontact.id);
        // invoke method
        Test.startTest();
        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();      
        User_Registration__c userregrecord = [select id from User_Registration__c where Contact__c =:usercontact.Id];
        System.assert(userregrecord.Id!=null,true);
        Test.stopTest();        
    }
     // negative test
     /*
     static testmethod void testUserRegistrationEmailnegative() {
        // create test data
        TestDataFactoryUtil.testUserRegistrationEmailnegative();  
        Contact usercontact = [select id from Contact LIMIT 1];
        System.assert(usercontact.id!=null,true);
        System.debug('Contact id ::: '+usercontact.id);
        // invoke method
        Test.startTest();
        UserRegistrationEmail userReg = new UserRegistrationEmail(usercontact.Id);
        userReg.userRegistrationEmail();  
         User_Registration__c userregrecord = [select id from User_Registration__c where Contact__c =:usercontact.Id];
        System.assert(userregrecord.Id==null,true);
        Test.stopTest(); 
     }
     */
}