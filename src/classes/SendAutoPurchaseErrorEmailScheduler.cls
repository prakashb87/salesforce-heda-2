/*
  *****************************************************************************************************************
  * @author       :Resmi Ramakrishnan
  * @date         : 16/06/2018 
  * @description  : Schedule Class for sending Embedded Credential AutoPurchase Error records to 21CC Support team
  ******************************************************************************************************************
*/
public without sharing class SendAutoPurchaseErrorEmailScheduler implements Schedulable 
{
     public void execute(SchedulableContext ctx) 
	 {	 	         
     	List<RMErrLog__c> errorLogList= [SELECT id,Error_Message__c,Error_Type__c,Is_Admin_Notified__c,Record_Id__c,Business_Function_Name__c,CreatedDate from RMErrLog__c where Error_Type__c = 'Application' and Business_Function_Name__c= 'Auto Purchase' and Is_Admin_Notified__c = false];
	    System.debug('RRRR:Total Number Of Records=' + errorLogList.size()); 
            
        SendAutoPurchaseErrorEmailBatch batchObj = new SendAutoPurchaseErrorEmailBatch(errorLogList); 
	    database.executebatch( batchObj,20);     
     }

}