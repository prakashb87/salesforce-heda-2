/*****************************************************************************************************
 *** @Class             : IntegrationLogCleanUpBatchTest
 *** @Author            : Praneet Vig
 *** @Requirement       : Test Class to cover the coverage of IntegrationLogCleanUpBatch
 *** @Created date      : 07/06/2018
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is a Test Class to cover the class Coverage of IntegrationLogCleanUpBatch.
 *****************************************************************************************************/ 
@isTest
public class IntegrationLogCleanUpBatchTest {

    static testmethod void positivetestcase() {        
        Test.startTest();
        TestDataFactoryUtilRefOne.IntegrationLogCleanUpScheduler();
        IntegrationLogCleanUpBatch cub = new IntegrationLogCleanUpBatch();
        Id batchId = Database.executeBatch(cub);
        Test.stopTest();
        System.assert(true);
    }
    
     static testmethod void negativetestCase() {  
        Test.startTest();
        IntegrationLogCleanUpBatch cub = new IntegrationLogCleanUpBatch();
        Id batchId = Database.executeBatch(cub);
        Test.stopTest();
        System.assert(true);
    }
    
    static testmethod void schedulerTest() {
         TestDataFactoryUtilRefOne.IntegrationLogCleanUpScheduler();
    
        String cronExcp = '0 15 17 ? * MON-FRI';        
        IntegrationLogCleanUpScheduler p = new IntegrationLogCleanUpScheduler();
        String jobId = System.schedule('Product Data sync',  cronExcp, p);
       CronTrigger ct = [SELECT Id, state, PreviousFireTime, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
       System.assertEquals(cronExcp, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
            
        
    }
    
}