/*******************************************
Purpose: ResearcherPortal Case Wrapper
History:
Created by Subhajit on 11/10/18
*******************************************/
public class ResearcherPortalCaseWrapper {
    
    @AuraEnabled public String caseId;
    @AuraEnabled public String recordType;
    @AuraEnabled public String subject;
    @AuraEnabled public String description;
    @AuraEnabled public String status;
    @AuraEnabled public String researcherCaseTopic;
    @AuraEnabled public String researcherCaseSubTopic;
    @AuraEnabled public String caseNumber;
    @AuraEnabled public String portalFeedback;
    @AuraEnabled public String researcherCaseRoutedToEmail;
    @AuraEnabled public String contactId;
    @AuraEnabled public String satisfactionRating;
    @AuraEnabled public List<CaseAttachmentWrapper> caseAttachments; //RPORW-912
    
    public ResearcherPortalCaseWrapper()
    {
            this.caseId='';
            this.recordType='';
            this.subject='';
            this.description='';
            this.status='';
            this.researcherCaseTopic='';
            this.researcherCaseSubTopic='';
            this.caseNumber='';
            this.portalFeedback='';
            this.researcherCaseRoutedToEmail='';
            this.contactId='';
            this.satisfactionRating = '';
    }
    
    /*@Description : Wrapper class to hold attachments for a Feedback Case
      @Story : RPORW-912
      @Author : Gourav Bhardwaj
    */
    public class CaseAttachmentWrapper{
        public String fileName;
        public String contentType;
        public String fileContent;
        public CaseAttachmentWrapper(String fileName,String contentType,String fileContent){
            this.fileName 		= fileName;
            this.contentType 	= contentType;
            this.fileContent 	= fileContent;
        }
        
    }

}