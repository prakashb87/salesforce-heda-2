@isTest
private class AllProductBasketTriggersHandlerTest {
	
	@testSetup 
    static void setupTestData() {
        List<String> lstParams = new List<String>{'Test', 'Con'};
        Account acc = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        
        Contact con = TestUtility.createTestContact(true,lstParams, acc.Id);
        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = con.Id;
        insert u;
        
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', acc.Id);
        opp.Assign_To_Contact__c=con.Id;
        update opp;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
    }
	@isTest static void updateBasketOwnerTest() {
		setupTestData();
		Test.startTest();
		List<cscfga__Product_Basket__c> pb=[select Id,cscfga__Opportunity__c from cscfga__Product_Basket__c];
		AllProductBasketTriggersHandler.handleBeforeInsert(pb);
		System.assertNotEquals([select OwnerId from cscfga__Product_Basket__c],[select cscfga__Opportunity__r.OwnerId from cscfga__Product_Basket__c]);
		Test.stopTest();
	}
}