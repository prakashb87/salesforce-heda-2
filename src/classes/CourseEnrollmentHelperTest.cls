/*****************************************************************
Name: CourseEnrollmentHelperTest
Author: Pawan [CloudSense]
Purpose: Test Class For CourseEnrollmentHelper 
*****************************************************************/
/*==================================================================
History
--------
Version   Author           Date              Detail
1.0       Pawan            15/10/2018         Initial Version
********************************************************************/
@isTest
public class CourseEnrollmentHelperTest {
    
   /* @isTest
    public static void testCourseEnrollmentHelper(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtil.deltaTestSuccess(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'LS2017-abc','LS2017-xyz','LS2017-qaz'};
        CourseEnrollmentHelper.iPassOpenEdxApiEnroll('abc@email.com',setCourses);  
        CourseEnrollmentHelper.iPassOpenEdxApiUnEnroll('abc@email.com',setCourses);          
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock()); 
        Test.stopTest();
        
    }  */      
    
    public class iPassOpenEdxApiCallServiceMock implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }   
    }
    
    @isTest
    public static void testOpenEdxEnrollSuccess(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 001'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock1()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiEnroll('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 200);  
        CourseEnrollmentHelper.handleOpenEdxEnrollResposne(response,'abc@email.com',setCourses);          
        Course_Connection_Life_Cycle__c clrec = [SELECT Status__c FROM Course_Connection_Life_Cycle__c WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :setCourses];
        system.assert(clrec.Status__c == 'SUCCESS'); 
        Test.stopTest();
        
        
    }   
    
     public class iPassOpenEdxApiCallServiceMock1 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "success","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Success","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 001","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }   
    }
    
        @isTest
    public static void testOpenEdxEnrollError(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 001'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock2()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiEnroll('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 200);  
        CourseEnrollmentHelper.handleOpenEdxEnrollResposne(response,'abc@email.com',setCourses);          
        Course_Connection_Life_Cycle__c clrec = [SELECT Status__c FROM Course_Connection_Life_Cycle__c WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :setCourses];
        system.assert(clrec.Status__c == 'Error'); 
        Test.stopTest();
        
        
    }   
    
     public class iPassOpenEdxApiCallServiceMock2 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "error","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Error","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 001","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }   
    }
    
    @isTest
    public static void testOpenEdxUnEnrollSuccess(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 002'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock3()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiUnEnrollCall('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 200);  
        CourseEnrollmentHelper.handleOpenEdxUnEnrollResposne(response,'abc@email.com',setCourses);          
        Course_Connection_Life_Cycle__c clrec = [SELECT Status__c FROM Course_Connection_Life_Cycle__c WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :setCourses];
        system.assert(clrec.Status__c == 'SUCCESS'); 
        Test.stopTest();
        
        
    } 
    
    public class iPassOpenEdxApiCallServiceMock3 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "success","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Success","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 002","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }   
    }  
    
     @isTest
    public static void testOpenEdxUnEnrollError(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 002'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock4()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiUnEnrollCall('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 200);  
        CourseEnrollmentHelper.handleOpenEdxUnEnrollResposne(response,'abc@email.com',setCourses);          
        Course_Connection_Life_Cycle__c clrec = [SELECT Status__c FROM Course_Connection_Life_Cycle__c WHERE Course_Connection__r.hed__Course_Offering__r.Name IN :setCourses];
        system.assert(clrec.Status__c == 'Error'); 
        Test.stopTest();
        
        
    }   
    
     public class iPassOpenEdxApiCallServiceMock4 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "error","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Error","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 002","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }   
    }
    
        @isTest
    public static void testOpenEdxEnroll(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 001'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock5()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiEnroll('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 404);  
        
        Test.stopTest();
        
        
    }   
    
     public class iPassOpenEdxApiCallServiceMock5 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "success","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Success","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 001","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(404);
            return response;
        }   
    }
    
     @isTest
    public static void testOpenEdxUnEnroll(){                
       
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://rmit-experience-api-sfdc.npe.integration.rmit.edu.au/api/course/enrolment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = '158d01b8ef47451ebdbbeda679a98731'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.setUpOpenEdxTestData(); 
        
        Test.startTest(); 
        Set<String> setCourses = new Set<String>{'Course Offering 002'};
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock6()); 
        HttpResponse response = CourseEnrollmentHelper.iPassOpenEdxApiUnEnrollCall('abc@email.com',setCourses);
        system.assert(response.getStatusCode() == 404);  
        
        Test.stopTest();
        
        
    }   
    
     public class iPassOpenEdxApiCallServiceMock6 implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "error","code": "200","application": "rmit-publisher-api-sfdc","provider": "openEdx",';
                   jsonBody = jsonBody+'"payload": {"userId": "abc@email.com","response":[{"courseId": "123 CopyFromRequest","result": "Error","errorMessage": "","created": "2018-08-15T04:38:53.274727Z",';
                   jsonBody = jsonBody+'"mode": "no-id-professional","is_active": false,"course_details": {"course_id": "Course Offering 002","course_name": "App Development with Swift Foundations",';
                   jsonBody = jsonBody+'"enrollment_start": "","enrollment_end": "","course_start": "2018-04-20T01:00:00Z","course_end": null,"invite_only": false,"course_modes":[{"slug": "no-id-professional",';
                   jsonBody = jsonBody+'"name": "Professional Education","min_price": 549,"suggested_prices": "","currency": "usd","expiration_datetime": null,"description": null,"sku": "9780377","bulk_sku": "6CD9C42"}]}}]}}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(404);
            return response;
        }   
    }
    
    
}