/*****************************************************************
Name: CS_CustomLookupProgramUpgradeTest
Author: Pawan [CloudSense]
Purpose: Test Class For CS_CustomLookupProgramUpgrade 
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Pawan            20/09/2018         Initial Version
********************************************************************/
@isTest
public class CS_CustomLookupProgramUpgradeTest {
    
    @testSetup 
    static void setupTestData() {
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());

        Account accProgram = TestUtility.createTestAccount(true
            , 'Program Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId());
            
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', accDepartment.Id);
        List<String> lstParams = new List<String>{'Test', 'Con'};        
        Contact con = TestUtility.createTestContact(true
            , lstParams
            , accAdministrative.Id);
        
        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = con.Id;
        insert u;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;
        
        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        hed__Course__c course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;
       
        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;

        hed__Course_Enrollment__c courseEnrolment = new hed__Course_Enrollment__c(hed__Status__c = 'Enrolled'
            , hed__Course_Offering__c = courseOffering.id
            , hed__Contact__c = con.Id);
        insert courseEnrolment;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name = course.Name
            , Course_Offering_ID__c = courseOffering.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c
            , cspmb__Is_Active__c = true
            , Course_Program__c  = 'Program'
            , Product_Line__c = 'Sample Product'
            , Program__c = accProgram.Id
            , List_Price__c=1000
            , cspmb__One_Off_Cost__c = 2
            , cspmb__One_Off_Charge__c = 4);
        insert priceItem;
        
        cspmb__Add_On_Price_Item__c addonPriceItem = new cspmb__Add_On_Price_Item__c(name = 'Test');
        insert addonPriceItem;
        
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAssociation = new cspmb__Price_Item_Add_On_Price_Item_Association__c(cspmb__Add_On_Price_Item__c=addonPriceItem.Id
            , cspmb__Price_Item__c=priceItem.Id
            , cspmb__One_Off_Charge__c = 4
            , List_Price__c=100);
        insert priceItemAssociation;

        hed__Program_Plan__c programPlan=new hed__Program_Plan__c(hed__Account__c=accProgram.Id);
        insert programPlan;
        
        priceItem.Program_Plan__c = programPlan.Id;
        update priceItem;

        hed__Plan_Requirement__c planRequirement =new hed__Plan_Requirement__c(hed__Category__c='Required'
            , hed__Course__c=course.Id
            , hed__Program_Plan__c=programPlan.Id);
        insert planRequirement;
        
        List<Config_Data_Map__c> configsDataMap = new List<Config_Data_Map__c>();   
        Config_Data_Map__c csDataMapCourseConnectionStatusComplete  = TestDataFactoryUtil.createCongigDataMapRecordsFromNameValue('CourseConnectionStatus_Completed', 'Assessment Complete');
        configsDataMap.add(csDataMapCourseConnectionStatusComplete);
        Config_Data_Map__c csDataMapCourseConnectionStatusEnrolled  = TestDataFactoryUtil.createCongigDataMapRecordsFromNameValue('CourseConnectionStatus_Enrolled', 'Enrolled');
        configsDataMap.add(csDataMapCourseConnectionStatusEnrolled);
        insert configsDataMap;

    }
    
    @isTest
    public static void testCustomLookupPriceItem(){
        
        setupTestData();    
        Map<String, String> searchFields = new Map<String, String>();
        cspmb__Price_Item__c priceItem  =  [SELECT 
                                                Id, Course_Prerequisites__c,Course_Program__c,Program__c,Program_Plan__c,Name,Product_Line__c,cspmb__Effective_Start_Date__c,cspmb__Effective_End_Date__c
                                            FROM 
                                                cspmb__Price_Item__c 
                                            limit 1];                                                
        cspmb__Add_On_Price_Item__c addonPriceItem  =  [SELECT 
                                                            Id, Name
                                                        FROM 
                                                            cspmb__Add_On_Price_Item__c 
                                                        limit 1];                                    
                                            
        cscfga__Product_Definition__c prodDefintion  =  [SELECT 
                                                            Id
                                                        FROM 
                                                            cscfga__Product_Definition__c 
                                                        limit 1];
        hed__Course_Enrollment__c courseEnrollment  =  [SELECT 
                                                            Id, hed__Contact__c
                                                        FROM 
                                                            hed__Course_Enrollment__c 
                                                        limit 1];                                               
         
        //Setting value in searchFields Map
        searchFields.put('PriceItemId' , priceItem.Id);        
        searchFields.put('ContactId' , courseEnrollment.hed__Contact__c);
        searchFields.put('DeliveryMode' ,addonPriceItem.Name);
        searchFields.put('CourseType' ,priceItem.Course_Program__c);
       
        Test.startTest();        
        System.assert(priceItem != null, 'Invalid data'); 
        CS_CustomLookupProgramUpgrade  customLookupProgramUpgrade = new CS_CustomLookupProgramUpgrade();
        customLookupProgramUpgrade.doDynamicLookupsearch(searchFields,prodDefintion.Id);
        
        Id[] excludeIds = new Id[]{priceItem.Id};
        customLookupProgramUpgrade.doLookupSearch(searchFields,prodDefintion.Id,excludeIds,1,2);        
        customLookupProgramUpgrade.getRequiredAttributes();
        Test.stopTest();
    }
}