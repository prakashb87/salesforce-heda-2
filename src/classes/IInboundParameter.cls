/**
 * Created by mmarsson on 1/03/19.
 */

public interface IInboundParameter {

    void addMandatoryField(String field);
    void validateParameters();
    List<String> getMandatoryFields();
    Object getFieldValueByName(String fieldName);

}