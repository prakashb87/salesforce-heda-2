@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CustomButtonSaveBasket extends csbb.CustomButtonExt {
    
   
    
    cscfga__Product_Basket__c basket;
    
    public String performAction (String basketId) {
        
        set<String> setValidStatus = new set<String> {'Valid', 'Is Valid'};
        //UpdateCustomOpportunityStatus.changeCustomopportunityStatus(basketId);
        
        if(Schema.sObjectType.cscfga__Product_Basket__c.isAccessible()){
            basket = [SELECT Id,ownerid, cscfga__Basket_Status__c,
                      csordtelcoa__Synchronised_with_Opportunity__c,
                      csbb__Synchronised_With_Opportunity__c,
                      cscfga__Opportunity__c,
                      cscfga__Opportunity__r.Id, 
                      cscfga__Opportunity__r.Name,
                      cscfga__Opportunity__r.ownerid,                                                 
                      cscfga__Total_Price__c
                      FROM cscfga__Product_Basket__c 
                      WHERE Id = :basketId];
            
            
        }
        
        insertCustomOppty();
        
        return '{"status":"ok"}';
    }
    
    public void insertCustomOppty(){
        List<Custom_Opportunity__c> customOppList = new List<Custom_Opportunity__c>();
        if(Schema.sObjectType.Custom_Opportunity__c.isAccessible()){
            customOppList = [SELECT id,Stage__c from Custom_Opportunity__c where Opportunity__c =: basket.cscfga__Opportunity__c];
        }
        if(customOppList.isEmpty() && Schema.sObjectType.Custom_Opportunity__c.isCreateable()){
            
            system.debug('>> create Customopportunity');
            Custom_Opportunity__c customOppObj = new Custom_Opportunity__c(); 
            customOppObj.Opportunity__c = basket.cscfga__Opportunity__c;
            customOppObj.Name = 'Custom_' + basket.cscfga__Opportunity__r.Name;
            //customOppObj.Stage__c = 'Closed Won';
            customOppObj.OwnerId = basket.cscfga__Opportunity__r.OwnerId;
            insert customOppObj;
      
        }
         
         updateOwnerproductConfig();
    }
    public void updateOwnerproductConfig(){
        List<cscfga__Product_Configuration__c> prodConfigList = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> prodConfigListUpdate = new List<cscfga__Product_Configuration__c>();
        if(Schema.sObjectType.cscfga__Product_Configuration__c.isAccessible()){
            prodConfigList = [SELECT id,ownerid,cscfga__Product_Basket__c  from cscfga__Product_Configuration__c  where cscfga__Product_Basket__c =: basket.id];        
        }
        if(prodConfigList != null && !prodConfigList.isempty()){
            for(cscfga__Product_Configuration__c prodConfig : prodConfigList){
                prodConfig.ownerid = basket.ownerid;
                prodConfigListUpdate.add(prodConfig);
            }                 
            
        }
        if(!prodConfigListUpdate.isempty() && prodConfigListUpdate !=null){  
            if(Schema.sObjectType.cscfga__Product_Configuration__c.isUpdateable()){     
                update prodConfigListUpdate;   
            }               
        }
        
        updateownerProductConfigrequest();
        
    }
    public void updateownerProductConfigrequest(){
        List<csbb__Product_Configuration_Request__c> prodConfigReqList = new List<csbb__Product_Configuration_Request__c>();
        List<csbb__Product_Configuration_Request__c> prodConfigReqListUpdate = new List<csbb__Product_Configuration_Request__c>();
        
        if(Schema.sObjectType.csbb__Product_Configuration_Request__c.isAccessible()){
            prodConfigReqList = [SELECT id,ownerid,csbb__Product_Basket__c  from csbb__Product_Configuration_Request__c  where csbb__Product_Basket__c =: basket.id];        
        }
        if(prodConfigReqList != null && !prodConfigReqList.isempty()){
            for(csbb__Product_Configuration_Request__c  prodConfigReq : prodConfigReqList){
                prodConfigReq.ownerid = basket.ownerid;
                prodConfigReqListUpdate.add(prodConfigReq);
            }                 
            
        }
        if(!prodConfigReqListUpdate.isempty() && prodConfigReqListUpdate !=null){  
            if(Schema.sObjectType.csbb__Product_Configuration_Request__c.isUpdateable()){       
                update prodConfigReqListUpdate; 
            }               
        }
        
   }
   }