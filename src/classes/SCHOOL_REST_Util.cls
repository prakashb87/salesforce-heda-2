/*********************************************************************************
*** @ClassName         : SCHOOL_REST_Util
*** @Author            : Dinesh Kumar 
*** @Requirement       : Moved some code from SCHOOL_REST to this class
*** @Created date      : 05/02/2019
*** @Modified by       : Khushman Deomurari
*** @modified date     : 05/02/2019
**********************************************************************************/
public with sharing class SCHOOL_REST_Util {
	
	public static void compareEffDateAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
       Future_Change__c futureChangeRecord = new Future_Change__c();
        
               string effDateDB = null;
                if (prevAccounts[0].Effective_Date__c != null){
                    effDateDB = String.valueOf(prevAccounts[0].Effective_Date__c);
                }
                
                string effDateJSON = QuickUtils.returnOnlyIfLengthMoreThanZero(postString[0].effectiveDate);
                
                if(effDateDB != effDateJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Effective_Date__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = effDateJSON;
                    lstFutureChange.add(futureChangeRecord);
                }
	}
	
	public static void compareDescriptionsAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
       Future_Change__c futureChangeRecord = new Future_Change__c();
        
                String descriptionDB = prevAccounts[0].Description;
                
                string descriptionJSON = null;
                if(postString[0].description.length() >0 ){
                    descriptionJSON = postString[0].description;
                }  
                
                if(descriptionDB != descriptionJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Description');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = descriptionJSON;
                    lstFutureChange.add(futureChangeRecord);
                }    
                
                string shortDescriptionDB = prevAccounts[0].Short_Description__c;
                
                string shortDescriptionJSON = null;
                if(postString[0].shortDescription.length() >0 ){
                    shortDescriptionJSON = postString[0].shortDescription;
                }  
                
                if(shortDescriptionDB != shortDescriptionJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Short_Description__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = shortDescriptionJSON;
                    lstFutureChange.add(futureChangeRecord);
                }
                  
                compareFormalDescriptionAndAdd(lstFutureChange, postString, prevAccounts);
	}
	
	public static void compareFormalDescriptionAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
		        Future_Change__c futureChangeRecord = new Future_Change__c();

                string formalDescriptionDB = prevAccounts[0].Formal_Description__c;
                
                string formalDescriptionJSON = null;
                if(postString[0].formalDescription.length() >0 ){
                    formalDescriptionJSON = postString[0].formalDescription;
                }  
                
                if(formalDescriptionDB != formalDescriptionJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Formal_Description__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = formalDescriptionJSON;
                    lstFutureChange.add(futureChangeRecord);
                }
	}
	
	public static void compareStatusAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
    		    Future_Change__c futureChangeRecord = new Future_Change__c();
                string statusDB = prevAccounts[0].Status__c;
            
                string statusJSON = null;
                    if(postString[0].status == 'A'){
                        statusJSON = 'A';
                    }else if(postString[0].status == 'I'){
                         statusJSON = 'I';
                    }
                if(statusDB != statusJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Status__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = statusJSON;
                    lstFutureChange.add(futureChangeRecord);
                }
	}

	public static void compareAcademicInstitutionAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
                string academicInstitutionDB = null;
                if(prevAccounts[0].Academic_Institution__r.Name != null){
                    academicInstitutionDB = prevAccounts[0].Academic_Institution__r.Name;
                }
                
                string academicInstitutionJSON = null;
                if(postString[0].academicInstitution.length() >0 ){
                    academicInstitutionJSON = postString[0].academicInstitution;
                }  
                
                if(academicInstitutionDB != academicInstitutionJSON){
                    Future_Change__c futureChangeRecord = SCHOOL_REST.createFutChange('Academic_Institution__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = academicInstitutionJSON != null?[select Id from Account where Name =: academicInstitutionJSON Limit 1].size()>0?[select Id from Account where Name =: academicInstitutionJSON Limit 1].Id:null:null;
                    lstFutureChange.add(futureChangeRecord);
                }
	}		    

	public static void compareAouCodeAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
    		    Future_Change__c futureChangeRecord = new Future_Change__c();
                string aouCodeDB = null;
                if(prevAccounts[0].AOUCode__c != null){
                    aouCodeDB = prevAccounts[0].AOUCode__c;
                }
                
                string aouCodeJSON = null;
                if(postString[0].aouCode.length() >0 ){
                    aouCodeJSON = postString[0].aouCode;
                }  
                
                if(aouCodeDB != aouCodeJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('AOUCode__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = aouCodeJSON!=null?[SELECT Id FROM AOU_Code__c WHERE Name =: aouCodeJSON LIMIT 1].size()>0?[SELECT Id FROM AOU_Code__c WHERE Name =: aouCodeJSON LIMIT 1].Id:null:null;
                    lstFutureChange.add(futureChangeRecord);
                }
	}

	public static void compareCampusAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
    		    Future_Change__c futureChangeRecord = new Future_Change__c();
            
                string campusDB = null;
                if(prevAccounts[0].Campus__c != null){
                    campusDB = prevAccounts[0].Campus__c;
                }
                
                string campusJSON = null;
                if(postString[0].campus.length() >0 ){
                    campusJSON = postString[0].campus;
                }  
                
                if(campusDB != campusJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('Campus__c');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = campusJSON != null?[SELECT Id FROM Campus__c WHERE Name =: campusJSON LIMIT 1].size()>0?[SELECT Id FROM Campus__c WHERE Name =: campusJSON LIMIT 1].Id:null:null;
                    lstFutureChange.add(futureChangeRecord);
                }
	}

	public static void compareParentAcademicOrganisatioAndAdd(List<Future_Change__c> lstFutureChange, List<SCHOOL_REST> postString, List<Account> prevAccounts){
    		    Future_Change__c futureChangeRecord = new Future_Change__c();
            
                string parentAcademicOrganisationDB = null;
                if(prevAccounts[0].ParentId != null){
                    parentAcademicOrganisationDB = string.valueOf(prevAccounts[0].ParentId) ;
                }
                
                string parentAcademicOrganisationJSON = null;
                if(postString[0].parentAcademicOrganisation.length() >0 ){
                    parentAcademicOrganisationJSON = postString[0].parentAcademicOrganisation;
                }  
                
                if(parentAcademicOrganisationDB != parentAcademicOrganisationJSON){
                    futureChangeRecord = SCHOOL_REST.createFutChange('ParentId');
                    futureChangeRecord.Parent_Lookup__c = prevAccounts[0].Id;
                    futureChangeRecord.Value__c = parentAcademicOrganisationJSON!=null?[select Id from Account where Name =: parentAcademicOrganisationJSON LIMIT 1].size()>0? [select Id from Account where Name =: parentAcademicOrganisationJSON LIMIT 1].Id:null:null;
                    lstFutureChange.add(futureChangeRecord);
                }
	}

}