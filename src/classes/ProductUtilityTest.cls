@isTest(seeAllData=false)
private class ProductUtilityTest {

    @testSetup 
    static void setupTestData() {
        
        Account acc = TestUtility.createTestAccount(true, 'Test Account'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        List<String> lstParams = new List<String>{'Test First', 'Test Last'};
        Contact con = TestUtility.createTestContact(true, lstParams, acc.Id);
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', acc.Id);

        //create Product Category
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        //create Product definition
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1'
            , csordtelcoa__Product_Type__c = 'Subscription');
        insert prodDefintion;
        
        //create attribute definition, pay attention to the required one to fulfill the vlaidation rule on data
        List<cscfga__Attribute_Definition__c> attrDefs = new List<cscfga__Attribute_Definition__c>();
        cscfga__Attribute_Definition__c attrDef1 = new cscfga__Attribute_Definition__c(Name = 'Test Attribute'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0);
        attrDefs.add(attrDef1);
        cscfga__Attribute_Definition__c attrDef2 = new cscfga__Attribute_Definition__c(Name = 'Test Attribute2'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0);
        attrDefs.add(attrDef2);
        cscfga__Attribute_Definition__c reqAttrDefLicenceType = new cscfga__Attribute_Definition__c(Name = 'Licence Type'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = false
            , cscfga__Data_Type__c = 'String');
        attrDefs.add(reqAttrDefLicenceType);
        cscfga__Attribute_Definition__c reqAttrDefStartDate = new cscfga__Attribute_Definition__c(Name = 'Start Date'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = false
            , cscfga__Data_Type__c = 'Date');
        attrDefs.add(reqAttrDefStartDate);
        cscfga__Attribute_Definition__c reqAttrDefEndDate = new cscfga__Attribute_Definition__c(Name = 'End Date'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = false
            , cscfga__Data_Type__c = 'Date');
        attrDefs.add(reqAttrDefEndDate);
        insert attrDefs;
        
        //create the basket
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        
        //create the product configuration
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id
            , cscfga__Product_Family__c = 'Test Family');  
        insert config;
        
        List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();
        cscfga__Attribute__c attr1 = new cscfga__Attribute__c(Name = 'Test Attribute1'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDefs[0].Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Test Product 001'
            , cscfga__is_active__c = true);        
        attributes.add(attr1);
        cscfga__Attribute__c attr2 = new cscfga__Attribute__c(Name = 'Test Attribute2'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDefs[1].Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Test Product 002'
            , cscfga__is_active__c = true
            , cscfga__Price__c = 3.40);
        attributes.add(attr2);
        cscfga__Attribute__c attr3 = new cscfga__Attribute__c(Name = 'Licence Type'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDefs[2].Id
            , cscfga__Value__c = 'Single User'
            , cscfga__is_active__c = true);
        attributes.add(attr3);
        cscfga__Attribute__c attr4 = new cscfga__Attribute__c(Name = 'Start Date'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDefs[3].Id
            , cscfga__Value__c = '' + Date.today()
            , cscfga__is_active__c = true);
        attributes.add(attr4);
        cscfga__Attribute__c attr5 = new cscfga__Attribute__c(Name = 'End Date'
            , cscfga__Product_Configuration__c = config.Id
            , cscfga__Attribute_Definition__c = attrDefs[4].Id
            , cscfga__Value__c = '' + Date.today()
            , cscfga__is_active__c = true);
        attributes.add(attr5);

        insert attributes;

    }
    
    static testmethod void testProductUtility1() {
        Set<String> basketIds = new Set<String>();
        for (cscfga__Product_Basket__c basket : [SELECT Id FROM cscfga__Product_Basket__c]) {
            basketIds.add((String)basket.Id);
        }
        
        /* Product2 prod1 = TestUtility.createProduct(true, 'Test Product 001', 'code1', 'Test Family');
        PriceBookEntry pb1 = TestUtility.createPriceBookEntry(true, prod1.Id);
        system.debug('testProductUtility2: pb1.PriceBook2Id >> ' + pb1.PriceBook2Id);
        
        Product2 prod2 = TestUtility.createProduct(true, 'Test Product 002', 'code1', 'Test Family');
        PriceBookEntry pb2 = TestUtility.createPriceBookEntry(true, prod2.Id);
        system.debug('testProductUtility2: pb2.PriceBook2Id >> ' + pb2.PriceBook2Id); */
        
        Test.startTest();
            ProductUtility.CreateOLIs(basketIds);
        Test.stopTest();
        
        System.assert([SELECT Id FROM OpportunityLineItem].size() > 0);
    }
    
    static testmethod void testProductUtility2() {
        Set<String> basketIds = new Set<String>();
        for (cscfga__Product_Basket__c basket : [SELECT Id FROM cscfga__Product_Basket__c]) {
            basketIds.add((String)basket.Id);
        }
        
        List<String> createProductLst1 = new List<String>{'Test Product 001', 'code1', 'Test Family'};
        Product2 prod1 = TestUtility.createProduct(true,createProductLst1);
        PriceBookEntry pb1 = TestUtility.createPriceBookEntry(true, prod1.Id);
        system.debug('testProductUtility2: pb1.PriceBook2Id >> ' + pb1.PriceBook2Id);
        
        List<String> createProductLst2 = new List<String>{'Test Product 002', 'code1', 'Test Family'};
        Product2 prod2 = TestUtility.createProduct(true,createProductLst2);
        PriceBookEntry pb2 = TestUtility.createPriceBookEntry(true, prod2.Id);
        system.debug('testProductUtility2: pb2.PriceBook2Id >> ' + pb2.PriceBook2Id);
        
        Test.startTest();
            ProductUtility.CreateOLIs(basketIds);
        Test.stopTest();
        
        System.assert([SELECT Id FROM OpportunityLineItem].size() > 0);
    }
    
    static testmethod void testProductUtility3() {
        Set<String> basketIds = new Set<String>();
        List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c];
        for (cscfga__Product_Basket__c basket : baskets) {
            basketIds.add((String)basket.Id);
        }
        
        List<String> createProductLst3 = new List<String>{'Test Product 001', 'code1', 'Test Family'};
        Product2 prod = TestUtility.createProduct(true, createProductLst3);
        PriceBookEntry pb = TestUtility.createPriceBookEntry(true, prod.Id);
        
        TestUtility.createOpportunityLineItem(true, baskets[0].cscfga__Opportunity__c, pb.Id);
        Test.startTest();
            ProductUtility.DeleteHardOLIs(basketIds);
        Test.stopTest();
        System.assert([SELECT Id FROM OpportunityLineItem].size() == 0);
    }
}