/*@Author     : Gourav Bhardwaj
@description  : Util class holds generic methods for FOR Related Functionality
@Story        : RPROW-979
@CreatedDate  : 17/4/2019
*/
public with Sharing class ResearcherProjectFORCodeUtil {


	 public static Map<id,List<Research_Project_Classification__c>> getFORCodesForProjects(list<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> lstResearcherPortalProject){
        Map<Id,List<Research_Project_Classification__c>> allProjectClassificationMap = new Map<Id,List<Research_Project_Classification__c>>();
        set<Id> projectIdSet = new set<Id>();



        for(ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherPortalProjectData :lstResearcherPortalProject)
        {
            projectIdSet.add(researcherPortalProjectData.recordId); 
        }

        system.debug('## projectIdSet : '+projectIdSet);
        
        for(Research_Project_Classification__c projectClassification : [SELECT For_Four_Digit_Detail__c,ResearcherPortalProject__c,Id,For_Six_Digit_Detail__c FROM Research_Project_Classification__c
                                                   WHERE ResearcherPortalProject__c IN : projectIdSet ])
        {
              system.debug('## projectClassification : '+projectClassification);
              allProjectClassificationMap = getClasificationMap(allProjectClassificationMap,projectClassification);
        }
        return allProjectClassificationMap;
    }


 //RPROW-1024
    public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isFORCodePresentInProject(ResearcherPortalProject__c project,ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper){
      ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper(); 
      Boolean isSearchTextPresent = false;
      String forCode;

      	system.debug('configure 3 : '+rmitPublicProjectWrapper.configuration);
      
        system.debug('project : '+project);
        
        system.debug('forCodesForProjects Map : '+rmitPublicProjectWrapper.forCodesForProjects);
        
        //This check is necessary for AND condition where filter should include Text Search AND FOR AND Impact Category
        if(rmitPublicProjectWrapper.configuration.forCodeDetailSearch.isEmpty()){
			filteredRecordWrapper.isSearchTextPresent = true;
			filteredRecordWrapper.searchOrder         = 0;
			return filteredRecordWrapper;
        }
      
        if(!rmitPublicProjectWrapper.forCodesForProjects.isEmpty() && rmitPublicProjectWrapper.forCodesForProjects.containskey(project.Id) && !rmitPublicProjectWrapper.configuration.forCodeDetailSearch.isEmpty()){
          for(Research_Project_Classification__c forCodes : rmitPublicProjectWrapper.forCodesForProjects.get(project.Id)){
              
				isSearchTextPresent = checkFORCodePresenceInProject(project,forCodes,rmitPublicProjectWrapper);
          		if(isSearchTextPresent){
          			break;
          		}
              
            } //for Ends
        }
        //1383
		filteredRecordWrapper.isSearchTextPresent = isSearchTextPresent;
		filteredRecordWrapper.searchOrder         = 6;

        system.debug(' ## filteredRecordWrapper : '+filteredRecordWrapper);

         return filteredRecordWrapper;
    }

    private static Boolean checkFORCodePresenceInProject(ResearcherPortalProject__c project,Research_Project_Classification__c forCodes,ResearcherPublicProjectsHelperUtil.RMITPublicProjectWrapper rmitPublicProjectWrapper){
    	Boolean isSearchTextPresent = false;
    	String forCode;

			if(ResearcherPublicProjectsHelperUtil.isRMProject(project) && !String.isBlank(forCodes.For_Six_Digit_Detail__c)){//6 Digit FOR Code
			    system.debug(' is RM Project');
                forCode = forCodes.For_Six_Digit_Detail__c.split('-')[0];
                isSearchTextPresent = checkFORCodeForSixDigit(rmitPublicProjectWrapper.configuration.forCodeDetailSearch,forCode.trim());//RPORW-1355
                system.debug('forCodeDetail : '+rmitPublicProjectWrapper.configuration.forCodeDetailSearch);
                system.debug('forCodes : 6 '+forCode.trim());
                
                system.debug(' ## isSearchTextPresent : '+isSearchTextPresent);

			}else if(!ResearcherPublicProjectsHelperUtil.isRMProject(project) && !String.isBlank(forCodes.For_Four_Digit_Detail__c)){//4 Digit FOR Code
			  	system.debug(' is User Created');
			  	forCode = forCodes.For_Four_Digit_Detail__c.split('-')[0];
			  	isSearchTextPresent =  rmitPublicProjectWrapper.configuration.forCodeDetailSearch.contains(forCode.trim());

				system.debug('forCodeDetail : '+rmitPublicProjectWrapper.configuration.forCodeDetailSearch);
				system.debug('forCodes : 4 '+forCode.trim());

				system.debug(' ## isSearchTextPresent : '+isSearchTextPresent);
			}
		return isSearchTextPresent;
    }

  /*@Author     : Gourav Bhardwaj
  @description  : Special Logic for 6Digit code, as from UI we are getting 4digits only. So 6 digit needs to be compared with ech 4digit in a loop
  @Story        : RPORW-1355
  @CreatedDate  : 23-4-2019
  */
    private static Boolean checkFORCodeForSixDigit(Set<String> forCodesSearched,String forCodeOfProject){
        Boolean isSearchTextPresent = false;
        for(String forCode : forCodesSearched){
         //   if(forCodeOfProject.containsIgnoreCase(forCode)){
            if(forCodeOfProject.startsWithIgnoreCase(forCode)){ //RPORW-1403
                isSearchTextPresent = true;
                break;
            }
        }
        return isSearchTextPresent;
    }


  /*@Author     : Gourav Bhardwaj
  @description  : Method maps Clasification records to Project id
  @Story        : RPORW-294
  @CreatedDate  : 1/4/2019
  */
    public static Map<id,List<Research_Project_Classification__c>> getClasificationMap(Map<Id,List<Research_Project_Classification__c>> allProjectClassificationMap,
                                                                       Research_Project_Classification__c projectClassification )
    {

      system.debug('### allProjectClassificationMap : '+allProjectClassificationMap);

        if(!allProjectClassificationMap.containskey(projectClassification.ResearcherPortalProject__c))
        {
            List<Research_Project_Classification__c> projectClassficationsList = new List<Research_Project_Classification__c>();
            projectClassficationsList.add(projectClassification);
            allProjectClassificationMap.put(projectClassification.ResearcherPortalProject__c,projectClassficationsList);
        }
        else
        {
            List<Research_Project_Classification__c> projectClassficationsList = allProjectClassificationMap.get(projectClassification.ResearcherPortalProject__c);
            projectClassficationsList.add(projectClassification);
            allProjectClassificationMap.put(projectClassification.ResearcherPortalProject__c,projectClassficationsList);
        }
        
        return allProjectClassificationMap;
    }
}