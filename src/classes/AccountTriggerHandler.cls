/*********************************************************************************
*** @Class Name        : AccountTriggerHandler
*** @Author            : Anupam Singhal
*** @Requirement       : It is Handler class for AccountTrigger.
*** @Created date      : 5/09/2018
**********************************************************************************/
public without sharing class AccountTriggerHandler {
    
    public void accountDeDupeFieldUpdate(List<Account> accObjects){
        Id acc_BusinessOrganizationRT   = Schema.SObjectType.Account.RecordTypeInfosByName.get('Business Organization').RecordTypeId;
        Id acc_AcademicProgramRT        = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Program').RecordTypeId;
        Id acc_ActivatorStartUpRT       = Schema.SObjectType.Account.RecordTypeInfosByName.get('Activator Start Up').RecordTypeId;
        Id acc_BusinessAccountRT        = Schema.SObjectType.Account.RecordTypeInfosByName.get('Business Account').RecordTypeId;
        Id acc_EducationalInstitutionRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Educational Institution').RecordTypeId;
        Id acc_SecondarySchoolRT        = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id acc_BusinessOrganissationRT  = Schema.SObjectType.Account.RecordTypeInfosByName.get('Business Organisation').RecordTypeId;
        for(Account updateAcc: accObjects){
            if(updateAcc.RecordTypeId == acc_BusinessOrganizationRT){
                updateAcc.Business_Organization_RT__c = 'Business_Organization_RT';
            }else if(updateAcc.RecordTypeId == acc_BusinessOrganissationRT){
                updateAcc.Business_Organisation_RT__c = 'Business_Organisation_RT';
            }else if(updateAcc.RecordTypeId == acc_AcademicProgramRT){
                updateAcc.Academic_Program_RT__c = 'Academic_Program_RT';
            }else if(updateAcc.RecordTypeId == acc_ActivatorStartUpRT){
                updateAcc.Activator_Start_Up_RT__c = 'Activator_Start_Up_RT';
            }else if(updateAcc.RecordTypeId == acc_BusinessAccountRT){
                updateAcc.Business_Account_RT__c = 'Business_Account_RT';
            }else if(updateAcc.RecordTypeId == acc_SecondarySchoolRT){
                updateAcc.Secondary_School_RT__c = 'Secondary_School_RT';
            }else if(updateAcc.RecordTypeId == acc_EducationalInstitutionRT){
                updateAcc.Educational_Institution_RT__c = 'Educational_Institution_RT';
            }else{
                System.debug('Nothing to Update!');
            }
        }
    }
}