@isTest
public class EmailSuccessControllerTest {
    
    @TestSetup
    static void makeData()
    {
        List<Config_Data_Map__c> configurations = new List<Config_Data_Map__c>();
        Config_Data_Map__c configDataObj =  new Config_Data_Map__c();
        configDataObj.Name = 'Captcha';
        configDataObj.Config_Value__c = 'true';
        configurations.add(configDataObj);
        
        
        Account acc = new Account(Name = '21CC');
        insert acc;
        List<Contact> contacts = new List<Contact>();
        Contact con1 = new Contact(FirstName = 'Test', LastName = 'Contact1', AccountId = acc.id, Student_ID__c = '123',Email = 'puser000@amamama.com');
        contacts.add(con1);
        
        Contact con2 = new Contact(FirstName = 'Test', LastName = 'Contact2', AccountId = acc.id, Student_ID__c = '234',Email = 'puser000@amamama.com');
        contacts.add(con2);
        
        Contact con3 = new Contact(FirstName = 'Test', LastName = 'Contact3', AccountId = acc.id, Student_ID__c = '345',Email = 'test@mailnator.com');
        contacts.add(con3);
        
        Contact con4 = new Contact(FirstName = 'Test', LastName = 'Contact4', AccountId = acc.id, Student_ID__c = '567',hed__AlternateEmail__c = 'newtestemail@mailnator.com', Email = '');
        contacts.add(con4);
        
        insert contacts;
        
        User testusr = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community Login User - RMIT'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = contacts[0].id);
        insert testusr; 
        
        User selectUser = [SELECT id from User LIMIT 1];
        
        Config_Data_Map__c configDataObjUser =  new Config_Data_Map__c();
        configDataObjUser.Name = 'IntegrationUserId';
        configDataObjUser.Config_Value__c = selectUser.id;
        configurations.add(configDataObjUser);
        
        Config_Data_Map__c configCustomerAccountOwner =  new Config_Data_Map__c();
        configCustomerAccountOwner.Name = 'ECBNewCustomerAccountOwner';
        configCustomerAccountOwner.Config_Value__c = selectUser.id;
        configurations.add(configCustomerAccountOwner);
        
        
        insert configurations;
        TestDataFactoryUtilRefOne.testUserRegistrationEmail(); 
    }
    
    @isTest
    static void registerCustomerSuccessTest() 
    {
        
        String response = '03ADlfD1_klSxKX9zPSyDW0LK1oxpLSgSR2aXrm9vOSFTL9jX4-GtUYX8bZvops7KjoD4XA7uqxNkH69Yr1EgcH0xISvBnf6vYtnwVE2pVVZUOcm37ytv0gTT-oBSJ2VfofKPDoGfby7SJl55jsUYbGVe4QLHfuK-iwqEkJXngt4MmfkfgPQD1sjNuW9iRwnGmdKrq_uLaKi46L8jTVsSgpehrKluXPnttLprNDo_D_QAL_EK9pD4EuEcHtR-jxS54Xi-z5-9f94M--vLM2VdyxNqlydOZXCCzmKWffGHv7CNoXubSJV0j-Rk';
        String objWrapper = '{"givenname":"dsdsfdsf","familyname":"ssssss","email":"test@g.com","password":"4Jingle#","confirmpassword":"4Jingle#","mobilenumber":"","bday":"07-11-2018","country":"Australia","emailoptout":false}';
        ECBRegisterPageController.RegistrationResponse result = ECBRegisterPageController.register(response,objWrapper, new MockSuccessRegAbstractFactory());
        
        Contact cObj = [SELECT id from Contact where Email = 'test@g.com'];
        System.assert(cObj!= null);
        
        Boolean emailSuccess = EmailSuccessController.resendConfirmationEmail(cObj.id);
        system.assert(emailSuccess==true);  
        
    } 
}