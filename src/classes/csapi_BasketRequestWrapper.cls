@SuppressWarnings('PMD.AvoidGlobalModifier')
global class csapi_BasketRequestWrapper {
    
    global class csapi_CourseDetails {
        
        ///
        /// sfdc product definition reference
        ///
        @AuraEnabled
        public string ProductReference      { get; set; }

        ///
        /// sfdc product configuration reference
        ///
        @AuraEnabled
        public string ProductId      { get; set; }
        
        ///
        /// rmit course details
        ///
        @AuraEnabled
        public string CourseName            { get; set; }
        
        @AuraEnabled
        public string CourseType            { get; set; }
        
        @AuraEnabled
        public string ProductLine           { get; set; }
        
        @AuraEnabled
        public string DeliveryMode          { get; set; }
        
        @AuraEnabled
        public string CourseOfferingId      { get; set; }
        
        @AuraEnabled
        public string SessionStartDate      { get; set; }
        
        @AuraEnabled
        public string SessionEndDate        { get; set; }
        
        @AuraEnabled
        public string Fees                  { get; set; }
        
        /* 
        @AuraEnabled
        public string ContactId             { get; set; }

        @AuraEnabled
        public string ContactType           { get; set; } 
        */
    }
    
    global class csapi_CourseInfo {

        ///
        /// personal data
        ///
        @AuraEnabled
        public string ContactId             { get; set; }

        
        @AuraEnabled
        public string ContactType           { get; set; } 
    
        ///
        /// rmit course details
        ///
        
        @AuraEnabled
        public string Id                    { get; set; }
        
        @AuraEnabled
        public string CourseName            { get; set; }
        
        @AuraEnabled
        public string CourseType            { get; set; }

        @AuraEnabled
        public string CourseOfferingId      { get; set; }

        @AuraEnabled
        public string SessionStartDate      { get; set; }
        
        @AuraEnabled
        public string SessionEndDate        { get; set; }

        @AuraEnabled
        public string ProductLine           { get; set; }

        @AuraEnabled
        public string ProgramPlan           { get; set; }

        @AuraEnabled
        public string DeliveryMode          { get; set; }
        
        ///
        /// pricing, discount and promocode details
        ///
        @AuraEnabled
        public string ListPrice             { get; set; }
        
        @AuraEnabled
        public string Discount              { get; set; }
        
        @AuraEnabled
        public string DiscountedPrice       { get; set; }

        @AuraEnabled
        public string Price                 { get; set; }
        
        //this is for 21 cc credential product compatibility! 
        //this product should be substituted with Learning Activity product and thus this property should then be removed!
        @AuraEnabled
        public string Fees                  { get; set; }

        @AuraEnabled
        public string PriceOverride         { get; set; }
            
        @AuraEnabled
        public string PromoCode             { get; set; }   
                    
        @AuraEnabled
        public string PromoCodeDiscount     { get; set; }   
                    
        @AuraEnabled
        public string PromoCodeDiscountType { get; set; }   
    }
    
    global class csapi_ProductInfo {
        
        @AuraEnabled
        public string Name { get; set; }
        
        @AuraEnabled
        public string Id { get; set; }
        
        @AuraEnabled
        public string Type { get; set; } 
        
        // Added on 21-01-2019 for ECB-5151 - Rishabh
        @AuraEnabled
        public string OfferingId { get; set; } 
        
        @AuraEnabled
        //public string CourseId { get; set; } Nikhil 7/2/2019 Changed CourseId to courseId for PMD Voilations
        public string courseId { get; set; }
        
        @AuraEnabled
        //public string ProgramId { get; set; } Nikhil 7/2/2019 Changed ProgramId to programId for PMD Voilations
        public string programId { get; set; }
        
        @AuraEnabled
        public string MaxPrice { get; set; }      
       
        @AuraEnabled
        public List<csapi_DeliveryModeInfo> DeliveryModes {
            get {
                if (this.DeliveryModes == null) {
                    this.DeliveryModes = new List<csapi_DeliveryModeInfo>();
                }
                return this.DeliveryModes;
            }
            set {
                this.DeliveryModes = value;
            }
        }
    }
    
    global class csapi_DeliveryModeInfo {
        
        @AuraEnabled
        public string Name { get; set; }
        
        @AuraEnabled
        public string Price{ get; set; }        
       
    }
    
    public abstract class csapi_SessionRequest {
        
        private string m_userId;
        private string m_userSessionId;
        private boolean m_isBatchProcessed;
        
        public string UserId {
            get {
                if (this.m_userId == null) {
                    return UserInfo.getUserId();
                }
                return this.m_userId;
            }
            set {
                this.m_userId = value;
            }
        }
        
        public string UserSessionId {
            get {
                if (this.m_userId == null) {
                    return UserInfo.getSessionId();
                }
                return this.m_userSessionId;
            }
            set {
                this.m_userSessionId = value;
            }
        }
        
        public boolean IsBatchProcessed {
            get {
                if (this.m_isBatchProcessed == null) {
                    this.m_isBatchProcessed = false;
                }
                return this.m_isBatchProcessed;
            }
            set {
                this.m_isBatchProcessed = value;
            }
        }
        
        public csapi_SessionRequest() {
        }
    }
    
    public abstract class csapi_SessionResponse {
        
        @AuraEnabled
        public string Status                        { get; set; }
        
        @AuraEnabled
        public string Message                       { get; set; }
        
        @AuraEnabled
        public string ErrorCode                     { get; set; }
    }
    
    global class ProductCatalogRequest extends csapi_SessionRequest {
       
    }
    
    global class ProductCatalogResponse extends csapi_SessionResponse {
        
        @AuraEnabled
        public List<csapi_ProductInfo> Products    { get; set; }
    }
    
    global class BasketInfoRequest extends csapi_SessionRequest {
    }
    
    global class BasketInfoResponse extends csapi_SessionResponse {
        
        @AuraEnabled
        public String Name                          { get; set; }
        
        @AuraEnabled
        public String TotalPrice                    { get; set; }
        
        @AuraEnabled
        public String BasketNumber                  { get; set; }
        
        @AuraEnabled
        public String PromoCode                     { get; set; }
        
        @AuraEnabled
        public List<csapi_CourseInfo> Courses       { get; set; }
        
    }

    global class BasketPromoRequest extends csapi_SessionRequest {
    
        @AuraEnabled
        public String PromoCode                     { get; set; }
    }
    
    global class BasketPromoResponse extends csapi_SessionResponse {
        
        @AuraEnabled
        public String Name                          { get; set; }
        
        @AuraEnabled
        public String TotalPrice                    { get; set; }
        
        @AuraEnabled
        public String BasketNumber                  { get; set; }
        
        @AuraEnabled
        public String PromoCode                     { get; set; }
        
        @AuraEnabled
        public List<csapi_CourseInfo> Courses       { get; set; }
    }
    
    global class ValidatePromoRequest extends csapi_SessionRequest {
        
        @AuraEnabled
        public String promoCode                     { get; set; }
    }
    
    global class ValidatePromoResponse extends csapi_SessionResponse {
        
        @AuraEnabled
        public Boolean isValid                     { get; set; }
        
        @AuraEnabled
        public String validationMessage            { get; set; }
    }
    
    global class IncrementPromoUsageRequest extends csapi_SessionRequest {
        
        @AuraEnabled
        public String promoCode                     { get; set; }
    }
    
    global class IncrementPromoUsageResponse extends csapi_SessionResponse {
        
    }

    global class CreateBasketRequest extends csapi_SessionRequest {
    
    }
    
    global class CreateBasketResponse extends csapi_SessionResponse {
        
        public Id BasketId { get; set; }
    }
    
    global class AddProductRequest extends csapi_SessionRequest {
        
        public csapi_CourseDetails Course   { get; set; }
    }
    
    global class AddProductResponse extends csapi_SessionResponse {
        
    }
    
    global class UpdateProductRequest extends csapi_SessionRequest {
        
        public csapi_CourseDetails Course   { get; set; }
    }
    
    global class UpdateProductResponse extends csapi_SessionResponse {
        
    }
    
    global class DeleteProductRequest extends csapi_SessionRequest {
        
        public csapi_CourseDetails Course   { get; set; }
    }
    
    global class DeleteProductResponse extends csapi_SessionResponse {
        
    }
    
    global class SyncBasketRequest extends csapi_SessionRequest {
    }
    
    global class SyncBasketResponse extends csapi_SessionResponse {
        
    }  
    
}