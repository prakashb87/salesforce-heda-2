/************************************************************************************************
 *** @TestClass  		: UpdateEmailOnContactBatchTest
 *** @Author		    : Shubham Singh
 *** @Requirement     	: Batch to updated the Email value on Contact
 *** @Created date    	: 19/12/2017
 ************************************************************************************************/
/*************************************************************************************************
 *** @About Class
 *** This Testclass is to test the Batch UpdateEmailOnContactBatch.
 *************************************************************************************************/
@isTest
public class UpdateEmailOnContactBatchTest {
    //create test data and inserting it in contact
    @isTest static void emailOnContact() {
        list < Contact > lstcon = new list < Contact > ();
        
        Contact con3 = new Contact(lastname = 'Test name2', hed__Preferred_Email__c = 'Work', Work_Email__c = 'testUniversity2@gmail.com',
        							email = 'testUniversity2@gmail.com', Home_Email__c = 'testUniversity2@gmail.com',
        							Other_Email__c = 'testUniversity2@gmail.com',
        							Latest_Email__c = 'testUniversity2@gmail.com',hed__UniversityEmail__c = 'testUniversity2@gmail.com', 
        							hed__WorkEmail__c = 'testwork2@gmail.com', hed__AlternateEmail__c = 'testalternate2@gmail.com',
        							Home_Email_manual__c= 'testalternate2@gmail.com');
        insert con3;
        Contact con4 = new Contact(lastname = 'Test name3', hed__Preferred_Email__c = 'Alternate', Work_Email__c = 'testUniversity2@gmail.com',
        							email = 'testUniversity2@gmail.com', Home_Email__c = 'testUniversity2@gmail.com',
        							Other_Email__c = 'testUniversity2@gmail.com',
        							Latest_Email__c = 'testUniversity2@gmail.com',hed__UniversityEmail__c = 'testUniversity2@gmail.com', 
        							hed__WorkEmail__c = 'testwork2@gmail.com', hed__AlternateEmail__c = 'testalternate2@gmail.com',
        							Home_Email_manual__c= 'testalternate2@gmail.com');
        insert con4;
		//run the batch class
        Test.startTest();
        UpdateEmailOnContactBatch ema = new UpdateEmailOnContactBatch();
        Database.executeBatch(ema, 200);
        Test.stopTest();
        //verifying the results
        System.assertEquals('testUniversity2@gmail.com', con3.Email);
        System.assertEquals('testUniversity2@gmail.com', con4.Email);

    }
}