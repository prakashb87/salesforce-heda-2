@isTest
public class AddMicroCredentialsBatchTest {
    
    static Account accDepartment;
    //static Account accAdministrative;
    
    static User testRunUser;
    
    static hed__Course__c course;
    static hed__Course_Offering__c courseOffering;
    
    static csapi_Message__c csInstanceAddSuccess;
    static csapi_Message__c csInstanceAddFail;
    
    static void setupTestData() {
        
        /* string testRunUserName = Label.TestRunUser;
        system.debug('testRunUserName >> ' + testRunUserName);
        
        testRunUser = [SELECT Id, name, UserRoleId FROM User WHERE UserName = :testRunUserName limit 1];
        system.debug('testRunUser >> ' + testRunUser); */
        
        Account accDepartment = TestUtility.createTestAccount(true
            , 'Department Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        
        Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        
        List<String> lstParams = new List<String>{'Test', 'Con'};
        Contact con = TestUtility.createTestContact(true
            , lstParams
            , accAdministrative.Id);
        
        testRunUser = TestUtility.createUser('Customer Community Login User - RMIT', false);
        testRunUser.ContactId = con.Id;
        insert testRunUser;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;

        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;
        
        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;
        
        courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;

        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name = course.Name
            , Course_Program__c = 'Course'
            , Course_Offering_ID__c = courseOffering.Id
            , cspmb__Product_Definition_Name__c = 'Test definition'
            , cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c
            , cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c);
        insert priceItem;
        
        csInstanceAddSuccess = new csapi_Message__c();         
        csInstanceAddSuccess.Name = 'CSAPI_PROD_ADDED_SUCCESSFULLY';        
        csInstanceAddSuccess.Status__c = 'Pass';
        csInstanceAddSuccess.Message__c = 'CSAPI_PROD_ADDED_SUCCESSFULLY';
        csInstanceAddSuccess.Code__c = 'S001';
        insert csInstanceAddSuccess;
        
        csapi_Message__c csInstanceDelSuccess = new csapi_Message__c();         
        csInstanceDelSuccess.Name = 'CSAPI_PROD_DELETED_SUCCESSFULLY';        
        csInstanceDelSuccess.Status__c = 'Pass';
        csInstanceDelSuccess.Message__c = 'CSAPI_PROD_DELETED_SUCCESSFULLY';
        csInstanceDelSuccess.Code__c = 'S002';
        insert csInstanceDelSuccess;
        
        csapi_Message__c csInstanceInfoSuccess = new csapi_Message__c();         
        csInstanceInfoSuccess.Name = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';        
        csInstanceInfoSuccess.Status__c = 'Pass';
        csInstanceInfoSuccess.Message__c = 'CSAPI_BASKET_REFRESHED_SUCCESSFULLY';
        csInstanceInfoSuccess.Code__c = 'S003';
        insert csInstanceInfoSuccess;
        
        csapi_Message__c csInstanceSyncSuccess = new csapi_Message__c();         
        csInstanceSyncSuccess.Name = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';        
        csInstanceSyncSuccess.Status__c = 'Pass';
        csInstanceSyncSuccess.Message__c = 'CSAPI_BASKET_SYNCED_SUCCESSFULLY';
        csInstanceSyncSuccess.Code__c = 'S004';
        insert csInstanceSyncSuccess;

        csapi_Message__c csInstanceSyncFail = new csapi_Message__c();         
        csInstanceSyncFail.Name = 'CSAPI_BASKET_SYNC_FAIL';        
        csInstanceSyncFail.Status__c = 'Fail';
        csInstanceSyncFail.Message__c = 'CSAPI_BASKET_SYNC_FAIL';
        csInstanceSyncFail.Code__c = 'S007';
        insert csInstanceSyncFail;

        csInstanceAddFail = new csapi_Message__c();         
        csInstanceAddFail.Name = 'CSAPI_PROD_ADD_FAIL';        
        csInstanceAddFail.Status__c = 'Fail';
        csInstanceAddFail.Message__c = 'CSAPI_PROD_ADD_FAIL';
        csInstanceAddFail.Code__c = 'S005';
        insert csInstanceAddFail;

        csapi_Message__c csInstanceAlreadyPurchased = new csapi_Message__c();         
        csInstanceAlreadyPurchased.Name = 'CSAPI_COURSE_ALREADY_PURCHASED';        
        csInstanceAlreadyPurchased.Status__c = 'Fail';
        csInstanceAlreadyPurchased.Message__c = 'CSAPI_COURSE_ALREADY_PURCHASED';
        csInstanceAlreadyPurchased.Code__c = 'S006';
        insert csInstanceAlreadyPurchased;

        csapi_Message__c csInstanceCreateBasket = new csapi_Message__c();         
        csInstanceCreateBasket .Name = 'CSAPI_BASKET_CREATED_SUCCESSFULLY';        
        csInstanceCreateBasket .Status__c = 'Pass';
        csInstanceCreateBasket .Message__c = 'CSAPI_BASKET_CREATED_SUCCESSFULLY';
        csInstanceCreateBasket .Code__c = 'S008';
        insert csInstanceCreateBasket ;
    }
    
    static testmethod void testAddMicroCredentials() {
        
        setupTestData();
        
        csapi_BasketRequestWrapper.CreateBasketResponse basketResponse = null;
        
        System.runAs(testRunUser) {
            
            Test.StartTest();
            
            csapi_BasketRequestWrapper.CreateBasketRequest basketRequest = new csapi_BasketRequestWrapper.CreateBasketRequest();
            basketRequest.UserId = testRunUser.Id;
            basketResponse = csapi_BasketExtension.createBasket(basketRequest);

            csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
            details.CourseName = course.Name;
            details.CourseType = 'Course';
            details.ProductReference = 'Test definition';
            details.CourseOfferingId = courseOffering.Id;
            details.SessionStartDate = string.valueOf(courseOffering.hed__Start_Date__c);
            details.SessionEndDate = string.valueOf(courseOffering.hed__End_Date__c);
            
            csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
            request.UserId = testRunUser.Id;
            request.Course = details;

            list<csapi_BasketRequestWrapper.AddProductRequest> requests=new list<csapi_BasketRequestWrapper.AddProductRequest> ();
            requests.add(request);
           
            AddMicroCredentialsBatch addMicrCredBatch=new AddMicroCredentialsBatch(requests);
            Database.executeBatch(addMicrCredBatch); 
            
            Test.StopTest();
                
            cscfga__Product_Basket__c basket = [select Id, Name from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id limit 1];
            system.debug('basket >> ' + basket);
            system.debug('basketResponse.BasketId >> ' + basketResponse.BasketId);
            
            list<cscfga__Product_Configuration__c> products = [select Id, Name
                from cscfga__Product_Configuration__c 
                where cscfga__Product_Basket__c = :basketResponse.BasketId];
                
            system.debug('products.size() >> ' + products.size());
            system.assertEquals(products.size(), 1);
        }
    }
    
    static testmethod void testAddMicroCredentialsFailure() {
        
        setupTestData();
        delete csInstanceAddFail;
        delete csInstanceAddSuccess;
        
        csapi_BasketRequestWrapper.CreateBasketResponse basketResponse = null;
        
        System.runAs(testRunUser) {
            Test.StartTest();
            
            csapi_BasketRequestWrapper.CreateBasketRequest basketRequest = new csapi_BasketRequestWrapper.CreateBasketRequest();
            basketRequest.UserId = testRunUser.Id;
            basketResponse = csapi_BasketExtension.createBasket(basketRequest);
            
            csapi_BasketRequestWrapper.csapi_CourseDetails details = new csapi_BasketRequestWrapper.csapi_CourseDetails();
            details.CourseName = course.Name;
            details.CourseType = 'Course';
            details.ProductReference = 'Test definition';
            details.CourseOfferingId = courseOffering.Id;
            details.SessionStartDate = string.valueOf(courseOffering.hed__Start_Date__c);
            details.SessionEndDate = string.valueOf(courseOffering.hed__End_Date__c);
            
            csapi_BasketRequestWrapper.AddProductRequest request = new csapi_BasketRequestWrapper.AddProductRequest();
            request.UserId = testRunUser.Id;
            request.Course = details;
            
            list<csapi_BasketRequestWrapper.AddProductRequest> requests=new list<csapi_BasketRequestWrapper.AddProductRequest> ();
            requests.add(request);
           
            AddMicroCredentialsBatch addMicrCredBatch=new AddMicroCredentialsBatch(requests);
            Database.executeBatch(addMicrCredBatch); 
            
            Test.StopTest();
            
            cscfga__Product_Basket__c basket = [select Id, Name from cscfga__Product_Basket__c where OwnerId = :testRunUser.Id limit 1];
            system.debug('basket >> ' + basket);
            system.debug('basketResponse.BasketId >> ' + basketResponse.BasketId);
            
            list<cscfga__Product_Configuration__c> products = [select Id, Name
                from cscfga__Product_Configuration__c 
                where cscfga__Product_Basket__c = :basketResponse.BasketId];
                
            system.debug('products.size() >> ' + products.size());
            system.assertEquals(products.size(), 0);
        }
    }
}