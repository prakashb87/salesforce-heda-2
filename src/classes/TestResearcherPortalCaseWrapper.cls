/*******************************************
Purpose: Test class ResearcherPortalCaseWrapper
History:
Created by Md Ali on 12/11/2018
*******************************************/
@istest
public class TestResearcherPortalCaseWrapper {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Md Ali
    // Created Date :  12/01/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
    /************************************************************************************
    // Purpose      :  Test functionality for ResearcherPortalSupportController
    // Developer    :  Md Ali
    // Created Date :  11/12/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void testPositiveUseCaseForResearcherPortalCaseWrapperMethods(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {
           
           //Positive Test case  
           String caseStringWrapper = '{"portalFeedback":"Articles & FAQs","description":"dasdas","satisfactionRating":"4","recordType":"Researcher Portal Feedback","caseAttachments":[{"fileName":"download.png","fileContent":"iVBORw0KGgoAAAANSUhEUgAAANcAAADrCAMAAADNG/","contentType":"png"}]}';
           ResearcherPortalCaseWrapper caseWrapper = new ResearcherPortalCaseWrapper();
           caseWrapper=(ResearcherPortalCaseWrapper)JSON.deserialize(caseStringWrapper, ResearcherPortalCaseWrapper.class);
            
            ResearcherPortalCaseWrapper.CaseAttachmentWrapper caseAttachment = new ResearcherPortalCaseWrapper.CaseAttachmentWrapper(
            'ContactUs.png','.png','iVBORw0KGgoAAAANSUhEUgAAANcAAADrCAMAAADNG/NRAAAAeFBMVEX/');
           System.assert(caseWrapper !=null,'Wrapper record has been set'); 
        } 
    }   
      /************************************************************************************
    // Purpose      :  Test functionality for ResearcherPortalSupportController
    // Developer    :  Md Ali
    // Created Date :  12/05/2018                 
    //***********************************************************************************/
    
    @isTest
    public static void testNegativeUseCaseForResearcherPortalCaseWrapperMethods(){
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {
           
           //Negative Test case  
           String caseStringWrapper = '{"researcherCaseTopic":"Enabling Capability Platform (ECPs)","researcherCaseSubTopic":"Funding options, application, deadlines and management","Subject":"test subject","Description":"test description","recordType":"Researcher Portal Support Case","status":"Closed"}';
           ResearcherPortalCaseWrapper caseWrapper = new ResearcherPortalCaseWrapper();
           caseWrapper=(ResearcherPortalCaseWrapper)JSON.deserialize(caseStringWrapper, ResearcherPortalCaseWrapper.class);
           System.assert(caseWrapper !=null,'Wrapper record has been set'); 
        } 
    }     
}