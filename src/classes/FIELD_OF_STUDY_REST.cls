/*********************************************************************************
*** @ClassName         : FIELD_OF_STUDY_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 16/08/2018
*** @Modified by       : Dinesh Kumar
*** @modified date     : 25/10/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/FieldOfStudy')
global with sharing class FIELD_OF_STUDY_REST {
    public String groups;
    public String code;
    public String effectiveDate;
    public String effectiveStatus;
    public String description;
    public String shortDescription;
    
    @HttpPost
    global static Map < String, String > upsertFieldOfStudy() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert fieldOFStudy
        list < Field_of_Study__c > UpsertFOS = new list < Field_of_Study__c > ();
        //fieldOFStudy
        Field_of_Study__c fieldOFStudy;
        //to check the effective date
        map < String,Date > updateEffectiveDate = new map < String,Date >();
        
        //to make unique key
        String keyValue ;
        
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;

            //no need to add [] in JSON
            String reqStrOne = '[' + request.requestBody.toString() + ']';
            String reqStr = reqStrOne.replaceFirst('"group"', '"groups"');
            
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < FIELD_OF_STUDY_REST > postString = (List < FIELD_OF_STUDY_REST > ) System.JSON.deserialize(reqStr, List < FIELD_OF_STUDY_REST > .class);
            system.debug('postString ----->'+postString);
            //getting fieldOFStudy code
            list < String > updatefieldOFStudyCode = new list < String > ();
            //getting fieldOFStudy groups
            list < String > updatefieldOFStudyGroup = new list < String > ();
            if (postString != null) {
                for (FIELD_OF_STUDY_REST fieldOFStudyrest: postString) {
                    if (fieldOFStudyrest.groups != null && (fieldOFStudyrest.groups).length() > 0 && fieldOFStudyrest.code != null && (fieldOFStudyrest.code).length() > 0 ) {
                        updatefieldOFStudyCode.add(fieldOFStudyrest.code);
                        updatefieldOFStudyGroup.add(fieldOFStudyrest.groups);
                        if(fieldOFStudyrest.effectiveDate != null && (fieldOFStudyrest.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(fieldOFStudyrest.groups+''+fieldOFStudyrest.code,Date.ValueOf(fieldOFStudyrest.effectiveDate));
                        }
                    } else {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
    
            //getting the old fieldOFStudy for update
            list < Field_of_Study__c > listfieldOFStudy = new list < Field_of_Study__c > ();
            if (updatefieldOFStudyCode.size() > 0 && updatefieldOFStudyCode != null) {
                listfieldOFStudy = [select id,Name,Field_of_Study_Code__c,Effective_date__c,Effective_status__c,Description__c,Short_description__c from Field_of_Study__c where Name IN: updatefieldOFStudyCode and Field_of_Study_Code__c IN : updatefieldOFStudyGroup];
            }
            
            //map to update the old fieldOFStudy
            map < String, ID > oldfieldOFStudy = new map < String, ID > ();
            map < String,  Field_of_Study__c > stringFOSMap = new map < String, Field_of_Study__c > ();
            for (Field_of_Study__c fos: listfieldOFStudy) {
                //String uniqueKey = fos.Name+''+fos.Field_of_Study_Code__c;
                String uniqueKey = fos.Field_of_Study_Code__c+''+fos.Name;
                if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){
                    if(updateEffectiveDate.get(uniqueKey) <= System.today() && fos.Effective_date__c <= updateEffectiveDate.get(uniqueKey))
                    {
                        oldfieldOFStudy.put(uniqueKey, fos.ID);
                        stringFOSMap.put(uniqueKey,fos);
                    }else if(fos.Effective_date__c > updateEffectiveDate.get(uniqueKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend; 
                }
            }
            
            //insert or update the fieldOFStudy
            for (FIELD_OF_STUDY_REST fieldOFStudyRest: postString) {
                keyValue = fieldOFStudyRest.groups+''+fieldOFStudyRest.code;
                fieldOFStudy = new Field_of_Study__c();
                fieldOFStudy.Name = fieldOFStudyRest.code;//Field of Study Code will be displayed in all the lookups to FOS - RM-1944
                fieldOFStudy.Field_of_Study_Code__c = fieldOFStudyRest.groups;//This refers to the field of Study Group Code from SAMS - RM-1944
                fieldOFStudy.Effective_date__c = fieldOFStudyRest.effectiveDate != null ? Date.valueOf(fieldOFStudyRest.effectiveDate) : null;
                fieldOFStudy.Effective_status__c = fieldOFStudyRest.effectiveStatus;
                fieldOFStudy.Description__c = fieldOFStudyRest.description;
                fieldOFStudy.Short_description__c= fieldOFStudyRest.shortDescription;
                fieldOFStudy.FieldOfStudy_Key__c = keyValue;
                //assigning the fund source id for update
                if (oldfieldOFStudy.size() > 0 && oldfieldOFStudy.get(fieldOFStudyRest.groups+''+fieldOFStudyRest.code) != null){
                    if (stringFOSMap.get(fieldOFStudyRest.groups+''+fieldOFStudyRest.code).Effective_date__c <= fieldOFStudy.Effective_date__c){
                        fieldOFStudy.ID = oldfieldOFStudy.get(fieldOFStudyRest.groups+''+fieldOFStudyRest.code);
                    }else{
                        fieldOFStudy=null;
                    }
                }     
                if(fieldOFStudy !=null)
                {
                    UpsertFOS.add(fieldOFStudy);
                }
            }
            //upsert fieldOFStudy
            if (UpsertFOS != null && UpsertFOS.size() > 0 && Date.valueOf(postString[0].effectiveDate) <= system.today()) {
                upsert UpsertFOS FieldOfStudy_Key__c;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Student was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}