/*********************************************************************************
*** @TestClassName     : Location_Rest_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 16/08/2018
**********************************************************************************/
@isTest
public class Location_Rest_Test {
static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Location';
        
        //creating test data
        String JsonMsg = '{"setId": "RMITU","effectiveDate": "1901-05-01","effectiveStatus": "A","description": "Distance mandatorhy Students","shortDescription": "nonoe","building": "","floor": "","sector": "18","location": "DOF","jurisdiction": "","country": "AUS","addressLine1": "20","addressLine2": "","addressLine3": "","city": "","addressLine4": "","state": "","postalCode": "","countryCode": "","county": "","phoneNumber": "","extension": "","faxNumber": "","languageCode": "","region": "AUS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = LOCATION_REST.upsertLocation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Location';
        
        //creating test data
        String JsonMsg = '{"setId": "","effectiveDate": "1901-05-01","effectiveStatus": "A","description": "Distance mandatorhy Students","shortDescription": "nonoe","building": "","floor": "","sector": "18","location": "DOF","jurisdiction": "","country": "AUS","addressLine1": "20","addressLine2": "","addressLine3": "","city": "","addressLine4": "","state": "","postalCode": "","countryCode": "","county": "","phoneNumber": "","extension": "","faxNumber": "","languageCode": "","region": "AUS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = LOCATION_REST.upsertLocation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = LOCATION_REST.upsertLocation();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Location__c lctn = new Location__c();
        
        lctn.Name = 'RMITU';
        
        lctn.Effective_Date__c = Date.valueOf('1902-05-01');
        
        lctn.Location__c = 'DOF';
            
        insert lctn;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Location';
        
        //creating test data
        String JsonMsg = '{"setId": "RMITU","effectiveDate": "1901-05-01","effectiveStatus": "A","description": "Distance mandatorhy Students","shortDescription": "nonoe","building": "","floor": "","sector": "18","location": "DOF","jurisdiction": "","country": "AUS","addressLine1": "20","addressLine2": "","addressLine3": "","city": "","addressLine4": "","state": "","postalCode": "","countryCode": "","county": "","phoneNumber": "","extension": "","faxNumber": "","languageCode": "","region": "AUS"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = LOCATION_REST.upsertLocation();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}