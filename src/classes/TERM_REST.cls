/*********************************************************************************
 *** @ClassName         : TERM_REST
 *** @Author            : Shubham Singh 
 *** @Requirement       : 
 *** @Created date      : 23/08/2018
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 23/08/2018
 **********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Term')
global with sharing class TERM_REST {
    public String institution;
    public String academicCareer;
    public String term;
    public String description;
    public String shortDescription;
    public String termCategory;
    public String termBeginDate;
    public String termEndDate;
    public String academicYear;
    public String holidaySchedule;
    public String weeksOfInstruction;
    public String termType;
    @HttpPost
    global static Map < String, String > upsertTerm() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Term__c
        list < Term__c > upsertTermObject = new list < Term__c > ();
        //Term__c
        Term__c term;

   try {

        //request and response variable
        RestRequest request = RestContext.request;

        RestResponse response = RestContext.response;

        //no need to add [] in JSON
        String reqStr = '[' + request.requestBody.toString() + ']';
        system.debug('reqStr-->' + reqStr);
        response.addHeader('Content-Type', 'application/json');

        //getting the data from JSON and Deserialize it
        List < TERM_REST > postString = (List < TERM_REST > ) System.JSON.deserialize(reqStr, List < TERM_REST > .class);
        //get institution
        list < String > listInstitution = new list < String > ();
        //get Career
        list < String > listAcademicCareer = new list < String > ();
        //get Term code
        list < String > listTermCode = new list < String > ();
        if (postString != null) {
            for (TERM_REST termrest: postString) {
                if (termrest.institution != null && (termrest.institution).length() > 0 && termrest.academicCareer != null && (termrest.academicCareer).length() > 0 && termrest.term != null && (termrest.term).length() > 0) {
                    listInstitution.add(termrest.institution);
                    listAcademicCareer.add(termrest.academicCareer);
                    listTermCode.add(termrest.term);
                }else{
                    responsetoSend = throwError();
                    return responsetoSend;
                }
            }
        }

        //getting the old term for update
        list < Term__c > previousTerm = new list < Term__c > ();
        if (listInstitution != null && listInstitution.size() > 0 && listAcademicCareer != null && listAcademicCareer.size() > 0 && listTermCode != null && listTermCode.size() > 0) {
            previousTerm = [SELECT id,Institution__r.Name,Institution__c, Academic_Career__c, Term_Code__c, Description__c, Short_Description__c, Term_Category__c, Term_Begin_Date__c, Term_End_Date__c, Academic_Year__c, Holiday_Schedule__c, Weeks_of_Instruction__c, Term_Type__c
                FROM Term__c WHERE Institution__r.Name IN: listInstitution AND Academic_Career__c IN: listAcademicCareer AND Term_Code__c IN: listTermCode
            ];
        }

        //get institution from account (as one record will be there from JSON)
        Account institution;
        if (listInstitution != null && listInstitution.size() > 0)
        {
            institution = [SELECT id FROM account WHERE Name =: listInstitution[0] LIMIT 1];
        }
        //map to update the previous term
        map < String, Term__c > oldTermMap = new map < String, Term__c > ();
        String uniqKey;
        for (Term__c trm: previousTerm) {
            if (trm != null & trm != null) {
                uniqKey = trm.Institution__r.Name + '' + trm.Academic_Career__c + '' + trm.Term_Code__c;
                oldTermMap.put(uniqKey, trm);
            }
        }

        //insert or update the term
        for (TERM_REST termRest: postString) {
            String key = termRest.institution + '' + termRest.academicCareer +''+ termRest.term;
            term = new Term__c();
            term.Institution__c = institution != null ? institution.Id : null;
            term.Academic_Career__c = termRest.academicCareer;
            term.Term_Code__c = termRest.term;
            term.Description__c = termRest.description;
            term.Short_Description__c = termRest.shortDescription;
            term.Term_Category__c = termRest.termCategory;
            term.Term_Begin_Date__c = termRest.termBeginDate != null ? Date.valueOf(termRest.termBeginDate) : null;
            term.Term_End_Date__c = termRest.termEndDate != null ? Date.ValueOF(termRest.termEndDate) : null;
            term.Academic_Year__c = termRest.academicYear;
            term.Holiday_Schedule__c = termRest.holidaySchedule;
            term.Weeks_of_Instruction__c = termRest.weeksOfInstruction != null ? Integer.ValueOF(termRest.weeksOfInstruction) : null;
            term.Term_Type__c = termRest.termType;

            if (oldTermMap.size() > 0 && oldTermMap.get(key) != null) {
                term.ID = oldTermMap.get(key).ID;
            }
            upsertTermObject.add(term);
        }
        //upsert term
        if (upsertTermObject != null && upsertTermObject.size() > 0) {
            upsert upsertTermObject;
        }
        responsetoSend.put('message', 'OK');
        responsetoSend.put('status', '200');
        return responsetoSend;
    } catch (Exception excpt) {
        //If exception occurs then return this message to IPASS
        responsetoSend = throwError();
        return responsetoSend;
    }

}
public static Map < String, String > throwError() {
    //to store the error response
    Map < String, String > errorResponse = new Map < String, String > ();
    errorResponse.put('message', 'Oops! Term was not well defined, amigo');
    errorResponse.put('status', '404');
    //If error occurs then return this message to IPASS
    return errorResponse;
}
}