public class SuccessPageController
 {
 	public String dashboardUrl {get;set;}
 	public String backButtonUrl {get;set;}
 	public String googleTagManagerUrl{get;set;}
 	public String googleTagManagerID{get;set;}
    public String surveyURL{get;set;} // Added FOR ECB-5657 - 29/04/2019 - Rishabh

	public SuccessPageController() 
	{
		 dashboardUrl = ConfigDataMapController.getCustomSettingValue('MyDashBoardUrl'); 
		 backButtonUrl = ConfigDataMapController.getCustomSettingValue('KeepShopping'); 
		 googleTagManagerUrl = ConfigDataMapController.getCustomSettingValue('GoogleTagManagerUrl'); 
		 googleTagManagerID = ConfigDataMapController.getCustomSettingValue('GoogleTagManagerID'); 
         surveyURL = ConfigDataMapController.getCustomSettingValue('SurveyURL'); // Added FOR ECB-5657 - 29/04/2019 - Rishabh

	}
}