@isTest
public with sharing class TestResearcherPortalContentSearch {
    private static final String CONTENT_TYPE = 'TestContentType';
    private static final String CONTENT_NAME = 'TestContent';
    private static final String SITE_NAME = 'TestSite';

    static cms__Content__c testContent;

    static void buildTestContent() {
        // Normally we wouldn't touch any cms__ objects, even in a test
        // Here we need a valid cms__Content__c ID to insert an OCMS_Search__c record
        testContent = new cms__Content__c(
                cms__Name__c = CONTENT_NAME,
                cms__Site_Name__c = SITE_NAME,
                cms__Version_Number__c = 1,
                cms__Revision_Number__c = 0
        );

        insert testContent;
    }

    static ContentBundle buildContentBundle(Id originId, Integer importance) {
        ContentBundle bundle = new ContentBundle();
        bundle.originId = originId;

        bundle.contentType = new ContentTypeBundle();
        bundle.contentType.name = CONTENT_TYPE;

        AttributeBundle.ContentAttribute importanceAttribute = new AttributeBundle.ContentAttribute();
        importanceAttribute.name = 'importance';
        importanceAttribute.value = String.valueOf(importance);
        bundle.contentAttributes = new Map<String, AttributeBundle.ContentAttribute[]>();
        bundle.contentAttributes.put('en_US', new AttributeBundle.ContentAttribute[] { importanceAttribute });

        return bundle;
    }

    static JsonMessage.ApiResponse buildApiResponse(Object responseObject) {
        JsonMessage.ApiResponse response = new JsonMessage.ApiResponse();
        response.isSuccess = true;
        response.responseObject = Json.serialize(responseObject);
        return response;
    }

    static testmethod void testBatchExecution() {
        buildTestContent();

        ContentBundle bundle;
        OCMS_Search__c[] searchRecords;

        // Test insert
        RenderResultBundle renderResultBundle = new RenderResultBundle();
        renderResultBundle.renderings = new RenderResultBundle.RenderedContent[] {};
        renderResultBundle.renderings.add(new RenderResultBundle.RenderedContent());
        renderResultBundle.renderings[0].contentBundle = buildContentBundle(testContent.Id, 50);

        JsonMessage.ApiResponse renderResponse = buildApiResponse(renderResultBundle);
        cms.ServiceEndPoint.addMockResponse('OrchestraRenderingApi', Json.serialize(renderResponse));

        Test.startTest();
            ResearcherPortalContentSearch.executeAsBatch(SITE_NAME);
        Test.stopTest();

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), '1 record should be created');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(50, searchRecords[0].Importance__c);
    }

    static testmethod void testUpsertSearchObjectsForContent() {
        buildTestContent();

        ContentBundle bundle;
        OCMS_Search__c[] searchRecords;

        // Test insert
        bundle = buildContentBundle(testContent.Id, 50);

        ResearcherPortalContentSearch.upsertSearchObjectsForContent(new ContentBundle[] { bundle });

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), '1 record should be created');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(50, searchRecords[0].Importance__c);

        // Test update
        bundle = buildContentBundle(testContent.Id, 10);

        ResearcherPortalContentSearch.upsertSearchObjectsForContent(new ContentBundle[] { bundle });

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), 'Existing record should be updated');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(10, searchRecords[0].Importance__c, 'Importance should be updated');
    }

    static testmethod void testLifecycleTrigger() {
        buildTestContent();

        ContentBundle bundle;
        OCMS_Search__c[] searchRecords;

        // Test insert
        bundle = buildContentBundle(testContent.Id, 50);

        cms__LifeCycleEvent__e evt = new cms__LifeCycleEvent__e(
                cms__EventName__c = 'CONTENT_PUBLISHED',
                cms__JsonObject__c = Json.serialize(bundle)
        );

        Test.startTest();
        EventBus.publish(evt);
        Test.stopTest();

        searchRecords = [SELECT Content__c, Importance__c, Content_Type__c FROM OCMS_Search__c];

        System.assertEquals(1, searchRecords.size(), '1 record should be created');
        System.assertEquals(testContent.Id, searchRecords[0].Content__c);
        System.assertEquals(50, searchRecords[0].Importance__c);
    }
}