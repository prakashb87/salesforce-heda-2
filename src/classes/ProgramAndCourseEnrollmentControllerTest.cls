/*
  ***************************************************************************************************************
  * @author       :Shreya Barua
  * @date         :09/10/2018 
  * @description  :Test Class For CreateEnrollmentForSectectedOffering class 
  ***************************************************************************************************************/
@isTest
public  class ProgramAndCourseEnrollmentControllerTest
 {
    static testMethod void testSelectedCourseOffering()
    {
        Test.startTest();
        TestDataFactoryUtil.dummycustomsetting();
        TestDataFactoryUtil.testDataForSelectedOffering();
        
        csord__Service__c selectServiceObj = [SELECT id,name from csord__Service__c LIMIT 1];
        hed__Program_Enrollment__c pgmEnrollId = [SELECT id from hed__Program_Enrollment__c LIMIT 1];
        hed__Course_Offering__c courseOffObj = [SELECT id,name from hed__Course_Offering__c where name ='test course offering 2'];

        if( selectServiceObj != null && pgmEnrollId != null && courseOffObj != null )
        {
            System.debug('Objects are  not empty');
            ProgramAndCourseEnrollmentController.createCourseConnectionForSelectedOffering(selectServiceObj.id, courseOffObj.id, pgmEnrollId.id );
            
            hed__Course_Enrollment__c newCourseCC = [SELECT id from hed__Course_Enrollment__c where hed__Course_Offering__c =:courseOffObj.id ];
            System.assert(newCourseCC!=null);
        }
        
 
    }
    
    static testMethod void testProgramAndCourseEnrollmentController()
    {
        Test.startTest();
        TestDataFactoryUtil.dummycustomsetting();
        TestDataFactoryUtilRefTwo.testMyDashboard();
        
        
        User usr = [SELECT Id,ContactId FROM User WHERE Username ='communityuser@unittest.com' ];
        System.assert(usr!=null);
        
        hed__Course__c crec = [SELECT Id FROM hed__Course__c WHERE Name ='Test RMITOnline Course'];
        System.assert(crec!=null);
        
        List<hed__Course_Enrollment__c> cenrollList = [Select Id from hed__Course_Enrollment__c where hed__Contact__c = :usr.ContactId AND Source_Type__c = 'MKT'];
        System.assert(cenrollList!=null);
        
       // Test.startTest();
        System.runAs(usr) {
            
                ProgramAndCourseEnrollmentController.getProgramCourseEnrollDetails();
                ProgramAndCourseEnrollmentController.getCourseOfferingDetails(crec.Id);
                ProgramAndCourseEnrollmentController.getProgressDetails();
                
            
        }

        Test.stopTest();
 
    }
    
    static testMethod void testGetCanvasDashboardRedirectURL()
    {
        Test.startTest();
        
        String[] canvasDashboardRedirectURLwithNoData = ProgramAndCourseEnrollmentController.getCanvasDashboardRedirectURL(); 
        System.AssertEquals(canvasDashboardRedirectURLwithNoData ,null,'Invalid Data');     
        
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name='CanvasDashboardRedirect', Config_Value__c='https://rmit.instructure.com/courses/'));
        configDataMaps.add(new Config_Data_Map__c(Name='OpenEDXDashboardRedirect', Config_Value__c='https://learn.rmitonline.edu.au/courses/'));
        insert configDataMaps;
        String[] canvasDashboardRedirectURL = ProgramAndCourseEnrollmentController.getCanvasDashboardRedirectURL(); 
        System.AssertEquals(canvasDashboardRedirectURL.size(),2,'Invalid Data');        
        Test.stopTest();
        
        
    }

    
}