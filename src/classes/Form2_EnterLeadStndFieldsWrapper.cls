/*******************************************
Purpose: Form2 Enter Lead Standard Fields Wrapper
History:
Created by Shalu on 27/02/19
*******************************************/
    @SuppressWarnings('PMD.VariableNamingConventions') 
    public class Form2_EnterLeadStndFieldsWrapper {
    	
	public String FirstName {get; set;}
    public String LastName {get; set;}
    public String Company {get; set;}
    public String Email {get; set;}
    public String Country {get; set;}
    public String AttachedFileName {get; set;}
    public blob AttachedFileBody {get; set;}
    
    public Form2_EnterLeadStndFieldsWrapper()
    {
    this.FirstName ='';
    this.LastName = '';
    this.Company ='';
    this.Email='';
    this.Country=''; 
    this.AttachedFileName='';
    this.AttachedFileBody=null;
    }
    }