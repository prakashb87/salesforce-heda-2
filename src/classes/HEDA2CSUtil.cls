/*****************************************************************************************************
 *** @Author            : Ming Ma
 *** @Purpose           : Utility class to sync HEDA courses info into price items in Cloud Sense
 *** @Created date      : 28/06/2018
 *** @JIRA ID           : TBA (Fixes to ECB-168,246,2968)
 *****************************************************************************************************/
public without sharing class HEDA2CSUtil {

    public static void transformCourses2PriceItems (List<hed__Course_Offering__c> courseOfferings) {
        createAllDeliveryMode();
        System.debug('HEDA2CSUtil.transformCourses2PriceItems entered');
        if (courseOfferings.size() > 0) {
            
            List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addOnAssociations = New List<cspmb__Price_Item_Add_On_Price_Item_Association__c>();
            List<cspmb__Add_On_Price_Item__c> rmitOnlineAddonItems = getRMITAddonItems();

            Id accountID21CC = get21CCAccountId();
            Id accountIDRMITOnline = getRMITOnlineAccountId();
            Id accountIDRMITTraining = getRMITTrainingAccountId();

            for (hed__Course_Offering__c courseOffering : courseOfferings) {
                //System.debug('transformCourses2PriceItems courseOffering ' + courseOffering);
                cspmb__Price_Item__c aPriceItme = New cspmb__Price_Item__c();
                //System.debug('*** courseOffering.hed__course__r.name ' + courseOffering.hed__course__r.name);
                aPriceItme.Name = courseOffering.hed__course__r.Name;
                aPriceItme.Course_Program__c = 'Course';
                aPriceItme.Course_ID__c = courseOffering.hed__course__c;
                aPriceItme.Course_Offering_ID__c = courseOffering.ID;
                if (accountIDRMITOnline != null && courseOffering.hed__course__r.hed__Account__c == accountIDRMITOnline)
                {
                    aPriceItme.cspmb__Product_Definition_Name__c = 'Learning Activity';
                    aPriceItme.Product_Line__c = 'RMIT Online';
                } 
                else if (accountID21CC != null && courseOffering.hed__course__r.hed__Account__c == accountID21CC)
                {
                    aPriceItme.cspmb__Product_Definition_Name__c = '21cc Credential';
                    aPriceItme.Product_Line__c = '21cc Credential';
                }
                else if (accountIDRMITTraining != null && courseOffering.hed__course__r.hed__Account__c == accountIDRMITTraining)
                {
                    aPriceItme.cspmb__Product_Definition_Name__c = 'Learning Activity';
                    aPriceItme.Product_Line__c = 'RMIT Training';
                }
                aPriceItme.cspmb__Effective_Start_Date__c = courseOffering.hed__Start_Date__c;
                aPriceItme.cspmb__Effective_End_Date__c = courseOffering.hed__End_Date__c;
                //aPriceItme.cspmb__One_Off_Charge__c = courseOffering.hed__course__r.price__c; //ECB-4182 -- Commented as a part of ECB-5551
                aPriceItme.External_Unique_ID__c = courseOffering.hed__course__c + '-' + courseOffering.ID;
                aPriceItme.cspmb__Is_Active__c = true;
                //System.debug('***HEDA2CSUtil aPriceItme ' + aPriceItme);
                //priceItems.add(aPriceItme);
                upsert aPriceItme External_Unique_ID__c;

                if (!rmitOnlineAddonItems.isEmpty()) {
                    if (accountIDRMITOnline != null && courseOffering.hed__course__r.hed__Account__c == accountIDRMITOnline) {
                        for (cspmb__Add_On_Price_Item__c addOn : rmitOnlineAddonItems) {
                            cspmb__Price_Item_Add_On_Price_Item_Association__c addOnAssociation = getAddOnAssociation (aPriceItme, addOn);
                            if (addOnAssociation != null) {
                                addOnAssociations.add(addOnAssociation);
                            }
                        }
                    }
                }
            }

            // DML statement
            if (!addOnAssociations.isEmpty()) {
                Insert addOnAssociations;
            }
        }
    }

    public static void transformProgram2PriceItems (List<hed__Program_Plan__c> programPlans)
    {
        createAllDeliveryMode();
        System.debug('HEDA2CSUtil.transformProgram2PriceItems entered');
        if (programPlans.size() > 0) 
        {
            List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addOnAssociations = New List<cspmb__Price_Item_Add_On_Price_Item_Association__c>();
            List<cspmb__Add_On_Price_Item__c> rmitOnlineAddonItems = getRMITAddonItems();

            Id accountID21CC = get21CCAccountId();
            Id accountIDRMITOnline = getRMITOnlineAccountId();

            //[select Id, hed__Start_Date__c from hed__Course_Offering__c where hed__Course__c IN (select hed__Course__c from hed__Plan_Requirement__c where hed__Program_Plan__c IN :programPlans) order by hed__Start_Date__c ASC]


            for (hed__Program_Plan__c plan : programPlans) 
            {
                //System.debug('transformCourses2PriceItems program ' + program);
                cspmb__Price_Item__c aPriceItme = New cspmb__Price_Item__c();
                //System.debug('*** program.hed__course__r.name ' + program.hed__course__r.name);
                aPriceItme.Name = plan.hed__Account__r.name;
                aPriceItme.Program__c = plan.hed__Account__c;
                aPriceItme.Program_Plan__c = plan.id;
                aPriceItme.Course_Program__c = 'Program';
                if (accountIDRMITOnline != null && plan.hed__Account__r.ParentId == accountIDRMITOnline) 
                {
                    aPriceItme.cspmb__Product_Definition_Name__c = 'Learning Activity';
                    aPriceItme.Product_Line__c = 'RMIT Online';
                }
                else if (accountID21CC != null && plan.hed__Account__r.ParentId == accountID21CC)
                {
                    aPriceItme.cspmb__Product_Definition_Name__c = '21cc Credential';
                    aPriceItme.Product_Line__c = '21cc Credential';
                }
                aPriceItme.External_Unique_ID__c = plan.hed__Account__r + '-' + plan.id;
                aPriceItme.cspmb__Is_Active__c = true;
                //System.debug('***HEDA2CSUtil aPriceItme ' + aPriceItme);
                //priceItems.add(aPriceItme);
                upsert aPriceItme External_Unique_ID__c;

                // Create Addon Assoications for RMIT Online Programs
                System.debug('accountID_RMITOnline ' + accountIDRMITOnline);
                System.debug('plan.hed__Account__r.ParentId ' + plan.hed__Account__r.ParentId);
                if (!rmitOnlineAddonItems.isEmpty()) 
                {
                    if (accountIDRMITOnline != null && plan.hed__Account__r.ParentId == accountIDRMITOnline)
                    {
                        for (cspmb__Add_On_Price_Item__c addOn : rmitOnlineAddonItems)
                        {
                            cspmb__Price_Item_Add_On_Price_Item_Association__c addOnAssociation = getAddOnAssociation (aPriceItme, addOn);
                            if (addOnAssociation != null) 
                            {
                                addOnAssociations.add(addOnAssociation);
                            }
                        }
                     }
                }
                
            }

            // DML statement
            if (!addOnAssociations.isEmpty()) 
            {
                insert addOnAssociations;
            }
        }
    }


    private static cspmb__Price_Item_Add_On_Price_Item_Association__c getAddOnAssociation(cspmb__Price_Item__c priceItem,  cspmb__Add_On_Price_Item__c addOn) {
        cspmb__Price_Item_Add_On_Price_Item_Association__c addOnAssociation = null;
        List<cspmb__Price_Item_Add_On_Price_Item_Association__c> dbResultList = [Select id from cspmb__Price_Item_Add_On_Price_Item_Association__c
                WHERE cspmb__Price_Item__c = :priceItem.Id AND cspmb__Add_On_Price_Item__r.Name = :addOn.Name];

        if (dbResultList.size() == 0) {
            cspmb__Price_Item_Add_On_Price_Item_Association__c newItem = new cspmb__Price_Item_Add_On_Price_Item_Association__c();
            newItem.cspmb__Price_Item__c = priceItem.id;
            newItem.cspmb__Add_On_Price_Item__c = addOn.id;

            addOnAssociation = newItem;
        }
        return addOnAssociation;
    }


    private static List<cspmb__Add_On_Price_Item__c> getRMITAddonItems() {
        List<cspmb__Add_On_Price_Item__c> rmitAddOnItems = [Select Id, name
                from cspmb__Add_On_Price_Item__c where name = 'All' OR name = 'Assessment Only' OR name = 'Content Only'];
        return rmitAddOnItems;
    }

    public static Id getRMITOnlineAccountId () {
        string accountIDRMITOnline = null;

        Config_Data_Map__c accountRMITOnlineCustomeSetting = Config_Data_Map__c.getValues('AccountID_RMITOnline');
        System.debug('accountRMITOnline_CustomeSetting' + accountRMITOnlineCustomeSetting);


        if (accountRMITOnlineCustomeSetting != null) {
            accountIDRMITOnline = accountRMITOnlineCustomeSetting.Config_Value__c;
        }

        return accountIDRMITOnline;
    }

    public static String get21CCAccountId () {
        string accountID21CC = null;

        Config_Data_Map__c account21CCCustomeSetting = Config_Data_Map__c.getValues('AccountID_21CC');
        System.debug('account21CC_CustomeSetting' + account21CCCustomeSetting);

        if (account21CCCustomeSetting != null) {
            accountID21CC = account21CCCustomeSetting.Config_Value__c;
        }

        return accountID21CC;
    }
     public static String getRMITTrainingAccountId () {
        string accountIDRMITTraining = null;

        Config_Data_Map__c accountRMITTrainingCustomeSetting = Config_Data_Map__c.getValues('AccountID_RMITTraining');
        System.debug('accountRMITTrainingCustomeSetting' + accountRMITTrainingCustomeSetting);

        if (accountRMITTrainingCustomeSetting != null) {
            accountIDRMITTraining = accountRMITTrainingCustomeSetting.Config_Value__c;
        }
        System.debug('RMIT Training record type Id=' + accountIDRMITTraining);
        return accountIDRMITTraining;
    }

    private static void createAllDeliveryMode(){
        List<cspmb__Add_On_Price_Item__c> lstAOPI = [select Id, Name from cspmb__Add_On_Price_Item__c where Name = 'All' limit 1];
        if(lstAOPI.isEmpty()){
            cspmb__Add_On_Price_Item__c aopi = new cspmb__Add_On_Price_Item__c();
            aopi.Name = 'All';
            aopi.cspmb__Is_Active__c = true;
            Database.insert(aopi);

        }
    }

}