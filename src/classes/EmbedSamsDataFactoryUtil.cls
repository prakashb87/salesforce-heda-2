/*****************************************************************
Name: EmbedSamsDataFactoryUtil
Author: Capgemini [Shreya]
Purpose: Test data creation for Embed SAMS Integration
*****************************************************************/
@isTest
 public class EmbedSamsDataFactoryUtil{
   
    public static void testEmbeds(){
        
        TestDataFactoryUtil.dummycustomsetting();
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Organization').getRecordTypeId();  
        Id courseEnrollRecordTypeId = Schema.SObjectType.hed__Course_Enrollment__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        
        Account acc = new Account(name = 'RMIT Online', recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
        insert acc;
        
        Account accountRec = new Account(Name='Test Partner',RecordTypeId=accountRecordTypeId);
        Insert accountRec;
        
        //Creating Address test data
        hed__Address__c addr = new hed__Address__c();
        addr.hed__MailingStreet__c = 'abc';
        addr.hed__MailingStreet2__c = 'xyz';
        addr.hed__MailingCity__c= 'Test';
        addr.hed__MailingState__c= 'Mel';
        addr.hed__MailingPostalCode__c= '0121';
        addr.hed__MailingCountry__c = 'Aus';
        addr.Last_Name__c = 'test';
        Insert addr;
        
        
        List<Contact> conList = createbulkContactData(addr);
        Database.SaveResult[] srList = Database.insert(conList);
        List<Id> conIdSet= new List<Id>();   
        for(Database.SaveResult sr : srList){
            if (sr.isSuccess()){                                    
                conIdSet.add(sr.getId());                                                           
            }
        }
        
       // List<User> usrList = createBulkUser(conIdSet);
       // Insert usrList;
        
        List<hed__Course__c> courseList = createBulkCourseData(acc);
        Database.SaveResult[] srCourseList = Database.insert(courseList);
        List<Id> courseIdList= new List<Id>();   
        for(Database.SaveResult sr : srCourseList){
            if (sr.isSuccess()){                                    
                courseIdList.add(sr.getId());                                                           
            }
        }
        
        hed__Term__c term = new hed__Term__c(name = 'test term', hed__Account__c = acc.id);
        insert term;
        
        List<hed__Course_Offering__c> cooferList = createBulkCourseOfferList(term,courseIdList);
        Database.SaveResult[] srCrOfferList = Database.insert(cooferList);
        List<Id> coIdList= new List<Id>();   
        for(Database.SaveResult sr : srCrOfferList){
            if (sr.isSuccess()){                                    
                coIdList.add(sr.getId());                                                           
            }
        }
        
        List<hed__Course_Enrollment__c> allCenrollList = new List<hed__Course_Enrollment__c>();
        
        allCenrollList.addAll(createBulkCourseEnrollData(conIdSet,coIdList));
        
        Database.SaveResult[] srCenrollList = Database.insert(allCenrollList);
        List<Id> cenrollIdList= new List<Id>();   
        for(Database.SaveResult sr : srCenrollList){
            if (sr.isSuccess()){                                    
                cenrollIdList.add(sr.getId());                                                           
            }
        }
        
        List<Course_Connection_Life_Cycle__c> cclyfcycleList = createBulkLifecycleData(cenrollIdList);
        Insert cclyfcycleList;
    }
    
    public static List<Contact> createbulkContactData(hed__Address__c addr){
        List<Contact> conList = new List<Contact>();
        for(Integer i=1;i<=5;i++){
            if(i<5){
                Contact con = new Contact();
                con.LastName='TestCon'+i;
                con.Student_ID__c='123'+i;
                con.Email=i+'mail@mail.com';
                con.hed__UniversityEmail__c = i+'mail@mail.com';
                con.hed__Current_Address__c=addr.Id;
                con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
                conList.add(con);
             }else{
                 Contact con = new Contact();
                con.LastName='TestCon'+i;
                con.Student_ID__c='s123'+i;
                con.Email=i+'mail@mail.com';
                con.hed__UniversityEmail__c = i+'mail@mail.com';
                con.hed__Current_Address__c=addr.Id;
                con.hed__Preferred_Email__c = 'SAMS/SAP University Email';
                conList.add(con);
             }   
        }
        return conList;
    }
    public static  List<hed__Course__c> createBulkCourseData(Account pt){
        List<hed__Course__c> courseList = new List<hed__Course__c>();
        for(Integer i=1;i<=5;i++){
            
            hed__Course__c course = new hed__Course__c();
            course.Name = 'test Name'+i;
            course.hed__Account__c = pt.id;
            course.Status__c='Active';
            course.SIS_Course_Id__c='1254';
            course.RecordTypeId=Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId();
            courseList.add(course);
                
            
        }   
        return courseList;
    }
    
    public static  List<hed__Course_Offering__c> createBulkCourseOfferList(hed__Term__c term1,List<Id> courseIdList){
        List<hed__Course_Offering__c> cooferList = new List<hed__Course_Offering__c>();
        for(Integer i=0;i<courseIdList.size();i++){                    
            hed__Course_Offering__c crsoffer = new hed__Course_Offering__c();
            crsoffer.name = 'test courseoffer'+i;
            crsoffer.hed__Course__c = courseIdList[i];
            crsoffer.hed__Term__c = term1.id;
            crsoffer.hed__Start_Date__c=Date.newInstance(2018,05,03);
            crsoffer.hed__End_Date__c=Date.newInstance(2018,06,03);
            cooferList.add(crsoffer);    
        }
        return cooferList;
    }
    
    public static List<hed__Course_Enrollment__c> createBulkCourseEnrollData(List<Id> contactIdList,List<Id> coIdList){
        //List<Contact> con = [Select Id,Student_ID__c From Contact where Id = :contactId];
        List<hed__Course_Enrollment__c> cenrollList = new List<hed__Course_Enrollment__c>();
        
        for(Integer i=0;i<coIdList.size();i++){
            hed__Course_Enrollment__c ce = new hed__Course_Enrollment__c();
            ce.hed__Contact__c = contactIdList[i];
            ce.hed__Course_Offering__c = coIdList[i];
            ce.hed__Status__c = 'Current';
            ce.Enrolment_Status__c = 'E'; 
            ce.Canvas_Integration_Processing_Complete__c = true;  
            ce.SAMS_Integration_Processing_Complete__c = false;        
            ce.Source_Type__c = 'MKT-Batch';
            //ce.Service__c= serIdList[i];
            cenrollList.add(ce);
        }
        
        return cenrollList;
    }
    
    public static List<Course_Connection_Life_Cycle__c> createBulkLifecycleData(List<Id> cenrollIdList){
        List<Course_Connection_Life_Cycle__c> clifecycleList = new List<Course_Connection_Life_Cycle__c>();
        
        for(Integer i=0;i<cenrollIdList.size();i++){
            Course_Connection_Life_Cycle__c cl1 = new Course_Connection_Life_Cycle__c();
            cl1.Course_Connection__c= cenrollIdList[i];
            cl1.Stage__c  = 'Post to Canvas';
            cl1.Status__c  = 'Not Started';
            
            clifecycleList.add(cl1);
            
            Course_Connection_Life_Cycle__c cl2 = new Course_Connection_Life_Cycle__c();
            cl2.Course_Connection__c  = cenrollIdList[i];
            cl2.Stage__c  = 'Post To SAMS';
            cl2.Status__c  = 'Not Started';
            
            clifecycleList.add(cl2);
        }
        
        return clifecycleList;
    }
 }