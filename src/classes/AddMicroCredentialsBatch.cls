public class AddMicroCredentialsBatch implements Database.Batchable<csapi_BasketRequestWrapper.AddProductRequest> {
 
    list<csapi_BasketRequestWrapper.AddProductRequest> addRequests;
    //list<csapi_BasketRequestWrapper.SyncBasketRequest> syncRequests;
    set<id> usersIds;

    public AddMicroCredentialsBatch (list<csapi_BasketRequestWrapper.AddProductRequest> requests) {
        
        //this.syncRequests = new list<csapi_BasketRequestWrapper.SyncBasketRequest>();
        this.addRequests = requests;
        this.usersIds = new set<Id>();
        for (csapi_BasketRequestWrapper.AddProductRequest request: requests) {
            this.usersIds.add(request.UserId);
        }
    }
    
    public Iterable<csapi_BasketRequestWrapper.AddProductRequest> start(Database.BatchableContext ctx) {
        return this.addRequests;
    }

    public void execute(Database.BatchableContext bc, list<csapi_BasketRequestWrapper.AddProductRequest> requests) {
    
        csapi_BasketRequestWrapper.AddProductRequest request = requests[0];
        
        Exception ee;
        System.Savepoint sp = Database.setSavepoint();
        try {
            csapi_BasketRequestWrapper.AddProductResponse response = csapi_BasketExtension.addProduct(request);
            system.debug('AddMicroCredentialsBatch|execute|request >> ' + request);
            system.debug('AddMicroCredentialsBatch|execute|response >> ' + response);
            system.debug('AddMicroCredentialsBatch|execute|request.UserId >> ' + request.UserId);
            //usersIds.add(request.UserId);
        } catch (Exception e) {
            Database.rollback(sp);
            ee = e;
        } finally {
            
            if (ee != null) {
                
                system.debug('AddMicroCredentialsBatch.execute error: ' + ee);
                system.debug('AddMicroCredentialsBatch.execute error msg: ' + ee.getMessage());
                system.debug('AddMicroCredentialsBatch.execute error trace: ' + ee.getStackTraceString());
                
                // 
                // log exception message, cause, line and other details
                //
                ApplicationErrorLogger.logError('Auto Purchase' //String BusinessFunctionName
                    , 'Student ID=' + request.UserId + '& Micro-Credential Name=' + request.Course.CourseName //String FailedRecordId,
                    , ee.getMessage());   //String ErrorMessage        
            }
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        
        system.debug('AddMicroCredentialsBatch >> finish');
        
        list<csapi_BasketRequestWrapper.SyncBasketRequest> syncRequests = new list<csapi_BasketRequestWrapper.SyncBasketRequest>();
        for (Id userId: usersIds) {
            csapi_BasketRequestWrapper.SyncBasketRequest request = new csapi_BasketRequestWrapper.SyncBasketRequest();
            request.UserId = userId;
            request.IsBatchProcessed = true;
            syncRequests.add(request);
        }
        
        FinalizeMicroCredentialsBasketsBatch nextBatch = new FinalizeMicroCredentialsBasketsBatch(syncRequests);  
        Database.executeBatch(nextBatch, 1);
    }

}