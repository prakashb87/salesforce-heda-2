/**
 * @description Test class for GoogleCaptchaVerificationRequest
 */
@isTest
public class GoogleCaptchaVerificationRequestTest {

    @TestSetup
    static void makeData(){
        Config_Data_Map__c captchaSecret = new Config_Data_Map__c();
        captchaSecret.name = 'GoogleCaptchaSecret';
        captchaSecret.Config_Value__c ='abc123';
        insert captchaSecret;
    }


    @isTest
    static void blankResponseProvidedTest() {
        GoogleCaptchaVerificationRequest google = new GoogleCaptchaVerificationRequest();
        try {
            Boolean result = google.isCaptchaVerified('');
            System.assert(false);
        } catch (ArgumentException e) {
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }


    @isTest
    static void nullResponseProvidedTest() {
        GoogleCaptchaVerificationRequest google = new GoogleCaptchaVerificationRequest();
        try {
            Boolean result = google.isCaptchaVerified(null);
            System.assert(false);
        } catch (ArgumentException e) {
            System.assert(true);
        } catch (Exception e) {
            System.assert(false);
        }
    }


    @isTest
    static void validCaptchaResponseTest() {
        Test.setMock(HttpCalloutMock.class, new GoogleCaptchaValidVerificationMock());

        Test.startTest();
        GoogleCaptchaVerificationRequest google = new GoogleCaptchaVerificationRequest();
        Boolean result = google.isCaptchaVerified('4972fjnsdnd8yf98u3orjfkslnfskfnsdj');
        Test.stopTest();
        System.assertEquals(true, result);
    }


    @isTest
    static void invalidCaptchaResponseTest() {
        Test.setMock(HttpCalloutMock.class, new GoogleCaptchaInvalidVerificationMock());

        Test.startTest();
        GoogleCaptchaVerificationRequest google = new GoogleCaptchaVerificationRequest();
        Boolean result = google.isCaptchaVerified('fakecodes2353989387y93873yhufshfis');
        Test.stopTest();
        System.assertEquals(false, result);
    }


    @isTest
    static void invalidCaptchaErrResponseTest() {
        Test.setMock(HttpCalloutMock.class, new GoogleCaptchaInvalidVerifyWithErrMock());

        Test.startTest();
        GoogleCaptchaVerificationRequest google = new GoogleCaptchaVerificationRequest();
        Boolean result = google.isCaptchaVerified('fakecodes2353989387y93873yhufshfis');
        Test.stopTest();
        System.assertEquals(false, result);
    }

    private class GoogleCaptchaValidVerificationMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"success": true,"challenge_ts": "2018-11-07T03:21:06Z","hostname": "ecbdev2-rmitheda.cs31.force.com"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }


    private class GoogleCaptchaInvalidVerificationMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"success": false,"challenge_ts": "2018-11-07T03:21:06Z","hostname": "ecbdev2-rmitheda.cs31.force.com"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }


    private class GoogleCaptchaInvalidVerifyWithErrMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            String jsonBody = '{"success": false,"challenge_ts": "2018-11-07T03:21:06Z","hostname": "ecbdev2-rmitheda.cs31.force.com", "error-codes": ["bad-request", "invalid-input-response"]}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }

}