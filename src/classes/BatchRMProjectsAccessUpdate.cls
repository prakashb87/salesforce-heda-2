/**************************************************************
Purpose: Batch class to upsert the Access of RM Projects for the existing records.
History:
Created by Ankit on 17/04/2019
*************************************************************/
public class BatchRMProjectsAccessUpdate implements Database.Batchable<sObject> {
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        Id rmProjectRecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId() ;    
        
        String query = 'Select id, Access__c,Project_Type__c, Group_Status__c, ls_Funded__c, ls_Confidential__c,recordTypeId  FROM ResearcherPortalProject__c WHERE recordtypeId =: rmProjectRecordTypeId' ;
        return Database.getQueryLocator(query);
    }
   
    public void execute(Database.BatchableContext bc, List<ResearcherPortalProject__c> scope) {
                 
         Set<Id> projectIdsSet = new Set<Id>();
         for(ResearcherPortalProject__c eachProject: scope ){
         	projectIdsSet.add(eachProject.id);
         }         
         ResearcherPortalProjectTriggerHandler.getProjectListToUpsert(projectIdsSet,scope);
    }   
    
    public void finish(Database.BatchableContext bc) {  
      System.debug('@@@Inside Finish Method');
      // No need to Send Mail   
    }
}