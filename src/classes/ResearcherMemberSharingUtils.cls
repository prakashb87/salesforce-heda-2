/*******************************************
Purpose: To create Researcher Member Sharing Object
History:
Created by Ankit Bhagat on 05/09/2018
*******************************************/
public with sharing class ResearcherMemberSharingUtils {
    
    /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static final string ACCESS_LEVEL_MEMBER = Label.RSTP_MembersAccessLevel; //'Members' // Added by subhajit :: 10.10.18 ::RPORW-152
    public static final string ACCESS_LEVEL_PUBLIC = Label.RSTP_PublicAccessLevel; //'RMIT Public'// Added by subhajit :: 10.10.18 ::RPORW-152
    public static final string ACCESS_LEVEL_PRIVATE =Label.RSTP_PrivateAccessLevel; // 'Private'; // Added by subhajit :: 10.10.18 ::RPORW-152
    public static final string NO_ACCESS_ROLE =Label.RSTP_Role_No_Access;
    public static final string ACCESS_LEVEL_RESEARCHER=Label.researcher;
    public static final string APEX_SHARE_EDIT_ACCESSLEVEL=Label.RSTP_Edit;
    public static final string ACCESS_LEVEL_STUDENT='Student';  // Added by Ankit for 891,Project Contract Permissions
    
    /*****************************************************************************************
    // Purpose      :  This method called from CreateProject Controller.
    // Parameters   :  List<ProjectDetailsWrapper.ContactDetail> ,Id projectType
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //****************************************************************************************/
    public static void createResearcherProjectMemberSharing(Id projectId, Id curentUser,string memberRole,boolean isPrimaryContact) {
        User userdetails = SobjectUtils.fetchUser();
        
        //1638, Dual Access Starts,Ankit
 	    Id contId ;
	    contId = ResearcherUserContactMapHelper.getContactId(userinfo.getUserId());
        
        System.debug('@@@contactIdinitial'+contId);
        Researcher_Member_Sharing__c newMember=createResearchProjectMemberSharing(contId,userdetails.id ,projectId,memberRole);
        //1638, Dual Access Ends,Ankit
        newMember.Primary_Contact_Flag__c=true; 
        newMember.Currently_Linked_Flag__c=true;
        if(!Schema.sObjectType.Researcher_Member_Sharing__c.fields.Primary_Contact_Flag__c.IsCreateable())
        {
        	system.debug('Primary_Contact_Flag__c==> is not updateable');
        }
        insert newMember;
        
    }
    
    /*****************************************************************************************
    // Purpose      :  pass projectId and Contact to create Researcher Member Sharing record
    // Parameters   :  List<ProjectDetailsWrapper.ContactDetail> ,Id projectType
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //****************************************************************************************/
    public static List<ProjectDetailsWrapper.ContactDetail> createResearcherMemberSharing(List<ProjectDetailsWrapper.ContactDetail> selectedMembers ,Id projectId) {
        
        List<Researcher_Member_Sharing__c>  linkedContactObjs = new List<Researcher_Member_Sharing__c>();
        List<ProjectDetailsWrapper.ContactDetail> updatedMembers = new List<ProjectDetailsWrapper.ContactDetail>();
        //get all the userid for selectmembers
        map<id,id> contactUserIdMap= new map<id,id>();
        set<id> selectedContactIDSet = new set<id>();
        
        //get all the selected contact ids from members.
        for(ProjectDetailsWrapper.ContactDetail contDetails: selectedMembers){
            selectedContactIDSet.add(contDetails.contactId);
        }
        
        //create the user and contactid map
         if (!Schema.sObjectType.User.fields.contactId.isAccessible()){
            system.debug('contactId==> is not accessible');
            //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
        } 
        List<User> userlist =[SELECT id,contactId FROM User WHERE contactId =:selectedContactIDSet];        
        if(userlist !=null && !userlist.isEmpty())
        {
            for(User usr :userlist ) {
                contactUserIdMap.put(usr.contactId,usr.id);
            }    
        }
        // Convert the Wrapper into member object and create the Member sharing record.
        for (ProjectDetailsWrapper.ContactDetail contDetails: selectedMembers){
            //Portal User only
            if(contactUserIdMap.get(contDetails.contactId) != null){
                linkedContactObjs.add(createResearchProjectMemberSharing(contDetails.contactId,contactUserIdMap.get(contDetails.contactId),projectId,contDetails.memberRole));
            //External contact do not have user will be inserted here.             
            }
            else if(contDetails.contactId!=null)
            {
            	linkedContactObjs.add(createResearchProjectMemberSharing(contDetails.contactId,null,projectId,contDetails.memberRole));
            }	
            
            //Admin user can be added only during Create project but this will handle if any future scenerio.
            else {
                linkedContactObjs.add(createResearchProjectMemberSharing(null,userinfo.getUserId(),projectId,contDetails.memberRole));
            }
        }
        System.debug('@@@linkedContactObjs'+linkedContactObjs);
        insert linkedContactObjs;
        
        Set<Id> memberIds = new Set<Id>();
        for(Researcher_Member_Sharing__c acc : linkedContactObjs){
            memberIds.add(acc.id);
        }

        updatedMembers = getUpdatedMembers(memberIds); // 1751, Called in different method for PMD,Ankit
        return updatedMembers;
        
    }
    
    
     public static List<ProjectDetailsWrapper.ContactDetail> getUpdatedMembers(Set<Id> memberIds){
     	
        //RPORW-1751
		String rmProjectRecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Master Project').getRecordTypeId();
		String rppProjectRecordTypeId = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get('Researcher Portal Project').getRecordTypeId();
		//RPORW-1751 Ends
		
		List<ProjectDetailsWrapper.ContactDetail> updatedMembers = new List<ProjectDetailsWrapper.ContactDetail>();
        List<Researcher_Member_Sharing__c>  linkedContactObjs = new List<Researcher_Member_Sharing__c>();
        //linkedContactObjs = new List<Researcher_Member_Sharing__c>();
        
        if (!Schema.sObjectType.Researcher_Member_Sharing__c.fields.AccessLevel__c.isAccessible()){
            system.debug('AccessLevel__c==> is not accessible');
            //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
        } 
        linkedContactObjs = [SELECT id, MemberRole__c, AccessLevel__c,RM_Member__c,Position__c, SF_Role__c,Contact__c,Currently_Linked_Flag__c,
                             Contact__r.name,  Contact__r.hed__Primary_Organization__r.name,
                             Contact__r.email, Contact__r.Current_Person_Type__c,Primary_Contact_Flag__c,User__c,
                             Contact__r.Preferred_Name__c, Contact__r.hed__UniversityEmail__c,
                             Contact__r.hed__WorkEmail__c,Researcher_Portal_Project__r.recordTypeId//RPORW-1751
                             FROM Researcher_Member_Sharing__c 
                             WHERE id = :memberIds];
        
        
        if (linkedContactObjs!=null && !linkedContactObjs.isempty()) {
            for (Researcher_Member_Sharing__c researchMemberSharing: linkedContactObjs){
                //updatedMembers.add(new ProjectDetailsWrapper.ContactDetail(researchMemberSharing));
					if(rmProjectRecordTypeId.contains(researchMemberSharing.Researcher_Portal_Project__r.recordTypeId) && researchMemberSharing.AccessLevel__c !='NO Access'){
						updatedMembers.add(new ProjectDetailsWrapper.ContactDetail(researchMemberSharing));
					}
					//If UC Project show all Members - RPORW-1751 
					else if(rppProjectRecordTypeId.containsIgnoreCase(researchMemberSharing.Researcher_Portal_Project__r.recordTypeId)){
						updatedMembers.add(new ProjectDetailsWrapper.ContactDetail(researchMemberSharing));
					}
                
            }
        }
        
        return updatedMembers;
        
    }
    
    /*****************************************************************************************
    // Purpose      :  pass projectId and Contact to create Researcher Member Sharing record
    // Parameters   :  String accessLevel ,String projectType
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //****************************************************************************************/
    public static Researcher_Member_Sharing__c createResearchProjectMemberSharing(id contactid,id userid,Id projectId ,String memberRole) {
        
        System.debug('@@@@contactid==>'+contactid);
        System.debug('@@@@userid==>'+userid);
        System.debug('@@@@projectId==>'+projectId);
        
        Researcher_Member_Sharing__c  linkedContactObj = new Researcher_Member_Sharing__c();
        linkedContactObj.Researcher_Portal_Project__c =  projectId;
        linkedContactObj.Contact__c  = ContactId!=null ? ContactId:null;
        linkedContactObj.User__c  = userid!=null ? userid:null;
     //   linkedContactObj.MemberRole__c  = memberRole; (This will be populated via trigger)
        linkedContactObj.Position__c =  memberRole;
        linkedContactObj.Currently_Linked_Flag__c = true; //added by subhajit for US-198
        linkedContactObj.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRecordtype).getRecordTypeId();
        
        return linkedContactObj;
        
    }
    
    /*****************************************************************************************
    // Purpose      :  Ethics Member sharing creation
    // Parameters   :  Id,Id,Id,String
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //****************************************************************************************/
    /*public static Researcher_Member_Sharing__c createResearchEthicsMemberSharing(Id contactid,Id userid,Id ethicsId ,String memberRole) {
         
        Researcher_Member_Sharing__c  linkedContactObj = new Researcher_Member_Sharing__c();
        linkedContactObj.Ethics__c =  ethicsId;
        linkedContactObj.Contact__c  = ContactId!=null ? ContactId:null;
        linkedContactObj.User__c  = userid!=null ? userid:null;
        //linkedContactObj.MemberRole__c  = memberRole; 
        linkedContactObj.Currently_Linked_Flag__c = true; //added by subhajit for US-198
        linkedContactObj.Position__c =  memberRole;
        linkedContactObj.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId();
        
        return linkedContactObj;
        
    }*/
    
	/*****************************************************************************************
    // Purpose      :  Contract Member sharing creation, 891
    // Parameters   :  Id,Id,Id,String
    // Developer    :  Ankit
    // Created Date :  13/02/2018                 
    //****************************************************************************************/
    /*public static Researcher_Member_Sharing__c createResearchContractMemberSharing(Id contactid,Id userid,Id contractId ,String memberRole) {
         
        Researcher_Member_Sharing__c  linkedContactObj = new Researcher_Member_Sharing__c();
        linkedContactObj.ResearchProjectContract__c =  contractId;
        linkedContactObj.Contact__c  = ContactId!=null ? ContactId:null;
        linkedContactObj.User__c  = userid!=null ? userid:null;
        //linkedContactObj.MemberRole__c  = memberRole; 
        linkedContactObj.Currently_Linked_Flag__c = true; //added by subhajit for US-198
        linkedContactObj.Position__c =  memberRole;
        linkedContactObj.RecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project Contract').getRecordTypeId();
        
        return linkedContactObj;
        
    }*/
       
    /*****************************************************************************************
    // Purpose      :  When User clicks delete button from the Portal Page.
    //				   This delete methods trigger the delete event in ProgramMember sharing method.
    //				   Delete event will remove the access from Project and Ethics (based on certain conditions)		
    // Parameters   :  List<ProjectDetailsWrapper.ContactDetail> ,Id 
    // Developer    :  Ankit/Subhajit
    // Created Date :  10/24/2018                 
    //****************************************************************************************/
    public static void deleteResearcherMemberSharing(List<ProjectDetailsWrapper.ContactDetail> selectedMembers ,Id projectId) {
        
        System.debug('@@@selectedMembers'+selectedMembers); 
        System.debug('@@@projectId'+projectId); 
        
        List<Researcher_Member_Sharing__c>  linkedContactObjs = new List<Researcher_Member_Sharing__c>();
        //List<Researcher_Member_Sharing__c>  adminlinkedContactObjs = new List<Researcher_Member_Sharing__c>();
        Set<Id> ownerIdsSet = new Set<Id>();
        Set<Id> rmsIdsSet = new Set<Id>();   
        Set<Id> contactsIdsSet = new Set<Id>();   
        Boolean isPortalUser=true;
                
        Map<id,Boolean> userIdToPortalMemberMap = new  Map<id,Boolean>();
        
        for (ProjectDetailsWrapper.ContactDetail contDetails: selectedMembers){
            Researcher_Member_Sharing__c  linkedContactObj = new Researcher_Member_Sharing__c();
            System.debug('contDetails.memberId:'+contDetails.memberId);
            if (contDetails.memberId!=null){
                linkedContactObj.Id = contDetails.memberId;
                linkedContactObjs.add(linkedContactObj);
                rmsIdsSet.add(contDetails.memberId);
                contactsIdsSet.add(contDetails.contactId);
            }
        }
        
        //Set<Id>  ethicsIdsSet = getethicsIdsSet(projectId); 
        
        if (!Schema.sObjectType.Researcher_Member_Sharing__c.fields.Ethics__c.isAccessible()){
            system.debug('Ethics__c==> is not accessible');
            //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
        } 
        
        //linkedContactObjs.addAll( getlinkedContactObjs(ethicsIdsSet,contactsIdsSet,projectId));
        System.debug('@@@linkedContactObjs'+linkedContactObjs);
        deleteLinkedContacts(linkedContactObjs);
        
    }
    
    
    public static void deleteLinkedContacts(List<Researcher_Member_Sharing__c>  linkedContactObjs){
    	List<Researcher_Member_Sharing__c>  adminlinkedContactObjs = new List<Researcher_Member_Sharing__c>();
    	
	    if (!linkedContactObjs.isEmpty()){ 
                    
            List<Researcher_Member_Sharing__Share> rmsShareList = new List<Researcher_Member_Sharing__Share>();
            
            for(Researcher_Member_Sharing__c eachRMS : linkedContactObjs){
               
                if(eachRMS.OwnerId !=UserInfo.getUserId()){
                    eachRMS.OwnerId = UserInfo.getUserId();                    
                    Researcher_Member_Sharing__Share rmsShare = ApexWithoutSharingUtils.shareRMSrecords(eachRMS,APEX_SHARE_EDIT_ACCESSLEVEL);                   
                    rmsShareList.add(rmsShare); 
                    adminlinkedContactObjs.add(eachRMS);
                }
            }
            
            if(adminlinkedContactObjs!=null && !adminlinkedContactObjs.isEmpty())
               {
                     ApexWithoutSharingUtils.updateRMSrecords(adminlinkedContactObjs);
                     //update adminlinkedContactObjs;
               }
               
            System.debug('@@@linkedContactObjs'+linkedContactObjs); 
            if (!Researcher_Member_Sharing__c.sObjectType.getDescribe().isDeletable()){
                System.debug('Researcher_Member_Sharing__c is not deletable');
            }
            delete linkedContactObjs;
        }
    }
   
    /*****************************************************************************************
    // JIRA No      :  RPORW-152
    // SPRINT       :  SPRINT-4
    // Purpose      :  Change project access - permissions for Portal Project Type
    // Parameters   :  String accessLevel ,String projectType
    // Developer    :  Subhajit
    // Created Date :  10/08/2018                 
    //****************************************************************************************/
    public static void operatingProjectAccess(list<ResearcherPortalProject__c> lstResearchProject) {
        
        
        Id rppRecordtypeID = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
        
        // Project Related variables..
        list<Researcher_Member_Sharing__c> projectAccessMemberList = new list<Researcher_Member_Sharing__c>();
        list<Researcher_Member_Sharing__c>  researcherPrivateProjectMemberList = new list<Researcher_Member_Sharing__c> ();
        list<Researcher_Member_Sharing__c>  researcherMemberProjectMemberList = new list<Researcher_Member_Sharing__c> ();
        //Ethics Variables
        set<Id> privateToMemberSetProjectIds = new set<Id>();
        set<Id> memberToPrivateSetProjectIds = new set<Id>();
        system.debug('lstResearchProject'+lstResearchProject);
        
        if(lstResearchProject!=null && ! lstResearchProject.isEmpty())
        {
            for(ResearcherPortalProject__c rproj : lstResearchProject) 
            {
                if(rproj.Access__c==ACCESS_LEVEL_PRIVATE && rproj.RecordtypeId ==rppRecordtypeID){                    
                    memberToPrivateSetProjectIds.add(rproj.id);
                }
                else if(rproj.Access__c != ACCESS_LEVEL_PRIVATE && rproj.RecordtypeId ==rppRecordtypeID){                  
                    privateToMemberSetProjectIds.add(rproj.id);
                }
            }
        }
        system.debug('#### P T M'+privateToMemberSetProjectIds);
        system.debug('#### P T M'+memberToPrivateSetProjectIds);     
        //USECASE 1 : RPP PROJECT TYPE : MEMBER TO PRIVATE ::STARTS
        if(memberToPrivateSetProjectIds != null && ! memberToPrivateSetProjectIds.isEmpty())
        {
            //Fetching members list for respective projects 
            // Ignore the Primary Contact and RM Member fields
            // Adding the RPP recordtype to filter records
            if (!Schema.sObjectType.Researcher_Member_Sharing__c.fields.SF_Role__c.isAccessible()){
                system.debug('SF_Role__c==> is not accessible');
                //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
            } 
            researcherMemberProjectMemberList =[SELECT id, SF_Role__c, Researcher_Portal_Project__c,User__c,Position__c,RecordTypeId 
                                                FROM Researcher_Member_Sharing__c
                                                WHERE Researcher_Portal_Project__c IN : memberToPrivateSetProjectIds 
                                                AND Primary_Contact_Flag__c=false
                                                AND RM_Member__c=false]; 
            system.debug ('$$$ researcherMemberProjectMemberList'+ researcherMemberProjectMemberList);
            // update the access to No role and add into the projectAccessMemberlist
            if(researcherMemberProjectMemberList!=null && ! researcherMemberProjectMemberList.isEmpty())
            {
                for(Researcher_Member_Sharing__c rprojShareMem : researcherMemberProjectMemberList) {
                    rprojShareMem.MemberRole__c = NO_ACCESS_ROLE;                    
                    projectAccessMemberList.add(rprojShareMem);
                    
                }                 
            }
        }
        //USECASE 1 : RPP PROJECT TYPE : MEMBER TO PRIVATE ::ENDS
        
        /*** Added by Prakash as part of RPORW-763 / RPORW-839 to include permission for Public access ***/
		
		 if (!Schema.sObjectType.RM_Position_RRI_Role_Mapping__mdt.fields.RM_Position_Name__c.isAccessible()){
            system.debug('RM_Position_Name__c==> is not accessible');
            //ResearcherExceptionHandlingUtil.addLog('Field is not accessible',ResearcherExceptionHandlingUtil.getParamMap());
        } 
        List<RM_Position_RRI_Role_Mapping__mdt> rmPositionRRIRole=[SELECT DeveloperName,Id,Label,RecordType__c,
                                                               RM_Position_Name__c,RRIRoleName__c,RRIRoleName__r.RRI_Role__c FROM RM_Position_RRI_Role_Mapping__mdt];
    
		
        Map<string,string> rmPositionRecordTypeRRIMap = new Map<string,string>();
        
        for(RM_Position_RRI_Role_Mapping__mdt aRMPositionRRI: rmPositionRRIRole){        
            rmPositionRecordTypeRRIMap.put(aRMPositionRRI.RM_Position_Name__c+''+aRMPositionRRI.RecordType__c,aRMPositionRRI.RRIRoleName__r.RRI_Role__c);
        }
        
        Id projectRecordTypeId = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId();
        String projectRecordTypeName = Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getName();
        
        Map<Id, String> recordTypeIdVsName = new Map<Id, String> { projectRecordTypeId => projectRecordTypeName };        
   
        //*** Code added by Prakash ends *****//
        
        //USECASE 2 : RPP PROJECT TYPE : PRIVATE TO MEMBER ::STARTS 
        if(privateToMemberSetProjectIds != null && ! privateToMemberSetProjectIds.isEmpty())
        {
            //Fetching members list for respective projects
            researcherPrivateProjectMemberList =[SELECT id, SF_Role__c, Researcher_Portal_Project__c,User__c,Position__c,RecordTypeId
                                                 FROM Researcher_Member_Sharing__c
                                                 WHERE Researcher_Portal_Project__c IN : privateToMemberSetProjectIds 
                                                 AND MemberRole__c =: NO_ACCESS_ROLE AND RM_Member__c=false]; 
            // update the access to No role and add into the projectAccessMemberlist
            if(researcherPrivateProjectMemberList!=null && ! researcherPrivateProjectMemberList.isEmpty())
            {
                for(Researcher_Member_Sharing__c rprojShareMem : researcherPrivateProjectMemberList) {
                    rprojShareMem.MemberRole__c = rmPositionRecordTypeRRIMap.get(rprojShareMem.Position__c+''+recordTypeIdVsName.get(rprojShareMem.RecordTypeId));
                    //rprojShareMem.MemberRole__c = ACCESS_LEVEL_RESEARCHER;
                    projectAccessMemberList.add(rprojShareMem);
                }
            }
        }
        
        for(Researcher_Member_Sharing__c eachRMS : projectAccessMemberList){
                if(eachRMS.OwnerId !=UserInfo.getUserId()){
                    eachRMS.OwnerId = UserInfo.getUserId();    
				}
		}	

        if(projectAccessMemberList!=null && !projectAccessMemberList.isEmpty()) {
                ApexWithoutSharingUtils.updateRMSrecords(projectAccessMemberList);
            }		       
                
    }
    
    /*****************************************************************************************
    // Purpose      :  This is handled via backend. Reveiw and delete the below methods. UI
    // Parameters   :  Id,Id,string
    // Developer    :  Ankit
    // Created Date :  10/10/2018                 
    //****************************************************************************************/
    public static void addResearcherMemberSharing(Id projectId, Id contId, string memberRole) {
        
        Researcher_Member_Sharing__c  linkedContactObj = new Researcher_Member_Sharing__c();
        
        linkedContactObj.Researcher_Portal_Project__c =  projectId;
        linkedContactObj.Contact__c  = contId;
        linkedContactObj.MemberRole__c  = memberRole;
        insert linkedContactObj;    
        
    }
	
	/*****************************************************************************************
    // Purpose      :  This is a utility method to return list of Reasercher member Sharing based on Set of RecordtypeIds and UserId
    // Parameters   :  Set<Id>,Id
    // Developer    :  Ankit
    // Created Date :  12/11/2018                 
    //****************************************************************************************/
	public static List<Researcher_Member_Sharing__c> geResearcherMemberSharingList(Set<Id> recordTypeIdSet, Id userId){ 

        List<Researcher_Member_Sharing__c> reseacherMemberSharingList = new List<Researcher_Member_Sharing__c>();
        reseacherMemberSharingList = [SELECT id, Contact__c, Ethics__c, Account__c, Publication__c,User__c,
		                                      Researcher_Portal_Project__c, ResearchProjectContract__c,MemberRole__c,RM_Member__c
                                              FROM Researcher_Member_Sharing__c 
                                              WHERE User__c =: UserId
											  AND RecordTypeId IN : RecordTypeIdSet
											  AND AccessLevel__c !=:NO_ACCESS_ROLE
                                              AND Currently_Linked_Flag__c  = true];
                                                                   
        System.debug('@@@reseacherMemberSharingList1'+reseacherMemberSharingList);

        return reseacherMemberSharingList;
    } 
}