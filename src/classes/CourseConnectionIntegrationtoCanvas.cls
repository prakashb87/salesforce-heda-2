/*****************************************************************************************************
*** @Class             : CourseConnectionIntegrationtoCanvas
*** @Author            : Avinash Machineni 
*** @Requirement       : Integration between Salesforce and IPaaS for sending Contact details
*** @Created date      : 21/05/2018
*** @JIRA ID           : ECB-367
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is used to integrate between Salesforce and IPaaS for sending course SIS Id, Course Offering Id's
*****************************************************************************************************/
public without sharing class CourseConnectionIntegrationtoCanvas 
{
    
    @future(callout=true)
    
    // Sending Request from Salesforce to IPaas
    public static void sendCourseConnIds(Set<Id> incomingCourseEnrolments)
    {
        Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c();
        CourseConnCanvasResponseWrapper jstr = new CourseConnCanvasResponseWrapper();
        User usr = [Select Id, name, ContactId from user WHERE id =: UserInfo.getUserId() LIMIT 1];
        if(Test.isRunningTest())
        {
            usr = TestDataFactoryUtil.usr4;
        }
        
        Contact con = [SELECT id,
                              Name,
                              Student_ID__c,
                              hed__UniversityEmail__c,
                              Email,
                              (SELECT ID, hed__Course_Offering__r.Name, hed__Course_Offering__c,hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c FROM hed__Student_Course_Enrollments__r WHERE Id IN: incomingCourseEnrolments)
                              FROM Contact WHERE id =: usr.ContactId 
                                     ];
        
        set<id> crsconnids = new set<id>();
        
        CourseConnCanvasRequestWrapper crsconnwrp;
        if(con != null)
        {
            
            crsconnwrp = New CourseConnCanvasRequestWrapper();
            crsconnwrp.studentId = con.Student_ID__c;
            crsconnwrp.studentName = con.Name;
           // crsconnwrp.studentEmail = con.hed__UniversityEmail__c;
            if(con.hed__UniversityEmail__c!=null){
                crsconnwrp.studentEmail = con.hed__UniversityEmail__c;
            }else{
                crsconnwrp.studentEmail = con.Email;
            } 
            crsconnwrp.accountId = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasAccountId');
            crsconnwrp.type = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvastype');
            crsconnwrp.enrollment_state = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasenrolmentstate');
            crsconnwrp.notify = ConfigDataMapController.getCustomSettingValue('CourseConnectionCanvasnotify');
            
            for(hed__Course_Enrollment__c crsenr : con.hed__Student_Course_Enrollments__r)
            {
                crsconnids.add(crsenr.id);
                CourseConnCanvasRequestWrapper.EnrolDetails enrdet = new CourseConnCanvasRequestWrapper.EnrolDetails();
                enrdet.courseConnectionId = crsenr.id;
                enrdet.courseOfferingId = crsenr.hed__Course_Offering__r.Name;
                enrdet.sisCourseId = crsenr.hed__Course_Offering__r.hed__Course__r.SIS_Course_Id__c;
                crsconnwrp.enrolmentDetails.add(enrdet);
            }
            
        }        
        // HTTP Request Creation
        String requestJSONString = JSON.serialize(crsconnwrp);
        
        String endpoint = ConfigDataMapController.getCustomSettingValue('CanvasEndPointURL');
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('CanvasClientId'));
        req.setHeader('client_secret', ConfigDataMapController.getCustomSettingValue('CanvasClientSecretId'));
        req.setHeader('Content-Type','application/json');
        req.setMethod('POST');
        req.setbody(requestJSONString);
        req.setTimeout(30000);
        Http http = new Http();
        HTTPResponse response;
        try
        {
            requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('Canvas_IPaaS',requestJSONString, 'Outbound Service','',false);
            response = http.send(req);
            responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Canvas_IPaaS',response.getBody(), 'Acknowledgement','',false);
        }
        catch (exception e) 
        {
            ApplicationErrorLogger.logError(e);
        }
        
       System.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
       List<Course_Connection_Life_Cycle__c> cclyfcycles = [SELECT Id,
                                                                    Name,
                                                                    Status__c,
                                                                    Course_Connection__c
                                                                    FROM Course_Connection_Life_Cycle__c 
                                                                    WHERE Course_Connection__c in : crsconnids AND Stage__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStageCanvas')];
        
        if(cclyfcycles.size() > 0 && cclyfcycles.size() != null)
        {
            
            for(Course_Connection_Life_Cycle__c cclyfcyc : cclyfcycles)
            {
                if (response.getStatusCode() == 200){
                    cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleInitiated'); 
                }
                else {
                    cclyfcyc.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                }
            }
           
            try
            {
                update cclyfcycles;
                
                //List of Course Connections life cycles in course connections coming from Ipaas
                System.debug('Response body------'+response.getBody());
                
                jstr = (CourseConnCanvasResponseWrapper) JSON.deserializeStrict(response.getBody(), CourseConnCanvasResponseWrapper.class);
                
                System.debug('Json String---------->'+jstr);
            } 
            catch(Exception e)
            {
              ApplicationErrorLogger.logError(e); 
            }
        }
        
      	Map<Id,CourseConnCanvasResponseWrapper.ResponseInfo> canvasResWrp = new Map<Id,CourseConnCanvasResponseWrapper.ResponseInfo>();
       
        CourseConnCanvasResponseWrapper.ResponseInfo crsconreswrp;
        
        Map<Id, CourseConnCanvasResponseWrapper.Response> canvasres = new  Map<Id, CourseConnCanvasResponseWrapper.Response>();
        
	   for(CourseConnCanvasResponseWrapper.Response responsefromcanvas : jstr.payload.enrolmentResponse)
                
            {
                canvasres.put(responsefromcanvas.courseConnectionId, responsefromcanvas);
            }
        
        List<Course_Connection_Life_Cycle__c> cclifecycless = [SELECT Id,
                                                               Name,
                                                               Status__c,
                                                               Course_Connection__c, 
                                                               Course_Connection__r.Enrolment_Status__c,
                                                               Course_Connection__r.Name
                                                               FROM Course_Connection_Life_Cycle__c 
                                                               WHERE Course_Connection__c in :canvasres.keyset() AND Stage__c =: ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleStageCanvas')];
        List<hed__Course_Enrollment__c> crsconnections = new List<hed__Course_Enrollment__c>();
        if(cclifecycless.size()>0)
        {
            for(Course_Connection_Life_Cycle__c cclyf : cclifecycless)
            {
                if(canvasres.get(cclyf.Course_Connection__c).result == ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess'))
                {
                    cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleSuccess');
                    
                }
                else
                {
                    cclyf.Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionLifeCycleError');
                    crsconnections.add(new hed__Course_Enrollment__c(id=cclyf.Course_Connection__c,Enrolment_Status__c=''));
                    
                    ApplicationErrorLogger.logError(cclyf, 'Canvas Integration', canvasres);
                    
                    if(!Test.isRunningTest()){
                         ErrorLogger.sendErrorEmailAlert(cclyf.Course_Connection__c);
                    }else{
                        System.debug('Test is Running');
                    }
                   
                }
            }
            
            try{
                
                update cclifecycless;
                update crsconnections;
                System.debug('Life Cycles after updated'+cclifecycless);
                
                if( requestLogObjectToInsert != null)
                {
                    insert requestLogObjectToInsert;        
                }
                if( responseLogObjectToInsert != null )
                {
                    insert responseLogObjectToInsert;
                }
                
            }
            catch (exception e) 
            {
                ApplicationErrorLogger.logError(e);
            }
            
        }                                        
        System.debug(canvasResWrp.keySet());
        
    }
    
}