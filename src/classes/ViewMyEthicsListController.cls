/*******************************************
 Purpose: To create View Ethics List view page with the Search Functionality
 History:
 Created by Ankit Bhagat on 12/09/2018
 *******************************************/
public class ViewMyEthicsListController {
       
     /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
   
    /************************************************************************************
    // Purpose      : getting all Ethic List functionality 
    // Parameters   : String searchText         
    // Developer    : Ankit  
    // Created Date : 09/12/2018                  
    //***********************************************************************************/ 
    @AuraEnabled    
    public static List<EthicsDetailWrapper.ethicsWrapper> getEthicsList(){
        
        List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapperList = new List<EthicsDetailWrapper.ethicsWrapper>();
      
       try{
             ethicsWrapperList = ViewMyEthicsHelper.getEthicsWrapperList();
          
           	 ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {    
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
       return ethicsWrapperList;
    }
     
     /************************************************************************************
    // Purpose      : To check current login user is community user or salesforce user
                   
    //***********************************************************************************/ 
    
    @AuraEnabled     
    public static Boolean checkInternalDualAccessUser(){
            Boolean isInternalDualAccessUser = ApexWithoutSharingUtils.checkInternalDualAccessUser();
            return isInternalDualAccessUser;
    } 
     
     /************************************************************************************
    // Purpose      : Searching Ethics in All Ethics List
    // Parameters   : String searchText         
    // Developer    : Ankit  
    // Created Date : 09/12/2018                  
    //***********************************************************************************/  
    @AuraEnabled    
    public static List<EthicsDetailWrapper.ethicsWrapper> searchtEthicsList(String searchText){
        
       List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapperMasterSearchList = new List<EthicsDetailWrapper.ethicsWrapper>();
     
       try{
           ethicsWrapperMasterSearchList = ViewMyEthicsHelper.getEthicsSearchWrapperList(searchText);
           
           ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
       }
       catch(Exception ex) 
       {   
           //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
           //Exception handling Error Log captured :: Ends
           System.debug('Exception occured : '+ex.getMessage());
       } 
      
        return ethicsWrapperMasterSearchList;
  
    }
     

}