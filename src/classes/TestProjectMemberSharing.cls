/*******************************************
Purpose: Test class for ProjectMemberSharing
History:
Created by Ankit Bhagat on 04/10/2018
*******************************************/
@isTest
public class TestProjectMemberSharing {

    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
    	
    
        Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
        allnumbersOfDataMap.put('user',1);
        allnumbersOfDataMap.put('account',1);
        allnumbersOfDataMap.put('contact',1);
        
        Map<String,String> requiredInfoMap = new Map<String,String>();
        requiredInfoMap.put('userType','CommunityUsers');
        requiredInfoMap.put('profile','RMIT Researcher Portal Plus Community User');
        
        Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
        System.assert(userCreatedFlag==true,'Community User Created');
    }    
    
        
    /************************************************************************************
    // Purpose      :  Test functionality for Trigger of Project Member Sharing
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/    
    @isTest
    public static void projectmemberSharingMethod(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='RMIT Researcher Portal Plus Community User'];
        
        Contact con = new Contact();
        con.FirstName='research';
        con.LastName='project'; 
        con.hed__UniversityEmail__c='xyz@rmit.edu.au';
        insert con; 
        system.assert(con!=null);
                
        User usr = new User(Alias = 'sysAd', Email='test12345@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,ContactId = con.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test12345@test.com');
        insert usr;
        system.assert(usr!=null);

        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
        system.assert(projectObjList.size() > 0);

        
        Ethics__c ethics = new Ethics__c();
        ethics.Application_Title__c = 'Test Title';
		ethics.Ethics_Id__c = '12345Test';
        insert ethics; 
        system.assert(ethics!=null);

        List<Researcher_Member_Sharing__c> newMemberSharingList = new List<Researcher_Member_Sharing__c>();
        List<Researcher_Member_Sharing__c> oldMemberSharingList = new List<Researcher_Member_Sharing__c>();
        List<Researcher_Member_Sharing__c> deleteMemberSharingList = new List<Researcher_Member_Sharing__c>();
        
        Researcher_Member_Sharing__c memberSharing = new Researcher_Member_Sharing__c();
        memberSharing.MemberRole__c = 'Researcher';
        memberSharing.Contact__c = con.Id;
        memberSharing.Researcher_Portal_Project__c = projectObjList[0].id;
        memberSharing.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Research Project').getRecordTypeId(); 
        newMemberSharingList.add(memberSharing);
        
        Researcher_Member_Sharing__c memberSharing1 = new Researcher_Member_Sharing__c();
        memberSharing1.MemberRole__c = 'Researcher';
        memberSharing1.Contact__c = con.Id;
        memberSharing1.Ethics__c = ethics.Id; 
        memberSharing1.RecordTypeId=Schema.SObjectType.Researcher_Member_Sharing__c.getRecordTypeInfosByName().get('Ethics').getRecordTypeId(); 
        newMemberSharingList.add(memberSharing1);
        
        insert newMemberSharingList ;
        System.assert(newMemberSharingList.size() > 0);

        
        for (Researcher_Member_Sharing__c eachmemberSharing : newMemberSharingList){
            eachmemberSharing.MemberRole__c = 'CI';
        }
        update newMemberSharingList;
        
        
        Researcher_Member_Sharing__c memberSharing2 = new Researcher_Member_Sharing__c();
        memberSharing2.MemberRole__c = 'Researcher';
        memberSharing2.Contact__c = con.Id;
        memberSharing2.Researcher_Portal_Project__c = projectObjList[0].Id;
        deleteMemberSharingList.add(memberSharing2);
        
        Researcher_Member_Sharing__c memberSharing3 = new Researcher_Member_Sharing__c();
        memberSharing3.MemberRole__c = 'CI';
        memberSharing3.Ethics__c = ethics.Id;
        deleteMemberSharingList.add(memberSharing3);
        
        insert deleteMemberSharingList;
        System.assert(deleteMemberSharingList.size() > 0);

        delete deleteMemberSharingList ;

         
    }
}