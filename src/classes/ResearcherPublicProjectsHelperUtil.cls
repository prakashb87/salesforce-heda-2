/*@Author     : Gourav Bhardwaj
@description  : Util class holds generic methods which will be used by for Public projects functionality in the Portal
@Story        : RPROW-979
@CreatedDate  : 15/4/2019
*/
public without Sharing class ResearcherPublicProjectsHelperUtil {
  /*@Author     : Gourav Bhardwaj
    @Description  : Generic Method to return All Public Researcher Project list of wrapper, without any filters
    @Story        : RPORW-294
    @CreatedDate  : 1/4/2019 
  */

    public static final Integer PAGE_SIZE = 1000;
    public static RSTP_Project_Collaboration__mdt collaborationMetadata;//RPORW-1415

    public static ProjectDetailsWrapper.ResearchProjectPublicProjects createPublicProjectListView(RMITPublicProjectWrapper rmitPublicProjectWrapper)
    {
        list<ProjectDetailsWrapper.ResearchProjectPublicProjectListView > lstResearchProjectWrapper= new list<ProjectDetailsWrapper.ResearchProjectPublicProjectListView>();
        List<Researcher_Member_Sharing__c> allProjectMemberList;
        List<Research_Project_Classification__c> allFORCodesForProjectList;//RPORW-979
        
        ProjectDetailsWrapper.ResearchProjectPublicProjectListView myResearchProjectListView;
        ProjectDetailsWrapper.ResearchProjectPublicProjects rmitPublicProjects = new ProjectDetailsWrapper.ResearchProjectPublicProjects();//Gourav

        Integer recordsCounter = 0;//For Pagination

        Boolean hasMoreRecords = false;

        system.debug('## lstResearcherPortalProject : '+rmitPublicProjectWrapper.lstResearcherPortalProject);

        Integer pageNo=1;
        if(rmitPublicProjectWrapper.configuration.pageNo != null || rmitPublicProjectWrapper.configuration.pageNo >0 ){
          pageNo = rmitPublicProjectWrapper.configuration.pageNo;
        }
		Decimal pageSize = ResearcherPublicProjectsHelperUtil.getCollaborationMetadata().Records_Size__c;//RPORW-1415
        Integer totalRecordsToFetch = pageSize.intValue() * pageNo;

        hasMoreRecords = totalRecordsToFetch < rmitPublicProjectWrapper.lstResearcherPortalProject.size();

          for(ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherPortalProjectData :rmitPublicProjectWrapper.lstResearcherPortalProject)
          {
            system.debug('totalRecordsToFetch : '+totalRecordsToFetch);
            allProjectMemberList = new List<Researcher_Member_Sharing__c>();
            allFORCodesForProjectList = new List<Research_Project_Classification__c>();//RPORW-979
            
            if(recordsCounter<totalRecordsToFetch){
              system.debug('recordsCounter : '+recordsCounter);

              //Filtering records as per the text inserted in the search field
              system.debug('Contains keyword'+researcherPortalProjectData);
                if(rmitPublicProjectWrapper.allProjectMemberMap!= null && rmitPublicProjectWrapper.allProjectMemberMap.containsKey(researcherPortalProjectData.recordId))
                {
                  allProjectMemberList = rmitPublicProjectWrapper.allProjectMemberMap.get(researcherPortalProjectData.recordId);
                }

                //RPORW-979 
                if(rmitPublicProjectWrapper.forCodesForProjects!= null && rmitPublicProjectWrapper.forCodesForProjects.containsKey(researcherPortalProjectData.recordId))
                {
                  allFORCodesForProjectList = rmitPublicProjectWrapper.forCodesForProjects.get(researcherPortalProjectData.recordId);
                }
                
                List<ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper> fieldOfResearchListSorted = new ProjectDetailsWrapper.FieldOfResearchForProjectsWrapper().getSortedListOfFieldOfResearch(allFORCodesForProjectList,ResearcherPublicProjectsHelperUtil.isRMProject(researcherPortalProjectData));

                system.debug('## fieldOfResearchListSorted : '+fieldOfResearchListSorted);
                //RPORW-979  Ends

                myResearchProjectListView = new ProjectDetailsWrapper.ResearchProjectPublicProjectListView(researcherPortalProjectData,allProjectMemberList,fieldOfResearchListSorted); 
                system.debug('myResearchProjectListView '+myResearchProjectListView);
                lstResearchProjectWrapper.add(myResearchProjectListView);

                recordsCounter++;
            }
           
          }//for Ends

         system.debug('wrapper 1'+lstResearchProjectWrapper);

         rmitPublicProjects.publicProjectList           = lstResearchProjectWrapper;
         rmitPublicProjects.hasMoreRecords              = hasMoreRecords;
         rmitPublicProjects.totalPublicProjects         = rmitPublicProjectWrapper.lstResearcherPortalProject.size();

         return rmitPublicProjects;
    }
    //createPublicProjectListView() Ends

    //@Story : RPROW-1302
    //@Author : Gourav Bhardwaj
    //@Description : Method to check if the search text is present in the Project's Title, Keyword or Summary
    public static FilteredPublicProjectRecordWrapper isSearchTextPresentInProject(ResearcherPortalProject__c project,RMITPublicProjectWrapper rmitPublicProjectWrapper){
       
        Boolean isSearchTextPresent = false;
		String searchedText = rmitPublicProjectWrapper.configuration.searchText; //1303
		
		Integer searchOrder = 4;
        FilteredPublicProjectRecordWrapper filteredRecordWrapper = new FilteredPublicProjectRecordWrapper();
        
        List<String> searchedWords = new List<String>();//RPROW-1303

        system.debug('configure : '+rmitPublicProjectWrapper.configuration);
        
        //This check is necessary for AND condition where filter should include Text Search AND FOR AND Impact Category
        if(String.isBlank(rmitPublicProjectWrapper.configuration.searchText))
        {
        	filteredRecordWrapper.searchOrder         = 0;
        	filteredRecordWrapper.isSearchTextPresent = true; //RPORW-1383
        	return filteredRecordWrapper;
        } 
        
        //RPROW-1303
        //Getting all the words in a String
        searchedWords = searchedText.split(' ');
        
        //Passing Project and List of Words seached for.
        filteredRecordWrapper = ResearcherPublicProjectsTextSearchUtil.isWordsInSentencePresentInProject(project,searchedWords);
        
        //RPROW-1303 Ends
        system.debug('### filteredRecordWrapper = : '+filteredRecordWrapper);

        return filteredRecordWrapper;
    }//isSearchTextPresentInProject Ends
    
   
    //@Story : RPROW-1302
    //@Description : Method to check if searched text contains any one of the members of a Project
    //@Author : Gourav Bhardwaj
    public static ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper isSearchTextPresentInMemberName(ResearcherPortalProject__c project,RMITPublicProjectWrapper rmitPublicProjectWrapper){
        //1383
        ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper filteredRecordWrapper = new ResearcherPublicProjectsHelperUtil.FilteredPublicProjectRecordWrapper();
        Boolean isSearchTextPresent = true;
        List<String> searchedWords = new List<String>();//1383
        
        system.debug('configure 2 : '+rmitPublicProjectWrapper.configuration);
        system.debug('allProjectMemberMap : '+rmitPublicProjectWrapper.allProjectMemberMap);
      
        //If Project Contains no Members then this is not returned in filtered data
        if(rmitPublicProjectWrapper.allProjectMemberMap.isEmpty() 
           || !rmitPublicProjectWrapper.allProjectMemberMap.containskey(project.Id)
           || rmitPublicProjectWrapper.allProjectMemberMap.get(project.Id) == null
          ){
            //1383
			filteredRecordWrapper.isSearchTextPresent = false;
			filteredRecordWrapper.searchOrder         = 0;
            return filteredRecordWrapper;
        }
        
        if(!String.isBlank(rmitPublicProjectWrapper.configuration.searchText)){
            //Gathering all the words in a sentence
            searchedWords = rmitPublicProjectWrapper.configuration.searchText.split(' '); 
            
            //Itterating each word and checking if present in the name of member
            for(String searchedWord : searchedWords){
                
                    if(!ResearcherPublicProjectsTextSearchUtil.isWordsInSentencePresentInProjectMember(searchedWord,rmitPublicProjectWrapper.allProjectMemberMap.get(project.Id)))
                {
                    isSearchTextPresent = false;
                }

            }//for ends 
        }//if ends
        else{
            isSearchTextPresent = false;
        }
        
		//1383
		filteredRecordWrapper.isSearchTextPresent = isSearchTextPresent;
		filteredRecordWrapper.searchOrder         = 4;
        
        return filteredRecordWrapper;
    }//isSearchTextPresentInMemberName Ends
    
   

    /*****************************************************************************************
       RPORW-919 : Fetching member list for RMIT Public project
       SPRINT : SPRINT-3
       ResearchProjectListViewController class
       Parameters : list<ResearcherPortalProject__c>
       Developer : Subhajit
       Created Date : 14/03/2018 
       @description : to prepare public project list view data and pass it to ProjectDetailsWrapper data to getMyResearchProjectPublicListView methods of 
       //Move this to util class - gourav
    //****************************************************************************************/
    public static Map<id,List<Researcher_Member_Sharing__c>> getProjectMembersForRMITPublicAccess(list<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> lstResearcherPortalProject)
    {
        Map<Id,List<Researcher_Member_Sharing__c>> allProjectMemberMap = new Map<Id,List<Researcher_Member_Sharing__c>>();
        set<Id> projectIdSet = new set<Id>();
        for(ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherPortalProjectData :lstResearcherPortalProject)
        {
            projectIdSet.add(researcherPortalProjectData.recordId); 
        }
        
        for(Researcher_Member_Sharing__c rmsRec : [SELECT RM_Member__c,Researcher_Portal_Project__c,Primary_Contact_Flag__c,contact__r.Name,MemberRole__c,AccessLevel__c,Currently_Linked_Flag__c
                                                   FROM Researcher_Member_Sharing__c
                                                   WHERE Researcher_Portal_Project__c IN : projectIdSet
                                                   AND AccessLevel__c!='No Access'
                                                   AND Currently_Linked_Flag__c=true ])
        {
            allProjectMemberMap = getRMSMap(allProjectMemberMap,rmsRec);
        }

        system.debug('### allProjectMemberMap : '+allProjectMemberMap);
        return allProjectMemberMap;
    }
    //getProjectMembersForRMITPublicAccess() Ends

      /*****************************************************************************************
    // RPORW-919 : getting map of RMS records for project members and RRI roles
    // SPRINT : SPRINT-3
    // @description : to prepare public project list view data and pass it to ProjectDetailsWrapper 
    // data to getMyResearchProjectPublicListView methods of 
    // ResearchProjectListViewController class
    // Parameters : list<ResearcherPortalProject__c>
    // Developer : Subhajit
    // Created Date : 14/03/2019 
    //Move this to util class - gourav
    //****************************************************************************************/
    public static Map<id,List<Researcher_Member_Sharing__c>> getRMSMap(Map<Id,List<Researcher_Member_Sharing__c>> allProjectMemberMap,
                                                                       Researcher_Member_Sharing__c rmsRec )
    {
        if(!allProjectMemberMap.containskey(rmsRec.Researcher_Portal_Project__c))
        {
            List<Researcher_Member_Sharing__c> contactNamesWithRoles = new List<Researcher_Member_Sharing__c>();
            contactNamesWithRoles.add(rmsRec);
            allProjectMemberMap.put(rmsRec.Researcher_Portal_Project__c,contactNamesWithRoles);
        }
        else
        {
            List<Researcher_Member_Sharing__c> contactNamesWithRoles = allProjectMemberMap.get(rmsRec.Researcher_Portal_Project__c);
            contactNamesWithRoles.add(rmsRec);
            allProjectMemberMap.put(rmsRec.Researcher_Portal_Project__c,contactNamesWithRoles);
        }
        
        return allProjectMemberMap;
    }
    //getRMSMap() Ends

  /*@Author     : Gourav Bhardwaj
  @description  : This method is used to check if a project is RM or User Created
  @Story        : RPORW-294
  @CreatedDate  : 1/4/2019
  */
    public static Boolean isRMProject(ProjectDetailsWrapper.AllResearchProjectPublicWrapper researcherProject){
      String recordtypename = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosById().get(researcherProject.recordTypeId).getname();

      system.debug('## researcherProject : '+researcherProject+' : '+Label.RSTP_ResearchProjectRMRecordtype);

      if(recordtypename == Label.RSTP_ResearchProjectRMRecordtype){
        return true;
      }else
      {
        return false;
      }
    }

    public static Boolean isRMProject(ResearcherPortalProject__c researcherProject){
      String recordtypename = Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosById().get(researcherProject.recordTypeId).getname();

      system.debug('## researcherProject : '+researcherProject+' : '+Label.RSTP_ResearchProjectRMRecordtype);

      if(recordtypename == Label.RSTP_ResearchProjectRMRecordtype){
        return true;
      }else
      {
        return false;
      }
    }
    
 	/*@Story 		: RPORW-1396
     @Description 	: Method to retrieve all the 6Digit FOR Codes assigned by quering Classification object
     @Author 		: Gourav Bhardwaj
     @Return Type 	: List of 6 Digit FOR Codes in format 010101 - PURE MATHEMATICS
   */
    public static List<String> getSixDigitForCodes(){
   	 List<String> forCodesDetail = new List<String>();
		for(AggregateResult projectClassification : [SELECT For_Six_Digit_Code__c,For_Six_Digit_Description__c FROM Research_Project_Classification__c WHERE For_Six_Digit_Code__c != null AND For_Six_Digit_Code__c != '' GROUP BY For_Six_Digit_Code__c,For_Six_Digit_Description__c]){
		   String sixDigitCode = String.valueOf(projectClassification.get('For_Six_Digit_Code__c'));
		   String sixDigitDescription = String.valueOf(projectClassification.get('For_Six_Digit_Description__c'));
		   if(sixDigitCode.length()==6 && !String.isBlank(sixDigitDescription)){
			   	forCodesDetail.add(sixDigitCode+' - '+sixDigitDescription.toUpperCase());
		   }
				   
		} 
	  system.debug('## forCodesDetail : '+forCodesDetail);
	  return forCodesDetail;
   }
   //getSixDigitForCodes Ends
   
   //RPORW-1415 : Method to retrive Records Size from Metadata.
   public static RSTP_Project_Collaboration__mdt getCollaborationMetadata(){
   	if(collaborationMetadata==null){
   		collaborationMetadata = [SELECT Records_Size__c FROM RSTP_Project_Collaboration__mdt WHERE DeveloperName='CollaborationSearch'][0];
   	}
   	
   	return collaborationMetadata;
   	
   }

/*============================================================================================================================
 *					WRAPPER CLASSES
 ============================================================================================================================*/      
    
    public class RMITPublicProjectWrapper{
        public List<ProjectDetailsWrapper.AllResearchProjectPublicWrapper> lstResearcherPortalProject;
        public Map<id,List<Researcher_Member_Sharing__c>> allProjectMemberMap;
        public ResearcherPublicProjectsHelper.ResearchProjectPublicListViewConfig configuration;
        public Map<id,List<Research_Project_Classification__c>> forCodesForProjects;
    }
    
    public class FilteredPublicProjectRecordWrapper{
        public Integer searchOrder;
        public Boolean isSearchTextPresent;
    } 

}