public with Sharing class Batch_StaffAffiliation_helper {
    public Static String rmitStaffRtId = Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('RMIT Staff').getRecordTypeId();
    public void updateAffiliation(List<hed__Affiliation__c> scope){

        for(hed__Affiliation__c afRt: scope){
            if(afRt.hed__Account__c == afRt.hed__Contact__r.hed__Primary_Organization__c){
        		afRt.RecordTypeId = rmitStaffRtId;
	            if(afRt.hed__Contact__r.Finish_Date__c >= Date.TODAY()){
	        		afRt.hed__Status__c = 'Current';
	            }else{
	            	afRt.hed__Status__c = 'Former';
	            }
	            afRt.hed__Role__c = afRt.hed__Contact__r.Position_Description__c;
	            afRt.hed__StartDate__c = afRt.hed__Contact__r.Hire_Date__c;
	            afRt.hed__EndDate__c = afRt.hed__Contact__r.Finish_Date__c;
            }
        }
        
        try{
            Database.update(scope);
        }catch(Exception e){
            System.debug('Exception------->'+ e.getMessage());
        }
    }
}