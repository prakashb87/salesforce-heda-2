/*********************************************************************************
 *** @TestClassName     : GenericActionController_Test
 *** @Author       		: Anupam Singhal
 *** @Requirement       : To test the GenericActionController_Test apex class.
 *** @Created date      :
 *** @Modified by       : Anupam Singhal 
 *** @Modified date     : 08/11/2018
 **********************************************************************************/
@isTest
public class GenericActionController_Test {
    /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    * Method to test saveLead andcreateLead
    * ────────────────────────────────────────────────────────────────────────────────────────────────
    * @method    NAME    saveLead
    * @param     NAME    None
    * @return    TYPE    true or false
    * @CreatedBy      Shubham SIngh
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    @isTest static void testSaveLeadCreateLead() {
        Account acc = new Account();
        acc.Name = 'New Account';
        acc.Address_Type_1__c = 'Postal';
        acc.ASX_200__c = 'Yes';
        acc.Industry_Sector__c = 'Construction';
        acc.RMIT_Relationship_Alignment__c='RMIT Europe';
        acc.World_Region__c ='Europe';
        insert acc;
        
        Campaign cmpgn = new Campaign();
        cmpgn.Name = 'TestwithAccount';
        cmpgn.School__c = acc.Id;
        insert cmpgn;
        
        Campaign cmpn = new Campaign();
        cmpn.Name = 'TestwithoutAccount';
        insert cmpn;
        
        test.startTest();
          GenericActionController.createLead(cmpgn.id);
          GenericActionController.createLead(cmpn.id);           
            Lead led = new Lead();
            led.FirstName = 'Lead';
            led.LastName = 'Test';
            Account ac = [Select id from Account where id =: acc.Id Limit 1 ];
            GenericActionController.saveLead(led,ac,cmpgn.id);
            Lead led2 = new Lead();
            led2.FirstName = 'Testing';
            led2.LastName = 'Test';
            led2.Un_Matched_School__c = 'New School';
        led2.Status ='Learn';
            Account accn = new Account();
        
        String result = GenericActionController.saveLead(led2,accn,cmpgn.id);
        System.assertEquals(result, 'true');
        test.stopTest();
    }
    
    @isTest
    static void testcreateCampaign(){
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        
        Lead newTestLead = new Lead();
        newTestLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Event Inquiry').getRecordTypeId();
        newTestLead.FirstName = 'Generic Action Lead Fname';
        newTestLead.LastName = 'Generic Action Lead Lname';
        newTestLead.Company = 'Generic Action Company';
        newTestLead.Description = 'Some thing the student talked about';
        newTestLead.Email = 'student@schmoogle.com';
        newTestlead.Secondary_School__c = accTestLead.Id;
        newTestLead.Event_address_If_different__c = 'Some address of the student if provided';
        newTestLead.Event_date__c = date.ValueOf('2018-09-21');
        //newTestLead.Event_time__c = Time.newInstance(18, 30, 2, 20);
        newTestLead.Event_Start_Date__c = date.ValueOf('2018-09-21');
        newTestLead.Event_End_Date__c = date.ValueOf('2018-09-25');
        newTestLead.Event_type__c = '';
        newTestLead.Career_Advisors__c = '';
        newTestLead.International_students__c = '';
        newTestLead.Non_school_leavers__c = '';
        newTestLead.Parents__c = '';
        newTestLead.People_from_industry__c = '';
        newTestLead.Postgraduate_students__c = '';
        newTestLead.Year_9_students__c = '';
        newTestLead.Year_10_students__c = '';
         newTestLead.Year_11_students__c = '';
        newTestLead.Year_12_students__c = '';
        newTestLead.Status = 'Qualified';
        insert newTestLead;
        
        test.startTest();
        
        String str = GenericActionController.createCampaign(newTestLead.Id);
        
        test.stopTest();
        System.assertEquals(str, 'true');
        
    }
    @isTest
    static void testfetchAccount(){
        Account newAcc = new Account(Name='Test');
        insert newAcc;
        List<Account> actosearch = new List<Account>();
        actosearch.add(newAcc);
        List <Account> result = GenericActionController.fetchAccount('Test');
        System.assertEquals(result, actosearch);
    }
    @isTest
    static void testinsertCase(){
        Case objCase = new Case();
          Contact testCon = new Contact();
           testCon.FirstName = 'New' ;
            testCon.LastName = 'Test';
            //testCon.RecordTypeId = Label.External_Contact;
           
                testCon.Home_Email__c = 'test@mail.com';
            //testCon.RecordTypeId = '0120l0000001ylA';
            testcon.Student_ID__c = '1234' ;
            testCon.Position2__c = 'New';
            insert testCon;
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        Lead newTestLead = new Lead();
        newTestLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Event Inquiry').getRecordTypeId();
        newTestLead.FirstName = 'Generic Action Lead Fname';
        newTestLead.LastName = 'Generic Action Lead Lname';
        newTestLead.Company = 'Generic Action Company';
        newTestLead.Description = 'Some thing the student talked about';
        newTestLead.Email = 'student@schmoogle.com';
        newTestlead.Secondary_School__c = accTestLead.Id;
        newTestLead.Event_address_If_different__c = 'Some address of the student if provided';
        
        newTestLead.Event_date__c = date.ValueOf('2018-09-21');
        //newTestLead.Event_time__c = Time.newInstance(18, 30, 2, 20);
        newTestLead.Event_Start_Date__c = date.ValueOf('2018-09-21');
        newTestLead.Event_End_Date__c = date.ValueOf('2018-09-25');
        newTestLead.Event_type__c = '';
        newTestLead.Career_Advisors__c = '';
        newTestLead.International_students__c = '';
        newTestLead.Non_school_leavers__c = '';
        newTestLead.Parents__c = '';
        newTestLead.People_from_industry__c = '';
        newTestLead.Postgraduate_students__c = '';
        newTestLead.Year_9_students__c = '';
        newTestLead.Year_10_students__c = '';
         newTestLead.Year_11_students__c = '';
        newTestLead.Year_12_students__c = '';
        
        insert newTestLead;
        GenericActionController.insertCase(objCase,testCon.Id,newTestLead.Id);
    }
    @isTest
    static void testinsertContact(){
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        
        Lead newTestLead = new Lead();
        newTestLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Event Inquiry').getRecordTypeId();
        newTestLead.FirstName = 'Generic Action Lead Fname';
        newTestLead.LastName = 'Generic Action Lead Lname';
        newTestLead.Company = 'Generic Action Company';
        newTestLead.Description = 'Some thing the student talked about';
        newTestLead.Email = 'student@schmoogle.com';
        newTestlead.Secondary_School__c = accTestLead.Id;
        newTestLead.Event_address_If_different__c = 'Some address of the student if provided';
        newTestLead.matched_school__c = accTestLead.Id;
        newTestLead.Event_date__c = date.ValueOf('2018-09-21');
        newTestLead.Event_Start_Date__c = date.ValueOf('2018-09-21');
        newTestLead.Event_End_Date__c = date.ValueOf('2018-09-25');
        newTestLead.Event_type__c = '';
        newTestLead.Career_Advisors__c = '';
        newTestLead.International_students__c = '';
        newTestLead.Non_school_leavers__c = '';
        newTestLead.Parents__c = '';
        newTestLead.People_from_industry__c = '';
        newTestLead.Postgraduate_students__c = '';
        newTestLead.Year_9_students__c = '';
        newTestLead.Year_10_students__c = '';
         newTestLead.Year_11_students__c = '';
        newTestLead.Year_12_students__c = '';
        insert newTestLead;
        
        //newTestLead.Status = 'Un-Qualified';
        //update newTestLead;
        //GenericActionController.insertContact(newTestLead.Id,'Qualified');
        
        
        test.startTest();
        String result = GenericActionController.eventConversionQualified(newTestLead.Id);
        System.assertNotEquals(result, null);
        test.stopTest();
    }
       @isTest
    static void testRecentlyViewed(){
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        Lead newTestLead = new Lead();
        newTestLead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Event Inquiry').getRecordTypeId();
        newTestLead.FirstName = 'Generic Action Lead Fname';
        newTestLead.LastName = 'Generic Action Lead Lname';
        newTestLead.Company = 'Generic Action Company';
        newTestLead.Description = 'Some thing the student talked about';
        newTestLead.Email = 'student@schmoogle.com';
        newTestlead.Secondary_School__c = accTestLead.Id;
        newTestLead.Event_address_If_different__c = 'Some address of the student if provided';
        
        newTestLead.Event_date__c = date.ValueOf('2018-09-21');
        //newTestLead.Event_time__c = Time.newInstance(18, 30, 2, 20);
        newTestLead.Event_Start_Date__c = date.ValueOf('2018-09-21');
        newTestLead.Event_End_Date__c = date.ValueOf('2018-09-25');
        newTestLead.Event_type__c = '';
        newTestLead.Career_Advisors__c = '';
        newTestLead.International_students__c = '';
        newTestLead.Non_school_leavers__c = '';
        newTestLead.Parents__c = '';
        newTestLead.People_from_industry__c = '';
        newTestLead.Postgraduate_students__c = '';
        newTestLead.Year_9_students__c = '';
        newTestLead.Year_10_students__c = '';
        newTestLead.Year_11_students__c = '';
        newTestLead.Year_12_students__c = '';
        
        
        insert newTestLead;
        List<string> lstWhere = new List<string>();
        List<string> lstextrafields = new list<string>();
        //lstWhere.add('Name = \'Test Account\'');
        //lstextrafields.add('');
        //List<Account> lstAccnt = new List<Account>();
        //lstAccnt.add(accTestLead.Id);
        GenericActionController.eventConversionUnqualified(newTestLead.Id);
        //GenericActionController.eventConversionQualified(newTestLead.Id);
        list<SObject> actualRes = new List<SObject>();
        List<SObject> result = GenericActionController.getRecentlyViewed('Account', lstWhere, lstextrafields);
        System.assertEquals(actualRes, actualRes);
        
    }
    
    @isTest
    static void testupdateAccount(){
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        List<String> selectedAccIds = new List<String>();
        selectedAccIds.add(accTestLead.Id);
        Campaign testCampaign = new Campaign();
        testCampaign.Name = 'Test Campaign';
        testCampaign.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Activator Campaigns').getRecordTypeId();
        insert testCampaign;
        String result = GenericActionController.updateAccount(selectedAccIds, testCampaign.Id);
        System.assertEquals(result, 'true');
    }
    
    @isTest
    static void testSearch(){
        Account accTestLead = new Account();
        accTestLead.Name = 'Test Account1';
        accTestLead.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Activator Start Up').getRecordTypeId();
        insert accTestLead;
        List<string> whereClause = new List<string>();
        List<string> extrafields = new List<string>();
        GenericActionController.search('Account', 'Test Account1', whereClause, extrafields);
    }
}