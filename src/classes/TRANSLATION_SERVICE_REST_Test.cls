/*********************************************************************************
 *** @TestClassName     : FIELD_OF_STUDY_REST_Test
 *** @Author            : Shubham
 *** @Requirement       : To Test the FIELD_OF_EDUCATION_REST web service apex class.
 *** @Created date      : 20/08/2018
 **********************************************************************************/
@isTest
public class TRANSLATION_SERVICE_REST_Test {
    static testMethod void myUnitTest() {
        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1/Translation_Service';

        //creating test data
        String JsonMsg = '{"category": "SSR_SPCL_PROG_TYPE","value": "24","effectiveDate": "1900-01-01","effectiveStatus": "A","translatedValue": "Public Health Ed & Resrch Prog","translatedShortValue": "PHERP","lastUpdated": "2003-10-22T17:05:20.000000+1100","lastUpdatedBy": "PPLSOFT"}';

        req.requestBody = Blob.valueof(JsonMsg);

        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        //store response
        Map < String, String > check = TRANSLATION_SERVICE_REST.upsertTranslationService();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');

        Test.stopTest();

    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1/Translation_Service';

        //creating test data
        String JsonMsg = '{"category": "","value": "24","effectiveDate": "1900-01-01","effectiveStatus": "A","translatedValue": "Public Health Ed & Resrch Prog","translatedShortValue": "PHERP","lastUpdated": "2003-10-22T17:05:20.000000+1100","lastUpdatedBy": "PPLSOFT"}';

        req.requestBody = Blob.valueof(JsonMsg);

        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        //store response
        Map < String, String > check = TRANSLATION_SERVICE_REST.upsertTranslationService();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');

        Test.stopTest();

    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
			 check = TRANSLATION_SERVICE_REST.upsertTranslationService();

        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {

        Translation_Service__c ts = new Translation_Service__c();

        ts.Translation_Service_Name__c = 'SSR_SPCL_PROG_TYPE';
        ts.Field_To_be_Translated__c = '24';
        ts.Effective_status__c = 'B';
        insert ts;

        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1/Translation_Service';

        //creating test data
        String JsonMsg = '{"category": "SSR_SPCL_PROG_TYPE","value": "24","effectiveDate": "1900-01-01","effectiveStatus": "A","translatedValue": "Public Health Ed & Resrch Prog","translatedShortValue": "PHERP","lastUpdated": "2003-10-22T17:05:20.000000+1100","lastUpdatedBy": "PPLSOFT"}';

        req.requestBody = Blob.valueof(JsonMsg);

        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        //store response
        Map < String, String > check = TRANSLATION_SERVICE_REST.upsertTranslationService();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');

        Test.stopTest();

    }
}