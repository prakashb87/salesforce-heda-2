/*****************************************************************
Name: EmbeddedInvoiceGenerationController
Author: Capgemini [Shreya]
Purpose: Handle Invoice Generation  for all the course connection records which are created via embedded batch credential job.
Jira Reference : ECB-3900,ECB-5545
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           02/04/2019         Initial Version
********************************************************************/
public without sharing class EmbeddedInvoiceGenerationController{
    
        //Processing the List of service ids to generate invoice pdf
     public static void getServices(List < Id > serviceIds) {
        
        Map<Id,Blob> paymentRecAttachMap = new Map<Id,Blob>();
        system.debug('serviceId:::'+serviceIds);
        
        String query = 'SELECT csordtelcoa__Product_Configuration__r.Id,csordtelcoa__Product_Basket__r.Basket_Number__c, csordtelcoa__Product_Basket__r.cscfga__Total_Price__c,';
        query+='csordtelcoa__Main_Contact__r.Name,csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__Formula_MailingAddress__c, csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet__c,';
        query+='csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingState__c,csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingStreet2__c,';    
        query+='csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCity__c,csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingPostalCode__c,Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c,';  
        query+='csordtelcoa__Main_Contact__r.hed__Current_Address__r.hed__MailingCountry__c,csordtelcoa__Main_Contact__r.Student_ID__c,csordtelcoa__Main_Contact__r.Email,csordtelcoa__Main_Contact__r.id ';    
        query+='FROM csord__Service__c WHERE Id IN: serviceIds';    
        
        List <csord__Service__c> serviceList = Database.query(query);
                     
        system.debug('serviceList:::'+serviceList);

       
        Map<String,List<csord__Service__c>> contactServiceMap = new Map<String,List<csord__Service__c>>();
        Set<Id> productBasketIdSet = new Set<Id>();
        
        if(!serviceList.isEmpty()){
            for(csord__Service__c ser : serviceList){
            productBasketIdSet.add(ser.csordtelcoa__Product_Basket__c);
            String mapKeyString = ser.csordtelcoa__Main_Contact__r.id + ' ' + ser.csordtelcoa__Product_Basket__r.Basket_Number__c;
            
            if(contactServiceMap.containsKey(mapKeyString)){
                contactServiceMap.get(mapKeyString).add(ser);
            }else{
                List<csord__Service__c> contactServiceList = new List<csord__Service__c>();
                contactServiceList.add(ser);
                contactServiceMap.put(mapKeyString,contactServiceList);
            }
            
        }
        } 
        
        
        List<cscfga__Product_Basket__c> prodBasketList = Database.query('Select Id,Basket_Number__c,cscfga__Total_Price__c from cscfga__Product_Basket__c where Id IN :productBasketIdSet');
        
        
        System.debug('prodBasketList ----------------->'+prodBasketList );
        Set<Id> basketIdSet = checkPaymentRecordExists(productBasketIdSet);   
        Map<Id,Payment__c> prodBasketPaymentMap = createPaymentRecord(prodBasketList,basketIdSet);
       
        if(!contactServiceMap.isEmpty()){
            for(String keyVal : contactServiceMap.KeySet()){                
                paymentRecAttachMap = generatePDF(contactServiceMap.get(keyVal),prodBasketPaymentMap,paymentRecAttachMap );
            }   
        }
        
        insertPDFAttachment(paymentRecAttachMap);
    }
    
    //checking if a payment record exists for the product basket or not
    private static Set<Id> checkPaymentRecordExists( Set<Id> productBasketIdSet){
        
        Set<Id> basketIdSet = new Set<Id>();
        String paymentQuery = 'SELECT Id,Product_Basket__c FROM Payment__c WHERE Product_Basket__c IN :productBasketIdSet';
        List<Payment__c> existingPaymentRecList = Database.query(paymentQuery);
        
        for(Payment__c pay : existingPaymentRecList){
            basketIdSet.add(pay.Product_Basket__c);
        }
        
        return basketIdSet;
    }
    
    //Inserting new Payment record
    private static Map<Id,Payment__c> createPaymentRecord( List<cscfga__Product_Basket__c> prodBasketList,Set<Id> basketIdSet){
        
        List<Payment__c> paymentRecordList = new List<Payment__c>();
        
        for(cscfga__Product_Basket__c pb : prodBasketList ){
            if(!basketIdSet.contains(pb.Id)){
                //Creating Payment Record
                Payment__c paymentObj = new Payment__c();
                paymentObj.Receipt_Number__c = 'n/a';  
                paymentObj.Product_Basket__c = pb.Id;
                if(pb.cscfga__Total_Price__c!=null){
                    paymentObj.Amount__c = pb.cscfga__Total_Price__c;
                }else{
                    paymentObj.Amount__c = 0.0;
                }    
                paymentRecordList.add(paymentObj);
            }   
            
        }

        System.debug('paymentRecordList------------------>'+paymentRecordList);
        
        
        Database.SaveResult[] srList;
        try {
           // Insert paymentRecordList;
            srList = Database.insert(paymentRecordList,false);
            if(Test.isRunningTest()){
            throw new DMLException();
        }
        } catch (DMLException ex) {
            system.debug('The following exception has occurred: ' + ex.getMessage());
        }  
        
        //system.debug('contactServiceMap>>>>'+contactServiceMap);
        
        Set<Id> paymentRecIdSet= new Set<Id>();   
        for(Database.SaveResult sr : srList){
            if (sr.isSuccess()){                                    
                paymentRecIdSet.add(sr.getId());                                                           
            }
        }
        
        
        Map<Id,Payment__c> prodBasketPaymentMap = createProdBasketPaymentMap(paymentRecIdSet);
        
        return prodBasketPaymentMap;
        
    }
    
    //Creating Map of Product Basket and its related Payment record
    private static Map<Id,Payment__c> createProdBasketPaymentMap(Set<Id> paymentRecIdSet){
        
        
        List<Payment__c> paymentRecList;
        try{ 
            paymentRecList = Database.query('Select Id,Product_Basket__c,Receipt_Number__c from Payment__c where Id IN :paymentRecIdSet');
            system.debug('paymentRecList ::::' + paymentRecList);
        }catch(QueryException ex){
            system.debug('Exception:::'+ex.getMessage());
        }
        
        Map<Id,Payment__c> prodBasketPaymentMap = new Map<Id,Payment__c>();
        for(Payment__c pay : paymentRecList ){
            prodBasketPaymentMap.put(pay.Product_Basket__c,pay);
        }
        
        return prodBasketPaymentMap;
    }

    //Creating the invoice pdf data  
    private static Map<Id,Blob> generatePDF(List <csord__Service__c> serviceList,Map<Id,Payment__c> prodBasketPaymentMap,Map<Id,Blob> paymentRecAttachMap) {
    
        system.debug('serviceList>>>>'+serviceList);
        
        csord__Service__c service = new csord__Service__c();
        String prodConfigIdString = ''; //To store all the prod configuration Id as a string and pass it to the vf page 
        String address;
        
      /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/
        CreateAddressWrapperForInvoice.AddressWrapper addrWrap;
        if(!serviceList.isEmpty()){
            service = serviceList[0];
            addrWrap = CreateAddressWrapperForInvoice.CreateAddressWrapper(serviceList);
        }                   
    
       /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
        
        //Passing parameters to the InvoicePDF vf page
        PageReference pdf = Page.InvoicePDF;
        PassPDFParameters(pdf,serviceList,addrWrap);
        if(prodBasketPaymentMap.containsKey(serviceList[0].csordtelcoa__Product_Basket__c)){
             pdf.getParameters().put('receiptNo', prodBasketPaymentMap.get(serviceList[0].csordtelcoa__Product_Basket__c).Receipt_Number__c);
        } 
        
        
        Blob body;
        //getting the dynamic content of pdf
        try {
            body = pdf.getContent();
        } catch (VisualforceException e) {
            body = Blob.valueOf('No Pdf');
        }

        system.debug('body:::' + body);
        
        /*//Code to save the generated pdf in Notes & Attachment
        if(prodBasketPaymentMap.containsKey(serviceList[0].csordtelcoa__Product_Basket__c)){     
            InsertPDFAttachment(prodBasketPaymentMap,body,serviceList);
        }*/     
        
        paymentRecAttachMap.put(prodBasketPaymentMap.get(serviceList[0].csordtelcoa__Product_Basket__c).Id,body);
        
        return paymentRecAttachMap; 

    }
    

    
    private static void passPDFParameters(PageReference pdf,List<csord__Service__c> serviceList,CreateAddressWrapperForInvoice.AddressWrapper addWrap){
              
        if(!serviceList.isEmpty()){
            pdf.getParameters().put('basketNumber', serviceList[0].csordtelcoa__Product_Basket__r.Basket_Number__c);
            pdf.getParameters().put('studentName', serviceList[0].csordtelcoa__Main_Contact__r.Name);
            pdf.getParameters().put('studentId', serviceList[0].csordtelcoa__Main_Contact__r.Student_ID__c);
            pdf.getParameters().put('totalPrice', String.valueOf(serviceList[0].csordtelcoa__Product_Basket__r.cscfga__Total_Price__c));
            pdf.getParameters().put('invoiceABN', serviceList[0].Course_Offering__r.hed__Course__r.hed__Account__r.ABN__c);
        }
        
     /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf:Starts : 25/07/2018 **/
        if(String.isNotBlank(addWrap.street)){
            pdf.getParameters().put('street', addWrap.street );
        } 
        if(String.isNotBlank(addWrap.cityStatePOCode)){
            pdf.getParameters().put('cityStatePOCode', addWrap.cityStatePOCode);
        } 
        if(String.isNotBlank(addWrap.country)){
            pdf.getParameters().put('country', addWrap.country );
        } 
        
        /**ECB-4309:Shreya -Added for receiver address formatting in Invoice pdf: Ends: 25/07/2018 **/
              
        if(addWrap.prodConfigIdString!='' && addWrap.prodConfigIdString!=null){
            pdf.getParameters().put('prodConfigIds', addWrap.prodConfigIdString);
        }
 
        
         
    }
    
   //Attaching the invoice pdf to the Payment record
    private static void insertPDFAttachment(Map<Id,Blob> paymentRecAttachMap){
        
        List<Attachment> attachList = new List<Attachment>();
        for(Id payid : paymentRecAttachMap.keyset()){
            Attachment att = new Attachment();
            att.Body = paymentRecAttachMap.get(payid);
            att.ParentId = payid; //paymentRec.Id;
           // att.Name = System.Label.InvoiceAttName;
            att.Name = ConfigDataMapController.getCustomSettingValue('InvoiceAttName'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
            attachList.add(att);
        }
        
        try{
            if(!Schema.sObjectType.Attachment.isCreateable())
            {
              throw new DMLException();
            }
            Insert attachList;                   
        }catch(DmlException ex){
            system.debug('Exception::'+ex);
        }   
    }
    
 }