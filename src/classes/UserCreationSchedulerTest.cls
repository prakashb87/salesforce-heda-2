/*****************************************************************************************************
*** @Class             : UserCreationScheduler
*** @Author            : Rishabh Anand
*** @Requirement       : test class for UserCreationScheduler
*** @Created date      : 31/05/2018
*** @JIRA ID           : ECB-3249
*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is a test class for UserCreationScheduler(scheduler to schedule User Creation)
*****************************************************************************************************/ 
@isTest
public class UserCreationSchedulerTest {
    
    @isTest
    public static void test(){
        
       
        Test.startTest();
		TestDataFactoryUtil.testUserdata();
        UserCreationScheduler sc = new UserCreationScheduler();
        String sch='0 0 5 * * ?';
        System.Schedule('Every 6 hrs', sch, sc);
        Test.stopTest();
		Contact Contacts = [SELECT FirstName, LastName, Community_User_Provisioned__c FROM Contact limit 1];
        system.assertEquals(false,Contacts.Community_User_Provisioned__c);
    }
}