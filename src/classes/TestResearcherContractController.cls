/*******************************************
Purpose: Test class ResearcherContractController
History:
Created by Subhajit on 12/10/2018
*******************************************/
@isTest
public class TestResearcherContractController {

    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
        
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
        
    }    
      /************************************************************************************
    // Purpose      :  Test functionality of positive for getAllResearcherContractRecsInfo
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/        
    @isTest
    public static void testGetAllResearcherContractRecsInfoPositiveMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userRec!=null);
        
        System.runAs(userRec) {
            
            ResearchProjectContract__c newContractRec = new ResearchProjectContract__c(
                Ecode__c ='0000123456',
                ContractTitle__c='test Contract',
                Status__c='Completed',
                Contract_Logged_Date__c = system.today()-2,
                Current_Active_Action__c='With Researcher (12/05/15)',
                Fully_Executed_Date__c = system.today()+1,
                Contact_Preferred_Full_Name__c ='Dr test researcher',
                Activity_Type__c='Contract Research',
                Contract_Type__c ='RA-Variation'
            );
            insert newContractRec;
            System.assert(newContractRec!=null);  
                                            
            /*String resultJson= ResearcherContractController.getAllResearcherContractRecsInfo();
            System.assert(resultJson!=null);*/
            
            list<ResearcherContractWrapper> contractRecs = ResearcherContractController.getAllResearcherContractRecsInfo();
            System.assert(contractRecs!=null);
            List <String> contractStatusRecs = ResearcherContractController.getContractStatusValue();
            System.assert(contractStatusRecs!=null);
            List <String>  contractResearchTypeRecs = ResearcherContractController.getContractResearchTypeValue();
            System.assert(contractResearchTypeRecs!=null);
            
            List<ResearcherContractWrapper> contractWrapperList = ResearcherContractController.getContracts(newContractRec.Id);
            System.assert(contractWrapperList!=null);
            
            List<MilestonesDetailsWrapper.milestonesWrapper> milestonesWrapperList = ResearcherContractController.getContractMilestonesList(newContractRec.Id);
			System.assert(milestonesWrapperList!=null);
            
        }  
    }
    
     /************************************************************************************
    // Purpose      :  Test functionality of negative for getAllResearcherContractRecsInfo
    // Developer    :  Subhajit
    // Created Date :  12/10/2018                 
    //***********************************************************************************/        
    @isTest
    public static void testGetAllResearcherContractRecsInfoNegativeMethod(){
        User userRec =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        System.assert(userRec!=null);
        System.runAs(userRec) {
            
          /*  ResearchProjectContract__c newContractRec = new ResearchProjectContract__c(
                Ecode__c ='0000123456',
                ContractTitle__c='test Contract',
                Status__c='Completed',
                Contract_Logged_Date__c = system.today()-2,
                Current_Active_Action__c='With Researcher (12/05/15)',
                Fully_Executed_Date__c = system.today()+1,
                Contact_Preferred_Full_Name__c ='Dr test researcher',
                Activity_Type__c='Contract Research',
                Contract_Type__c ='RA-Variation'
            );
            insert newContractRec;
            System.assert(newContractRec!=null);  */
          
            /*String resultJson= ResearcherContractController.getAllResearcherContractRecsInfo();
            System.assert(resultJson!=null);*/
            
            list<ResearcherContractWrapper> contractRecs = ResearcherContractController.getAllResearcherContractRecsInfo();
            System.assert(contractRecs!=null);
           	List <String> contractStatusRecs = ResearcherContractController.getContractStatusValue();
            System.assert(contractStatusRecs!=null);
            List <String>  contractResearchTypeRecs = ResearcherContractController.getContractResearchTypeValue();
            System.assert(contractResearchTypeRecs!=null);
            
           
        }   
    }
     @isTest
    public static void testExceptionHandlingMethod(){
        
        User u =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1]; 
        
        System.runAs(u) {      
            
            ResearcherContractController.isForceExceptionRequired=true;
            list<ResearcherContractWrapper> contractRecs1 = ResearcherContractController.getAllResearcherContractRecsInfo();
            System.assert(contractRecs1!=null);
        }
    }

}