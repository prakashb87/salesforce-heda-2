@isTest
private class HEDA2CSUtilTest {

	@testSetup static void setup() {
		Account account21CC = New Account(name = '21CC');
		Insert account21CC;

		Account accountRMITOnline = New Account(name = 'RMIT Online');
		Insert accountRMITOnline;
        
        // RMIT Training Fix
		Account accountRMITTraining = New Account(name = 'RMIT Training');
		Insert accountRMITTraining;
		// RMIT Training Fix

        Config_Data_Map__c account21CCcustomSetting = new Config_Data_Map__c();
        account21CCcustomSetting.Name = 'AccountID_21CC';
        account21CCcustomSetting.Config_Value__c = account21CC.id; 
        Insert account21CCcustomSetting;

        Config_Data_Map__c accountRMITTrainingCustomSetting = new Config_Data_Map__c();
        accountRMITTrainingCustomSetting.Name = 'AccountID_RMITTraining';
        accountRMITTrainingCustomSetting.Config_Value__c = accountRMITTraining.id; 
        Insert accountRMITTrainingCustomSetting;

        Config_Data_Map__c accountRMITOnlineCustomSetting = new Config_Data_Map__c();
        accountRMITOnlineCustomSetting.Name = 'AccountID_RMITOnline';
        accountRMITOnlineCustomSetting.Config_Value__c = accountRMITOnline.id; 
        Insert accountRMITOnlineCustomSetting;


        cspmb__Add_On_Price_Item__c addOn1 = new cspmb__Add_On_Price_Item__c();
		addOn1.name = 'All';
		Insert addOn1;

		cspmb__Add_On_Price_Item__c addOn2 = new cspmb__Add_On_Price_Item__c();
		addOn2.name = 'Assessment Only';
		Insert addOn2;


		cspmb__Add_On_Price_Item__c addOn3 = new cspmb__Add_On_Price_Item__c();
		addOn3.name = 'Content Only';
		Insert addOn3;
    }
    

	@isTest static void testTransformCourses2PriceItemsWithoutAddon() {
		// Create test data
		Account department = New Account(name = 'RMITU');
		Insert department;

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = department.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = department.Id;
		course.price__c = 0;
		Insert course;

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		hed__Course_Offering__c courseOffering2 = New hed__Course_Offering__c();
		courseOffering2.name = 'Course Offering ID 2';
		courseOffering2.hed__course__c = course.Id;
		courseOffering2.Status__c = 'Inactive';
		courseOffering2.hed__Term__c = term.Id;
		courseOffering2.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering2.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering2;

		hed__Course_Offering__c courseOffering3 = New hed__Course_Offering__c();
		courseOffering3.name = 'Course Offering ID 3';
		courseOffering3.hed__course__c = course.Id;
		courseOffering3.Status__c = 'Active';
		courseOffering3.hed__Term__c = term.Id;
		courseOffering3.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering3.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering3;

		Test.startTest();

		List<hed__Course_Offering__c> courseOfferingsFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c, hed__End_Date__c,
		                              hed__course__r.name,
		                              hed__course__r.price__c, Status__c,
		                              hed__course__r.hed__Account__c
		                              FROM hed__Course_Offering__c];

		// upsert twice
		HEDA2CSUtil.transformCourses2PriceItems (courseOfferingsFromDB);

		Test.stopTest();

		List<cspmb__Price_Item__c> priceItems = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c,
		                           External_Unique_ID__c from cspmb__Price_Item__c];


		// We should only have 3 records after two upsert.
		System.assertEquals(3, priceItems.size());

		System.debug(priceItems);

		// Test individual record
		List<cspmb__Price_Item__c> priceItems2 = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c, cspmb__Is_Active__c,
		                           External_Unique_ID__c,
		                           cspmb__One_Off_Charge__c
		                           from cspmb__Price_Item__c
		                           Where Course_Offering_ID__c = :courseOffering1.ID];

		System.assertEquals(1, priceItems2.size());
		System.assertEquals(course.Name, priceItems2[0].Name);
		System.assertEquals(course.Id, priceItems2[0].Course_ID__c);
		System.assertEquals(courseOffering1.Id, priceItems2[0].Course_Offering_ID__c);
		System.assertEquals(courseOffering1.hed__Start_Date__c, priceItems2[0].cspmb__Effective_Start_Date__c);
		System.assertEquals(courseOffering1.hed__End_Date__c, priceItems2[0].cspmb__Effective_End_Date__c);
		System.assertEquals(true, priceItems2[0].cspmb__Is_Active__c);
	}

	@isTest static void testTransformCourses2PriceItemsWithAddon() {
		// Create test data
		Account rmitOnlineAccount = [Select Id from Account where name = 'RMIT Online'];

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = rmitOnlineAccount.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = rmitOnlineAccount.Id;
		course.price__c = 0;
		Insert course;

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		hed__Course_Offering__c courseOffering2 = New hed__Course_Offering__c();
		courseOffering2.name = 'Course Offering ID 2';
		courseOffering2.hed__course__c = course.Id;
		courseOffering2.Status__c = 'Inactive';
		courseOffering2.hed__Term__c = term.Id;
		courseOffering2.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering2.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering2;

		hed__Course_Offering__c courseOffering3 = New hed__Course_Offering__c();
		courseOffering3.name = 'Course Offering ID 3';
		courseOffering3.hed__course__c = course.Id;
		courseOffering3.Status__c = 'Active';
		courseOffering3.hed__Term__c = term.Id;
		courseOffering3.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering3.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering3;

		Test.startTest();

		List<hed__Course_Offering__c> courseOfferingsFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c, hed__End_Date__c,
		                              hed__course__r.name,
		                              hed__course__r.price__c, Status__c,
		                              hed__course__r.hed__Account__c
		                              FROM hed__Course_Offering__c];

		// upsert twice
		HEDA2CSUtil.transformCourses2PriceItems (courseOfferingsFromDB);

		Test.stopTest();

		List<cspmb__Price_Item__c> priceItems = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c,
		                           External_Unique_ID__c from cspmb__Price_Item__c];

		// We should only have 3 records after two upsert.
		System.assertEquals(3, priceItems.size());

		System.debug(priceItems);

		// Test individual record
		List<cspmb__Price_Item__c> priceItems2 = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c, cspmb__Is_Active__c,
		                           External_Unique_ID__c,
		                           cspmb__One_Off_Charge__c
		                           from cspmb__Price_Item__c
		                           Where Course_Offering_ID__c = :courseOffering1.ID];

		System.assertEquals(1, priceItems2.size());
		System.assertEquals(course.Name, priceItems2[0].Name);
		System.assertEquals(course.Id, priceItems2[0].Course_ID__c);
		System.assertEquals(courseOffering1.Id, priceItems2[0].Course_Offering_ID__c);
		System.assertEquals(courseOffering1.hed__Start_Date__c, priceItems2[0].cspmb__Effective_Start_Date__c);
		System.assertEquals(courseOffering1.hed__End_Date__c, priceItems2[0].cspmb__Effective_End_Date__c);
	
		List<cspmb__Price_Item_Add_On_Price_Item_Association__c> priceItemAddonAssociations = [Select Id, Name, cspmb__Price_Item__c, cspmb__Add_On_Price_Item__c from cspmb__Price_Item_Add_On_Price_Item_Association__c];

		System.assertEquals(9, priceItemAddonAssociations.size());
		System.assertNotEquals (null, priceItemAddonAssociations[0].cspmb__Price_Item__c);
		System.assertNotEquals (null, priceItemAddonAssociations[0].cspmb__Add_On_Price_Item__c);
	}


	@isTest static void testTransformProgram2PriceItems() {
		Account rmitOnlineAccount = [Select Id from Account where name = 'RMIT Online'];

		// Create test data
		Id recordTypeIdProgram = Schema.SObjectType.account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
		Account testProgram = New Account();
		testProgram.Name = 'iOS App Development with Swift';
		testProgram.RecordTypeId = recordTypeIdProgram;
		testProgram.ParentId = rmitOnlineAccount.id;
		Insert testProgram;
		
		hed__Program_Plan__c testProgramPlan = New hed__Program_Plan__c ();
		testProgramPlan.Name = 'iOS App Development with Swift Program Plan V2.0';
		testProgramPlan.hed__Is_Primary__c = True;
		testProgramPlan.hed__Account__c = testProgram.Id;
		Insert testProgramPlan;

		Test.startTest();
		List<hed__Program_Plan__c> dbData = [SELECT Id, hed__Account__c, hed__Is_Primary__c, 
								hed__Account__r.name, hed__Account__r.ParentId
								FROM hed__Program_Plan__c 
								where hed__Is_Primary__c = true];

	
		HEDA2CSUtil.transformProgram2PriceItems (dbData);
		Test.stopTest();

		List<cspmb__Price_Item__c> priceItems = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c,
		                           External_Unique_ID__c from cspmb__Price_Item__c];


		List<cspmb__Price_Item_Add_On_Price_Item_Association__c> priceItemAddonAssociations = [Select Id, Name, cspmb__Price_Item__c, cspmb__Add_On_Price_Item__c from cspmb__Price_Item_Add_On_Price_Item_Association__c];

		System.assertEquals(1, priceItems.size());	

		System.assertEquals(3, priceItemAddonAssociations.size());

		System.assertNotEquals (null, priceItemAddonAssociations[0].cspmb__Price_Item__c);
		System.assertNotEquals (null, priceItemAddonAssociations[0].cspmb__Add_On_Price_Item__c);
	}

	@isTest static void testTransformCourses2PriceItemsRMITTraining() 
	{
		// Create test data
		Account rmitTrainingAccount = [Select Id from Account where name = 'RMIT Training'];

		hed__Term__c term = New hed__Term__c();
		term.hed__Account__c = rmitTrainingAccount.Id;
		Insert term;

		hed__course__c course = New hed__course__c();
		course.Name = 'Test Course';
		course.hed__Account__c = rmitTrainingAccount.Id;
		course.price__c = 0;
		Insert course;

		hed__Course_Offering__c courseOffering1 = New hed__Course_Offering__c();
		courseOffering1.name = 'Course Offering ID 1';
		courseOffering1.hed__course__c = course.Id;
		courseOffering1.Status__c = 'Active';
		courseOffering1.hed__Term__c = term.Id;
		courseOffering1.hed__Start_Date__c = Date.newInstance(2018, 1, 16);
		courseOffering1.hed__End_Date__c = Date.newInstance(2018, 2, 16);
		Insert courseOffering1;

		Test.startTest();

		List<hed__Course_Offering__c> courseOfferingsFromDB = [SELECT Id, name, hed__course__c,
		                              hed__Start_Date__c, hed__End_Date__c,
		                              hed__course__r.name,
		                              hed__course__r.price__c, Status__c,
		                              hed__course__r.hed__Account__c
		                              FROM hed__Course_Offering__c];

		// upsert twice
		HEDA2CSUtil.transformCourses2PriceItems (courseOfferingsFromDB);

		Test.stopTest();
		// Test individual record
		List<cspmb__Price_Item__c> priceItems2 = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c, cspmb__Is_Active__c,
		                           External_Unique_ID__c,
		                           cspmb__One_Off_Charge__c
		                           from cspmb__Price_Item__c
		                           Where Course_Offering_ID__c = :courseOffering1.ID];

		System.assertEquals(1, priceItems2.size());

	}
}