/**************************************************************
Purpose    : The controller class is for the chatter component on the public projects
Created by : Prakash 13/03/2019
History    :
*************************************************************/

public with sharing class ChatterFeedController {
    
    public static Boolean isForceExceptionRequired = true;
    
    /** Method to return the current logged in User ID for delete scenarios **/
    
    @AuraEnabled 
    public static String getLoggedInuserId(){
    
        return [ SELECT ID FROM User WHERE ID =: UserInfo.getUserID() ]. ID ;
    }
    
    /** Method to get the feed comment from the portal and create a new feed for the project **/

    @AuraEnabled
    public static void createFeedItem(String chatterComment, Id recordIdfromcontroller) {
        try{
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), recordIdfromcontroller, ConnectApi.FeedElementType.FeedItem, chatterComment);
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }catch(Exception ex){
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
        }
    }
    
    /** Method to get the feed reply from the portal and create a new comment for the feed **/
    
    @AuraEnabled
    public static void createCommentForFeed(Id feedId, String feedComment){
        try{
            ConnectApi.Comment comment = ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), feedId, feedComment );
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }catch(Exception ex){
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());    
        }
    }
       
    /** Method to pass the list of existing comments for the project **/
    
    @AuraEnabled
    public static List<ChatterWrapper> getChatterComments(String projectId){
    
        Set<Id> userIds = new Set<Id>();
        Set<Id> feedIds = new Set<Id>();
        Map<Id, List<FeedComment>> commentsMap = new Map<Id, List<FeedComment>>();
        
        /* Get all the feeds and the related feed comments for the project */
        
        for ( ResearcherPortalProject__Feed  feedItem : [SELECT Id, Body, CreatedById, CreatedDate,Type, (SELECT Id, CommentBody, CreatedDate, FeedItemId, InsertedById, LastEditDate FROM FeedComments ORDER BY CreatedDate DESC) FROM ResearcherPortalProject__Feed WHERE ParentId =: projectId AND Type = 'TextPost' ORDER BY CreatedDate DESC LIMIT 50000]){
            userIds.add( feedItem.CreatedById );
            feedIds.add( feedItem.Id);
            commentsMap.put(feedItem.Id, feedItem.FeedComments);
            for(FeedComment feedComment : feedItem.FeedComments){
                userIds.add(feedComment.InsertedById);
            }
        }
        
        /* Form a user map to display the users name, photo and email next to the comments */
    
        Map<Id, User> userMap = new Map<Id, User>();
        userMap  = ApexWithoutSharingUtils.getUserMap(userIds);
        
        Map<Id,String> userRoleMap  = new Map<Id, String>();
        
        for(Researcher_Member_Sharing__c projectRMS : [SELECT ID, User__c, Position__c, MemberRole__c, SF_Role__c FROM Researcher_Member_Sharing__c WHERE Researcher_Portal_Project__c =: projectId AND User__c IN: userIds]){
            userRoleMap.put(projectRMS.User__c, projectRMS.Position__c);
        }
        
        /* Form a wrapper to send it to the front end to display the list of feeds and the related comments to the user in the portal */
      
        List<ChatterWrapper> commentsList = new List<ChatterWrapper>();
        for ( ResearcherPortalProject__Feed  feedItem : [SELECT Id, Body, CreatedById, CreatedDate FROM ResearcherPortalProject__Feed WHERE ParentId =: projectId AND Type = 'TextPost' ORDER BY CreatedDate DESC LIMIT 50000]){
            ChatterWrapper commentsInfo = new ChatterWrapper();
            commentsInfo.userName = userMap.get(feedItem.CreatedById).Name;
            commentsInfo.timeStamp = String.valueof(feedItem.CreatedDate );
            commentsInfo.sortTimeStamp = feedItem.CreatedDate;
            commentsInfo.comment = feedItem.Body;
            commentsInfo.userId = feedItem.CreatedById;
            commentsInfo.feedId = feedItem.Id;
            commentsInfo.userEmail = userMap.get(feedItem.CreatedById).Email;
            commentsInfo.photoURL = userMap.get(feedItem.CreatedById).SmallPhotoUrl;
            commentsInfo.userRole = userRoleMap.get(feedItem.CreatedById) != null ? userRoleMap.get(feedItem.CreatedById) : '';
            
            if(commentsMap.containsKey(feedItem.ID)){
                commentsInfo.feedComments = new List<ChatterWrapper.feedComments>();
                for(FeedComment feedCmt : commentsMap.get(feedItem.ID)){
                    System.debug(feedCmt);
                    ChatterWrapper.feedComments comments = new ChatterWrapper.feedComments();
                    comments.feedComment = new ChatterWrapper();
                    comments.feedComment.userName = userMap.get(feedCmt.InsertedById ).Name;
                    comments.feedComment.timeStamp = String.valueof(feedCmt.CreatedDate);
                    commentsInfo.sortTimeStamp = feedCmt.CreatedDate > commentsInfo.sortTimeStamp ? feedCmt.CreatedDate : commentsInfo.sortTimeStamp;
                    comments.feedComment.comment = feedCmt.CommentBody;
                    comments.feedComment.userId = feedCmt.InsertedById;
                    comments.feedComment.feedItemId = feedCmt.FeedItemId;//Gourav
                    comments.feedComment.feedId = feedCmt.Id;
                    comments.feedComment.userEmail = userMap.get(feedCmt.InsertedById).Email;
                    comments.feedComment.photoURL = userMap.get(feedCmt.InsertedById).SmallPhotoUrl;
                    comments.feedComment.userRole = userRoleMap.get(feedCmt.InsertedById) != null ? userRoleMap.get(feedCmt.InsertedById) : '';
                    commentsInfo.feedComments.add(comments);
                }
            
            }
            commentsList.add(commentsInfo);
        }
        
        commentsList.sort();
        return commentsList;
    }
    
    @AuraEnabled
    public static void deleteChatterPost(String feedId,String idType,String feedItemId){
        system.debug('   '+feedId+' : '+userinfo.getuserid()+' : '+idType+' : '+feedItemId);
        
        
        ResearcherPortalProject__Feed feedRecord = new ResearcherPortalProject__Feed();
        FeedComment feedCommentRecord = new FeedComment();
        if(idType == 'feedComment'){
            
            ChatterFeedController chatterclass = new ChatterFeedController();
            chatterclass.deleteFeedComments(feedId, feedItemId);//feedcomment method call
            
        }else if(idType == 'feed'){
            
            ChatterFeedController chatterclass = new ChatterFeedController();
            chatterclass.deleteFeed(feedId) ;//feed method call
        }
    }
    
    private void deleteFeedComments(String feedId,String feedItemId){
        
        ResearcherPortalProject__Feed feedRecord = new ResearcherPortalProject__Feed();
        FeedComment feedCommentRecord = new FeedComment();
        
		if((Schema.sObjectType.ResearcherPortalProject__Feed.isAccessible()) && (Schema.sObjectType.FeedComment.isDeletable()) &&(Schema.sObjectType.FeedComment.isAccessible())){
			   ResearcherPortalProject__Feed projectFeed = [SELECT  Id,Body, CreatedById, CreatedDate,Type,(SELECT Id, CommentBody, CreatedDate, FeedItemId, InsertedById, LastEditDate,CreatedById
                                                                                                            FROM FeedComments) FROM ResearcherPortalProject__Feed WHERE Id =:feedItemId];
               system.debug('@@@@@projectFeed'+projectFeed);
               for(FeedComment  feedCom : projectFeed.FeedComments ){
                   if(feedCom.Id==feedId && feedCom.CreatedById==userinfo.getuserid()){
                       feedCommentRecord = feedCom;
                   }
               }
               system.debug('@@@feedCommentRecord'+feedCommentRecord);
               Delete feedCommentRecord;
           }
    }  
    
    private void deleteFeed(String feedId){
        
        ResearcherPortalProject__Feed feedRecord = new ResearcherPortalProject__Feed();
        if((Schema.sObjectType.ResearcherPortalProject__Feed.isDeletable())){
            feedRecord = [SELECT  Id,Body, CreatedById, CreatedDate,Type FROM ResearcherPortalProject__Feed 
                          WHERE Id =: feedId AND CreatedById =: userinfo.getuserid()];
            
            system.debug('@@@feedRecord'+feedRecord);
            Delete feedRecord;    
            
        }
    }
}