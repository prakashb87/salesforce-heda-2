/*********************************************************************************
 *** @ClassName         : TermService
 *** @Author            : Shubham Singh 
 *** @Requirement       : TermService class for methods related to web service version 1.1
 *** @Created date      : 18/03/2019
 *** @Modified by       : Shubham Singh 
 *** @modified date     : 01/04/2019
 **********************************************************************************/
public with sharing class TermService {
    //wrapper class to create variables to store the respective values     
    public class TermServiceWrapper {
        public Boolean allExist = true;
        public List < hed__Term__c > terms;
        public Set < String > notFound;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check for Term record exists with respect to term code
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    getTermByTermCode
     * @param     Set< String > term
     * @return    TermServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static TermServiceWrapper getTermByTermCode(Set < String > term) {
        TermServiceWrapper wrapper = new TermServiceWrapper();
        wrapper = checkAllExistTerm(term);
        return wrapper;
    }

    /**
     * -----------------------------------------------------------------------------------------------+
     * Query records from term object
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    checkAllExistTerm
     * @param     termSet
     * @return    wrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static TermServiceWrapper checkAllExistTerm(Set < String > termSet) {
        TermServiceWrapper wrapper = new TermServiceWrapper();
        system.debug('termSet ==' + termSet);
        List < hed__Term__c > listOfTerm = new List < hed__Term__c > ();
        if (Schema.sObjectType.hed__Term__c.fields.Term_Code__c.isAccessible()) {
            listOfTerm = [Select Id, Term_Code__r.Term_Code__c, Term_Code__c FROM hed__Term__c WHERE Term_Code__r.Term_Code__c =: termSet];
        }
        wrapper.notFound = termSet.clone();
        for (hed__Term__c loopTerm: listOfTerm) {
            wrapper.notFound.remove(loopTerm.Term_Code__r.Term_Code__c);
        }
        if (!wrapper.notFound.isEmpty()) {
            wrapper.allExist = false;
        }
        system.debug('wrapper ===' + wrapper);
        return wrapper;
    }
}