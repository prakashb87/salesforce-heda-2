/**
 * @name EmbeddedCredentialsBatchScheduled Scheduler 
 * @description scheduler class for 'EmbeddedCredentialsBatchScheduled' batch process
 * @revision
 * Boris Bjelan 13-06-2016 Created class
 */
public without sharing class EmbeddedCredentialsBatchScheduled implements Schedulable {
    
    public void execute(SchedulableContext ctx) {
        EmbeddedCredentialsBatch bc = new EmbeddedCredentialsBatch();  
        Database.executeBatch(bc, 1);
    }
    
    /* 
    public static String CRON_EXP = '0 05 * * * ?';
    
    global static String scheduleIt() {
        CloneServiceRelatedDataBatchScheduled scheduler = new CloneServiceRelatedDataBatch Scheduled ();
        return System.schedule('CloneServiceRelatedDataBatchScheduledJob', CRON_EXP, scheduler);
    } */
}