/**
 * Created by mmarsson on 1/03/19.
 */

@SuppressWarnings('PMD')
public with sharing class InboundParameterImplementations {
    /**
    * -----------------------------------------------------------------------------------------------+
    * Adding elements to validate madatory values for Enrolment
    * ------------------------------------------------------------------------------------------------
    * @author	 Shubham Singh 
    * @class     TranslationParameters
    * @extends   AbstractInboundParameter
    * @return    None
    * @Reference RM-2567
    * -----------------------------------------------------------------------------------------------+
    */
    public class TranslationParameters extends AbstractInboundParameter {
        public String category;
        public String value;
        public String effectiveDate;
        public String effectiveStatus;
        public String translatedValue;
        public String translatedShortValue;
        public String lastUpdated;
        public String lastUpdatedBy;
        public override void setMandatoryFields() {
            super.setMandatoryFields();
            addMandatoryField('category');
            addMandatoryField('value');
        }

    }
    /**
    * -----------------------------------------------------------------------------------------------+
    * Adding elements to validate madatory values for Fund Source
    * ------------------------------------------------------------------------------------------------
    * @author	 Mehreen Mansoor 
    * @class     FundSourceParameters
    * @extends   AbstractInboundParameter
    * @return    None
    * @Reference RM-2562
    * -----------------------------------------------------------------------------------------------+
    */
    
    public class FundSourceParameters extends AbstractInboundParameter {
        public String fundSourceCode;
        public String effectiveDate;
        public String status;
        public String description;
        public String longDescription;
        public String fundSourceState;
        public String fundSourceNational;
        public String fundSourceBroad;
        public String fundSourceDEEWR;
        public String state;
        public String apprenticeTraineeWaiver;
        public String apprenticeTraineeIndicator;
        public String subjectToEsos;

        public override void setMandatoryFields() {
            super.setMandatoryFields();
            addMandatoryField('fundSourceCode');
        }

    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * Adding elements to validate madatory values for HECS
    * ------------------------------------------------------------------------------------------------
    * @author	 Dinesh Kumar 
    * @class     HECSParameters
    * @extends   AbstractInboundParameter
    * @return    None
    * @Reference RM-2563
    * -----------------------------------------------------------------------------------------------+
    */

    public class HECSParameters extends AbstractInboundParameter {
 
        public String HecsExemptStatusCode; // hecsExemptStatusCode;
        public String effectiveDate;
        public String effectiveStatus;
        public String Description; // description;
        public String ShortDescription; // shortDescription;
        public String HecsCodeType; // hecsCodeType;
        public String LoanType; // loanType;
        public String CommonwealthAssisted; // commonwealthAssisted;
        public String LongDescription; // longDescription;
        
        public override void setMandatoryFields() {
            super.setMandatoryFields();
            //addMandatoryField('hecsExemptStatusCode');
            addMandatoryField('HecsExemptStatusCode');
        }
        
    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * Adding elements to validate madatory values for Location
    * ------------------------------------------------------------------------------------------------
    * @author	 Dinesh Kumar 
    * @class     LocationParameters
    * @extends   AbstractInboundParameter
    * @return    None
    * @Reference RM-2564
    * -----------------------------------------------------------------------------------------------+
    */

    public class LocationParameters extends AbstractInboundParameter {
 
        public String setId; 
        public String location;
        public String effectiveDate;
        public String effectiveStatus; 
        public String description; 
        public String shortDescription;
        public String building; 
        public String floor;
        public String sector; 
        public String jurisdiction;
        
        public override void setMandatoryFields() {
            super.setMandatoryFields();
            addMandatoryField('setId');
            addMandatoryField('location');
        }
        
    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * Adding elements to validate madatory values for Enrolment
    * ------------------------------------------------------------------------------------------------
    * @author	 Shubham Singh 
    * @class     EnrolmentParameters
    * @extends   AbstractInboundParameter
    * @return    None
    * @Reference RM-2580
    * -----------------------------------------------------------------------------------------------+
    */
    
    public class EnrolmentParameters extends AbstractInboundParameter {
        
        public String studentId;
        public String academicCareer;
        public String institution;
        public String term;
        public String session;
        public String status;
        public String statusReason;
        public String lastAction;
        public String lastActionReason;
        public String statusDate;
        public String addDate;
        public String dropDate;
        public String gradingBasis;
        public String gradingBasisDate;
        public String officialGrade;
        public String gradeInput;
        public String gradeDate;
        public String academicProgram;
        public String hecsExemptionStatusCode;
        public String fundingSourceCode;
        public Integer studentCareerNumber;
        public Integer classNumber;
        public Integer unitsTaken;
        public Integer progressUnits;
        public Integer billingUnits;
        
        public override void setMandatoryFields() {
            super.setMandatoryFields();
            addMandatoryField('studentId');
            addMandatoryField('academicCareer');
            addMandatoryField('institution');
            addMandatoryField('term');
            addMandatoryField('classNumber');
        }
        
        public override void validateParameters(){
            System.debug('Adding mandatory fields');
            super.validateParameters();
        }
    }
    
    // Webservice Framework Example Methods

	public class AOUParameters extends AbstractInboundParameter{
        public String studentId;
        public String AOUId;
        public string schoolId;

        public override void setMandatoryFields() {
            super.setMandatoryFields();
            addMandatoryField('studentId');
        }

    }
    
    public class ExampleParameteres extends AbstractInboundParameter{
        private String parameter1{get;set;}
        private String parameter2{get;set;}
        private String parameter3{get;set;}
        private List<ExampleParameteresChild> exampleChild{get;set;}


        //This is the declarative on object lvl for mandatory field declaration
        public override void setMandatoryFields(){
            super.setMandatoryFields();
            System.debug('Adding mandatory fields');
            addMandatoryField('parameter1');
            addMandatoryField('parameter3');
            addMandatoryField('exampleChild');
        }

        public override void validateParameters(){
            System.debug('Adding mandatory fields');
            super.validateParameters();
            for (ExampleParameteresChild child : exampleChild){
            child.validateParameters();
            }
        }

    }

    public class ExampleParameteresChild extends AbstractInboundParameter{
        private String parameter1{get;set;}
        private String parameter2{get;set;}
        private String parameter3{get;set;}

        public override void setMandatoryFields(){
            super.setMandatoryFields();
            System.debug('Adding mandatory fields');
            addMandatoryField('parameter1');
            addMandatoryField('parameter3');

        }
    }


}