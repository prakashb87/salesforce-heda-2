@isTest
private class AllOpportunityTriggersHandlerTest {
    
    private static User usr, usr2;
    private static Contact con, con2;
    private static Opportunity oppty;
    private static cscfga__Product_Basket__c basket;
    
    //@testSetup 
    static void setupTestData() {
        
        Account acc = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());
        List<String> lstParams1 = new List<String>{'Test' , 'Con'};        
        con = TestUtility.createTestContact(true
            , lstParams1
            , acc.Id);
        List<String> lstParams2 = new List<String>{'Test2', 'Con2'};
        con2 = TestUtility.createTestContact(true
            , lstParams2
            , acc.Id);
        
        usr = TestUtility.createUser('Customer Community Login User - RMIT', false);
        usr.ContactId = con.Id;
        insert usr;
        
        usr2 = TestUtility.createUser('Customer Community Login User - RMIT', false);
        usr2.ContactId = con2.Id;
        insert usr2;
        
        oppty = TestUtility.createTestOpportunity(false, 'Test Opportunity', acc.Id);
        oppty.Assign_To_Contact__c = con.Id;
        insert oppty;
        
        basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = oppty.Id);
        insert basket;
    }
    
    @isTest 
    static void updateOpptyOwnerTest() {
        
        setupTestData();
        
        Test.startTest();
        
        oppty = [select Id, Assign_To_Contact__c, OwnerId from Opportunity where Id = :oppty.Id];
        oppty.Assign_To_Contact__c = con2.Id;
        Id oldOwnerId = oppty.OwnerId;
        
        update oppty;
        
        oppty = [select Id, OwnerId from Opportunity where id = :oppty.Id];
        basket = [select Id, OwnerId from cscfga__Product_Basket__c where id = :basket.Id];
        
        Id newOwnerId = oppty.OwnerId; 
        
        Test.stopTest();
        
        system.debug('newOwnerId >> ' + newOwnerId);
        system.debug('oldOwnerId >> ' + oldOwnerId);
        
        system.assertNotEquals(oppty.OwnerId, oldOwnerId);
        system.assertNotEquals(basket.OwnerId, oldOwnerId);
    }
}