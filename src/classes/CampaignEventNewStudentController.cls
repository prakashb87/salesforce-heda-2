public without sharing class CampaignEventNewStudentController 
{
    private Lead newLead;
    private static Id prospectiveRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String mobile {get; set;}
    public String school {get; set;}
//    public String I_am_currently {get; set;} //This seems not in use
    public String interestArea {get; set;}
    public Boolean hasSchool {get; set;}
 //   public Boolean isError {get;set;}
    private static final String ENQUIRY_STATUS = 'Requested';
    private static final String DEFAULT_CONTACT_METHOD = 'Event';
    
    public CampaignEventNewStudentController(ApexPages.StandardController stdController)
    {
        system.debug('-------CampaignEventNewStudentController--------');
        this.newLead = (Lead) stdController.getRecord();
        
        firstName = '';
        lastName =  '';
        email =  '';
        mobile = '';
        hasSchool = false;
   //     isError = false;
        String message = '';
        String statusMessage =''; 
        school = getSchoolFromParams();
        if(String.isEmpty(school))
        {
            hasSchool = true;   
        }
        //I_am_currently = ''; //This seems not in use
        
        if(ApexPages.CurrentPage().GetParameters().Get('message')!=null){
        	 message = ('' + ApexPages.CurrentPage().GetParameters().Get('message')).escapeHtml4();
        }
        if(ApexPages.CurrentPage().GetParameters().Get('status')!=null){
        	statusMessage = ('' + ApexPages.CurrentPage().GetParameters().Get('status')).escapeHtml4();
        }
		showMessageAfterCheck(message, statusMessage);
		
    }
    
    private String getSchoolFromParams(){
    	return ApexPages.currentPage().getParameters().get('schoolname')==null? null:ApexPages.currentPage().getParameters().get('schoolname').escapeHtml4();
    }
    private void showMessageAfterCheck(String message, String statusMessage){
        String pageHeaderReferer = ApexPages.currentPage().getHeaders().get('Referer'); 
        if(pageHeaderReferer != null && pageHeaderReferer.containsIgnoreCase('Event_Student') && message != 'null' && statusMessage == 'Success')
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message)); 
        }
        else if(pageHeaderReferer != null && pageHeaderReferer.containsIgnoreCase('Event_Student') && message != 'null' && statusMessage == 'Failed')
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
        }
    }
    
    public PageReference createNewStudent()
    {
        PageReference openNewPage = new PageReference('/apex/Event_Student');
        String campaignID = '';
        String schoolID = '';
        String schoolName = '';
        try
        {
            campaignID = ApexPages.currentPage().getParameters().get('refcampaign');
            schoolID = ApexPages.currentPage().getParameters().get('schoolid');
            schoolName = ApexPages.currentPage().getParameters().get('schoolname');
            
            Lead createNewLead = new Lead();
            createNewLead.RecordTypeId = prospectiveRecordType;
            createNewLead.FirstName = firstName;
            createNewLead.LastName = lastName;
            createNewLead.Status = ENQUIRY_STATUS;
            createNewLead.Contact_Method__c = DEFAULT_CONTACT_METHOD;
            setSchoolOfLead(createNewLead, hasSchool, schoolID);
            createNewLead.Email = email;
            createNewLead.Company = firstName +'' + lastName + Label.ProspectiveStudent;
            createNewLead.MobilePhone = mobile;
            createNewLead.I_am_currently__c = this.newLead.I_am_currently__c;
            createNewLead.Interest_Area__c = interestArea;
            createNewLead.PostCode__c = this.newLead.PostCode__c;
            createNewLead.Residency_status__c = this.newLead.Residency_status__c;
            
            System.debug('++++ createNewLead ' + createNewLead);
            
            if (!Schema.sObjectType.Lead.isCreateable()){
            	throw new DMLException();
            }
            insert createNewLead;

			insertCampaignMember(createNewLead, campaignId);

            openNewPage.getParameters().put('message', 'Success! New student has been created.');
            
            openNewPage.getParameters().put('refcampaign', (campaignID!=null) ? campaignID.escapeHtml4(): null);
            
            openNewPage.getParameters().put('LeadId', createNewLead.Id);
            
            openNewPage.getParameters().put('schoolid', (schoolID!=null) ? schoolID.escapeHtml4(): null);
            openNewPage.getParameters().put('schoolname', (schoolName!=null) ? schoolName.escapeHtml4(): null);
            openNewPage.getParameters().put('status', 'Success');
            openNewPage.setRedirect(true);
            
            this.newLead = new Lead();
        }
        catch(System.Exception ex)
        {
         //   isError = true;
            openNewPage.getParameters().put('message', 'Failed! Cannot create a new student.');
            if(campaignID!=null){
            	openNewPage.getParameters().put('refcampaign', campaignID.escapeHtml4());
            }
            openNewPage.getParameters().put('status', 'Failed');
            openNewPage.setRedirect(true);
            System.debug('>>>>> Create New Student Error Message: '+ex.getMessage());
            return null;
            
        }
        
        return openNewPage;
    }

	private void insertCampaignMember(Lead createNewLead, String campaignId){
            if (!Schema.sObjectType.CampaignMember.isAccessible()){
            	throw new DMLException();
            }
            
            List<CampaignMember> mCampaignMemberExistingList = [SELECT Id, CampaignId, LeadId FROM CampaignMember WHERE
            LeadId =: createNewLead.Id and campaignId =: campaignID];
            
            if(mCampaignMemberExistingList.size() == 0)
            {
                CampaignMember mem = new CampaignMember (CampaignId = campaignID, LeadId = createNewLead.Id, Status = 'Sent'); //Status = 'Responded'
	            if (!Schema.sObjectType.CampaignMember.isCreateable()){
					throw new DMLException();
	            }
                insert mem;
                
                System.debug('+++ mem.Id ' + mem.Id);
            }
	}
	
	private void setSchoolOfLead(Lead createNewLead, Boolean hasSchool, String schoolID){
            if(hasSchool)
            {
                createNewLead.Secondary_School__c = (schoolID!=null) ? schoolID.escapeHtml4():null;
            }
            else
            {
                createNewLead.Un_Matched_School__c = (school!=null) ? school.escapeHtml4():null;
            }
	}
    
    public List<SelectOption> getIAmCurrently()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectIAmCurrentlySchema = Lead.I_am_currently__c.getDescribe();
        List<Schema.PicklistEntry> mSelectIAmCurrentlyValueLIST  = mSelectIAmCurrentlySchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectIAmCurrentlyValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getResidencyStatus()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mResidencyStatusSchema = Lead.Residency_status__c.getDescribe();
        List<Schema.PicklistEntry> mResidencyStatusValueLIST  = mResidencyStatusSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mResidencyStatusValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSelectAreaInterested()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectAreaInterestedSchema = Lead.Interest_Area__c.getDescribe();
        List<Schema.PicklistEntry> mSelectAreaInterestedValueLIST  = mSelectAreaInterestedSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectAreaInterestedValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
}