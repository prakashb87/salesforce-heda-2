/**
 * @description Methods to remove user in the Passport identity provider
 * @group Identity Management
 */
public with sharing class PassportIdentityProviderDeleteRequest implements IdentityProviderUserDeleteRequest
{

    public void deleteUser(String emailId){
        
        IpassDeleteAccountApiWrapper deleteAccWrap = new IpassDeleteAccountApiWrapper();
        String endpoint = ConfigDataMapController.getCustomSettingValue('IDAMIPaaSDeleteAccountEndPoint')+ '?email=' + emailId;          
        System.Debug('endpoint--'+endpoint);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientID')); 
        req.setHeader('client_secret',ConfigDataMapController.getCustomSettingValue('IDAMIpaaSClientSecret')); 
        req.setHeader('Content-Type','application/json');
        req.setMethod('DELETE');   
        
        Http http = new Http();
        HttpResponse response = http.send(req);          
        System.debug('response---------------'+response.getbody()); 
        
        String responseBody = response.getBody();
        // Due to inconsistent response payload structure, the below is required for JSON deserialization
        if (responseBody.contains('"payload": ""'))
        {
            responseBody = responseBody.replace('"payload": ""', '"payload": {"success":true, "message": "" }');
        }
        System.debug('User Deleted : Replaced Response=' + responseBody);
        
        deleteAccWrap = (IpassDeleteAccountApiWrapper) JSON.deserializeStrict(responseBody, IpassDeleteAccountApiWrapper.class);
        
        //Creating RMErrorLog incase iPaaS returns an error
        if(response.getStatusCode() != 200){
            throw new HttpCalloutException('Delete Request was not successful. Http Status Code: ' + response.getStatusCode());  
        }
        
    }
    
}