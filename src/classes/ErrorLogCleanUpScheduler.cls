/*****************************************************************************************************
 *** @Class             : ErrorLogCleanUpScheduler
 *** @Author            : Praneet Vig
 *** @Requirement       : scheduler to Clean Error Log records
 *** @Created date      : 07/06/2018 
 *** @JIRA ID           : ECB-3768
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to schedule batch to clean Error log records.
 *****************************************************************************************************/ 

//Schedule class to run ErrorogCleanUpBatch
 public without sharing class ErrorLogCleanUpScheduler Implements Schedulable{ 
 
	public void execute(SchedulableContext sc){
	    
	    ErrorLogCleanUpBatch bc= new ErrorLogCleanUpBatch();
	    ID batchprocessid = Database.executeBatch(bc);
	}
}