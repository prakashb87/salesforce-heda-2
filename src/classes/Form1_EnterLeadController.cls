@SuppressWarnings('PMD.TooManyFields,PMD.ExcessivePublicCount,PMD.VariableNamingConventions,PMD.MethodNamingConventions')
public without sharing class Form1_EnterLeadController 
{
    
    private Lead newLead;
    private static Id leadRecordType = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Business Lead').RecordTypeId;
    public String RMIT_Title        {get;set;}
    public String RMIT_Title_Other  {get;set;}
    public String Proposed_Title        {get;set;}
    public String Proposed_Title_Other        {get;set;}
    public String phone                         {get;set;}
    public String mobilePhone                   {get;set;}
    public String FirstName {get; set;}
    public String LastName {get; set;}
    public String Company {get; set;}
    public String Email {get; set;}
    public String LeadSource {get; set;}
    //public String Subject_Expert_Name {get; set;}
    public String Reason_for_not_Consulting_SE {get; set;}
    public String If_yes_please_provide_their_name_and_ti {get; set;}
    public String Reason_for_not_Contacting_HOS_Senior_Exe {get; set;}
    public String Proposed_Partner_Institution {get; set;}
    public String If_Yes_briefly_describe_relationship {get; set;}
    public String RMIT_Staff_Name {get; set;}
    public String RMIT_Staff_Position {get; set;}
    public String RMIT_Staff_Email {get; set;}
    public String RMIT_Staff_Phone {get; set;}
    public Date Proposed_Activity_Agreement_Start_Date {get; set;}
    public String Level_of_Urgency {get; set;}
    public String Rationale {get; set;}
    public String Expected_Deliverables {get; set;}
    public String AttachedFileName {get; set;}
    public blob AttachedFileBody {get; set;}
    //public String College {get; set;}
    //public String School {get; set;}
    public string Street {get; set;}
    public String State {get; set;}
    public String City {get; set;}
    public String Country {get; set;}
    public String Postal {get; set;}
    public String If_yes_please_provide_a_short_overview {get; set;}
    //private static final string LEAD_SOURCE = 'GPLR Form';
    public String Proposed_Partner_Contact_Position {get; set;}
    
    public Form1_EnterLeadController(ApexPages.StandardController stdController)
    {
        this.newLead = (Lead) stdController.getRecord();
        this.newLead.RecordTypeId = leadRecordType;
        //Iniate 
        FirstName = '';
        LastName =  '';
        Company = '';
        RMIT_Title = '';
        RMIT_Title_Other = '';
        Proposed_Title = '';
        Proposed_Title_Other = '';
        phone = '';
        mobilePhone = '';
        Email =  '';
        LeadSource = '';
        //Subject_Expert_Name = '';
        Reason_for_not_Consulting_SE = '';
        If_yes_please_provide_their_name_and_ti = '';
        Reason_for_not_Contacting_HOS_Senior_Exe = '';
        Proposed_Partner_Institution = '';
        If_Yes_briefly_describe_relationship = '';
        RMIT_Staff_Name = '';
        RMIT_Staff_Position = '';
        RMIT_Staff_Email = '';
        RMIT_Staff_Phone = '';
        Proposed_Activity_Agreement_Start_Date = null;
        Level_of_Urgency = '';
        Rationale = '';
        Expected_Deliverables = '';
        Street = '';
        State = '';
        City = '';
        Country = '';
        Postal = '';
        If_yes_please_provide_a_short_overview = '';
        Proposed_Partner_Contact_Position = '';
        
        String message = String.escapeSingleQuotes('' + ApexPages.CurrentPage().GetParameters().Get('message'));
        String Status_Message = String.escapeSingleQuotes('' + ApexPages.CurrentPage().GetParameters().Get('status'));
        String pageHeaderReferer = ApexPages.currentPage().getHeaders().get('Referer'); 
    }
    
    public PageReference createNewStudent()
    {
        PageReference OpenNewPage = new PageReference('/apex/GPLRThankPage');
        try
        {
            Group grpQueue;
            if (Schema.sObjectType.Group.isAccessible()) {
            	grpQueue = [select Id from Group where developerName = 'GDP_Queue' and Type = 'Queue'];
        	}
            Lead createNewLead=passFormValuesToLeadFieldsAsItIs(grpQueue);       
			populateValuedWithLogic(createNewLead);

//            

            OpenNewPage.getParameters().put('message', 'Success! New student has been created.');
            OpenNewPage.getParameters().put('status', 'Success');
            OpenNewPage.setRedirect(true);
            
            this.newLead = new Lead();
            
            /*
            if((AttachedFileBody!=null)&&(AttachedFileName!=null))
            {
                Attachment attachedFile = new Attachment();
                attachedFile.Body = AttachedFileBody;
                attachedFile.Name = AttachedFileName;
                attachedFile.ParentId = createNewLead.id;
                attachedFile.IsPrivate = false;
                insert attachedFile;
            }
	    */
	    if((AttachedFileBody!=null)&&(AttachedFileName!=null)){
                ContentVersion cv = new ContentVersion();
                cv.versionData = AttachedFileBody;
                cv.title = AttachedFileName;
                cv.pathOnClient ='/fakePath/'+AttachedFileName;
                if (Schema.sObjectType.ContentVersion.isCreateable()){
                	insert cv;
                }
                
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
                cdl.LinkedEntityId = createNewLead.Id;
                cdl.ShareType = 'V';
                if (Schema.sObjectType.ContentDocumentLink.isCreateable()){
	                insert cdl;
                }
            }
            return OpenNewPage;
        }
        catch(System.Exception ex)
        {
            OpenNewPage.getParameters().put('message', 'Failed! Cannot create a new student.');
            OpenNewPage.getParameters().put('status', 'Failed');
            OpenNewPage.setRedirect(true);
            
            System.debug('>>>>> Create New Student Error Message: '+ex.getMessage());
            return null;
        }
        
        
    }
    
	private Lead passFormValuesToLeadFieldsAsItIs(Group grpQueue){
            Lead createNewLead = new Lead(RecordTypeId = leadRecordType, FirstName = FirstName, LastName = LastName,
            Company = Proposed_Partner_Institution, LeadSource = 'GPLR Form', RMIT_Relationship_Alignment__c  = 'Global_Development_and_Portfolio',
            Email = Email, OwnerId = grpQueue.Id, Reason_for_not_Consulting_SE__c = Reason_for_not_Consulting_SE,
            If_yes_please_provide_their_name_and_ti__c = If_yes_please_provide_their_name_and_ti, Reason_for_not_Contacting_HOS_Senior_Exe__c = Reason_for_not_Contacting_HOS_Senior_Exe,
            If_Yes_briefly_describe_relationship__c = If_Yes_briefly_describe_relationship, RMIT_Staff_Name__c = RMIT_Staff_Name, RMIT_Staff_Position__c = RMIT_Staff_Position,
            RMIT_Staff_Email__c = RMIT_Staff_Email, RMIT_Staff_Phone__c = RMIT_Staff_Phone, Level_of_Urgency__c = Level_of_Urgency,
            Rationale__c = Rationale, Expected_Deliverables__c = Expected_Deliverables, Street = Street,
            State = State, City = City, Country = Country, Country__c = Country, PostalCode = Postal,
            If_yes_please_provide_a_short_overview__c = If_yes_please_provide_a_short_overview, Position__c = Proposed_Partner_Contact_Position, Phone = phone,
            MobilePhone = mobilePhone);
			return createNewLead;
	}

	private Lead populateValuedWithLogic(Lead createNewLead){
            if(RMIT_Title != 'Other'){
                createNewLead.RMIT_Staff_Title__c = RMIT_Title;
            }else{
                createNewLead.RMIT_Staff_Title__c = RMIT_Title_Other;
            }
            if(Proposed_Title !='Other'){
                createNewLead.Salutation = Proposed_Title;
            }else{
                createNewLead.Salutation = Proposed_Title_Other;
            }
            if(this.newLead.Subject_Expert_Name__c != null)
            {
                createNewLead.Subject_Expert_Name__c = this.newLead.Subject_Expert_Name__c;
            }
            
            if(this.newLead.Proposed_Activity_Agreement_Start_Date__c != null)
            {
                createNewLead.Proposed_Activity_Agreement_Start_Date__c = this.newLead.Proposed_Activity_Agreement_Start_Date__c;
            }
            
            Form1_EnterLeadHelper.setPicklistValuesIfNotNone(createNewLead, this.newLead);

            if(this.newLead.College2__c!='--None--')
            {
                createNewLead.College2__c = this.newLead.College2__c;
            }
            if(this.newLead.School__c!='--None--')
            {
                createNewLead.School__c = this.newLead.School__c;
            }
            
            if (Schema.sObjectType.Lead.isCreateable()){
            	insert createNewLead;
            }
			return createNewLead;
	}	

    public List<SelectOption> getCollege()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectCollageSchema = Lead.College2__c.getDescribe();
        List<Schema.PicklistEntry> mSelectCollageValueLIST  = mSelectCollageSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectCollageValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSchool()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectSchoolSchema = Lead.School__c.getDescribe();
        List<Schema.PicklistEntry> mSelectSchoolValueLIST  = mSelectSchoolSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectSchoolValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSubject_Expert_SE_Consulted()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectSubject_Expert_SE_ConsultedSchema = Lead.Subject_Expert_SE_Consulted__c.getDescribe();
        List<Schema.PicklistEntry> mSelectSubject_Expert_SE_ConsultedValueLIST  = mSelectSubject_Expert_SE_ConsultedSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectSubject_Expert_SE_ConsultedValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getHOS_Senior_Executive_informed()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectHOS_Senior_Executive_informedSchema = Lead.HOS_Senior_Executive_informed__c.getDescribe();
        List<Schema.PicklistEntry> mSelectHOS_Senior_Executive_informedValueLIST  = mSelectHOS_Senior_Executive_informedSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectHOS_Senior_Executive_informedValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getCurrent_Partner()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectCurrent_PartnerSchema = Lead.Current_Partner__c.getDescribe();
        List<Schema.PicklistEntry> mSelectCurrent_PartnerValueLIST  = mSelectCurrent_PartnerSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectCurrent_PartnerValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getRMIT_Staff_Title()
    {
            
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('--None--', '--None--'));
        options.add(new SelectOption('Mr.','Mr.'));
        options.add(new SelectOption('Ms.','Ms.'));
        options.add(new SelectOption('Mrs.','Mrs.'));
        options.add(new SelectOption('Dr.','Dr.'));
        options.add(new SelectOption('Prof.','Prof.'));
        options.add(new SelectOption('Assoc. Prof','Assoc. Prof'));
        options.add(new SelectOption('Other','Other'));
        return options;

    }
    
    public List<SelectOption> getProposed_Partner_Title()
    {
            
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('--None--', '--None--'));
        options.add(new SelectOption('Mr.','Mr.'));
        options.add(new SelectOption('Ms.','Ms.'));
        options.add(new SelectOption('Mrs.','Mrs.'));
        options.add(new SelectOption('Dr.','Dr.'));
        options.add(new SelectOption('Prof.','Prof.'));
        options.add(new SelectOption('Assoc. Prof','Assoc. Prof'));
        options.add(new SelectOption('Other','Other'));
        return options;

    }
    
    public List<SelectOption> getType_of_Planned_Activity()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectType_of_Planned_ActivitySchema = Lead.Type_of_Planned_Activity__c.getDescribe();
        List<Schema.PicklistEntry> mSelectType_of_Planned_ActivityValueLIST  = mSelectType_of_Planned_ActivitySchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectType_of_Planned_ActivityValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getHoS_Contacted()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectHoS_ContactedSchema = Lead.HoS_Contacted__c.getDescribe();
        List<Schema.PicklistEntry> mSelectHoS_ContactedValueLIST  = mSelectHoS_ContactedSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectHoS_ContactedValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSE_Supports_proposal()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectSE_Supports_proposalSchema = Lead.SE_Supports_proposal__c.getDescribe();
        List<Schema.PicklistEntry> mSelectSE_Supports_proposalValueLIST  = mSelectSE_Supports_proposalSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectSE_Supports_proposalValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getSubject_Expert_Name()
    {
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult mSelectSubject_Expert_NameSchema = Lead.Subject_Expert_Name__c.getDescribe();
        List<Schema.PicklistEntry> mSelectSubject_Expert_NameValueLIST  = mSelectSubject_Expert_NameSchema.getPicklistValues();   
        options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectSubject_Expert_NameValueLIST)
        {
           options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getCountryOption()
        {
                List<SelectOption> options = new List<SelectOption>();
                
                Schema.DescribeFieldResult mSelectCountrySchema = Lead.Country__c.getDescribe();
                List<Schema.PicklistEntry> mSelectCountryValueLIST  = mSelectCountrySchema.getPicklistValues();   
            options.add(new SelectOption('--None--', '--None--'));
        for( Schema.PicklistEntry mPicklistItem : mSelectCountryValueLIST)
            {
               options.add(new SelectOption(mPicklistItem.getLabel(), mPicklistItem.getValue()));
            }       
            return options;
        }
}