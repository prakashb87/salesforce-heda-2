/**
* @description This class provides the hashing functionality used to 
* verify payment details sent from OneStop Payment Gateway to Salesforce
* JIRA Reference        : ECB-4464 -Hashing the URL that is passed to OneStop
*/
public without sharing class OneStopPaymentGatewayHash {
    
    /**
    * @description This method compares the url string parameters and the expected hash value
    * @param urlStringParametersWithoutHash The url String parameters excluding the hash value
    * @param expectedHash The hash value to compare against
    * @return Returns true if the hash matches
    * @example
    * isHashVerified('Status=A&InvoiceNo=15874256&ReceiptNo=WB01412654&TotAmt=25.50&GUID=98765',
    * '0c12edc1fc3e2089464384805f880924cdffcc79c8351125e562d7d4f93fe806');
    */
    public boolean isHashVerified(String urlStringParametersWithoutHash, String expectedHash) {
        
        if (String.isBlank(urlStringParametersWithoutHash) || String.isBlank(expectedHash)) {
            throw new ArgumentException('URL String Parameter or Expected Hash is missing.');
        }
        String secret = ConfigDataMapController.getCustomSettingValue('HashingSecret');    // Store this in custom setting
        System.debug('>>> secret >> '+secret);
        String input = urlStringParametersWithoutHash + secret;
        Blob calculatedHash = Crypto.generateDigest(ConfigDataMapController.getCustomSettingValue('AlgorithmName'), Blob.valueOf(input));    // store the MD5 string in custom setting
        String result = EncodingUtil.convertToHex(calculatedHash);
        return expectedHash.equals(result);
    }
    
    /*Method to Check the Parameter and Create HashString */
    @AuraEnabled
    public static Boolean sendHashingDetails(String urlStringParametersWithoutHash, String expectedHash){
 
        Boolean result;
        OneStopPaymentGatewayHash onestop = new OneStopPaymentGatewayHash();
        result = onestop.isHashVerified(urlStringParametersWithoutHash,expectedHash);
        return result;
    }    
}