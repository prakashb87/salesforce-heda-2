/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 29/10/2018     ****************************/
/***   @JIRA ID: ECB-4585                       *********************************************************/
/***   This is the test class for PassportIdpUserVerificationReq              ****************************/
@isTest
public class PassportIdpUserVerificationReqTest
{
    @TestSetup
    static void makeData(){
        Config_Data_Map__c configDataMapObj4 = new Config_Data_Map__c();
        configDataMapObj4.Name = 'LogIntegrationRequestToIntegrationLog';
        configDataMapObj4.Config_Value__c = 'true';
        insert configDataMapObj4;

        Config_Data_Map__c configDataMapObj1 = new Config_Data_Map__c();
        configDataMapObj1.Name = 'IDAMIPaaSActivateEndPoint';
        configDataMapObj1.Config_Value__c =  'http://ecb-mock.getsandbox.com/api/users?email=test@test.com';
        insert configDataMapObj1;
        
        Config_Data_Map__c configDataMapObj2 = new Config_Data_Map__c();
        configDataMapObj2.Name = 'IDAMIpaaSClientID';
        configDataMapObj2.Config_Value__c = '123';
        insert configDataMapObj2;
        
        Config_Data_Map__c configDataMapObj3 = new Config_Data_Map__c();
        configDataMapObj3.Name = 'IDAMIpaaSClientSecret';
        configDataMapObj3.Config_Value__c = '123';
        insert configDataMapObj3;
    }

    @isTest
    static void blankEmailParameterTest() {
        PassportIdpUserVerificationReq req = new PassportIdpUserVerificationReq();
        try {
            req.updateUserToVerified('');
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        } catch(Exception e) {
            System.assert(false);
        }
        
    }

    static void nullEmailParameterTest() {
        PassportIdpUserVerificationReq req = new PassportIdpUserVerificationReq();
        try {
            req.updateUserToVerified(null);
            System.assert(false);
        } catch(ArgumentException e) {
            System.assert(true);
        } catch(Exception e) {
            System.assert(false);
        }
        
    }

    static testMethod void testActivateAccount() 
    {
        String json = '{ "id": "ID-dev-6r87t87587", "result": "success", "code": "200", "application": "rmit-system-api-idam", "provider": "idam", "payload": "" }';
        Test.setMock(HttpCalloutMock.class, new IDAMActivationSuccessMock());

        TestDataFactoryUtil.setupConfigDataforIDAMEmailCheckAndUserCreation( 'http://ecb-mock.getsandbox.com/api/users?email=test@test.com', '123','123');

        Test.startTest();
        PassportIdpUserVerificationReq req = new PassportIdpUserVerificationReq();
        req.updateUserToVerified('test@test.com');
        Test.stopTest();

        //verify outbound request record in SF
        list<Integration_Logs__c> dbResults = [select id, Service_Type__c, Type__c,Request_Response__c from Integration_Logs__c where Service_Type__c = 'Acknowledgement'];
        system.assertEquals(1, dbResults.size());
        System.debug('dbResults='+dbResults);

        system.assertEquals(json, dbResults[0].Request_Response__c);

    }

    @isTest
    static void testFailedResponse() {
        Test.setMock(HttpCalloutMock.class, new IDAMActivationFailureMock());
        Test.startTest();

        try {
            PassportIdpUserVerificationReq req = new PassportIdpUserVerificationReq();
            req.updateUserToVerified('someuserthatexists@test.com');
            System.assert(false);
        } catch(HttpCalloutException e) {
            System.assert(true);
        } catch(Exception e) {
            System.assert(false);
        }

        Test.stopTest();
    }

	private class IDAMActivationSuccessMock implements HttpCalloutMock 
	{
        // implement http mock callout
         public HTTPResponse respond(HTTPRequest request)
         {
            // Create a fake response
            String jsonBody = '{ "id": "ID-dev-6r87t87587", "result": "success", "code": "200", "application": "rmit-system-api-idam", "provider": "idam", "payload": "" }';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }


    private class IDAMActivationFailureMock implements HttpCalloutMock 
	{
        // implement http mock callout
         public HTTPResponse respond(HTTPRequest request)
         {
            // Create a fake response
            String jsonBody = '{ "id": "ID-dev-6r87t87587", "result": "success", "code": "200", "application": "rmit-system-api-idam", "provider": "idam", "payload": {"success":false, "message":"Match was found..."} }';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(200);
            return response;
        }
    }
}