public with sharing class ResearcherPortalOcmsSearchController {
    public class SearchResults {
        // public FilteringBundle filteringBundle;
        public OCMS_Search__c[] searchData;
    }

    @AuraEnabled public static String getResults(String term) {
        SearchResults results = new SearchResults();

        FilteringApiRequest req = new FilteringApiRequest();
        req.parameters = new Map<String, String> {
            'term' => term,
            'limit' => '200'
        };

        req.listParameters = new Map<String, String[]> {
            'contentTypesAndLayouts' => new String[] { 'ResearcherPortalFAQ', 'ResearcherPortalArticle' },
            'order' => new String[] { 'Relevance:DESC' }
        };

        req.requestFlags.put('targeted', true);

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
            'service' => 'FilteringApi',
            'action' => 'searchContent',
            'apiVersion' => '5.1',
            'filteringRequest' => Json.serialize(req)
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        FilteringBundle filteringBundle = (FilteringBundle)Json.deserialize(apiResponse.responseObject, FilteringBundle.class);
        results.searchData = [
                SELECT Content__c, Importance__c, Content_Type__c
                FROM OCMS_Search__c
                WHERE Content__c IN :filteringBundle.relevance.keySet()
                ORDER BY Importance__c
        ];

        return Json.serialize(results);
    }

    @AuraEnabled public static String[] getKeywords() {
        RenderingApiRequest apiRequest = new RenderingApiRequest();
        apiRequest.parameters = new Map<String, String> { 'renderType' => 'contentName' };
        apiRequest.listParameters = new Map<String, String[]> {
            'contentNames' => new String[] { 'Site Areas', 'Keywords' },
            'contentLayouts' => new String[] { 'TaxonomyMenu' }
        };

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
            'service' => 'OrchestraRenderingApi',
            'action' => 'getRenderedContent',
            'renderingRequest' => Json.serialize(apiRequest),
            'apiVersion' => '5.1'
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        RenderResultBundle renderResult = (RenderResultBundle)Json.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        String[] keywords = new String[] {};

        Pattern rootPattern = Pattern.compile('>([^<]+)');
        Pattern pathPattern = Pattern.compile('"[^"]*level-(\\d)-mapitem[^"]*"><span>([^<]+)');

        for (RenderResultBundle.RenderedContent content : renderResult.renderings) {
            String renderedHtml = content.renderMap.get('TaxonomyMenu');

            Matcher rootMatcher = rootPattern.matcher(renderedHtml);
            rootMatcher.find();

            Matcher pathMatcher = pathPattern.matcher(renderedHtml);
            String[] pathParts = new String[] { rootMatcher.group(1) };
            while (pathMatcher.find()) {
                Integer depth = Integer.valueOf(pathMatcher.group(1));
                while (depth < pathParts.size()) {
                    pathParts.remove(pathParts.size() - 1);
                }

                pathParts.add(pathMatcher.group(2));
                keywords.add('/' + String.join(pathParts, '/'));
            }
        }

        return keywords;
    }

    @AuraEnabled public static String getResultPage(Id[] originIds) {
        ContentBundle[] results = new ContentBundle[] {};

        RenderingApiRequest apiRequest = new RenderingApiRequest();
        apiRequest.parameters = new Map<String, String> { 'renderType' => 'originId' };
        apiRequest.listParameters = new Map<String, String[]> { 'originIds' => originIds };
        apiRequest.requestFlags = new Map<String, Boolean> {
            'noRendering' => true,
            'withContentBundle' => true
        };

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
            'service' => 'OrchestraRenderingApi',
            'action' => 'getRenderedContent',
            'renderingRequest' => Json.serialize(apiRequest),
            'apiVersion' => '5.1'
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        RenderResultBundle renderResult = (RenderResultBundle)Json.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        for (RenderResultBundle.RenderedContent content : renderResult.renderings) {
            results.add(content.contentBundle);
        }

        return Json.serialize(results);
    }
}