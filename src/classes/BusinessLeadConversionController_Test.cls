/**
* -------------------------------------------------------------------------------------------------+
* @description This class tests B2B lead conversion feature
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        0.1
* @created        2019-04-04
* @modified       -
* ------------------------------------------------------------------------------------------------+
*/
@isTest()
public class BusinessLeadConversionController_Test {
    private static final String CONTACTSTUDENT = 'Validation error on Contact: This contact has a student affiliation, therefore, the following fields cannot be amended; First Name, Last Name, Middle Name, Preferred Name, Gender, Mailing Address, Fax.';
    private static Id leadBusinessRecordTypeId= Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Business Lead').getRecordTypeId();
    private static Id affStudentRecordTypeId= Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId();
    private static Id leadActivatorRecordTypeId= Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Activator Registration').getRecordTypeId();
    @isTest
    static void testConvertBusinessLead(){
        
        
        Id accRecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
        Lead lRecord = new Lead(LastName = 'Fry', Company='Fry And Sons', RecordTypeId=leadBusinessRecordTypeId);
        insert lRecord;
        
        Account acc = new Account(Name='Test' , RecordTypeId=accRecordTypeId);
        insert acc;
        
        Account accAct = new Account(Name='Activator');
        insert accAct;
        
        Contact con = new Contact(LastName='Test', AccountId=acc.Id);
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Contact__c = con.Id;
        opp.Name = 'OppTest';
        opp.StageName = 'Close-won';
        opp.CloseDate = System.today() + 60;
        insert opp;
        
        Id accountId = acc.Id;
        Id contactId = con.Id;
        Id oppId = opp.Id;
        Id leadId = lRecord.Id;
        string idforConversion='';        
        
        //test scenario 1: New Lead and allow create opportunity       
        BusinessLeadConversionController.convertBusinessLead(lRecord);
        
        //test scenario 2: New Lead with Business Account     
        Lead lRecord2 = new Lead(LastName = 'Fry', Company='Fry And Sons', RecordTypeId=leadBusinessRecordTypeId, 
                                 Matched_Business_Account__c=accountId );
        insert lRecord2;	
        BusinessLeadConversionController.convertBusinessLead(lRecord2);
        
        //test scenario 3: New Lead with Selected Opportunity
        Lead lRecord3 = new Lead(LastName = 'Fry', Company='Fry And Sons', RecordTypeId=leadBusinessRecordTypeId, 
                                 Matched_Opportunity__c=oppId, Don_t_create_a_new_opportunity_upon_Lead__c=true);
        insert lRecord3;         
        BusinessLeadConversionController.convertBusinessLead(lRecord3);
        
        
        //test scenario 4: New Lead with Selected Contact
        Lead lRecord4 = new Lead(LastName = 'Fry', Company='Fry And Sons', RecordTypeId=leadBusinessRecordTypeId, 
                                 Matched_Contact__c=contactId, Opportunity_Name__c='Opportunity test');
        insert lRecord4;
        BusinessLeadConversionController.convertBusinessLead(lRecord4);
        
        
        
        Lead lRecord6 = new Lead(LastName = 'Fry', Company='Fry And Sons',Opportunity_Name__c='Test Opp',Don_t_create_a_new_opportunity_upon_Lead__c = true, RecordTypeId=leadBusinessRecordTypeId);
        insert lRecord6;
        try{
            BusinessLeadConversionController.convertBusinessLead(lRecord6);    
        }catch(Exception e){
            System.assertEquals(Label.Opportunity_error_for_conversion, e.getMessage(),'There was a different error');   
        }
        Group testGroup = new Group(Name='RMITO Queue', type='Queue');
        insert testGroup;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Lead');
            insert testQueue;
        }
        Lead lRecord7 = new Lead(LastName = 'Fry1', Company='Fry And Sons1',RecordTypeId=leadBusinessRecordTypeId, OwnerId = testGroup.Id);
        insert lRecord7;
        try{
            BusinessLeadConversionController.convertBusinessLead(lRecord7);    
        }catch(Exception e){
            System.assertEquals(Label.Owner_error_for_conversion, e.getMessage(),'There was a different error');   
        }        
    }
    
    /*@isTest
    static void activatorLeadTest(){
        // test scenario 5 Activator
        Account accAct1 = new Account(Name='Activator');
        insert accAct1;
        Contact lCon5 = new Contact(FirstName = 'Fry56',LastName='Vault76');
        insert lCon5;
        hed__Affiliation__c lAff5 = new hed__Affiliation__c(hed__Account__c = accAct1.Id, hed__Contact__c  = lCon5.Id, RecordTypeId = affStudentRecordTypeId );
        insert lAff5;
        Lead lRecord5 = new Lead(LastName = 'Fry', Company='Fry And Sons',Matched_Contact__c = lCon5.Id ,RecordTypeId=leadActivatorRecordTypeId);
        insert lRecord5;         
        BusinessLeadConversionController.convertBusinessLead(lRecord5);
        System.assert(lRecord5.isConverted,'Lead was not converted');
    }*/
    
    @isTest
    static void contactConversionErrorTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='RMIT Online User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com.rmdev2');
        
        System.runAs(u) {
            Contact lCon8 = new Contact(LastName = 'FryCon',System_Student__c=true);
            insert lCon8;
            hed__Affiliation__c lAff8 = new hed__Affiliation__c(hed__Contact__c = lCon8.Id,RecordTypeId = affStudentRecordTypeId);
            insert lAff8;
            Lead lRecord8 = new Lead(FirstName='FryCon ',LastName = 'Fry1', Company='Fry And Sons1',Matched_Contact__c = lCon8.Id,RecordTypeId=leadBusinessRecordTypeId);
            insert lRecord8;
            try{
                BusinessLeadConversionController.convertBusinessLead(lRecord8);    
            }catch(Exception e){
                List<String> errorMsg = CONTACTSTUDENT.split(':');
                System.assertEquals(errorMsg[1], e.getMessage(),'The error should be'+errorMsg[1]);   
            }
        }
        
        
    }
}