@isTest
private class InteractionServiceTest {
    @testSetup
    static void setupData() {
        //create interaction mapping
        list < Interaction_Mapping__c > intMapping = InteractionTestDataFactory.createInteractionmappingsTriggerHandlers();
        List<hed__Trigger_Handler__c> trgH= InteractionTestDataFactory.createTriggerHandlerData();
        insert trgH;
        //create accounts
        Id acadRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Institution').RecordTypeId;
        Id agentRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id secondRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        Id adminRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Account acc1 = new Account(Name = 'Academic Inst Test', RecordTypeId = acadRT);
        insert acc1;
        Account acc2 = new Account(Name = 'Agent Test', RecordTypeId = agentRT);
        insert acc2;
        Account acc3 = new Account(Name = 'Secondary Inst Test', RecordTypeId = secondRT);
        insert acc3;
        Account adminAcc= new Account (Name='Administrative Account', RecordTypeId=adminRT);
        insert adminAcc;
        //create contact
        Contact con = new Contact(AccountId=adminAcc.ID, LastName = 'asdfg', FirstName = 'qwerty', MobilePhone = '0408321123', hed__workEmail__c = 'asdfg.qwerty@email.com');
        insert con;
        // create affiliation
        Id studentrecodTypeID = Schema.SObjectType.hed__Affiliation__c.RecordTypeInfosByName.get('Prospect').RecordTypeId;
        hed__Affiliation__c aff = new hed__Affiliation__c(
            hed__Account__c = acc1.id,
            RecordTypeId = studentrecodTypeID,
            hed__Contact__c = con.id,
            hed__Role__c='Prospect');
        insert aff;
        //createOpportunity
         Id inqRT = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('Inquiry Opportunities').RecordTypeId;
            Opportunity opp= New Opportunity(Name='asdfg qwerty 2050', Admit_Term__c='2050', 
                                                Assign_To_Contact__c=con.id, 
                                                RecordTypeId=inqRT, 
                                                Segment__c='International',
                                                CloseDate=System.Today()+90,
                                                StageName='Confirmed Interest');
            insert opp;
        //createInteraction
        Interaction__c interaction = new Interaction__c(First_Name__c='qwerty', Last_Name__c='testdefault', 
                                                    Email__c='asdfg.qwerty@email.com',Admit_Term__c='2050',
                                                    Contact__c=con.id,
                                                    Education_Institution__c=acc1.Id);
        insert interaction;
        
        
    }

    static testMethod void testOpportunityAffiliationUpdate(){
        Profile profileGMSRId = [SELECT Id FROM Profile WHERE Name = 'RMIT GMSR User' LIMIT 1];
         User usr = new User(LastName = 'Test RMITGMSRUser',
                            FirstName='User',
                            Alias = 'asdfg',
                            Email = 'test.rmitGMSR@rmittest.com',
                            Username = 'test.rmitGMSR@rmittest.com.sandbox',
                            ProfileId = profileGMSRId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        System.runAs(usr){
            List<Interaction__c> intRec =[SELECT ID,Opportunity__c,Affiliation_Key__c FROM Interaction__c WHERE Last_name__c='testdefault'];
            InteractionService.updateOpportunityAffiliation(intRec); 
        }
        List<Interaction__c> intRec =[SELECT ID,Opportunity__c,Affiliation_Key__c FROM Interaction__c WHERE Last_name__c='testdefault'];
        List<Opportunity> opp=[SELECT Affiliation__c FROM Opportunity WHERE ID =:intRec[0].Opportunity__c];
        System.assert(opp[0].Affiliation__c!=null, 'Affiliation field must be populated');
    }
    static testMethod void testCampaignInteractionAssignment(){
        Profile profileGMSRId = [SELECT Id FROM Profile WHERE Name = 'RMIT GMSR User' LIMIT 1];
        Id prospectiveLeadRT = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
         User usr = new User(LastName = 'Test RMITGMSRUser',
                            FirstName='User',
                            Alias = 'asdfg',
                            Email = 'test.rmitGMSR@rmittest.com',
                            Username = 'test.rmitGMSR@rmittest.com.sandbox',
                            ProfileId = profileGMSRId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US',
                            UserPermissionsMarketingUser=true
                           );
        insert usr; 
        
        //insert Campaign
         Id campaignRT = Schema.SObjectType.Campaign.RecordTypeInfosByName.get('Sales and Conversion').RecordTypeId;
        Campaign campaign= new Campaign(Name='TestCampaign_TEST', RecordTypeId=campaignRT);
        insert campaign;
         Lead leadRec= new Lead(FirstName='TestDefault',
                                         LastName = 'DefaultSegment-XXX', 
                                         RecordTypeId = prospectiveLeadRT,
                                         Interest_Area__c = 'Law',
                                         Segment__c = 'International',
                                         Start_Semester__c = 'Semester 1, 2021',
                                         MobilePhone = '1234512345',
                                         Email =  'pdtest1@test.com',
                                         LeadSource = 'Phone',
                                         Company = 'Test Incorporated',
                                         Level_of_study__c = 'Bachelor degree');
            insert leadRec;
         
         CampaignMember cm= new CampaignMember(CampaignId=campaign.Id, leadId=leadRec.Id);
                insert cm;
               
            Interaction__c interaction = new Interaction__c(First_Name__c='zxcvbvbb', Last_Name__c='asdfg', 
                                                    Email__c='zxbbb@email.com',Admit_Term__c='2050',
                                                    Lead__c=leadRec.Id);
            insert interaction;

        List<Interaction__c> interactions=[SELECT Primary_Campaign__c FROM Interaction__c WHERE Id=:interaction.Id];
        System.assert(interactions[0].Primary_Campaign__c == campaign.Id, 'Primary campaign field must be populated by the campaign ID');
    }

}