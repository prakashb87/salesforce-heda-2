/*********************************************************************************
 *** @ClassName         : AccountService
 *** @Author            : Shubham Singh 
 *** @Requirement       : AccountService class for methods related to web service version 1.1
 *** @Created date      : 17/03/2019
 *** @Modified by       : Shubham Singh
 *** @modified date     : 01/04/2019   
 **********************************************************************************/
public with sharing class AccountService {
    //wrapper class to create collection of sets to store the respective values 
    public class AccountServiceWrapper {
        public List < Account > institution = new List < Account > ();
        public Boolean allExist = true;
        public Set < String > notFound = new Set < String > ();
        public List < Account > accounts = new List < Account > ();
        public Map < String, Id > institutionAccountNumberMap = new Map < String, Id > ();
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Getting institution record from account 
     * Note : As there will be only one instituion record W.R.T this
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    getInstitution
     * @param     Set < String > institution
     * @return    AccountServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static List < Account > getInstitution(Set < String > institution) {
        System.debug('institution ' + institution);
        List < Account > listInstitution = new List < Account > ();
        //Confirmed institution is always RMITU 
        if (Schema.sObjectType.Account.fields.Name.isAccessible()) {
            listInstitution = [SELECT Id, Name FROM Account WHERE name =: institution LIMIT 1];
        }
        System.debug('listInstitution ' + listInstitution);
        return listInstitution;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Getting institution record from account and create map of institution and account number
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    AccountServiceWrapper
     * @param     Set < String > academicProgram, Set < String > institution
     * @return    AccountServiceWrapper
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static AccountServiceWrapper getAccountMap(Set < String > academicProgram, Set < String > institution) {
        AccountServiceWrapper wrapper = new AccountServiceWrapper();

        system.debug('academicProgram ' + academicProgram);
        system.debug('institution ' + institution);
        //there will be only one institution
        wrapper.institution = getInstitution(institution);
        if (wrapper.institution.isEmpty()) {
            wrapper.allExist = false;
            wrapper.notFound = institution.clone();
        }
        List < Account > listAccount = new List < Account > ();
        listAccount = accountBasedOnAccountNumberAndInstitution(academicProgram);
        for (Account loopAccount: listAccount) {
            wrapper.institutionAccountNumberMap.put(String.ValueOf(loopAccount.AccountNumber) + loopAccount.Academic_Institution__r.Name, loopAccount.Id);
        }
        system.debug('wrapper== ' + wrapper);
        return wrapper;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Query account records based on accademic program and institution
     * As intituion is RMITU and it is same in previous logic
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    accountBasedOnAccountNumberAndInstitution
     * @param     Set < String > academicProgram
     * @return    List < Account >
     * @Reference RM-2580 
     * -----------------------------------------------------------------------------------------------+
     */
    public static List < Account > accountBasedOnAccountNumberAndInstitution(Set < String > academicProgram) {
        if (Schema.sObjectType.Account.fields.AccountNumber.isAccessible()) {
            return [SELECT Id, AccountNumber, Academic_Institution__r.Name, Academic_Institution__c, Academic_Career__c, Effective_Date__c FROM Account WHERE AccountNumber =: academicProgram AND Academic_Institution__r.Name = 'RMITU'
                And AccountNumber != null And Academic_Institution__c != null ORDER BY Effective_Date__c DESC
            ];
        }
        return null;
    }
}