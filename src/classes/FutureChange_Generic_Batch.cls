/*********************************************************************************
*** @ClassName         : FutureChange_Generic_Batch
*** @Author            : Anupam Singhal
*** @Requirement       : RM-1632
*** @Created date      : 21/09/2018
*** @Modified by       : Shubham Singh
*** @modified date     : 25/09/2018
**********************************************************************************/
public class FutureChange_Generic_Batch implements Database.Batchable<sObject>{
	public String queryString;
	public String objectName;
    
    FutureChange_Generic_Batch_Helper_New helperUpdateMethod = new FutureChange_Generic_Batch_Helper_New();
    public FutureChange_Generic_Batch(String QString, String objName){
       queryString = QString ;
       objectName = objName;
    }
	public Database.QueryLocator start(Database.BatchableContext BC1){
           
        return Database.getQueryLocator(queryString);
    }
    
    public void execute(Database.BatchableContext BC1,List <sObject> scope )
    {
        if(objectName.equalsIgnoreCase('Account') && scope != null && scope.size() > 0){
            //This is used to Update the Program,School and College records in Account
            helperUpdateMethod.updateObjectRecords((List <Future_Change__c>) scope,objectName);            
            
        }else if(objectName.equalsIgnoreCase('Campus__c') && scope != null && scope.size() > 0){
            //This is used to Update the Campus records
            helperUpdateMethod.updateObjectRecords((List <Campus_Future_Change__c>) scope,objectName);
        }else if(objectName.equalsIgnoreCase('hed__Course__c') && scope != null && scope.size() > 0){
            //This is used to Update the Course Offering records
            helperUpdateMethod.updateObjectRecords((List <Course_Offering_Future_Change__c>) scope,objectName);
        }else if(objectName.equalsIgnoreCase('Plan__c') && scope != null && scope.size() > 0){
            //This is used to Update the Plan records
            helperUpdateMethod.updateObjectRecords((List <Plan_Future_Change__c>) scope,objectName);
        }else if(objectName.equalsIgnoreCase('hed__Program_Enrollment__c') && scope != null && scope.size() > 0){
            //This is used to Update the Plan records
            helperUpdateMethod.updateObjectRecords((List <Plan_Future_Change__c>) scope,objectName);
        }
        
    }
    
    public void finish(Database.BatchableContext BC1){ 
    	 system.debug('FutureChange_Generic_Batch/finish');     
        //Nothing to be done once the batch has finished.
    } 
}