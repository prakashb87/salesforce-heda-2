/**
 * @description This class provides server side methods to verify the Google Captcha Response
 * The Google captcha type is: reCAPTCHA v2, Invisible reCAPTCHA 
 * @group Captcha
 */
public class GoogleCaptchaVerificationRequest implements CaptchaVerificationRequest {

    /**
     * @description Verifies with Google if the user has successfully completed the Captcha
     * @param response Response from the Captcha page, this is the 'g-recaptcha-response' value
     * @return True if the user has completed the Captcha successfully
     */
    public boolean isCaptchaVerified(String response) {
        if (String.isBlank(response)) {
            throw new ArgumentException('Response parameter cannot be blank');
        }
        String secret = ConfigDataMapController.getCustomSettingValue('GoogleCaptchaSecret');
        String endpoint = 'https://www.google.com/recaptcha/api/siteverify' + '?secret=' + secret + '&response=' + response;

        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Content-Length', '0');
        HTTPResponse httpResponse = http.send(req);
        string captchaResponseBody = httpResponse.getbody();
        system.debug('captchaResponseBody:::'+captchaResponseBody);
        // hack required due to Apex not supporting hypens in variable names
        captchaResponseBody = captchaResponseBody.replace('error-codes','errorCodes');  
        
        //to suppress PMD issues with the underscore since Google's API
        // returns a field with an underscore
        captchaResponseBody = captchaResponseBody.replace('challenge_ts','challengeTs');    

        CaptchaResponse res = (CaptchaResponse)JSON.deserialize(captchaResponseBody, CaptchaResponse.class);
        return res.success;
    }
    

    private class CaptchaResponse {
        public Boolean success;
        public String challengeTs;
        public String hostname;
        public List<String> errorCodes;
    }

}