/*****************************************************************
Name: CourseFullDataLoad
Author: Capgemini 
Purpose: Update MarketPlace with the full set of course and program data
Jira Reference : ECB-3247 - The Full load for the Event trigger not catered to
                 ECB-4430 - Implement Full program sync batch job in SF
*****************************************************************/
/*==================================================================
History
--------
Version   Author            Date              Detail
1.0       Shreya           29/05/2018         Initial Version
********************************************************************/
public without sharing class CourseFullDataLoad {

   /********************************************************************
    // Purpose              : Send full load course data to IPASS                             
    // Author               : Capgemini [Shreya]
    // Parameters           : null
    //  Returns             : void
    //JIRA Reference        : ECB-3247 - The Full load for the Event trigger not catered to
                              ECB-4430 - Implement Full program sync batch job in SF
    //********************************************************************/ 

    @future(callout=true)
    public static void loadCourseDetails() {
         // creating  Product_Catalog request log to Integration log object.
        Integration_Logs__c requestLogObjectToInsert = new Integration_Logs__c();
        Integration_Logs__c responseLogObjectToInsert = new Integration_Logs__c(); 
        List<hed__Course__c> courseList;
        
         // Quering all the courses
        if(Schema.sObjectType.hed__Course__c.isAccessible()){
            courseList = [SELECT hed__Course_ID__c, Name,Id FROM hed__Course__c WHERE (Product_Line__c = '21cc Credential' OR Product_Line__c ='RMIT Online' OR Product_Line__c ='RMIT Training')];
        }
        List<hed__Course_Offering__c> courseOfferingList;
        List<Course_Testimonial__c> courseTestimonialList;
        List<Course_Industry_Partner__c> coursePartnerList;
        List<Course_Offering_Session__c> courseOfferingSessionList;
        List<Course_Relationship__c> courseRelationshipList;
        Map<String,String> parentCourseMap= new Map<String,String>();
        
        Set<Id> courseIds = new Set<Id>();
        if(!courseList.isEmpty()){
            for(hed__Course__c course : courseList){
                courseIds.add(course.Id);
            }
               
            // Quering the related Course Offering records
            if(Schema.sObjectType.hed__Course_Offering__c.isAccessible()){
                courseOfferingList = [SELECT Id,Name, hed__Course__r.Id, hed__Course__r.Name FROM hed__Course_Offering__c WHERE hed__Course__c IN :courseIds];
            }
           // Quering the related Course Testimonial Records
           if(Schema.sObjectType.Course_Testimonial__c.isAccessible()){
               courseTestimonialList = [SELECT Id,Name, Course__r.Id, Course__r.Name FROM Course_Testimonial__c  WHERE Course__c IN :courseIds];
           }
            // Quering the related Course Industry Partner Records
            if(Schema.sObjectType.Course_Industry_Partner__c.isAccessible()){
                coursePartnerList =[SELECT Id,Name, Course_Id__r.Id, Course_Id__r.Name,Industry_Partner__c FROM Course_Industry_Partner__c  WHERE Course_Id__c IN :courseIds];
            }
             // Quering the related Course Offering Session Records
             if(Schema.sObjectType.Course_Offering_Session__c.isAccessible()){
                courseOfferingSessionList =  [SELECT Id,Course_Offering__c,Course_Offering__r.hed__Course__c,Course_Offering__r.hed__Course__r.Name FROM Course_Offering_Session__c WHERE Course_Offering__r.hed__Course__c IN :courseIds ];
             }
             
            //courseRelationshipList = [SELECT Related_Course__c,Related_Course__r.Name,Id FROM Course_Relationship__c WHERE Related_Course__c IN :courseIds];
            if(Schema.sObjectType.Course_Relationship__c.isAccessible()){
                courseRelationshipList = [SELECT id,Course__c,Course__r.Name FROM Course_Relationship__c WHERE Course__c != null AND Course__c IN :courseIds ];

                for( Course_Relationship__c courseRelObj : courseRelationshipList ) 
                {
                    parentCourseMap.put( String.valueOf(courseRelObj.Course__c), courseRelObj.Course__r.Name);
                }
            }   

        }
      
        
        Id academicProgramRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId(); 
        List<Account> programList;
        // Quering all the Program Records
        if(Schema.sObjectType.Account.isAccessible()){ 
            programList = [SELECT Id,Name FROM Account WHERE RecordTypeId = :academicProgramRecordTypeId AND (Product_Line__c = '21cc Credential' OR Product_Line__c ='RMIT Online' OR Product_Line__c ='RMIT Training')];
        }
        List<hed__Program_Plan__c> programPlanList;
        List<Program_Testimonial__c> programTestimonialList;
        List<Program_Industry_Partner__c> industryPlanList;
        List<hed__Plan_Requirement__c> planReqList;
        
        Set<Id> programIds = new Set<Id>();
        if(!programList.isEmpty()){
            for(Account prog : programList){
                programIds.add(prog.Id);
            }

            //Quering the related Program Plan Records
            if(Schema.sObjectType.hed__Program_Plan__c.isAccessible()){  
                programPlanList=  [SELECT Id,hed__Account__c,hed__Account__r.Name FROM hed__Program_Plan__c WHERE hed__Account__c IN :programIds];
            }
            //Quering the related Program Testimonial Records 
            if(Schema.sObjectType.Program_Testimonial__c.isAccessible()){
                programTestimonialList=  [SELECT Id,Program__c,Program__r.Name FROM Program_Testimonial__c WHERE Program__c IN :programIds];
            }
            //Quering the related Program Industry Partner Records
           if(Schema.sObjectType.Program_Industry_Partner__c.isAccessible()){ 
                industryPlanList=  [SELECT Id,Program__c,Program__r.Name FROM Program_Industry_Partner__c WHERE Program__c IN :programIds];
           }
           //Quering the related Program Requirement Records
           if(Schema.sObjectType.hed__Plan_Requirement__c.isAccessible()){
                planReqList = [SELECT Id,hed__Program_Plan__c,hed__Program_Plan__r.hed__Account__r.Name,hed__Program_Plan__r.hed__Account__c FROM hed__Plan_Requirement__c WHERE hed__Program_Plan__r.hed__Account__c IN :programIds];
           }
        }
        
        List<CourseWrapper> courseWrapperList = new List<CourseWrapper>();
        
        //Adding Courses to the CourseWrapperList 
        if(!courseList.isEmpty()){
            for(hed__Course__c course : courseList ){
                CourseWrapper courseObj = new CourseWrapper();
                courseObj.ObjectName = 'course';
                courseObj.key.put('courseId',course.Id);
                courseObj.key.put('courseName',course.Name);
                courseWrapperList.add(courseObj);
            }
        }   
       
        //Adding Course Offering to the CourseWrapperList 
        if(!courseOfferingList.isEmpty()){
            for(hed__Course_Offering__c cf : courseOfferingList){
                CourseWrapper cfObj = new CourseWrapper();
                cfObj.ObjectName = 'courseOffering';
                cfObj.key.put('courseId',cf.hed__Course__c);
                cfObj.key.put('courseName',cf.hed__Course__r.Name);
                cfObj.key.put('courseOfferingId',cf.Id);
                courseWrapperList.add(cfObj);
            }
        }   
           
        //Adding Course Testimonial to the CourseWrapperList 
        if(!courseTestimonialList.isEmpty()){
            for(Course_Testimonial__c ct : courseTestimonialList){
                CourseWrapper ctObj = new CourseWrapper();
                ctObj.ObjectName = 'courseTestimonial';
                ctObj.key.put('courseId',ct.Course__c);
                ctObj.key.put('courseName',ct.Course__r.Name);
                ctObj.key.put('testimonialId',ct.Id);
                courseWrapperList.add(ctObj);
            }
        }   
        
        //Adding Course Industry Partner to the CourseWrapper 
        if(!coursePartnerList.isEmpty()){
            for(Course_Industry_Partner__c cip : coursePartnerList){
                CourseWrapper cipObj = new CourseWrapper();
                cipObj.ObjectName = 'coursePartnerAccount';
                cipObj.key.put('courseId',cip.Course_Id__c);
                cipObj.key.put('courseName',cip.Course_Id__r.Name);
                cipObj.key.put('coursePartnerAccountId',cip.Industry_Partner__c);
                courseWrapperList.add(cipObj);
            }
        }  

        //Adding Program to the CourseWrapper 
        if(!programList.isEmpty()){
            for(Account prog : programList){
                CourseWrapper pObj = new CourseWrapper();
                pObj.ObjectName = 'program';
                pObj.key.put('programId',prog.Id);
                pObj.key.put('programName',prog.Name);
                courseWrapperList.add(pObj);
            }
        } 
        
        //Adding Program Plan to the CourseWrapper 
        if(!programPlanList.isEmpty()){
            for(hed__Program_Plan__c pplan : programPlanList){
                CourseWrapper pplanObj = new CourseWrapper();
                pplanObj.ObjectName = 'programPlan';
                pplanObj.key.put('programId',pplan.hed__Account__c);
                pplanObj.key.put('programName',pplan.hed__Account__r.Name);
                pplanObj.key.put('programPlanId',pplan.Id);
                courseWrapperList.add(pplanObj);
            }
        } 
        
         //Adding Plan Requirement to the CourseWrapper 
        if(!planReqList.isEmpty())
        {
            for(hed__Plan_Requirement__c pReq : planReqList)
            {
                CourseWrapper preqObj = new CourseWrapper();
                preqObj.ObjectName = 'planRequirement';
                preqObj.key.put('programId',pReq.hed__Program_Plan__r.hed__Account__c);
                preqObj.key.put('programName',pReq.hed__Program_Plan__r.hed__Account__r.Name);
                preqObj.key.put('programPlanId',pReq.hed__Program_Plan__c);
                preqObj.key.put('planRequirementId',pReq.Id);
                courseWrapperList.add(preqObj);
            }
        }
        
        //Adding Program Testimonial to the CourseWrapper 
        if(!programTestimonialList.isEmpty()){
            for(Program_Testimonial__c pTest : programTestimonialList){
                CourseWrapper pTestObj = new CourseWrapper();
                pTestObj.ObjectName = 'programTestimonial';
                pTestObj.key.put('programId',pTest.Program__c);
                pTestObj.key.put('programName',pTest.Program__r.Name);
                pTestObj.key.put('programTestimonialId',pTest.Id);
                courseWrapperList.add(pTestObj);
            }
        } 
        
        //Adding Program Industry Partner to the CourseWrapper 
        if(!industryPlanList.isEmpty()){
            for(Program_Industry_Partner__c pind : industryPlanList){
                CourseWrapper pindObj = new CourseWrapper();
                pindObj.ObjectName = 'programPartnerAccount';
                pindObj.key.put('programId',pind.Program__c);
                pindObj.key.put('programName',pind.Program__r.Name);
                pindObj.key.put('programPartnerAccountId',pind.Id);
                courseWrapperList.add(pindObj);
            }
        } 
        
        //Adding Course Offering Session to the CourseWrapper 
        if(!courseOfferingSessionList.isEmpty())
        {
            for(Course_Offering_Session__c cSession : courseOfferingSessionList)
            {
                CourseWrapper csesObj = new CourseWrapper();
                csesObj.ObjectName = 'courseOfferingSession';
                csesObj.key.put('courseId',cSession.Course_Offering__r.hed__Course__c);
                csesObj.key.put('courseName',cSession.Course_Offering__r.hed__Course__r.Name);
                csesObj.key.put('courseOfferingId',cSession.Course_Offering__c);
                csesObj.key.put('courseOfferingSessionId',cSession.Id);
                courseWrapperList.add(csesObj);
            }
        } 
        
        //Adding Related Courses to the CourseWrapper 
        /*if(!courseRelationshipList.isEmpty())
        {
            for(Course_Relationship__c crel : courseRelationshipList)
            {
                CourseWrapper crelObj = new CourseWrapper();
                crelObj.ObjectName = 'relatedCourses';
                crelObj.key.put('courseId',crel.Related_Course__c);
                crelObj.key.put('courseName',crel.Related_Course__r.Name);
                courseWrapperList.add(crelObj);
            }
        } */
        if( !parentCourseMap.IsEmpty())
        {
           for( String cID : parentCourseMap.keySet())
           {
                CourseWrapper crelObj = new CourseWrapper();
                crelObj.ObjectName = 'relatedCourses';
                crelObj.key.put('courseId', cID);
                crelObj.key.put('courseName',parentCourseMap.get(cID));
                courseWrapperList.add(crelObj);
            }
                    
        }
        
        system.debug('courseWrapperList:::::'+courseWrapperList.size());
        
        if(!courseWrapperList.isEmpty()){
            // HTTP Request Creation
            String bodyJSONString = JSON.serialize(courseWrapperList);
            System.debug('Serialized list of courses into JSON format: ' + bodyJSONString);
            String endpoint = ConfigDataMapController.getCustomSettingValue('IpassAPI'); 
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint);
            req.setHeader('client_id', ConfigDataMapController.getCustomSettingValue('IpassClientID')); 
            req.setHeader('client_secret',ConfigDataMapController.getCustomSettingValue('IpassCientSecret')); 
            req.setHeader('Content-Type','application/json');
            req.setMethod('POST');
            req.setbody(bodyJSONString);
            
            Http http = new Http();
            HTTPResponse response;
            try
            {
                //Product_Catalog request log to Integration log object.

                String bodyJSONStringNew = truncateIntegrationLogLength(bodyJSONString);

                requestLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog('Product_Catalog',bodyJSONStringNew, 'Outbound Service','',false);
                //creating Product_Catalog request log to Integration log object.
        
                response = http.send(req);

                String responseStringNew = truncateIntegrationLogLength( response.getBody() );
                
                //creating SAMS response log to Integration log object
                responseLogObjectToInsert = IntegrationLogWrapper.createIntegrationLog( 'Product_Catalog',responseStringNew, 'Acknowledgement','',false);
                //creating SAMS response log to Integration log object
            }
            catch(Exception ex)
            {
                system.debug('Exception:::'+ex);
            }    
            
            system.debug('response:::'+response);
            system.debug('Response Code ---------->'+response.getStatusCode()+'\nResponse Code ---------->'+response.getStatus());
            
            //Below code is inserting  log records on integration log object.
            
                    if( requestLogObjectToInsert != null)
                    {
                       insert requestLogObjectToInsert;        
                    }
                    if( responseLogObjectToInsert != null )
                    {
                        if (Schema.sObjectType.Integration_Logs__c.isCreateable()){ 
                            insert responseLogObjectToInsert;
                        }   
                    }
            // Below code is inserting  log records on integration log object.
            
            //Creating RMErrorLog incase iPaaS returns an error
            if(response.getStatusCode() != 200){
                RMErrLog__c errObj = new RMErrLog__c();
                errObj.Error_Cause__c = ConfigDataMapController.getCustomSettingValue('IpassErrorCause'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                errObj.Error_Message__c = ConfigDataMapController.getCustomSettingValue('FullLoadErrorMsg'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                errObj.Error_Type__c = ConfigDataMapController.getCustomSettingValue('IpassErrorType'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                errObj.Error_Payload__c = ConfigDataMapController.getCustomSettingValue('IpassErrorPayLoad'); //ECB-4308 - :Shreya -Replaced Custom Label with Custom Setting
                
                system.debug('errObj::::'+errObj);
                try
                {
                    if (Schema.sObjectType.RMErrLog__c.isCreateable()){ 
                        Insert errObj;
                    }   
                    
                }
                catch(DmlException ex)
                {
                    system.debug('Exception:::'+ex);
                }    
                
            }
        }
    }  
    public static string truncateIntegrationLogLength( String requestResponseString)
    {
        String bodyJSONStringNew = requestResponseString;
        Integer maxSize = 131072;
        if(bodyJSONStringNew.length() > maxSize )
        {
            bodyJSONStringNew = bodyJSONStringNew.substring(0, maxSize);
        }
        return bodyJSONStringNew;
    } 
}