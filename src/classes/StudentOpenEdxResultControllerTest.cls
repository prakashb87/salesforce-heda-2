@isTest
public class StudentOpenEdxResultControllerTest {
    
    @testSetup
    static void createTestData() {
        TestDataFactoryUtilRefOne.testOpenEdxResult();
        TestDataFactoryUtil.dummycustomsetting();
    }
    
    @isTest
    static void testupdateOpenEdxResult1(){
        
        hed__Course_Offering__c cofferrec = [SELECT Id,Name FROM hed__Course_Offering__c WHERE Name='test course offering 1'];
        system.assert(cofferrec!=null);
        
        User userrec = [SELECT Id,FederationIdentifier,ContactId FROM User WHERE Username ='communityuser@unittest.com'];
        system.assert(userrec.FederationIdentifier=='shr1234');
        
        hed__Course_Enrollment__c cenrollRec = [SELECT Id,hed__Status__c,hed__Grade__c,Certificate_URL__c,hed__Course_Offering__c,hed__Contact__c,Enrolment_Status__c
                              FROM hed__Course_Enrollment__c WHERE hed__Contact__c = :userrec.ContactId AND hed__Course_Offering__c = :cofferrec.Id 
                              AND hed__Status__c = 'Current' AND
                              Enrolment_Status__c= 'E'];
        system.debug('cenrollRec:::'+cenrollRec);
        system.assert(cenrollRec!=null);
        
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('userId', 'shr1234');
        obj.put('courseOfferingId', cofferrec.Name);
        obj.put('edxUserId', 'false');
        obj.put('grade', '0.3');
        obj.put('status', 'downloadable');
        obj.put('certUrl', 'www.test.com');
        obj.put('courseMode', 'false');
        obj.put('createdDate', 'false');
        obj.put('modifiedDate', 'false');
        String requestJsonString = JSON.serialize(obj);
        
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/student/openedx/result/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(requestJsonString);
        
        RestContext.request = request;
        Test.startTest();
            String responseStr = StudentOpenEdxResultController.updateOpenEdxResult();
            system.assert(responseStr!=null);
        Test.stopTest();    
        
        
    }
    
    @isTest
    static void testupdateOpenEdxResult2(){
        
        hed__Course_Offering__c cofferrec = [SELECT Id,Name FROM hed__Course_Offering__c WHERE Name='test course offering 1'];
        system.assert(cofferrec!=null);
        
        User userrec = [SELECT Id,FederationIdentifier,ContactId FROM User WHERE Username ='communityuser@unittest.com'];
        system.assert(userrec.FederationIdentifier=='shr1234');
                
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('userId', 'shr1234');
        obj.put('courseOfferingId', cofferrec.Name);
        obj.put('edxUserId', 'false');
        obj.put('grade', '0.3');
        obj.put('status', 'notpassing');
        obj.put('certUrl', 'www.test.com');
        obj.put('courseMode', 'false');
        obj.put('createdDate', 'false');
        obj.put('modifiedDate', 'false');
        String requestJsonString = JSON.serialize(obj);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://rmitheda--ecbdev2.cs31.my.salesforce.com/services/apexrest/student/openedx/result/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(requestJsonString);
        
        RestContext.request = request;
        Test.startTest();
            String responseStr = StudentOpenEdxResultController.updateOpenEdxResult();
            system.assert(responseStr!=null);
        Test.stopTest();    
        
        
    }
    
     @isTest
    static void testupdateOpenEdxResult3(){
        
        hed__Course_Offering__c cofferrec = [SELECT Id,Name FROM hed__Course_Offering__c WHERE Name='test course offering 1'];
        system.assert(cofferrec!=null);
        
        User userrec = [SELECT Id,FederationIdentifier,ContactId FROM User WHERE Username ='communityuser@unittest.com'];
        system.assert(userrec.FederationIdentifier=='shr1234');
        
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('userId', null);
        obj.put('courseOfferingId', null);
        obj.put('edxUserId', 'false');
        obj.put('grade', '0.3');
        obj.put('status', 'notpassing');
        obj.put('certUrl', 'www.test.com');
        obj.put('courseMode', 'false');
        obj.put('createdDate', 'false');
        obj.put('modifiedDate', 'false');
        String requestJsonString = JSON.serialize(obj);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://rmitheda--ecbdev2.cs31.my.salesforce.com/services/apexrest/student/openedx/result/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(requestJsonString);
        
        RestContext.request = request;
        Test.startTest();
            String responseStr = StudentOpenEdxResultController.updateOpenEdxResult();
            system.assert(responseStr!=null);
        Test.stopTest();    
        
        
    }
    
     @isTest
    static void testupdateOpenEdxResult4(){
        
        hed__Course_Offering__c cofferrec = [SELECT Id,Name FROM hed__Course_Offering__c WHERE Name='test course offering 1'];
        system.assert(cofferrec!=null);
        
        User userrec = [SELECT Id,FederationIdentifier,ContactId FROM User WHERE Username ='communityuser@unittest.com'];
        system.assert(userrec.FederationIdentifier=='shr1234');
                
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('userId', 'shr1234');
        obj.put('courseOfferingId', cofferrec.Name);
        obj.put('edxUserId', 'false');
        obj.put('grade', '');
        obj.put('status', 'notpassing');
        obj.put('certUrl', 'www.test.com');
        obj.put('courseMode', 'false');
        obj.put('createdDate', 'false');
        obj.put('modifiedDate', 'false');
        String requestJsonString = JSON.serialize(obj);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://rmitheda--ecbdev2.cs31.my.salesforce.com/services/apexrest/student/openedx/result/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(requestJsonString);
        
        RestContext.request = request;
        Test.startTest();
            String responseStr = StudentOpenEdxResultController.updateOpenEdxResult();
            system.assert(responseStr!=null);
        Test.stopTest();     
        
        
    }
    
    @isTest
    static void testupdateOpenEdxResult5(){
        
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('userId', 'shr1234');
        obj.put('courseOfferingId', '');
        obj.put('edxUserId', 'false');
        obj.put('grade', '');
        obj.put('status', 'notpassing');
        obj.put('certUrl', 'www.test.com');
        obj.put('courseMode', 'false');
        obj.put('createdDate', 'false');
        obj.put('modifiedDate', 'false');
        String requestJsonString = JSON.serialize(obj);
        
        RestRequest request = new RestRequest();
        request.requestUri ='https://rmitheda--ecbdev2.cs31.my.salesforce.com/services/apexrest/student/openedx/result/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(requestJsonString);
        
        RestContext.request = request;
        Test.startTest();
            String responseStr = StudentOpenEdxResultController.updateOpenEdxResult();
            system.assert(responseStr!=null);
        Test.stopTest();     
        
        
    }
}