public without sharing class ResearcherPortalSupportCaseHelper {
    public static final string NONE_VALUE ='--None--';
    public static final string KEY1 ='queueId';
    public static final string KEY2 ='routedEmailAddress';
    
    /*@Author : Gourav Bhardwaj
      @Story : RPORW-1423
      @Description : Hepler Method to create Support case records
    */
     public static Id createNewSupportCaseRecord(ResearcherPortalSupportCaseWrapper caseWrapper){    
        
        System.debug('@@@caseWrapper'+caseWrapper);
        // Declaration and initialisation Block  
        String recordtype='';
        Case theCase; 
        
        //get current user contact
        Id conId =[SELECT contactId FROM User WHERE Id =:userInfo.getUserId() LIMIT 1].contactId;
              
        // Recordtype selection as per records from frontEnd and Set in record   
        if(!String.isEmpty(caseWrapper.recordType) && caseWrapper.recordType.equalsIgnoreCase(label.RMITSupportRequestCase))
        {
            //Fetch queue name and routed email from TopicSubtopicQueueMapping__mdt by filtering topic and subtopic from frontend
            Map<String,String> getQueueAndEmailMap = ApexWithoutSharingUtils.getQueueIdAndEmailAddress(caseWrapper.researcherCaseTopic,caseWrapper.researcherCaseSubTopic,label.RMITSupportRequestCase);
            recordtype =Schema.SObjectType.Case.getRecordTypeInfosByName().get(label.RMITSupportRequestCase).getRecordTypeId();
            theCase = new Case(
                subject=caseWrapper.subject,
                description=caseWrapper.description,
                status=caseWrapper.status,
                researcherCaseTopic__c= caseWrapper.researcherCaseTopic ,
                researcherCaseSubTopic__c= caseWrapper.researcherCaseSubTopic,
                researcherCaseRoutedToEmail__c= getQueueAndEmailMap.get(KEY2),
                Is_the_query_related_to_a_project__c = caseWrapper.projectQuery,//RPORW-1426
                Researcher_Location__c = caseWrapper.researcherLocation,//RPORW-1426
                contactId=ResearcherUserContactMapHelper.getContactId(userinfo.getUserId()),
                recordTypeId= recordtype,
                OwnerId=  getQueueAndEmailMap.get(KEY1)  
            );  
        }
        
        System.debug('@@@theCase'+theCase);
        // Insert the new case records from front end.  
        if(theCase != null)
        {
            //if (conId == null){
            	theCase.IsResearcherSupportEmailToBeSent__c=true;
                System.debug('Error: Insufficient Access Case');
            //return null;
            //}
            System.debug('@@@ before the Case'+theCase);
            insert theCase;
            System.debug('@@@ after the Case'+theCase);           
        } 
        //if(!theCase.IsResearcherSupportEmailToBeSent__c){
       	//	createManualShareCaseForNonCommunityUser(theCase.Id);
        //}
        
        //Calling the Util method to create Case Junction Objects
        if(caseWrapper.caseJunction!=null){
            ResearcherPortalSupportCaseHelperUtil.createJunctionObjects(caseWrapper,thecase);
        }
        
        system.debug('### case insert completed'+thecase);
        
        return theCase.Id;  
    }
  
    
    
    
    
    @TestVisible
    private static void createManualShareCaseForNonCommunityUser(Id caseid){
	   
	    //To enable the send email create the case share record for the creator of the case only if user do not have access to the record.
       	 if( userInfo.getUserType()=='Standard' && Schema.sObjectType.case.isCreateable() && Schema.sObjectType.caseShare.isCreateable()){
    	 	System.debug('Accessible Case Object');  
    	 	 //Create the share record for current user
	        caseShare caseManualShare= new caseShare();
	        caseManualShare.caseId=caseid;
	        caseManualShare.UserORGroupID = userinfo.getUserid();
	        caseManualShare.CaseAccessLevel = 'Edit';
	        caseManualShare.RowCause = 'Manual';
	        	Insert caseManualShare; 
		 	system.debug('CREATEABLE Case Object'); 
    	  	 
    	 }  
	   
    	         
        	 
    }
    
	
}