/*****************************************************************************************************
*** @Class             : AffiliationRecCreationHelperTest
*** @Author            : Avinash Machineni
*** @Requirement       : when a course connection is created, link them to affiliation records 
*** @Created date      : 12/06/2018
*** @JIRA ID           : ECB-3748

*****************************************************************************************************/
/*****************************************************************************************************
*** @About Class
*** This class is a test class to check if salesforce sends course ID to IPASS API.Its called from apex schduler class scheduledCourse
*****************************************************************************************************/ 
@isTest
public class AffiliationRecCreationHelperTest {
    Static testmethod void testAffiliation(){
        TestDataFactoryUtil.test(); 
        
        Set<String> courseType = new Set<String>();
         courseType.add('21CC');
         courseType.add('RMIT Online');
         courseType.add('RMIT Training');
        
        Test.startTest();
        list<contact> conlist = [SELECT id, Student_ID__c FROM CONTACT WHERE Account.Name =: '21CC'];
        list<string> studentIds = new list<string>();
        for(Contact con :conlist){
            studentIds.add(con.Student_ID__c);    
        }
        
        AffiliationRecCreationHelper.createaffilaitions(studentIds,courseType );
        System.assert([SELECT Student_id__c FROM contact].size() > 0);
        Test.stopTest();
    }
     Static testmethod void testAffiliation1(){
        TestDataFactoryUtil.test1();    
         
         Set<String> courseType = new Set<String>();
         courseType.add('21CC');
         courseType.add('RMIT Online');
         courseType.add('RMIT Training');
        
        Test.startTest();
        list<contact> conlist = [SELECT id, Student_ID__c FROM CONTACT WHERE Account.Name =: '21CC'];
        list<string> studentIds = new list<string>();
        for(Contact con :conlist){
            studentIds.add(con.Student_ID__c);    
        }
        
        AffiliationRecCreationHelper.createaffilaitions(studentIds,courseType);
        System.assert([SELECT Student_id__c FROM contact].size() > 0);
        Test.stopTest();
    }
    Static testmethod void testRecordTypeId(){
    TestDataFactoryUtil.test1();    
    
    Account acc = new Account(Name = '21CC');
        insert acc;
        List<Contact> contacts = new List<Contact>();
        Contact con1 = new Contact(FirstName = '21CC Test Unique First name', LastName = '21CC Test Unique last name', AccountId = acc.id, Student_ID__c = '19463');
        contacts.add(con1);
        Contact con2 = new Contact(FirstName = '21CC Test Unique Contact FN', LastName = '21CC Test Unique Contact LN', AccountId = acc.id, Student_ID__c = '19663');
        contacts.add(con2);
        insert contacts;        
        
        user testusr1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community Login User - RMIT'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = contacts[0].id);
        insert testusr1; 
        
         
         Set<String> courseType = new Set<String>();
         courseType.add('21CC');
         courseType.add('RMIT Online');
         courseType.add('RMIT Training');
        
        Test.startTest();
        list<contact> conlist = [SELECT id, Student_ID__c FROM CONTACT WHERE Account.Name =: '21CC'];
        list<string> studentIds = new list<string>();
        for(Contact con :conlist){
            studentIds.add(con.Student_ID__c);    
        }
        System.runas(testusr1){
            AffiliationRecCreationHelper.createaffilaitions(studentIds,courseType);
        }
        System.assertequals(Schema.SObjectType.hed__Affiliation__c.getRecordTypeInfosByName().get('Student').getRecordTypeId(),[Select recordtypeid from hed__Affiliation__c limit 1 ].recordtypeid);         
        Test.stopTest();
      }
   
}