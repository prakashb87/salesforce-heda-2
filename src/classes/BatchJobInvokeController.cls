public without sharing class BatchJobInvokeController {
    
    //Invoking Delta Load 
    /*
    @AuraEnabled
    public static Boolean deltaLoad() {
        CourseDeltaDataLoad.sendCourseAsync();
        return true;
    }
    
    //Delta Load Completed date
    
    @AuraEnabled
    public static String deltaLoadDt() {
        String cmpdate;
        list<AsyncApexJob> asyncDeltaJob =  [SELECT CompletedDate FROM AsyncApexJob where Apexclass.name = 'CourseDeltaDataLoad' and Status = 'Completed' order by CompletedDate Desc limit 1];
        if(asyncDeltaJob.size() > 0){
            cmpdate =(asyncDeltaJob[0].CompletedDate).format('h:mm a DD/MM/YYYY');        
        }
        return cmpdate;
    
    
    //Delta Load button disable time
    @AuraEnabled
    public static Boolean deltaLoadDisablt() {
        List<AsyncApexJob> asyncDeltaJob =  [SELECT CompletedDate FROM AsyncApexJob 
                                             where Apexclass.name = 'CourseDeltaDataLoad' 
                                             and Status = 'Completed' 
                                             order by CompletedDate Desc limit 1];
         
        String timeDuration = Config_Data_Map__c.getInstance('DisabledDuration_TimeAttribute').Config_Value__c;
        Integer timeDurationI = Integer.ValueOf(timeDuration);
        Boolean timeInADay = buttonEnablT();
       
       if(asyncDeltaJob.size() > 0){
       if ((asyncDeltaJob[0].CompletedDate.addMinutes(timeDurationI) < system.now() ) && (timeInADay == true)){
        	return false;
        } else {
        	return true;
        }
        }
        else {
       return false;
        }
    }
    
        //Delta Load button disable time
    @AuraEnabled
    public static Boolean pricApiLoadDisablt() {
        List<AsyncApexJob> asyncDeltaJob =  [SELECT CompletedDate FROM AsyncApexJob 
                                             where Apexclass.name = 'ProductCatalogRefreshBatchApi' 
                                             and Status = 'Completed' 
                                             order by CompletedDate Desc limit 1];
        
        String timeDuration = Config_Data_Map__c.getInstance('DisabledDuration_TimeAttribute').Config_Value__c;
        Integer timeDurationI = Integer.ValueOf(timeDuration);
        Boolean timeInADay = buttonEnablT();
        
        if (asyncDeltaJob.size() > 0){
        if ((asyncDeltaJob[0].CompletedDate.addMinutes(timeDurationI) < system.now() ) &&(timeInADay == true)){
        	return false;
        } else {
        	return true;
        }
        }
        else
            return false;
    }*/
    
        //Comm Product Load button disable time
    @AuraEnabled
    public static Boolean commLoadDisablt() {
        List<AsyncApexJob> asyncDeltaJob;
        //if(Schema.sObjectType.AsyncApexJob.fields.CompletedDate.isAccessible()){ 
            asyncDeltaJob =  [SELECT CompletedDate FROM AsyncApexJob 
                                             where Apexclass.name in('HEDA2CSBatch','HEDAProgram2CSBatch') 
                                             and Status = 'Completed' 
                                             order by CompletedDate Desc limit 1];
        //}
        String timeDuration = Config_Data_Map__c.getInstance('DisabledDuration_TimeAttribute').Config_Value__c;
        Integer timeDurationI = Integer.ValueOf(timeDuration);
        Boolean timeInADay = buttonEnablT();
        
      if (asyncDeltaJob.size() > 0){
        if ((asyncDeltaJob[0].CompletedDate.addMinutes(timeDurationI) < system.now() ) && (timeInADay == true)){
        	return false;
        } else {
        	return true;
        }
         }
        else
        {
            return false;
        }
    }
    
    //Delta Load job status 
     
    //Commercial Prod completed date 
    @AuraEnabled
    public static String commProdDt() {
        String cmpdate;
        //datetime cmpdate;
        list<AsyncApexJob> asynCommProd;
            asynCommProd = [SELECT CompletedDate FROM AsyncApexJob where Apexclass.name in('HEDA2CSBatch','HEDAProgram2CSBatch') and Status = 'Completed' order by CompletedDate Desc limit 1];
          
        if(asynCommProd.size() > 0){
           // cmpdate = (asynCommProd[0].CompletedDate).format('h:mm a DD/MM/YYYY');
           //cmpdate = (asynCommProd[0].CompletedDate).format('YYYY-MM-DD');
            cmpdate = (asynCommProd[0].CompletedDate).hour() + ':' + (asynCommProd[0].CompletedDate).minute() + ' ' + (asynCommProd[0].CompletedDate).day() + '/' + (asynCommProd[0].CompletedDate).month() + '/' + (asynCommProd[0].CompletedDate).year();
           // cmpdate = asynCommProd[0].CompletedDate;
          //  system.debug('cmpdate: '+ cmpdate);
        }          
        return cmpdate;
    }
    //Commercial Prod job status
    @AuraEnabled
    public static String commProdStatus() {
        String commProdStatus; 
        list<AsyncApexJob> asynCommStatus;
        //if(Schema.sObjectType.AsyncApexJob.fields.status.isAccessible()){ 
            asynCommStatus = [SELECT status FROM AsyncApexJob where Apexclass.name in('HEDA2CSBatch','HEDAProgram2CSBatch')  order by CreatedDate Desc limit 1];        
        //}
        if(asynCommStatus.size() > 0){ 
            commProdStatus = asynCommStatus[0].status;      
        }
        return commProdStatus;
    }
    
    //Pricing API Completed date
    /*@AuraEnabled
    public static String pricingApiDt() {
        String cmpdate;
        list<AsyncApexJob> asynPricingdate= [SELECT CompletedDate FROM AsyncApexJob where Apexclass.name = 'ProductCatalogRefreshBatchApi' and Status = 'Completed' order by CompletedDate Desc limit 1];                      
        
        if(asynPricingdate.size() > 0){
            cmpdate = (asynPricingdate[0].CompletedDate).format('h:mm a DD/MM/YYYY');
        }         
        return cmpdate;
    }
    //Pricing API Completed Status Check
    @AuraEnabled
    public static String pricingLoadStatus() {
        String pricingApiStatus;
        list<AsyncApexJob> asynPricingStatus = [SELECT status FROM AsyncApexJob where Apexclass.name = 'ProductCatalogRefreshBatchApi'  order by CreatedDate Desc limit 1];        
        if(asynPricingStatus.size() > 0){ 
            pricingApiStatus = asynPricingStatus[0].status;      
        }
        return pricingApiStatus;
    }
    */
    
    //Invoking Commercial product sync
    @AuraEnabled
    public static boolean commPrdSync() {
        boolean cp1 = commPrdSync1();
        boolean cp2 = commPrdSync2();
        return true;
    }
    //Invoking Commercial product sync
    @AuraEnabled
    public static boolean commPrdSync1() {
        HEDA2CSBatch batchJob = new HEDA2CSBatch();
        Id batchId = Database.executeBatch(batchJob, 30);       
        return true; 
    }
    
    //Invoking Commercial product sync
    @AuraEnabled
    public static boolean commPrdSync2() {
        HEDAProgram2CSBatch batchJob2 = new HEDAProgram2CSBatch();
        Id batchId2 = Database.executeBatch(batchJob2, 30);           
        return true; 
        
    }
    
    //Invoking Pricing API
    /*@AuraEnabled
    public static boolean pricingAPI() {
        
        ProductCatalogRefreshBatchApi productCatalogbatch = new ProductCatalogRefreshBatchApi();    
        Database.executeBatch(productCatalogbatch);  
        
        return true;
    }   */
      
    //Button disable time 
    @AuraEnabled 
    public static Boolean buttonEnablT() {
        String enableTimeStart = Config_Data_Map__c.getInstance('EnabledDuration_TimeStart').Config_Value__c;
        Integer enableTimeStartI = Integer.ValueOf(enableTimeStart);
        
        String enableTimeEnd = Config_Data_Map__c.getInstance('EnabledDuration_TimeEnd').Config_Value__c;
        Integer enableTimeEndI = Integer.ValueOf(enableTimeEnd);
        
        String hourNow = string.valueOf(system.now().hour());
        Integer hourNowI = Integer.ValueOf(hourNow);
        
        if ((hourNowI >= enableTimeStartI ) && (hourNowI <= enableTimeEndI )) {
            system.debug('Inside IF hourNowI: '+hourNowI+' enableTimeStartI: '+enableTimeStartI+' enableTimeEndI: '+enableTimeEndI);
        	return true; //Button enabled
        } else {
            system.debug('Inside ELSE hourNowI: '+hourNowI+' enableTimeStartI: '+enableTimeStartI+' enableTimeEndI: '+enableTimeEndI);
        	return false;  //Button disabled
        }
    }
    
}