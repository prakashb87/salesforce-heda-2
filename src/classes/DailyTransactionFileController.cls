/*****************************************************************
Name: DailyTransactionFileController
Author: Capgemini 
Purpose: To get the daily transaction file and store it in file library SAP Interface
JIRA Reference : ECB-4356 :  Daily transaction text file consumable by SAP integration
                 
*****************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            16/08/2018         Initial Version
********************************************************************/
public without sharing class DailyTransactionFileController{

    public static void getDailyTransactionFile(){
        
       
        PageReference tansactionFile = Page.DailyTransactionFile;
        Blob body;
        
        //getting the dynamic content of text file
        try {
            body = tansactionFile.getContent();
        } catch (VisualforceException e) {
            body = Blob.valueOf('No Pdf');
        }

        system.debug('body:::' + body);
        
        //IG ecb 4743 20.11.
        String monthVal = (System.TODAY()).month() + 1 > 10 ? String.valueOf((System.TODAY()).month()) : '0'+(System.TODAY()).month() ;
        String dayVal = (System.TODAY()).day() + 1 > 10 ? String.valueOf((System.TODAY()).day()) : '0'+(System.TODAY()).day() ;
        String fileName = 'MKTP_SAP_'+(System.TODAY()).YEAR()+monthVal+dayVal+'.txt';


        //Querying the Sap Interface Content Workspace 
        ContentWorkspace workspaceRec = queryContentWorkspace();
        
        //Creating Content Version
        ContentVersion conVer = createContentVersion(fileName,body);
        
        //Querying the above inserted Content Version to get the ContentDocumentId       
        ContentVersion contentVerRec = queryContentVersion(conVer.Id);
        
        
        ContentWorkspaceDoc wdoc = new ContentWorkspaceDoc();
        wdoc.ContentDocumentId = contentVerRec.ContentDocumentId;
        wdoc.ContentWorkspaceId = workspaceRec.Id;
        
        try{
            if(Schema.sObjectType.ContentWorkspaceDoc.isCreateable()){
                Insert wdoc;
            }
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        }
        system.debug(wdoc.Id);
        
        
    }
    
      /********************************************************************
    // Purpose              : Function to query Content Workspace that has Name Sap Interface                             
    // Author               : Capgemini [Shreya]
    // Parameters           : null
    //  Returns             : ContentWorkspace 

    //********************************************************************/
    public static ContentWorkspace queryContentWorkspace(){
        ContentWorkspace workspaceRec;
        try{
            if(Schema.sObjectType.ContentWorkspace.isAccessible()){
                workspaceRec = [Select Id,Name FROM ContentWorkspace WHERE Name = :ConfigDataMapController.getCustomSettingValue('FileLibraryName') ];
            }   
        }catch(QueryException ex){
                system.debug('Exception:::'+ex);
        }
        
        return workspaceRec;
    }
    
    /********************************************************************
    // Purpose              : Function to create Content Version                           
    // Author               : Capgemini [Shreya]
    // Parameters           : String fileName,Blob body
    //  Returns             : ContentVersion 

    //********************************************************************/
    
    public static ContentVersion createContentVersion(String fileName,Blob body){
        
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVer.PathOnClient = fileName ; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = fileName ; // Display name of the files
        conVer.VersionData = body; // converting your binary string to Blog
        //conver.PublishStatus= 'P';
        try{
            if(Schema.sObjectType.ContentVersion.isCreateable()){
                insert conVer;
            }   
        }catch(Exception ex){
            system.debug('Exception:::::'+ex);
        } 
        
        return conVer;
    }
    
      /********************************************************************
    // Purpose              : Function to query Content Version to get the ContentDocumentId                          
    // Author               : Capgemini [Shreya]
    // Parameters           : Id converId
    //  Returns             : ContentVersion 

    //********************************************************************/
    
    public static ContentVersion queryContentVersion(Id converId){
        ContentVersion contentVerRec;
        try{
            if(Schema.sObjectType.ContentVersion.isAccessible()){
                contentVerRec = [SELECT ContentDocumentId from ContentVersion where Id = :converId];
            }
            system.debug('contentVerRec :::'+contentVerRec.ContentDocumentId );
        }catch(Exception ex){
            system.debug('Exception:::'+ex);
        } 

        return contentVerRec;
    }
}