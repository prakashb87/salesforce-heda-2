/**
* -------------------------------------------------------------------------------------------------+
* Generic class to control the behaviour of custom lightning actions and buttons.
* --------------------------------------------------------------------------------------------------
* @author         Anupam Singhal
* @version        1.0
* @created        2018-03-14
* @modified       2018-11-14
* @modified by    Binish Kumar
* -------------------------------------------------------------------------------------------------+
*/
public without sharing class GenericActionController {
	private static Id campaignEventRecordType = Schema.SObjectType.Campaign.RecordTypeInfosByName.get('Marketing Event Campaigns').RecordTypeId;//Need to Verify
    private static final String UNQUALIFIED = 'Unqualified';
    private static final String QUALIFIED = 'Qualified';
    private static final String SUCCESS = 'Success!';
    private static final String FAILED = 'Failed!';
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to insert a campaign record
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    createCampaign
    * @param     NAME    Id of the Current LeadRecord
    * @return    TYPE    true or false
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String createCampaign(String id)
    {
        try
        {
            List<Lead> leadList = new List<Lead>([
                SELECT  ID, 
                        Name,
                        Company,
                        Description,
                        Email,
                        Secondary_School__c,
                        Event_address_If_different__c,
                        Event_date__c,
                        Event_time__c,
                        Event_Start_Date__c,
                        Event_End_Date__c,
                        Event_type__c,
                        Career_Advisors__c,
                        International_students__c,
                        Non_school_leavers__c,
                        Parents__c,
                        People_from_industry__c,
                        Postgraduate_students__c,
                        Year_9_students__c,
                        Year_10_students__c,
                        Year_11_students__c,
                        Year_12_students__c
                FROM Lead 
                WHERE Id =: Id]);
            
            List<Campaign> campaignList = new List<Campaign>();
            for(Lead mLead : leadList)
            {
                Campaign mCampaign = new Campaign();
                //Campaign Event Detail
                mCampaign.Name = mLead.Name;
                mCampaign.Type = mLead.Event_type__c;
                if(mLead.Secondary_School__c != NULL)
                {
                    mCampaign.School__c = mLead.Secondary_School__c;
                }                
                mCampaign.RecordTypeId = campaignEventRecordType;
                mCampaign.Enquiries__c = mLead.Id;
                mCampaign.Description = mLead.Description;
                mCampaign.Event_address_If_different__c = mLead.Event_address_If_different__c;
                mCampaign.Event_date__c = mLead.Event_date__c;
                mCampaign.Event_time__c = mLead.Event_time__c;
                mCampaign.Event_Start_Time__c = mLead.Event_Start_Date__c;
                mCampaign.Event_End_Time__c = mLead.Event_End_Date__c;
                //Student Detail
                mCampaign.International_students__c = mLead.International_students__c;
                mCampaign.Non_school_leavers__c = mLead.Non_school_leavers__c;
                mCampaign.Parents__c = mLead.Parents__c;
                mCampaign.People_from_industry__c = mLead.People_from_industry__c;
                mCampaign.Postgraduate_students__c = mLead.Postgraduate_students__c;
                mCampaign.Career_Advisors__c = mLead.Career_Advisors__c;
                mCampaign.Year_9_students__c = mLead.Year_9_students__c;
                mCampaign.Year_10_students__c = mLead.Year_10_students__c;
                mCampaign.Year_11_students__c = mLead.Year_11_students__c;
                mCampaign.Year_12_students__c = mLead.Year_12_students__c;
                campaignList.add(mCampaign);
            }
            insert campaignList;
        	return 'true';    
        }catch (System.Exception ex){
            //Error Logging Framework required
            System.debug('>>>>> Create a Campaign error message: '+ex.getMessage());
            return 'false';
        }
        
    }
    //---------------------------------------------------------------------------------------------------------------------------------------//
    //
     /**
    * -----------------------------------------------------------------------------------------------+
    * Method to insert a campaign record
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    eventConversionUnqualified
    * @param     NAME    Id of the Current LeadRecord
    * @return    TYPE    true or false
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String eventConversionUnqualified(String id)
    {
        String returnMessage = '';
        try
        {
            String insertSuccess = insertContact(id, UNQUALIFIED);
            if(insertSuccess == '')
            {
                returnMessage = FAILED;                    
            }
            else
            {
                returnMessage = insertSuccess;
            }                 
        }
        catch(System.Exception ex)
        {
            System.debug('>>>>> Enquiry Event Conversion Error Message: '+ex.getMessage());
        }
        return returnMessage;
    }
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to insert a campaign record
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    eventConversionUnqualified
    * @param     NAME    Id of the Current LeadRecord
    * @return    TYPE    true or false
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String eventConversionQualified(String id)
    {
        String returnMessage = '';
        try
        {
            String insertSuccess = insertContact(id, QUALIFIED);
            
            if(insertSuccess == '')
            {
                returnMessage = FAILED;                    
            }
            else
            {
                returnMessage = insertSuccess;
            }         
        }
        catch(System.Exception ex)
        {
            System.debug('>>>>> Enquiry Event Conversion Error Message: '+ex.getMessage());
        }
        return returnMessage;
    }
    
    public static String insertContact(String id, String isQualified)
    {
        String returnMessage = '';
        List<Lead> updateLeadUnqualified = new List<Lead>();
        List<Contact> newContactList = new List<Contact>();
        Map<Id, Account> accountMap = new Map<Id, Account>();
        Set<Id> accIDSet = new Set<Id>();
        Contact mNewContact;
        
        List<Lead> newLeadIncomingList = [SELECT Id, Status, Company, Event_type__c, FirstName, LastName, 
                                          Secondary_School__c, MobilePhone, Phone, Email, matched_school__c, Event_date__c, Event_time__c, 
                                          Event_duration__c, Event_Start_Date__c, Event_End_Date__c, Event_address_If_different__c,
                                          Year_9_students__c, Year_12_students__c, Year_11_students__c, Year_10_students__c,
                                          Postgraduate_students__c, International_students__c, Non_school_leavers__c, People_from_industry__c,
                                          Parents__c, Career_Advisors__c, Additional_Information__c,ConvertedContactId
                                          FROM Lead
                                          WHERE Id =: Id
                                         ];
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        for(Lead mLead : newLeadIncomingList)
        {
            if(mLead.Status != isQualified && mLead.matched_school__c != NULL)
            {   
                //Update Lead Status
                mLead.Status = isQualified;
                updateLeadUnqualified.add(mLead);
                
                //Populate contact information
               // mNewContact = new Contact();
                //mNewContact.FirstName = mLead.FirstName;
                //mNewContact.LastName = mLead.LastName;
                //mNewContact.AccountId = mLead.matched_school__c;
                //mNewContact.MobilePhone = mLead.MobilePhone;
                //mNewContact.Phone = mLead.Phone;
                //mNewContact.Email = mLead.Email;
                //mNewContact.Trigger_Break__c = true;
                //newContactList.add(mNewContact);
                Database.LeadConvert lc = new database.LeadConvert();
                
                for(Lead mSchoolLead : newLeadIncomingList)
                {
                    lc.setLeadId(mLead.id);
                    //lc.setContactId(accIdFetch.Id);
                    //lc.setAccountId(accIdFetch.AccountId);
                    lc.setDoNotCreateOpportunity(true);
                    lc.setOwnerId(UserInfo.getUserId());
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    Database.LeadConvertResult lcr = Database.convertLead(lc);
                    accIDSet.add(mSchoolLead.matched_school__c);
                }
            }
            else if(mLead.Status == isQualified)
            {
                returnMessage = 'The record status is '+isQualified+' already.';
            }
        }
        //Database.insert(newContactList);
		//System.debug('NewInserted Contact MEI:------->'+ newContactList);
        //Contact accIdFetch = [SELECT Id,AccountId FROM Contact WHERE Id=: newContactList[0].Id];
       
        
        for(Account mAcc : [SELECT Id, Name FROM Account WHERE Id IN: accIDSet])
        {
            accountMap.put(mAcc.Id, mAcc);
        }
        System.debug('accountMap ---------->'+accountMap);
        Lead convertedLead = [SELECT id,ConvertedContactId,ConvertedAccountId FROM Lead WHERE id =: Id];
        //System.debug('>>>>> convertedLead: '+convertedLead);
        List<Contact> convertedContact = [SELECT id,FirstName,LastName,AccountId,MobilePhone,Phone,Email FROM Contact WHERE id =: convertedLead.ConvertedContactId];
        //System.debug('>>>>> convertedContact: '+convertedContact);
        
        
        List<sObject> createNewEventAndCampaign = new List<sObject>();
        List<Event> createNewEventList = new List<Event>();
        List<Campaign> createNewCampaignList = new List<Campaign>();
        if(isQualified == QUALIFIED && newLeadIncomingList.size() > 0 && convertedContact.size() > 0)
        {
            //Event naming convention is = School name - event type - date (dd/mm/yyyy).  
            //Create a new Event
            //Related to Account and Contact
            try
            {
                for(Lead mLead : newLeadIncomingList)
                {
                    for(Contact mContact : convertedContact)
                    {
                        Event mEvent = new Event();
                        DateTime startDate;
                        DateTime endDate;
                        /*Reason for Commenting??
                            if(mLead.Event_Start_Date__c != NULL && mLead.Event_End_Date__c != NULL)
                            {
                                startDate = mLead.Event_Start_Date__c;
                                endDate = mLead.Event_End_Date__c;
                            }
						*/
                        if(mLead.Event_date__c != NULL)
                        { 
                            Date mEventDate = mLead.Event_date__c;
                            Datetime mEventDateTime = datetime.newInstance(mEventDate.year(), mEventDate.month(),mEventDate.day());
                            startDate = mEventDateTime;
                            endDate = startDate.addHours(1);
                        }
                        else
                        {
                            startDate = DateTime.now();
                            endDate = DateTime.now().addHours(1);
                        }
                        System.debug('startDate------>+'+ startDate);
                        String mSubject = accountMap.get(mLead.matched_school__c).Name+' - '+mLead.Event_type__c+' - '+startDate.format('dd/MM/yyyy');
                        mEvent.Subject = mSubject;
                        System.debug('mSubject---------->'+mSubject);
                        mEvent.StartDateTime = startDate;
                        mEvent.EndDateTime = endDate;
                        mEvent.WhatId = convertedContact[0].AccountId;
                        mEvent.WhoId = convertedContact[0].Id;
                        if(Schema.sObjectType.Event.fields.StartDateTime.isCreateable()){
                        createNewEventList.add(mEvent);
                        }
                        
                        
                        Campaign mCampaign = new Campaign();
                        //Campaign Event Detail
                        mCampaign.Name = accountMap.get(mLead.matched_school__c).Name+' - '+mLead.Event_type__c;
                        mCampaign.Type = mLead.Event_type__c;
                        mCampaign.Additional_Information__c = mLead.Additional_Information__c;
                        mCampaign.School__c = mLead.matched_school__c;
                        mCampaign.School_Contact__c = convertedContact[0].Id;
                        mCampaign.RecordTypeId = campaignEventRecordType;
                        //mCampaign.Enquiries__c = mLead.Id;
                        mCampaign.Description = mLead.Description;
                        mCampaign.Event_address_If_different__c = mLead.Event_address_If_different__c;
                        mCampaign.Event_date__c = mLead.Event_date__c;
                        mCampaign.Event_Time__c = mLead.Event_time__c;
                        mCampaign.Event_duration__c = mLead.Event_duration__c;
                        mCampaign.Event_Start_Time__c = mLead.Event_Start_Date__c;
                        mCampaign.Event_End_Time__c = mLead.Event_End_Date__c;
                        mCampaign.Year_9_students__c =  mLead.Year_9_students__c;
                        mCampaign.Year_12_students__c =  mLead.Year_12_students__c;
                        mCampaign.Year_11_students__c =  mLead.Year_11_students__c;
                        mCampaign.Year_10_students__c = mLead.Year_10_students__c;
                        mCampaign.Postgraduate_students__c =  mLead.Postgraduate_students__c;
                        mCampaign.International_students__c =  mLead.International_students__c;
                        mCampaign.Non_school_leavers__c =  mLead.Non_school_leavers__c;
                        mCampaign.People_from_industry__c = mLead.People_from_industry__c;
                        mCampaign.Parents__c =  mLead.Parents__c;
                        mCampaign.Career_Advisors__c = mLead.Career_Advisors__c;
                        System.debug('.....mLead.Additional_Information__c' + mLead.Additional_Information__c);
                        System.debug('.....mCampaign ' + mCampaign );
                         if(Schema.sObjectType.Campaign.fields.Name.isCreateable()){
                        createNewCampaignList.add(mCampaign);
                    }
                        
                    }
                }
                if(!createNewEventList.isEmpty()){
               		insert createNewEventList;
				}
                   
                if(!createNewCampaignList.isEmpty()){
               		insert createNewCampaignList;
				}
                    
            }
            catch(System.Exception ex)
            {
                returnMessage = 'Cannot create event and campaign.';
                System.debug('>>>>> Enquiry Event Conversion Error Message: '+ex.getMessage());
            }
        }
        System.debug('returnMessage ------->'+returnMessage);
        if(convertedContact.size() > 0 && !returnMessage.contains('The record') && !returnMessage.contains('Cannot'))
        //if(!returnMessage.contains('The record') && !returnMessage.contains('Cannot'))
        {
            returnMessage = SUCCESS;
        }
        if(createNewCampaignList.size() > 0)
        {
            returnMessage = createNewCampaignList[0].Id;
        }
        
        return returnMessage;
        //return accIdFetch.Id;
    }
        /**
    * -----------------------------------------------------------------------------------------------+
    * Method to insert a Lead record
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    saveLead
    * @param     NAME    Lead record, Account
    * @return    TYPE    true or false
    * @CreatedBy 		 Shubham SIngh
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String saveLead(Lead newLead, Account school,Id recordId) {
        try {
            newLead.RecordTypeId = Schema.SObjectType.Lead.RecordTypeInfosByName.get('Prospective Student').RecordTypeId;
            newLead.Status = Label.LeadStatus;
            newLead.Company = Label.New_Account;
            newLead.Contact_Method__c = Label.Contact_Method;
            if (school != null) {
                newLead.Secondary_School__c = school.Id;
            }
            newLead.OwnerId = userinfo.getUserId();
            insert newLead;
            CampaignMember mem = new CampaignMember ();
            mem.CampaignId = recordId;
            mem.LeadId = newLead.Id;
            mem.Status = Label.Sent;
            insert mem;
            return 'true';
        } catch (System.Exception ex) {
            //Error Logging Framework required
            System.debug('>>>>> Create a Campaign error message: ' + ex.getMessage());
            return 'false';
        }
    }
	 /**
    * -----------------------------------------------------------------------------------------------+
    * Method to get the Account value
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    saveLead
    * @param     NAME    Id of the record
    * @return    TYPE    true or false
    * @CreatedBy 		 Shubham SIngh
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static Account createLead(Id recrdId) {
        try {
            String school;
            Campaign cmpgn = [Select id, School__c from campaign where Id =: recrdId];
            if (cmpgn.School__c != null) {
                school = cmpgn.School__c;
            } else {
                school = null;
            }
            Account acc;
            if (school != null) {
                acc = [select name from account where id =: school];
            } else {
                acc = null;
            }
            return acc;
        }catch (System.Exception ex) {
            //Error Logging Framework required
            System.debug('>>>>> Create a Campaign error message: ' + ex.getMessage());
            return null;
        }
    }
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to get the Account Ids based on the Search text
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    fetchAccount
    * @param     NAME    String of Search Text from Lightning component
    * @return    TYPE    List<Account>
    * @CreatedBy 		 Anupam Singhal
    * -----------------------------------------------------------------------------------------------+
    */
    
    @AuraEnabled
    public static List < Account > fetchAccount(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List < Account > returnList = new List < Account > ();
        List < Account > lstOfAccount = [select id, Name from account where Name LIKE: searchKey];
        
        for (Account acc: lstOfAccount) {
            returnList.add(acc);
        }
        return returnList;
    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to update the Account Related list based on selected accounts in Campaign
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    updateAccount
    * @param     NAME    List<String>-Selected Ids , String Campaign Id
    * @return    TYPE    String
    * @CreatedBy 		 Anupam Singhal
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String updateAccount(List <String> selectedIds,String campaignId)
    {	
        String successValue;
        List <Account> wrap = [SELECT Campaign__c FROM Account WHERE Id IN :selectedIds];
        try
        {
            if(selectedIds.size() > 0)
            {
                for(Account acc: wrap)
                {
                    acc.Campaign__c = campaignId;
                }
                Database.SaveResult[] srList = Database.update(wrap);

                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                        successValue='true';
                    }
                    else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                            successValue='false';
                        }
                    }
                }
            }
        }
        catch(System.DmlException dmlEx)
        {
            System.debug('>>>>> DML Exception message: '+dmlEx.getMessage());
            successValue='false';
        }
        return successValue;
    }
	/*-----------------------------Lookup controlling methods Start----------------------------------------------*/
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to query records using SOSL
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    search
    * @param     NAME    String objectAPIName , String searchText
    * @return    TYPE    String
    * @CreatedBy 		 Anupam Singhal
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String search(String objectAPIName, String searchText,
            List<String> whereClause, List<String> extrafields){

        objectAPIName = String.escapeSingleQuotes(objectAPIName);
        searchText = String.escapeSingleQuotes(searchText);
        String searchQuery = 'FIND \'' + searchText + '*\' IN ALL FIELDS RETURNING ' + objectAPIName + '(Id,Name' ;
        if(!extrafields.isEmpty()){
            searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
        }
        system.debug(whereClause);
        if(!whereClause.isEmpty()){
            searchQuery = searchQuery + ' WHERE ' ;
            searchQuery = searchQuery + String.join(whereClause, 'AND') ;
        }
        searchQuery = searchQuery + ' LIMIT 10 ) ';
        system.debug(searchQuery);
        return JSON.serializePretty(search.query(searchQuery)) ;
    }

    /* Method to query records using SOQL*/
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to query records using SOQL
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    getRecentlyViewed
    * @param     NAME    String objectAPIName , List<String> whereClause, List<String> extrafields
    * @return    TYPE    List
    * @CreatedBy 		 Anupam Singhal
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static List<SObject> getRecentlyViewed(String objectAPIName,List<String> whereClause,List<String> extrafields){
        String searchQuery = 'SELECT Id, Name';
        if(!extrafields.isEmpty()){
            searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
        }
        searchQuery = searchQuery + ' FROM ' + objectAPIName + ' WHERE LastViewedDate != NULL ';
        if(!whereClause.isEmpty()){
            searchQuery = searchQuery + ' AND ' ;
            searchQuery = searchQuery + String.join(whereClause, 'AND') ;
            system.debug(searchQuery);
        }
        searchQuery = searchQuery + ' ORDER BY LastViewedDate DESC' ;
        List<SObject> objectList =  new List<SObject>();
        system.debug('searchQuery-->'+ searchQuery);
        objectList = Database.query(String.escapeSingleQuotes(searchQuery.replace('\\\'','\'')));
        return objectList;
    }
    /*-----------------------------Lookup controlling methods End----------------------------------------------*/
    /**
    * -----------------------------------------------------------------------------------------------+
    * Method to insert Case(Request)
    * ------------------------------------------------------------------------------------------------
    * @method    NAME    insertCase
    * @param     NAME    Case Objcase , String conId, String leadId
    * @return    TYPE    String
    * @CreatedBy 		 Anupam Singhal
    * -----------------------------------------------------------------------------------------------+
    */
    @AuraEnabled
    public static String insertCase(Case objCase,String conId,String leadId){
        ObjCase.ContactId = conId;
        ObjCase.RecordTypeId = System.Label.Case_Student_RecordTypeId;
        ObjCase.Lead__c= leadId;
		String retVal;
        try{
        	Database.SaveResult sr = Database.insert(ObjCase);
            if (sr.isSuccess()) {
                System.debug('Successfully inserted Case. Case ID: ' + sr.getId());
                retVal='true';
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Case fields that affected this error: ' + err.getFields());
                    retVal='false';
                }
            }
        }catch(Exception e){
            System.debug('>>>>> DML Exception message: '+e.getMessage());
            retVal = 'false';
        }
        return retVal;
    }
}