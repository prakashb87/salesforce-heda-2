/*********************************************************************************
 *** @TestClassName     : AccountTriggerHandler_Test
 *** @Author		    : Anupam Singhal
 *** @Requirement     	: To Test the AccountTrigger and AccountTriggerHandler
 *** @Created date    	: 05/09/2018
 **********************************************************************************/
@isTest
public class AccountTriggerHandler_Test {
    @isTest
    public static void trigger_TestCase(){
        
        Account account_1 = new Account();
        account_1.Name = 'Test 1';
        account_1.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Business Organization').RecordTypeId;
        insert account_1;
        
        Account account_2 = new Account();
        account_2.Name = 'Test 2';
        account_2.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Academic Program').RecordTypeId;
        insert account_2;
        
        Account account_3 = new Account();
        account_3.Name = 'Test 3';
        account_3.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Activator Start Up').RecordTypeId;
        insert account_3;
        
        Account account_4 = new Account();
        account_4.Name = 'Test 4';
        account_4.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Business Account').RecordTypeId;
        insert account_4;
        
        Account account_5 = new Account();
        account_5.Name = 'Test 5';
        account_5.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Educational Institution').RecordTypeId;
        insert account_5;
        
        Account account_6 = new Account();
        account_6.Name = 'Test 6';
        account_6.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Secondary School').RecordTypeId;
        insert account_6;
        
        Account account_7 = new Account();
        account_7.Name = 'Test 7';
        account_7.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        insert account_7;
    }

}