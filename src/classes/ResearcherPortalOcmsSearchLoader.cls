global with sharing class ResearcherPortalOcmsSearchLoader implements cms.IContentBundlerOverride {
    Id[] originIds;
    String[] contentTypes;
    String[] tagPaths;
    Integer depth;
    Integer limitCount;
    Integer offset;
    Boolean isTargeted;
    String searchTerm;

    public String getContent(String jsonRequest) {
        this.initFromRequest(jsonRequest);

        Boolean hasOriginIds = this.originIds != null && this.originIds.size() > 0;

        if (!hasOriginIds && String.isNotBlank(searchTerm) && searchTerm.length() > 3) {
            // Use the Filtering API to search content
            originIds = getFilteredOriginIds();
        } else if (!hasOriginIds) {
            // Use the Rendering API to get content
            originIds = getAllOriginIds();
        }

        Integer totalResults = originIds.size();

        // Perform pagination and ordering
        this.originIds = orderAndLimitOriginIds(this.originIds);

        Integer resultsSeen = this.originIds.size() + this.offset;
        Boolean hasMore = (totalResults > resultsSeen);

        // Use the Rendering API to get content bundles for the paginated origin IDs
        ContentBundleList bundleList = getBundleListForContent(this.originIds, hasMore);

        return Json.serialize(bundleList);
    }

    private void initFromRequest(String jsonRequest) {
        ApiRequest request = (ApiRequest) Json.deserialize(jsonRequest, ApiRequest.class);

        this.originIds = (Id[])request.listParameters.get('originIds');
        this.contentTypes = request.listParameters.get('contentTypes');
        this.tagPaths = request.listParameters.get('tagPaths');
        this.depth = getValueOrDefault(request.parameters.get('depth'), 5);
        this.limitCount = getValueOrDefault(request.parameters.get('limitCount'), 5);
        this.offset = getValueOrDefault(request.parameters.get('offset'), 0);
        this.isTargeted = request.requestFlags.get('isTargeted');
        this.searchTerm = request.parameters.get('searchTerm');
    }

    private Id[] getFilteredOriginIds() {
        FilteringApiRequest req = new FilteringApiRequest();
        req.parameters.put('term', this.searchTerm);
        req.listParameters.put('contentTypesAndLayouts', this.contentTypes);
        if (this.tagPaths != null && this.tagPaths.size() > 0) {
            // We need to manually filter by taxonomy after getting searched content
            req.listParameters.put('filterGroups', new String[] { 'Taxonomy' });
        }
        req.listParameters.put('order', new String[] { 'Relevance:DESC' });
        req.requestFlags.put('targeted', true);

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
                'service' => 'FilteringApi',
                'action' => 'searchContent',
                'apiVersion' => '5.1',
                'filteringRequest' => Json.serialize(req)
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        FilteringBundle result = (FilteringBundle)Json.deserialize(apiResponse.responseObject, FilteringBundle.class);

        if (this.tagPaths != null && this.tagPaths.size() > 0) {
            // Iterate through returned filters to get content matching both search term + tag paths
            return getFilteredOriginIdsTaxonomy(result);
        } else {
            // No taxonomy specified - return all matching content
            return getFilteredOriginIdsNoTaxonomy(result);
        }
    }

    private Id[] getFilteredOriginIdsTaxonomy(FilteringBundle bundle) {
        Set<Id> idSet = new Set<Id>();
        Id[] originIds = new Id[] {};
        for (Filter filter : bundle.filters) {
            // Should only be filterType=TaxonomyItem - we're not interested in other types
            if (filter.filterType != 'TaxonomyItem') {
                continue;
            }

            for (FilterItem filterItem : filter.children) {
                for (String tagPath : this.tagPaths) {
                    // Assumes depth specified is >=6
                    // For shallow depths, this logic needs to look at how much deeper
                    // the filter path is vs. the requested tag path
                    // Should NOT be an issue for the Researcher Portal
                    if (!filterItem.filter.startsWith(tagPath)) {
                        continue;
                    }

                    for (Id originId : filterItem.originIds) {
                        idSet.add(originId);
                    }
                }
            }
        }

        originIds.addAll(idSet);
        return originIds;
    }

    private Id[] getFilteredOriginIdsNoTaxonomy(FilteringBundle bundle) {
        Id[] originIds = new Id[] {};
        originIds.addAll(bundle.contentBundles.keySet());
        return originIds;
    }

    private Id[] getAllOriginIds() {
        Id[] resultIds = new Id[] {};
        RenderingApiRequest req = new RenderingApiRequest();

        req.listParameters.put('contentTypes', this.contentTypes);
        req.requestFlags.put('targeted', isTargeted);

        if (this.tagPaths != null && this.tagPaths.size() > 0) {
            // Tag paths were requested - set up a Rendering Request by taxonomy
            req.parameters.put('renderType', 'taxonomy');
            req.listParameters.put('tagPaths', this.tagPaths);
            req.layoutsForTaxonomy = new List<List<String>>();
            for (Integer i = 0; i < this.depth; i ++) {
                // This custom template has no rendered HTML - we don't need HTML here so we use that
                req.layoutsForTaxonomy.add(new String[] { 'ResearcherPortalNoRendering' });
            }
        } else {
            // No tag paths - set up a Rendering Request by content type
            req.parameters.put('renderType', 'contentType');

            // This custom template has no rendered HTML - we don't need HTML here so we use that
            req.listParameters.put('contentLayouts', new String[] { 'ResearcherPortalNoRendering' });
        }

        // Regardless of render type, the response will look the same
        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
                'service' => 'OrchestraRenderingApi',
                'action' => 'getRenderedContent',
                'apiVersion' => '5.1',
                'renderingRequest' => Json.serialize(req)
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        RenderResultBundle result = (RenderResultBundle)Json.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        for (RenderResultBundle.RenderedContent rendering : result.renderings) {
            resultIds.add(rendering.originId);
        }

        return resultIds;
    }

    private Id[] orderAndLimitOriginIds(Id[] originIds) {
        Id[] orderedOriginIds = new Id[] {};

        for(OCMS_Search__c searchRecord : [
                SELECT Content__c
                FROM OCMS_Search__c
                WHERE Content__c IN :originIds
                ORDER BY Importance__c
                LIMIT :this.limitCount
                OFFSET :this.offset
        ]) {
            orderedOriginIds.add(searchRecord.Content__c);
        }

        return orderedOriginIds;
    }

    private ContentBundleList getBundleListForContent(Id[] originIds, Boolean hasMore) {
        RenderingApiRequest req = new RenderingApiRequest();

        req.parameters.put('renderType', 'originId');
        req.listParameters.put('originIds', originIds);

        // JsonDataTemplate is a "special" content layout that contains a cached, JSON-serialized
        // content bundle. We could use the withContentBundle flag but as of OrchestraCMS package 8.196
        // the withContentBundle flag does NOT hit a cache so is slower.
        req.listParameters.put('contentLayouts', new List<String> { 'JsonDataTemplate' });
        req.requestFlags.put('targeted', false);

        String response = cms.ServiceEndpoint.doActionApex(new Map<String, String> {
                'service' => 'OrchestraRenderingApi',
                'action' => 'getRenderedContent',
                'apiVersion' => '5.1',
                'renderingRequest' => Json.serialize(req)
        });

        JsonMessage.ApiResponse apiResponse = (JsonMessage.ApiResponse)Json.deserialize(response, JsonMessage.ApiResponse.class);
        RenderResultBundle result = (RenderResultBundle)Json.deserialize(apiResponse.responseObject, RenderResultBundle.class);

        // Re-order content and return (the Rendering API reorders by publish date)
        ContentBundle[] bundles = new ContentBundle[] {};
        Map<String, SocialBundle> socialBundles = new Map<String, SocialBundle>();
        Map<Id, ContentBundle> originToBundle = new Map<Id, ContentBundle>();

        for (RenderResultBundle.RenderedContent rendering : result.renderings) {
            String serializedContentBundle = rendering.renderMap.get('JsonDataTemplate');
            ContentBundle cb = (ContentBundle)Json.deserialize(serializedContentBundle, ContentBundle.class);
            originToBundle.put(cb.originId, cb);
        }

        for (Id originId : originIds) {
            bundles.add(originToBundle.get(originId));
            socialBundles.put(originId, new SocialBundle());
        }

        return new ContentBundleList(bundles, socialBundles, hasMore);
    }

    private Integer getValueOrDefault(String stringValue, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            value = Integer.valueOf(stringValue);
        } catch(Exception e) {
            value = defaultValue; // Redundant, but satisfies PMD
        }

        return value;
    }
}