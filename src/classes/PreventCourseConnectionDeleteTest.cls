@isTest
public class PreventCourseConnectionDeleteTest
{
     static testmethod void testPreventAccDel()
     {
        TestDataFactoryUtil.createCourseConnectionRecords();
         
        hed__Course_Enrollment__c courseConnTestObj = [SELECT id, hed__Status__c from hed__Course_Enrollment__c LIMIT 1];                
        try
        {
            delete courseConnTestObj;
        }
         catch(DMLexception e)
         {
            system.assert(e.getMessage().contains('Course Connection Cannot be deleted'),'Course Connection Cannot be deleted');                       
        }
    }
}