public without sharing class InteractionTriggerHandler extends TriggerHandlerExt {
    private map < Id, List < Lead >> leadForReconversion = new map < Id, List < Lead >> ();
    private InteractionTriggerHandlerHelper intHandler = new InteractionTriggerHandlerHelper();
    private map<Id, hed__Affiliation__c> mapContactAffiliation = new map<Id, hed__Affiliation__c>();
    public override void beforeInsert() {
        InteractionService.setPrimaryCampaignId(trigger.new);
    }
    public override void afterInsert() {
        processInteraction(trigger.new);
    }
    public override void afterUpdate() {
        InteractionService.updateOpportunityAffiliation(trigger.new);
    }
    private InteractionMappingService intMappingService {
        get {
            if (intMappingService == null) {
                intMappingService = new InteractionMappingService();
            }
            return intMappingService;
        }
        set;
    }
    private LeadStatus convertStatus {
        get {
            if (convertStatus == null) {
                convertStatus = [SELECT Id, Masterlabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
            }
            return convertStatus;
        }
        set;
    }
    private void processInteraction(List < Interaction__c > interactionToProcess) {

        map < Id, Interaction__c > mapLeadInteraction = new map < Id, Interaction__c > ();
        List < Interaction__c > interactionOnly = new List < Interaction__c > ();
        for (Interaction__c interaction: interactionToProcess) {
            Interaction__c clonedInteraction = interaction.clone(true, true, true, true);
            if (String.isNotBlank(interaction.Lead__c)) {
                mapLeadInteraction.put(interaction.Lead__c, clonedInteraction);
            } else {
                interactionOnly.add(clonedInteraction);
            }
        }
        if (!interactionOnly.isEmpty()) {
            mapLeadInteraction.putAll(intHandler.processInteractionOnly(interactionOnly));
        }
        if (!mapLeadInteraction.isEmpty()) {
            list < Lead > updatedLeads = processLeadsUpdate(mapLeadInteraction);
            map < Interaction__c, Database.LeadConvertResult > mapInteractionLCR = new map < Interaction__c, Database.LeadConvertResult > ();
            mapInteractionLCR = processConvertLead(processLeadInteractionCheck(updatedLeads), mapLeadInteraction);
            if (!mapInteractionLCR.isEmpty()) {
                processInteractionUpdate(intHandler.reprocessInteraction(mapInteractionLCR,mapContactAffiliation));
            }

        }

    }
    private void processInteractionUpdate(List < Interaction__c > interaction) {
        list < Interaction__c > interactionToUpdate = new list < Interaction__c > ();
        List < Database.LeadConvert > allLeadForConversion = new List < Database.LeadConvert > ();
        for (Interaction__c intRec: interaction) {
            if (intRec.Opportunity__r != null) {
                intRec.Opportunity__c = intRec.Opportunity__r.Id;
                intRec.Opportunity__r = null;
            }
            interactionToUpdate.add(intRec);
        }

        InteractionService.logPossibleErrors(Database.update(interactionToUpdate, true));
    

        if (!leadForReconversion.isEmpty()) {
           InteractionService.processAllOtherLeads(leadForReconversion,interactionToUpdate);

        }
    }
    private List < Lead > processLeadsUpdate(map < ID, Interaction__c > mapLeadsInteractions) {
        list < Lead > leadsToUpdate = new list < Lead > ();
        for (Lead leadRec: [SELECT ID, CreatedDate
                FROM Lead
                WHERE ID IN: mapLeadsInteractions.keySet()
            ]) {
            intMappingService.applyDataToSObject(mapLeadsInteractions.get(leadRec.ID), leadRec);
            leadsToUpdate.add(leadRec);
        }
        if (!leadsToUpdate.isEmpty()) {
        	Database.DMLOptions dml = new Database.DMLOptions(); 
			dml.DuplicateRuleHeader.allowSave = true;
			dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.update(leadsToUpdate, dml);
        }
        return leadsToUpdate;
    }
    private map < Interaction__c, Database.LeadConvertResult > processConvertLead(List < Database.LeadConvert > leadsToConvert, map < id, Interaction__c > mapLeadInteraction) {
        Database.LeadConvertResult[] leadConvertResults = Database.convertLead(leadsToConvert, true);
        map < Interaction__c, Database.LeadConvertResult > mapLeadConvertResults = new map < Interaction__c, Database.LeadConvertResult > ();

        for (Database.LeadConvertResult lcr: leadConvertResults) {
            if (lcr.isSuccess()) {
                if (mapLeadInteraction.containsKey(lcr.getLeadId())) {
                    Interaction__c interaction = mapLeadInteraction.get(lcr.getLeadId());
                    interaction.Contact__c = lcr.getContactId();
                    mapLeadConvertResults.put(interaction, lcr);
                }

            }
        }
        return mapLeadConvertResults;
    }
    private List < Database.LeadConvert > processLeadInteractionCheck(list < Lead > updatedLeads) {
        map < Id, ID > mapDupContact = new Map < Id, ID > ();
        map < Id, List < Lead >> mapDupLead = new Map < Id, List < Lead >> ();
        list < Lead > dupLeads;
        List < Database.LeadConvert > leadConverts = new List < Database.LeadConvert > ();
        for (Lead leadRec: updatedLeads) {
            dupLeads = new List < Lead > ();
            dupLeads.add(leadRec);
            mapDupLead.put(leadRec.Id, dupLeads);
            List < lead > idlist = new List < Lead > {
                leadRec
            };
            Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(idlist);
            for (Datacloud.FindDuplicatesResult findDupeResult: results) {
                for (Datacloud.DuplicateResult dupeResult: findDupeResult.getDuplicateResults()) {
                    for (Datacloud.MatchResult matchResult: dupeResult.getMatchResults()) {
                        Contact matchedContact;
                        Lead matchedLead;
                        for (Datacloud.MatchRecord match: matchResult.getMatchRecords()) {
                            if (match.getRecord().Id.getSObjectType().getDescribe().getName() == 'Lead') {
                                matchedLead = (Lead) match.getRecord();
                                mapDupLead.get(leadRec.Id).add(matchedLead);
                            }
                            if (matchedContact == null && match.getRecord().Id.getSObjectType().getDescribe().getName() == 'Contact') {
                                matchedContact = (Contact) match.getRecord();
                                mapDupContact.put(leadRec.Id, matchedContact.Id);
                            }
                        }
                    }
                }
            }
        }
        leadConverts = convertLead(mapDupContact, mapDupLead);
        return leadConverts;
    }

    private List < Database.LeadConvert > convertLead(map < Id, Id > mapDupContact, map < Id, List < Lead >> mapDuplicateLeads) {
        List < Database.LeadConvert > leadConvertResults = new List < Database.LeadConvert > ();
        map < ID, Contact > mapContacts = new map < ID, Contact > ();
        if (!mapDupContact.isEmpty()) {
            mapContacts = new map < Id, Contact > ([SELECT ID, AccountId FROM Contact WHERE ID IN: mapDupContact.values()]);
	        mapContactAffiliation = InteractionService.mappedContactAffiliation(mapContacts);
        }
        if (!mapDuplicateLeads.isEmpty()) {
            for (Id ids: mapDuplicateLeads.keySet()) {
                for (Lead leadRec: mapDuplicateLeads.get(ids)) {

                    Database.LeadConvert lc = new Database.LeadConvert();
                    lc.setLeadId(LeadRec.Id);
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    lc.setDoNotCreateOpportunity(true);
                    lc.setOwnerId(UserInfo.getUserId());
                    if (!mapDupContact.isEmpty() && mapDupContact.containsKey(ids)) {
                        Contact contactRec = mapContacts.get(mapDupContact.get(ids));
                        lc.setContactId(contactRec.Id);
                        lc.setAccountId(contactRec.AccountId);
                    }
                    if (leadRec.Id == ids) {
                        leadConvertResults.add(lc);
                    } else {
                        if (leadForReconversion.containsKey(ids)) {
                            leadForReconversion.get(ids).add(LeadRec);
                        } else {
                            leadForReconversion.put(ids, new list < Lead > {
                                leadRec
                            });
                        }
                    }
                }
            }
        }
        return leadConvertResults;
    }
	
   
}