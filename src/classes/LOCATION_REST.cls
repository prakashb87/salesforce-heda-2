/*********************************************************************************
*** @ClassName         : LOCATION_REST
*** @Author            : Shubham Singh 
*** @Requirement       : RM-1411
*** @Created date      : 14/08/2018
*** @Modified by       : Shubham Singh 
*** @modified date     : 14/08/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Location')
global with sharing class LOCATION_REST {
    public String setId;
    public String location;
    public String effectiveDate;
    public String effectiveStatus;
    public String description;
    public String shortDescription;
    public String building;
    public String floor;
    public String sector;
    public String jurisdiction;
    public String country;
    public String addressLine1;
    public String addressLine2;
    public String addressLine3;
    public String addressLine4;
    public String city;
    public String county;
    public String state;
    public String postalCode;
    public String countryCode;
    public String phoneNumber;
    public String extension;
    public String faxNumber;
    public String languageCode;
    public String region;
    @HttpPost
    global static Map < String, String > upsertLocation() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        //upsert Location
        list < Location__c > UpsertLocation = new list < Location__c > ();
        //Location object
        Location__c location;
        //to create map of effective date        
        map < String,Date > updateEffectiveDate = new map < String,Date >(); 
        //key to get unique values
        String key;
        try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            System.debug('PostString from IPASS------------------> '+ reqStr);
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < LOCATION_REST > postString = (List < LOCATION_REST > ) System.JSON.deserialize(reqStr, List < LOCATION_REST > .class);
            //getting location code
            set < String > listLocation = new set < String > ();
            set < String > setID = new set < String > ();
            if (postString != null) {
                for (LOCATION_REST loctn: postString) {
                    
                    if ((loctn.location != null && (loctn.location).length() > 0) && (loctn.setId != null && (loctn.setId).length() > 0)) {
                        listLocation.add(loctn.location);
                        setID.add(loctn.setId);
                        if(loctn.effectiveDate != null && (loctn.effectiveDate).length() > 0)
                        {
                            updateEffectiveDate.put(loctn.setId+''+loctn.location,Date.ValueOf(loctn.effectiveDate));
                        }
                    } else {
                        responsetoSend = throwError();
                        return responsetoSend;
                    }
                    
                }
            }
            
            //getting the old Location for update
            list < Location__c > previousLocation = new list < Location__c > ();
            if (listLocation.size() > 0 && listLocation != null && setID.size() > 0 && setID != null) {
                previousLocation = [SELECT id, Name, Location__c, Effective_Date__c, Effective_Status__c, Description__c, Short_Description__c, Building__c, Floor__c, Sector__c, Jurisdiction__c, Country__c, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c, City__c, County__c, State__c, Postal_code__c, Country_code__c, Phone_Number__c, Extension__c, Fax_Number__c, Language_Code__c, Region__c FROM Location__c WHERE Location__c IN: setID and Name IN: listLocation ];
            }           
            //map to update the old Location
            map < String, ID > oldLocationUpdate = new map < String, ID > ();
            map < String,  Location__c> stringLocationMap = new map < String, Location__c> ();
            for (Location__c loctn: previousLocation) {
                String uniqueKey = loctn.Location__c + '' + loctn.Name;
                if(updateEffectiveDate != null && updateEffectiveDate.get(uniqueKey) != null){  
                    if(updateEffectiveDate.get(uniqueKey) <= System.today() && loctn.Effective_Date__c <= updateEffectiveDate.get(uniqueKey)){
                        oldLocationUpdate.put(uniqueKey, loctn.ID);
                        stringLocationMap .put(uniqueKey,loctn);
                    }else if(loctn.Effective_Date__c > updateEffectiveDate.get(uniqueKey)){
                        responsetoSend.put('message', 'OK');
                        responsetoSend.put('status', '200');
                        return responsetoSend;
                    }
                    
                }else{
                    responsetoSend.put('message', 'OK');
                    responsetoSend.put('status', '200');
                    return responsetoSend;  
                }
            }
            
            //insert or update the Location
            for (LOCATION_REST locationRest: postString) {
                system.debug('Test1-->');
                location = new Location__c();
                key = locationRest.setId + '' + locationRest.location;
                location.Name = locationRest.location;
                location.Location__c = locationRest.setId;
                location.Effective_Date__c = locationRest.effectiveDate != null ? Date.valueOf(locationRest.effectiveDate) : null;
                location.Effective_Status__c = locationRest.effectiveStatus != null && locationRest.effectiveStatus.length() > 0? locationRest.effectiveStatus == 'A'? 'Active': 'Inactive': null;
                location.Description__c = locationRest.description;
                location.Short_Description__c = locationRest.shortDescription;
                location.Building__c = locationRest.building;
                location.Floor__c = locationRest.floor;
                location.Sector__c = locationRest.sector;
                location.Jurisdiction__c = locationRest.jurisdiction;
                location.Country__c = locationRest.country;
                location.Address_Line_1__c = locationRest.addressLine1;
                location.Address_Line_2__c = locationRest.addressLine2;
                location.Address_Line_3__c = locationRest.addressLine3;
                location.Address_Line_4__c = locationRest.addressLine4;
                location.City__c = locationRest.city;
                location.County__c = locationRest.county;
                location.State__c = locationRest.state;
                location.Postal_code__c = locationRest.postalCode;
                location.Country_code__c = locationRest.countryCode;
                location.Phone_Number__c = locationRest.phoneNumber;
                location.Extension__c = locationRest.extension;
                location.Fax_Number__c = locationRest.faxNumber;
                location.Language_Code__c = locationRest.languageCode;
                location.Region__c = locationRest.region;
                location.Location_key__c = key ;
                
                
                //assigning the Location id for update 
                if (oldLocationUpdate.size() > 0 && oldLocationUpdate.get(locationRest.setId + '' + locationRest.location) != null){
                    if (stringLocationMap.get(locationRest.setId + '' + locationRest.location).Effective_Date__c <= location.Effective_Date__c){
                        location.ID = oldLocationUpdate.get(locationRest.setId + '' + locationRest.location);
                    }else{
                        location =null;
                    }
                }
                //add location to the list
                if(location !=null){
                    UpsertLocation.add(location);
                }
            }
            //upsert location
            if (UpsertLocation != null && UpsertLocation.size() > 0 && UpsertLocation[0].Effective_Date__c <= System.today()) {
                upsert UpsertLocation Location_key__c ;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Location was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}