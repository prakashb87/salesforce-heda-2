@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class HEDAProgram2CSBatchScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		HEDAProgram2CSBatch batchJob = new HEDAProgram2CSBatch();
		Id batchId = Database.executeBatch(batchJob, 30);
	}
}