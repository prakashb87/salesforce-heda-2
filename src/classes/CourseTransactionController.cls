/*****************************************************************
Name: CourseTransactionRecordController
Author: Capgemini [Shreya]
Purpose: Recording Transactions in Course Transaction object
JIRA Reference : ECB-4416 :  Build : Recording Transactions in Course Transaction object
                 ECB-4493 :  Populate additional fields RecordType and Post Action
                 
********************************************************************/
/*==================================================================
History 
--------
Version   Author            Date              Detail
1.0       Shreya            2/09/2018         Initial Version
***********************************************************************/
public without sharing class CourseTransactionController {

    public static void createCourseTransactionRecord(Set<Id> courseConnIdList){
    
        
        List<hed__Course_Enrollment__c> courseEnrollList = [SELECT Id,hed__Contact__c,hed__Course_Offering__c,Service__c,hed__Course_Offering__r.Census_Date__c,hed__Course_Offering__r.hed__Course__r.RecordTypeId,
                                                            hed__Program_Enrollment__c,hed__Course_Offering__r.hed__Course__r.General_Ledger_Code__r.Name,hed__Course_Offering__r.hed__Course__r.Internal_Order_Number__r.Name
                                                            FROM hed__Course_Enrollment__c WHERE ID IN :courseConnIdList  ];
        
        //Map created to store service and related courese conection pair
        Map<Id,hed__Course_Enrollment__c> serviceCourseConnMap = new Map<Id,hed__Course_Enrollment__c>();
        Set<Id> serviceIdSet = new Set<Id>();
        
        for(hed__Course_Enrollment__c conn : courseEnrollList){
            serviceCourseConnMap.put(conn.Service__c,conn);
            serviceIdSet.add(conn.Service__c); 
        }
        
        
        List<csord__Service_Line_Item__c> serviceLineRecList = [SELECT Name,csord__Service__c,csord__Total_Price__c,csord__Price_Before_Discount__c,GST__c,csord__Discounted_Amount__c,
                                                                CreatedDate,csord__Service__r.Plan__c,csord__Service__r.csordtelcoa__Product_Basket__c,csord__Service__r.Is_Upgrade__c,csord__Service__r.Course_Count_Upgrade__c,csord__Service__r.Course_Program__c 
                                                                FROM csord__Service_Line_Item__c WHERE csord__Service__c IN :serviceIdSet];
                                                                
        Set<Id> programPlanIdSet = new Set<Id>();
        Set<Id> productBasketIdSet = new Set<Id>();
        
        if(!serviceLineRecList.isEmpty()){
            for(csord__Service_Line_Item__c ser : serviceLineRecList){
                if(ser.csord__Service__r.csordtelcoa__Product_Basket__c!=null){
                    productBasketIdSet.add(ser.csord__Service__r.csordtelcoa__Product_Basket__c);
                }   
                if(ser.csord__Service__r.Plan__c!=null){
                    programPlanIdSet.add(ser.csord__Service__r.Plan__c);
                }   
            }
        }   
        
        List<Payment__c> paymentRecList = [SELECT Id,Product_Basket__c,Receipt_Number__c FROM Payment__c WHERE Product_Basket__c IN :productBasketIdSet];
        //Map to store product basket and receipt no pair
        Map<Id,String> prodBasketReceiptNoMap = new Map<Id,String>();
        if(!paymentRecList.isEmpty()){
            for(Payment__c pay : paymentRecList){
                if(pay.Product_Basket__c!=null && pay.Receipt_Number__c!=null && pay.Receipt_Number__c!=''){
                    prodBasketReceiptNoMap.put(pay.Product_Basket__c,pay.Receipt_Number__c);
                }
            }
        }   
        
        List<hed__Plan_Requirement__c> planReqList = [SELECT Id,hed__Program_Plan__c,hed__Course__c FROM hed__Plan_Requirement__c WHERE hed__Program_Plan__c IN :programPlanIdSet];
        
        //Map created to store program plan and related list of plan requirement
        Map<Id,List<hed__Plan_Requirement__c>> programCourseMap = new Map<Id,List<hed__Plan_Requirement__c>>();
        //Set<Id> programPlanIdSet = new Set<Id>();
        if(!planReqList.isEmpty()){
            for(hed__Plan_Requirement__c plan : planReqList){ 
                if(plan.hed__Program_Plan__c != null && plan.hed__Course__c != null ){
                   if(programCourseMap.containsKey(plan.hed__Program_Plan__c)){
                        programCourseMap.get(plan.hed__Program_Plan__c).add(plan);
                        
                    }else{
                        List<hed__Plan_Requirement__c> planRequirementList = new List<hed__Plan_Requirement__c>();
                        planRequirementList.add(plan);
                        programCourseMap.put(plan.hed__Program_Plan__c,planRequirementList);
                    }
                    //programPlanIdSet.add(plan.hed__Program_Plan__c);
                }
            }
        }
        
        
        List<Course_Transaction__c> courseTransacList = new List<Course_Transaction__c>();
        
        Id rmitOnlineCourseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId();  
        // ECB-4493 Get Revenue Record Type
        Id revenueRecordTypeId = Schema.SObjectType.Course_Transaction__c.getRecordTypeInfosByName().get('Revenue').getRecordTypeId(); 
         Id rmitTrainingCourseRecordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Training').getRecordTypeId(); 
        
        if(!serviceLineRecList.isEmpty()){
            for(csord__Service_Line_Item__c ser : serviceLineRecList){
                if(ser.Name==ConfigDataMapController.getCustomSettingValue('ServiceLineItemName')){
                    //Logic to create course transaction record for individual courses (Except 21CC)
                    if(ser.csord__Service__r.Course_Program__c==ConfigDataMapController.getCustomSettingValue('Course')){ //Fix for ECB-4550:Shreya:07/10/18
                        Course_Transaction__c rec = new Course_Transaction__c();
                        if(prodBasketReceiptNoMap.containsKey(ser.csord__Service__r.csordtelcoa__Product_Basket__c)){
                            rec.Receipt_Number__c = prodBasketReceiptNoMap.get(ser.csord__Service__r.csordtelcoa__Product_Basket__c);
                        }   
                        if(serviceCourseConnMap.containsKey(ser.csord__Service__c)){
                            rec.Course_Connection__c = serviceCourseConnMap.get(ser.csord__Service__c).Id;
                            rec.Contact__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Contact__c;
                            rec.GL_Code__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.General_Ledger_Code__r.Name;//Fix for ECB-4551
                            rec.IO_Code__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.Internal_Order_Number__r.Name;//Fix for ECB-4551
                            rec.Census_Date__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.Census_Date__c;    
                            if(serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.RecordTypeId==rmitOnlineCourseRecordTypeId){
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITOCompanyCode');
                            }else if(serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.RecordTypeId==rmitTrainingCourseRecordTypeId){
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITTrainingCompanyCode');
                            }else{
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITCompanyCode');
                            }
                        }    
                        rec.Service__c = ser.csord__Service__c;             
                        rec.List_Price__c = (ser.csord__Price_Before_Discount__c).setScale(2);
                        rec.Discount_Amount__c = (ser.csord__Discounted_Amount__c).setScale(2);
                        
                        //Fix for ECB-4552:Sprint Testing- The net amount on the course transaction is blank
                        if(ser.csord__Total_Price__c!=null && (ser.GST__c!=null && ser.GST__c!='')){
                            rec.Net_Amount__c = getCourseNetAmount(ser.csord__Total_Price__c,Decimal.valueOf(ser.GST__c));
                            
                        }else if(ser.csord__Total_Price__c!=null && (ser.GST__c==null || ser.GST__c=='' || ser.GST__c=='0')){
                            rec.Net_Amount__c = (ser.csord__Total_Price__c).setScale(2);
                        }else{
                            rec.Net_Amount__c = 0.0;
                        }
                        
                        if(ser.GST__c!=null && ser.GST__c!=''){
                            rec.GST_Amount__c = (Decimal.valueOf(ser.GST__c)).setScale(2);
                        }
                        rec.Transaction_Date__c = Date.valueOf(ser.CreatedDate);
                        rec.Post_To_SAP__c = false;
                        // ECB-4493 Populate additional fields Record Type, Generated by and Post Action
                        rec.RecordTypeId = revenueRecordTypeId;
                        rec.Generated_by__c = 'System';
                        rec.Post_Action__c = 'Post';
                        
                        courseTransacList.add(rec);
                    }else if(ser.csord__Service__r.Course_Program__c==ConfigDataMapController.getCustomSettingValue('Program')){ //Fix for ECB-4550:Shreya:07/10/18
                        //Logic to create course transaction record for programs
                        //Fix for ECB-4572 -Upgraded Program Purchase Logic for Course Transaction
                        Integer courseCount = 0;                       
                        if(ser.csord__Service__r.Is_Upgrade__c==true){
                           courseCount = Integer.valueOf(ser.csord__Service__r.Course_Count_Upgrade__c);                    
                        }else{
                            courseCount = programCourseMap.get(ser.csord__Service__r.Plan__c).size();
                        }
                        
                        Course_Transaction__c rec = new Course_Transaction__c();
                        if(prodBasketReceiptNoMap.containsKey(ser.csord__Service__r.csordtelcoa__Product_Basket__c)){
                            rec.Receipt_Number__c = prodBasketReceiptNoMap.get(ser.csord__Service__r.csordtelcoa__Product_Basket__c);
                        }
                        if(serviceCourseConnMap.containsKey(ser.csord__Service__c)){
                            //ECB-4674 -  if the Course/Program field on the service is 'Program', the course transaction program enrolment should be populated.
                            rec.Program_Enrollment__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Program_Enrollment__c;
                            rec.Course_Connection__c = serviceCourseConnMap.get(ser.csord__Service__c).Id;
                            rec.Contact__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Contact__c;
                            rec.GL_Code__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.General_Ledger_Code__r.Name; //Fix for ECB-4551
                            rec.IO_Code__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.Internal_Order_Number__r.Name;//Fix for ECB-4551
                            rec.Census_Date__c = serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.Census_Date__c;    
                            if(serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.RecordTypeId==rmitOnlineCourseRecordTypeId){
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITOCompanyCode');
                            }else if(serviceCourseConnMap.get(ser.csord__Service__c).hed__Course_Offering__r.hed__Course__r.RecordTypeId==rmitTrainingCourseRecordTypeId){
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITTrainingCompanyCode');
                            }else{
                                rec.Company_Code__c = ConfigDataMapController.getCustomSettingValue('RMITCompanyCode');
                            }
                        }    
                        rec.Service__c = ser.csord__Service__c;
                        if(courseCount > 0){
                            if(ser.csord__Price_Before_Discount__c!=null){
                                rec.List_Price__c = getCourseListPrice(ser.csord__Price_Before_Discount__c,courseCount);
                            }
                            if(ser.csord__Discounted_Amount__c!=null){  
                                rec.Discount_Amount__c = getCourseDiscountAmount(ser.csord__Discounted_Amount__c,courseCount);
                            }
                            
                            //Fix for ECB-4552:Sprint Testing- The net amount on the course transaction is blank
                            Decimal disAmt;
                            Decimal gstAmt;
                            if(ser.csord__Discounted_Amount__c==null){
                                disAmt = 0.0;
                            }else{
                                disAmt = (ser.csord__Discounted_Amount__c).setScale(2);
                            }
                            if(ser.GST__c==null || ser.GST__c==''){
                                gstAmt = 0.0;
                            }else{
                                gstAmt = Decimal.valueOf(ser.GST__c).setScale(2);
                            }
                            
                            if(ser.csord__Price_Before_Discount__c!=null ){
                                rec.Net_Amount__c = getProgramCourseNetAmount(ser.csord__Price_Before_Discount__c,disAmt,gstAmt,courseCount);
                            }else{
                                rec.Net_Amount__c = 0.0;
                            }
                            
                            if(ser.GST__c!=null && ser.GST__c!=''){  
                                rec.GST_Amount__c = getCourseGSTAmount(Decimal.valueOf(ser.GST__c),courseCount);
                            }   
                        }
                        rec.Transaction_Date__c = Date.valueOf(ser.CreatedDate);
                        rec.Post_To_SAP__c = false;
                        // ECB-4493 Populate additional fields Record Type, Generated by and Post Action
                        rec.RecordTypeId = revenueRecordTypeId;
                        rec.Generated_by__c = 'System';
                        rec.Post_Action__c = 'Post';
                         
                        courseTransacList.add(rec);
                    }
                    
                }   
            }
        }   
        
        if(!courseTransacList.isEmpty())
        {
            try
            {
                system.debug('RRRRR:Course Transaction List=' + courseTransacList.size());
                Database.Insert(courseTransacList,false);
            }
            catch(DMLException ex){
                system.debug('Exception::'+ex);
            }
        }
    }
    
    /********************************************************************
    // Purpose              : Get the list price of individual courses under a program                                     
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal totalPriceBeforeDiscount,Integer courseCount
    //  Returns             : Decimal 
   
    //********************************************************************/
    
    public static Decimal getCourseListPrice(Decimal totalPriceBeforeDiscount,Integer courseCount){
        
        Decimal listPrice;
        try{
            listPrice = totalPriceBeforeDiscount/courseCount;
        }catch(Exception ex){
            system.debug('Exception'+ex);
        }
        
        return listPrice.setScale(2);
    }
    
    /********************************************************************
    // Purpose              : Get the discount amount of individual courses under a program                           
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal totalDiscountAmt,Integer courseCount
    //  Returns             : Decimal 
    
    //********************************************************************/
    
    public static Decimal getCourseDiscountAmount(Decimal totalDiscountAmt,Integer courseCount){
        
        Decimal discountAmount;
        try{
            discountAmount = totalDiscountAmt/courseCount;
        }catch(Exception ex){
            system.debug('Exception'+ex);
        }
        
        return discountAmount.setScale(2);
    }
    
    /********************************************************************
    // Purpose              : Get the gst amount of individual courses under a program                           
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal totalGSTAmt,Integer courseCount
    //  Returns             : Decimal 
    
    //********************************************************************/
    
    public static Decimal getCourseGSTAmount(Decimal totalGSTAmt,Integer courseCount){
        
        Decimal gstAmount;
        try{
            gstAmount = totalGSTAmt/courseCount;
        }catch(Exception ex){
            system.debug('Exception'+ex);
        }
        
        return gstAmount.setScale(2);
    }
    
   /********************************************************************
    // Purpose              : Get the net amount of individual courses under a program                           
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal totalPriceBeforeDiscount,Decimal totalDiscountAmt,Decimal totalGSTAmt,Integer courseCount
    //  Returns             : Decimal 
    
    //********************************************************************/
    
    public static Decimal getProgramCourseNetAmount(Decimal totalPriceBeforeDiscount,Decimal totalDiscountAmt,Decimal totalGSTAmt,Integer courseCount){
        
        system.debug('totalPriceBeforeDiscount::'+totalPriceBeforeDiscount);
        system.debug('totalDiscountAmt:::'+totalDiscountAmt);
        system.debug('totalGSTAmt:::'+totalGSTAmt);
        system.debug('courseCount:::'+courseCount);
        
        Decimal netAmount;
        try{
            Decimal listPrice = totalPriceBeforeDiscount/courseCount;
            Decimal discountAmt;
            if(totalDiscountAmt != 0){
                discountAmt = totalDiscountAmt/courseCount;
            }else{
                discountAmt = 0;
            }
            
            Decimal gstAmt;
            if(totalGSTAmt != 0){
                gstAmt = totalGSTAmt/courseCount;
            }else{  
                gstAmt = 0;
            }   
            
            netAmount = (listPrice - discountAmt) - gstAmt;
        }catch(Exception ex){
            system.debug('Exception'+ex);
        }
        
        return netAmount.setScale(2);
    }
    
    /********************************************************************
    // Purpose              : Get the net amount of individual courses                           
    // Author               : Capgemini [Shreya]
    // Parameters           : Decimal totalPrice,Decimal GSTAmt
    //  Returns             : Decimal 
    
    //********************************************************************/
    
    public static Decimal getCourseNetAmount(Decimal totalPrice,Decimal gstAmt){
        
        Decimal netAmount;
        try{           
            netAmount = totalPrice - gstAmt;
        }catch(Exception ex){
            system.debug('Exception'+ex);
        }
        
        return netAmount.setScale(2);
    }
    
    
    
}