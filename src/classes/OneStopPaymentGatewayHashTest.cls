@isTest
public class OneStopPaymentGatewayHashTest {

    @isTest
    public static void testSendHashingDetails(){
        
        TestDataFactoryUtil.dummycustomsetting();
        String urlStringParametersWithoutHash='Ref=&ReceiptNo=WP01000521&TotAmt=380.00&MPB=MPB-002766&Status=Successy7iJbfr45621Svl01d5G2i5f';
        String expectedHash ='4f7b6931cc5525d831efe99a0e89c19e';
        
        Test.startTest();
        Boolean result = OneStopPaymentGatewayHash.sendHashingDetails(urlStringParametersWithoutHash, expectedHash);
        Test.stopTest();
        
        System.assertEquals(result, true);
        
    }
}