/*****************************************************************************************************
 *** @Class             : EmbeddedSyncBasketIntegrationScheduler 
 *** @Author            : Shreya Barua
 *** @Requirement       : This class is used as a scheduler to schedule EmbeddedSyncBasketIntegrationBatch. 
 *** @Created date      : 25/06/2018
 *** @JIRA ID           : ECB-3900
 *****************************************************************************************************/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class EmbeddedSyncBasketIntegrationScheduler implements Schedulable {
   global void execute(SchedulableContext SC) {  
    
        EmbeddedSyncBasketIntegrationBatch bc = new EmbeddedSyncBasketIntegrationBatch();  
        Database.executeBatch(bc,1);
   }
}