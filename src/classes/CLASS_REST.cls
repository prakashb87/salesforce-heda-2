/*********************************************************************************
*** @ClassName         : CLASS_REST
*** @Author            : Shubham Singh 
*** @Requirement       : 
*** @Created date      : 23/08/2018
*** @Modified by       : Shubham Singh 
*** @modified date     : 25/08/2018
**********************************************************************************/
@RestResource(urlMapping = '/HEDA/v1/Class')
global with sharing class CLASS_REST {
    public String courseCode;
    public String courseOfferingNumber;
    public String term;
    public String session;
    public String classSection;
    public String institution;
    public String academicgroup;
    public String subject;
    public String catalogueNumber;
    public String academicCareer;
    public String description;
    public String classNumber;
    public String courseComponent;
    public String enrolmentStatus;
    public String classStatus;
    public String classType;
    public String enrolmentCapacity;
    public String enrolmentTotal;
    public String academicOrganisation;
    public String classCoordinator;
    public String campus;
    public String location;
    public String instructionMode;
    public String startDate;
    public String endDate;
    public String lastDayToAdd;
    public String censusDate;
    @HttpPost
    global static Map < String, String > upsertClass() {
        //final response to send
        Map < String, String > responsetoSend = new Map < String, String > ();
        
        //upsert courseOffering
        list < hed__Course_Offering__c > hedUpsertClass = new list < hed__Course_Offering__c > ();
        //getting courseOffering Term
        list < String > updateTerm = new list < String > ();
        //career
        list < String > career = new list < string > ();
        //session
        list < String > session = new list < string > ();
        //getting courseOffering class number
        list < String > updateClassNumber = new list < String > ();
        //get instituion
        list < String > listInstitution = new list < String > ();
        //get course id
        list < String > listCourseId = new list < String > ();
        //term list to store term record
        list < hed__Term__c > term = new list < hed__Term__c > ();
        //old courseOffrng for update
        list < hed__Course_Offering__c > listcourseOffrng = new list < hed__Course_Offering__c > ();
        //course record from course ID
        list < hed__Course__c > courseID = new list < hed__Course__c > ();
        //campus record
        list < Campus__c > campus = new list < Campus__c > ();
        //campus id
        list < string > listCampus = new list < string > ();
        //location record
        list < Location__c > location = new list < Location__c > ();
        //list location
        list < string > listLocation = new list < string > ();
        
        //courseOffrng
        hed__Course_Offering__c courseOffering;
        //store institution
        Account institution;
        
       try {
            
            //request and response variable
            RestRequest request = RestContext.request;
            
            RestResponse response = RestContext.response;
            
            //no need to add [] in JSON
            String reqStr = '[' + request.requestBody.toString() + ']';
            System.debug('Post from Ipaas -------------------> '+reqStr);
            response.addHeader('Content-Type', 'application/json');
            
            //getting the data from JSON and Deserialize it
            List < CLASS_REST > postString = (List < CLASS_REST > ) System.JSON.deserialize(reqStr, List < CLASS_REST > .class);
            
            if (postString != null) {
                for (CLASS_REST courseOfferingrest: postString) {
                    if (courseOfferingrest.term != null && (courseOfferingrest.term).length() > 0 && courseOfferingrest.classNumber != null && (courseOfferingrest.classNumber).length() > 0 
                        /*&& courseOfferingrest.courseCode != null && (courseOfferingrest.courseCode).length() > 0*/&& courseOfferingrest.academicCareer != null && (courseOfferingrest.academicCareer).length() > 0
                        && courseOfferingrest.session != null && (courseOfferingrest.session).length() > 0) {
                            updateTerm.add(courseOfferingrest.term);
                            updateClassNumber.add(courseOfferingrest.classNumber);
                            listCourseId.add(courseOfferingrest.courseCode);
                            career.add(courseOfferingrest.academicCareer);
                            session.add(courseOfferingrest.session);
                            if (courseOfferingrest.institution != null)
                            {
                                listInstitution.add(courseOfferingrest.institution);
                            }
                            if (courseOfferingrest.campus != null)
                            {
                                listCampus.add(courseOfferingrest.campus);
                            }
                            if (courseOfferingrest.location != null)
                            {
                                listLocation.add(courseOfferingrest.location);
                            }
                        } else {
                            responsetoSend = throwError();
                            return responsetoSend;
                        }
                    
                }
            }
            
            //as there will be one term record 
            if (career != null && career.size() > 0 && session!= null && session.size() > 0) {
                term = [select id, Name from hed__Term__c where Term_Code__r.Term_Code__c =: updateTerm[0] and Academic_Career__c =: career[0] and Session__c =: session[0] and hed__Account__r.name =: listInstitution[0] limit 1];
            }
            //as term is required in course offering so throwing error if no term found(for now)
            if (term == null || term.size() <= 0) {
                responsetoSend = throwError();
                return responsetoSend;
            }
            //Get previous course offering record 
            if (term != null && term.size() > 0 && updateClassNumber != null && updateClassNumber.size() > 0) {
                listcourseOffrng = [select id,hed__Term__r.Term_Code__c,hed__Term__r.Term_Code__r.Term_Code__c, Name, hed__Term__c, Session_Code__c, Class_Section__c, Institution__c, Academic_Group__c, Subject__c, Catalog_Number__c, Career__c, Description__c, Class_Number__c, Course_Component__c, Enrolment_Status__c, Class_Status__c, Class_Type__c, Enrolment_Capacity__c, Enrolment_Total__c, Academic_Organization__c, Class_Coordinator__c, /*Campus__c,*/Location__c, Instruction_Mode__c, hed__Start_Date__c, hed__End_Date__c, Last_Day_to_Add__c, Census_Date__c
                                    from hed__Course_Offering__c where hed__Term__c =: term[0].ID and Name =: updateClassNumber[0] limit 1
                                   ];
                
            }
            system.debug('listcourseOffrng-->' + listcourseOffrng );
            //map to update the old courseOffrng
            map < String, hed__Course_Offering__c > oldcourseOffrng = new map < String, hed__Course_Offering__c > ();
            for (hed__Course_Offering__c oldCourseOff: listcourseOffrng) {                
                if (oldCourseOff.hed__Term__c != null && oldCourseOff.hed__Term__r.Term_Code__c != null) {
                    String uniqueKey = oldCourseOff.Name +''+ oldCourseOff.hed__Term__r.Term_Code__r.Term_Code__c ;//5 Oct 2018 Scope Change RM-1909 - Name is not a key value now
                    oldcourseOffrng.put(uniqueKey, oldCourseOff);
                }
            }
            //Store institution record
            if (listInstitution != null && listInstitution.size() > 0)
            {
                institution = [select id from account where Name =: listInstitution[0] Limit 1];
            }
            //store course and academic organization of course record from course ID
            if (listCourseId != null && listCourseId.size() > 0)
            {
                courseID = [select id, Academic_Organization__c from hed__Course__c where hed__Course_ID__c =: listCourseId[0] limit 1];
            }
            //check for course as it is required
            if (courseId == null || courseId.size() <= 0) {
                responsetoSend = throwError();
                return responsetoSend;
            }
            //Get campus record 
            if (listCampus != null && listCampus.size() > 0 && listInstitution != null && listInstitution.size() > 0)
            {
                campus = [select id from Campus__c where Name =: listCampus[0] and Institution__r.Name =: listInstitution[0] and Valid_Location_Code__c =:listLocation[0] limit 1];
            }
            //Get location record
            if (listLocation != null && listLocation.size() > 0)
            {
                location = [select id from location__C where Name =: listLocation[0] limit 1];
            }
            //insert or update the courseOffrng
            for (CLASS_REST courseOfferingRest: postString) {
                String key = courseOfferingRest.classNumber +''+ courseOfferingRest.term ;
                courseOffering = new hed__Course_Offering__c();
                courseOffering.Name = courseOfferingRest.classNumber;//courseOfferingRest.courseCode;//5 Oct 2018 Scope Change RM-1909 - courseCode is not a key value now
                courseOffering.hed__Course__c = courseID != null ? courseID[0].ID : null;
                courseOffering.Course_Offer_Number__c = courseOfferingRest.courseOfferingNumber != null ? Integer.valueOf(courseOfferingRest.courseOfferingNumber) : null;
                courseOffering.hed__Term__c = term.size() > 0 && term[0] != null ? term[0].ID : null;
                courseOffering.Session_Code__c = courseOfferingRest.session;
                courseOffering.Class_Section__c = courseOfferingRest.classSection;
                courseOffering.Institution__c = institution != null ? institution.ID : null;
                courseOffering.Academic_Group__c = courseOfferingRest.academicgroup;
                courseOffering.Subject__c = courseOfferingRest.subject;
                courseOffering.Catalog_Number__c = courseOfferingRest.catalogueNumber;
                courseOffering.Career__c = courseOfferingRest.academicCareer;
                courseOffering.Description__c = courseOfferingRest.description;
                //courseOffering.Class_Number__c = courseOfferingRest.classNumber;//Scope Change RM-1909 - Not to use Class Number for any mapping, instead map to Name
                courseOffering.Course_Component__c = courseOfferingRest.courseComponent;
                courseOffering.Enrolment_Status__c = courseOfferingRest.enrolmentStatus;
                courseOffering.Class_Status__c = courseOfferingRest.classStatus;
                courseOffering.Class_Type__c = courseOfferingRest.classType;
                courseOffering.Enrolment_Capacity__c = courseOfferingRest.enrolmentCapacity != null ? Integer.valueOf(courseOfferingRest.enrolmentCapacity) : null;
                courseOffering.Enrolment_Total__c = courseOfferingRest.enrolmentTotal != null ? Integer.valueOf(courseOfferingRest.enrolmentTotal) : null; 
                courseOffering.Academic_Organization__c = courseID != null && courseID[0].Academic_Organization__c != null ? courseID[0].Academic_Organization__c : null;
                courseOffering.Class_Coordinator__c = courseOfferingRest.classCoordinator;
                courseOffering.Campus__c = campus != null && campus.size() > 0 ? campus[0].ID : null;
                courseOffering.Location__c = location != null && location.size() > 0 ? location[0].Id : null;
                courseOffering.Instruction_Mode__c = courseOfferingRest.instructionMode;
                courseOffering.hed__Start_Date__c = courseOfferingRest.startDate != null ? date.valueOf(courseOfferingRest.startDate) : null;
                courseOffering.hed__End_Date__c = courseOfferingRest.endDate != null ? Date.valueOF(courseOfferingRest.endDate) : null;
                courseOffering.Last_Day_to_Add__c = courseOfferingRest.lastDayToAdd != null ? Date.valueOf(courseOfferingRest.lastDayToAdd) : null;
                courseOffering.Census_Date__c = courseOfferingRest.censusDate != null ? Date.valueOf(courseOfferingRest.censusDate) : null;
                system.debug('oldcourseOffrng-->' + oldcourseOffrng);
                system.debug('key-->' + key );
                //assigning the course offering id
                if (oldcourseOffrng.size() > 0 && oldcourseOffrng.get(key) != null)
                {
                    courseOffering.ID = oldcourseOffrng.get(key).ID;
                }
                
                hedUpsertClass.add(courseOffering);
            }
            //upsert courseOffering
            if (hedUpsertClass != null && hedUpsertClass.size() > 0) {
                upsert hedUpsertClass;
            }
            responsetoSend.put('message', 'OK');
            responsetoSend.put('status', '200');
            return responsetoSend;
        } catch (Exception excpt) {
            //If exception occurs then return this message to IPASS
            responsetoSend = throwError();
            return responsetoSend;
        }
        
    }
    public static Map < String, String > throwError() {
        //to store the error response
        Map < String, String > errorResponse = new Map < String, String > ();
        errorResponse.put('message', 'Oops! Class was not well defined, amigo.');
        errorResponse.put('status', '404');
        //If error occurs then return this message to IPASS
        return errorResponse;
    }
}