/**
 * @description Custom Exception for any problems with HTTP callouts
 */
public class HttpCalloutException extends Exception {}