/*******************************************
Purpose: Controller class of Feedback and Support Case Pages. Functionalities: Case Creation, assignment to queue, sending email, providing list of Topics and Sub Topics
History:
Created by Khushman Deomurari on 08/11/2018
Modified by Subhajit on 11/10/18
*******************************************/
public without sharing class ResearcherPortalCaseController {

     /*****************************************************************************************
     Global variable Declaration
    //****************************************************************************************/
    public static Boolean isForceExceptionRequired=false;//added by Ali
    
    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Provide Topics, Subtopics and Queue Names
    // Developer    :  Khushman Deomurari
    // Parameter    :  Void 
    // Created Date :  08/11/2018 
    // Modified Date:  11/10/2018
    // Modified By  :  Subhajit              
    //************************************************************************************/ 
    @AuraEnabled
    public static String getAllTopicSubtopicInfo(){
        
        Map<String, list<String>>  topicSubtopicQueueMapping = new Map<String, list<String>>();
       
        try{
            
            topicSubtopicQueueMapping = ResearcherPortalCaseHelper.getAllRecsFromTopicSubtopicQueueMapping();
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
           ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
          
        }
        
        System.debug('@@@@topicSubtopicQueueMapping ==>'+JSON.serializePretty(topicSubtopicQueueMapping));
        return JSON.serialize(topicSubtopicQueueMapping);
    }
    
    /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Fetch Feedback Topic picklist value for UI
    // Developer    :  Subhajit
    // Parameter    :  Void 
    // Created Date :  11/10/2018 
    //************************************************************************************/ 
    @AuraEnabled
    public static List<String> getAllfeedbackTopicInfo(){
        
        List<String> feedBackTopics = new List<String>();
        try{
            
            feedBackTopics = ResearcherPortalCaseHelper.getPicklistValues('Case','PortalFeedback__c');
           
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
            
        }
        catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
        }
        
        return feedBackTopics;
    }
    
     /*****************************************************************************************
    // JIRA NO      :  470/314/469
    // SPRINT       :  7
    // Purpose      :  Create feeback and research support case
    // Parameters   :  String
    // Developer    :  Subhajit
    // Created Date :  10/30/2018                 
    //****************************************************************************************/
    @AuraEnabled
    public static Id createCase(String caseWrapperString){
        
        System.debug('@@@@caseWrapperString ==>'+caseWrapperString);
         
        Id caseId; 
        ResearcherPortalCaseWrapper caseWrapper=(ResearcherPortalCaseWrapper)JSON.deserialize(caseWrapperString, ResearcherPortalCaseWrapper.class);
       
        try
        {
            // Creating Case records from caseWrapper
            caseId = ResearcherPortalCaseHelper.createNewCaseRecord(caseWrapper);
          
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
            
        }
        
        return caseId;
    }
    
     /*@Author : Gourav Bhardwaj
      @Story : RPORW-1423
      @Description : Created a seperate method to create case for Support Case, as existing wrapper has PMD issues and has huge impact if we modify it
      need to create a new wrapper for Support to avoid PMD and deploy functionality.
    */
    @AuraEnabled
    public static Id createSupportCase(String caseWrapperString){
        System.debug('@@@@caseWrapperString ==>'+caseWrapperString);
        Id caseId; 
        ResearcherPortalSupportCaseWrapper caseWrapper
        =(ResearcherPortalSupportCaseWrapper)JSON.deserialize(caseWrapperString, ResearcherPortalSupportCaseWrapper.class);
       
        try
        {
            // Creating Case records from caseWrapper
            caseId = ResearcherPortalSupportCaseHelper.createNewSupportCaseRecord(caseWrapper);
            ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
        }
        catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
            
        }
        
        return caseId;
    }
    //createSupportCase Ends
    
    //1416 : Method to handle file attachments of individual files after first file is uploaded.
    @AuraEnabled
    public static void attachFilesToCaseRecord(String caseAttachments,String caseId){
    	 try
        {
			system.debug('## caseAttachmentJSON : '+caseAttachments);
		    ResearcherPortalCaseWrapper.CaseAttachmentWrapper caseAttachmentWrapper=(ResearcherPortalCaseWrapper.CaseAttachmentWrapper)JSON.deserialize(caseAttachments, ResearcherPortalCaseWrapper.CaseAttachmentWrapper.class);
		    system.debug('## caseAttachmentWrapper : '+caseAttachmentWrapper);
		    Case caseRecord = new Case(Id=caseId);
		   	ResearcherPortalCaseHelper.createCaseAttachment(new List<ResearcherPortalCaseWrapper.CaseAttachmentWrapper>{caseAttachmentWrapper},caseRecord);
		    ResearcherExceptionHandlingUtil.createForceExceptionForTestCoverage(isForceExceptionRequired);
		    
        }  catch(Exception ex)
        {
            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap()); //added by Ali
            //Exception handling Error Log captured :: Ends
            System.debug(ex.getMessage()+'<== This exception Occured at line Number ==>' + ex.getLineNumber());
            
        }
    }
    
    @AuraEnabled
    public static string sendResearchSupportEmailNotification(Id caseid){
    
        try{
            system.debug('### caseId'+caseid);
            if((Schema.sObjectType.Case.isAccessible())&&(Schema.sObjectType.Case.fields.IsResearcherSupportEmailToBeSent__c.isAccessible())){//PMD fix by Ali
{
            Case theCase= [Select Id,IsResearcherSupportEmailToBeSent__c From Case where id=:caseId];
            if(!theCase.IsResearcherSupportEmailToBeSent__c){
                ResearcherSupportCaseEmailTemplateHelper.sendResearchSupportEmailNotification(caseId);
                }
              }  
            }
            return 'success';
            
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());
			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
            return 'Failure';
            
    	} 
    }
    
           
    
    
    
    /*
    @Description : RPORW-1426 - Method to return Researcher Location to the UI
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static List<String> getResearcherLocation(){
    	List < String > picklistValues = new List<String>();
    	try{
    		sObject obj = Schema.getGlobalDescribe().get('Case').newSObject();
            System.debug('obj@@@'+obj);

            picklistValues = SobjectUtils.getselectOptions(obj,'Researcher_Location__c');

            system.debug('## picklistValues :'+picklistValues);
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
            
    	}
    	 return picklistValues;
    }
    //getResearcherLocation Ends
    
     /*
    @Description : RPORW-1426 - Method to return Researcher Location to the UI
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static List<String> getIsQueryRelatedToProjectValues(){
    	List < String > picklistValues = new List<String>();
    	try{
    		sObject obj = Schema.getGlobalDescribe().get('Case').newSObject();
            System.debug('obj@@@'+obj);

            picklistValues = SobjectUtils.getselectOptions(obj,'Is_the_query_related_to_a_project__c');

            system.debug('## picklistValues :'+picklistValues);
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
            
    	}
    	 return picklistValues;
    }
    //getResearcherLocation Ends
    
    /*
    @Description : RPORW-1423 - Method to return Projects of the Logged In Member
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static ProjectDetailsWrapper.ResearchProjectListView getProjectsAssigned(){
    	ProjectDetailsWrapper.ResearchProjectListView researchProjects = new  ProjectDetailsWrapper.ResearchProjectListView(); 
        
        try{
            researchProjects = ResearchProjectHelper.getMyResearchProjectListViewData();
            system.debug('### getProjectsAssigned : '+researchProjects);
        }  
        catch(Exception ex)     
        {   
           //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
        } 
        return researchProjects;
    }
    //getProjectsAssigned Ends
    
    /*
    @Description : RPORW-1423 - Method to return Ethics of the Logged In Member
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static List<EthicsDetailWrapper.ethicsWrapper> getEthicsAssigned(){
       
        List<EthicsDetailWrapper.ethicsWrapper> ethicsWrapper = new List<EthicsDetailWrapper.ethicsWrapper>();
        try{
            
            ethicsWrapper = ViewMyEthicsHelper.getEthicsWrapperList();
        }catch(Exception ex){
            //Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

            //Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
        }
         return ethicsWrapper;
    }
    //getEthicsAssigned Ends
    
       /*
    @Description : RPORW-1423 - Method to return Contracts of the Logged In Member
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static List<ResearcherContractWrapper> getContractsAssigned(){
    	List<ResearcherContractWrapper> contractRecsWrapper = new List<ResearcherContractWrapper>();
    	try{
    		contractRecsWrapper = ResearcherContractHelper.getResearcherContractRecords();
            system.debug('## contractRecsWrapper :'+contractRecsWrapper);
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
    	}
    	 return contractRecsWrapper;
    }
    //getContractsAssigned Ends
    
    /*
    @Description : RPORW-1423 - Method to return Milestones of the Logged In Member
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static MilestonesDetailsWrapper.milestonesListWithNotificationWrapper getMilesthonesAssigned(){
    	MilestonesDetailsWrapper.milestonesListWithNotificationWrapper milestonewrapper;
    	try{
    		milestonewrapper = ViewMyMilestonesHelper.getMilestonesWrapperList();
            system.debug('## milestonewrapper :'+milestonewrapper);
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
    	}
    	 return milestonewrapper;
    }
    //getMilesthonesAssigned Ends
    
    /*
    @Description : RPORW-1423 - Method to return Publications of the Logged In Member
    @Author      : Gourav Bhardwaj
    @Created Date: 3/6/2019
    */
    @AuraEnabled
    public static PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper getPublicationAssigned(){
    	PublicationDetailsWrapper.PublicationDetailsWithTotalsWrapper getPublicationWrapperList;
    	try{
    		getPublicationWrapperList = ViewMyPublicationHelper.getPublicationWrapperList();
            system.debug('## getPublicationWrapperList :'+getPublicationWrapperList);
    	}catch(Exception ex){
    		//Exception handling Error Log captured :: Ends
            System.debug('@@@Exception occured: '+ex.getMessage());
            System.debug('@@@Exception occured: '+ex.getStackTraceString());

			//Exception handling Error Log captured :: Starts
            ResearcherExceptionHandlingUtil.addExceptionLog(ex,ResearcherExceptionHandlingUtil.getParamMap());
    	}
    	 return getPublicationWrapperList;
    }
    //getPublicationAssigned Ends
}