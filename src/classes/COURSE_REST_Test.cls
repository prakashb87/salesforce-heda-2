/*********************************************************************************
*** @TestClassName     : COURSE_REST_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 31/08/2018
**********************************************************************************/
@isTest
public class COURSE_REST_Test {
static testMethod void myUnitTest() {
    
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Course';
        
        //creating test data
        String JsonMsg = '{"courseCode": "009342","effectiveDate": "1997-01-01","status": "A","title": "Masters Degree Thesis","description": "Masters Degree Thesis","longDescription": "","minimumUnits": 48,"maximumUnits": 48,"academicProgressUnits": 48,"scheduledContactHours": 240,"gradingbasis": "G00"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSE_REST.upsertCourse();
        List < String > str = check.values();
        //assert condition 
       // System.assertEquals(true, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Course';
        
        //creating test data
        String JsonMsg = '{"courseCode": "","effectiveDate": "1997-01-01","status": "A","title": "Masters Degree Thesis","description": "Masters Degree Thesis","longDescription": "","minimumUnits": 48,"maximumUnits": 48,"academicProgressUnits": 48,"scheduledContactHours": 240,"gradingbasis": "G00"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSE_REST.upsertCourse();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(false, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = COURSE_REST.upsertCourse();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
           // System.assertEquals(true, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        
        Course_List__c courseList = new Course_List__c();
        
        courseList.Name = '009342';
        
        courseList.Effective_Date__c = Date.valueOf('1902-05-01');
             
        courseList.Course_ID__c = '';
        
        insert courseList;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Course';
        
        //creating test data
        String JsonMsg = '{"courseCode": "009342","effectiveDate": "1997-01-01","status": "A","title": "Masters Degree Thesis","description": "Masters Degree Thesis","longDescription": "","minimumUnits": 48,"maximumUnits": 48,"academicProgressUnits": 48,"scheduledContactHours": 240,"gradingbasis": "G00"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = COURSE_REST.upsertCourse();
        List < String > str = check.values();
        //assert condition 
        //System.assertEquals(false, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}