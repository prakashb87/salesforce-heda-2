/*********************************************************************************
*** @ClassName         : EnrolmentInboundServiceTest      
*** @Author            : Shubham Singh 
*** @Requirement       : This is a test class to test the scenarios implemented for EnrolmentInboundService class
*** @Created date      : 21/03/2019
*** @Modified by       : Shubham Singh 
*** @modified date     : 24/04/2019
**********************************************************************************/
@IsTest
public class EnrolmentInboundServiceTest {
    /**
     * -----------------------------------------------------------------------------------------------+
     * to create test data to be used in test class
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    testRecords
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    @testsetup
    public static void testRecords() {
        //insert contacts
        Contact con1 = new Contact(FirstName = 'TestCon', LastName = 'Test', Student_ID__c = '88002P');
        insert con1;
        Contact con2 = new Contact(FirstName = 'Testing', LastName = 'TestNew', Student_ID__c = '98765D');
        insert con2;
        //insert rmitu account
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert accounts Effective_Date__c(YY-MM-DD)
        Account acc1 = new Account(Name = 'Program Test 1', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-03-04'), Academic_Program__c = 'GD110', Academic_Career__c = 'PGRD');
        insert acc1;
        Account acc2 = new Account(Name = 'Program Test 2', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-04-04'), Academic_Career__c = 'PGRD');
        insert acc2;
        Account acc3 = new Account(Name = 'Program Test 3', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-05-04'), Academic_Career__c = 'PGRD');
        insert acc3;
        //insert fund source
        Fund_Source__c fundsource = new Fund_Source__c(Name = 'AA', Effective_Date__c = Date.ValueOf('1995-03-04'), Description__c = 'this is the fund source description');
        insert fundsource;
        //insert HECS
        HECS__c hcs = new HECS__c(Name = '202');
        insert hcs;
        //insert term
        Term__c trm = new Term__c();
        trm.Institution__c = rmit.Id;
        trm.Term_Code__c = '1310';
        insert trm;
        //insert term session
        hed__Term__c term = new hed__Term__c(Name = 'Term Test', Term_Code__c = trm.ID, Academic_Career__c = 'PGRD', hed__Account__c = rmit.ID, hed__Start_Date__c = Date.ValueOf('1996-05-05'));
        insert term;
        //insert course
        hed__Course__c course = new hed__Course__c();
        course.hed__Account__c = acc1.id;
        course.Status__c = 'Active';
        course.Name = 'Test Course';
        insert course;
        //insert coourse offering
        hed__Course_Offering__c hedCourseOffr = new hed__Course_Offering__c();
        hedCourseOffr.hed__Course__c = course.ID;
        hedCourseOffr.Name = '1554';
        hedCourseOffr.hed__Term__c = term.ID;
        hedCourseOffr.Class_Number__c = '1554';
        insert hedCourseOffr;
        //insert program enrollment
        hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c(Institution__c = rmit.ID, hed__Contact__c = con1.ID, Academic_Program__c = 'GD110', Career__c = 'PGRD', Student_Career_Number__c = '5');
        insert pe;
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Insert multiple enrolment records and check they are inserted or not
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    insertMultipleRecords
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static testMethod void insertMultipleRecords() {
        Contact con1 = new Contact(FirstName = 'TestConNew', LastName = 'Testing', Student_ID__c = '88002N');
        insert con1;
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert accounts Effective_Date__c(YY-MM-DD)
        Account acc1 = new Account(Name = 'Program Test 1', Academic_Institution__c = rmit.ID, AccountNumber = 'GD110', Effective_Date__c = Date.ValueOf('1996-03-04'), Academic_Career__c = 'PGRD');
        insert acc1;

        hed__Course_Enrollment__c hedCourseEnroll = new hed__Course_Enrollment__c(Official_Grade__c = 'AB', Academic_Career__c = 'PGRD', Institution__c = rmit.ID, hed__Account__c = acc1.Id, Class_Number__c = '1554', Term__c = '1310', hed__Contact__c = con1.ID, Course_Connection_Unique_Key__c = 'PGRD1554131088002PRMITU');
        insert hedCourseEnroll;
        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();

        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1.1/Enrolment';

        String jsonMsg = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "88002N","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "2013-01-15","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5},{"studentId": "98765D","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';

        req.requestBody = Blob.valueof(jsonMsg);
        Exception except = null;
        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        try {
            //store response
            Map < String, String > check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
        } catch (Exception e) {
            except = e;
            System.debug('@@@@This is Exception ' + e);
        }
        //assert condition         
        Test.stopTest();
        List < hed__Course_Enrollment__c > listEnroll = [Select id, Course_Connection_Unique_Key__c from hed__Course_Enrollment__c];
        system.debug('listEnroll'+listEnroll);
        System.assertEquals('PGRD1554131088002PRMITU', listEnroll.get(0).Course_Connection_Unique_Key__c);
        System.assertEquals('PGRD1554131088002NRMITU', listEnroll.get(1).Course_Connection_Unique_Key__c);
        System.assertEquals('PGRD1554131098765DRMITU', listEnroll.get(2).Course_Connection_Unique_Key__c);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check for mandatory fields, otherwise throw error
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    checkForNotFoundErrrors
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static testMethod void checkForNotFoundErrrors() {

        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();

        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1.1/Enrolment';
        //Contact not found error
        String jsonMsg = '[{"studentId": "88002Q","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';
        //Academic career not found
        String jsonMsg2 = '[{"studentId": "88002P","academicCareer": "PGRE","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';
        //Term not found
        String jsonMsg3 = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "13110","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';

        req.requestBody = Blob.valueof(jsonMsg);
        Exception except = null;
        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        try {
            //store response
            Map < String, String > check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
            req.requestBody = Blob.valueOf(jsonMsg);
            EnrolmentInboundService.handlePostInboundEnrolmentCall();
            req.requestBody = Blob.valueof(jsonMsg2);
            EnrolmentInboundService.handlePostInboundEnrolmentCall();
            req.requestBody = Blob.valueof(jsonMsg3);
            EnrolmentInboundService.handlePostInboundEnrolmentCall();

        } catch (Exception e) {
            except = e;
            System.debug('@@@@This is Exception ' + e);
        }

        //assert condition         
        Test.stopTest();
        List < hed__Course_Enrollment__c > listEnroll = [Select id, Course_Connection_Unique_Key__c from hed__Course_Enrollment__c];
        List < RMErrLog__c > errorLogs = [Select Id, Class_Name__c, Business_Function_Name__c, Error_Code__c, Error_Cause__c, Error_Message__c, Error_Time__c, Error_Type__c, Logged_In_User__c, Method_Name__c from RMErrLog__c];
        System.assertEquals(0, listEnroll.size());
        System.assertEquals(4, errorLogs.size());
        System.assertEquals('05', errorLogs[0].Error_Code__c);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check institution is there, otherwise throw error
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    accountError
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static testMethod void accountError() {
        //update the Instituion which already exist with name RMITU
        Account rmitu = [SELECT Id, Name FROM Account WHERE name = 'RMITU'
            LIMIT 1
        ];
        rmitu.Name = 'RMITUN';
        update rmitu;

        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();

        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1.1/Enrolment';
        //Institution not found error
        String jsonMsg = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';

        req.requestBody = Blob.valueof(jsonMsg);
        Exception except = null;
        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        try {
            //store response
            Map < String, String > check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
            req.requestBody = Blob.valueOf(jsonMsg);
        } catch (Exception e) {
            except = e;
            System.debug('@@@@This is Exception ' + e);
        }
        List < hed__Course_Enrollment__c > listEnroll = [Select id, Course_Connection_Unique_Key__c from hed__Course_Enrollment__c];
        List < RMErrLog__c > errorLogs = [Select Id, Class_Name__c, Business_Function_Name__c, Error_Code__c, Error_Cause__c, Error_Message__c, Error_Time__c, Error_Type__c, Logged_In_User__c, Method_Name__c from RMErrLog__c];
        System.assertEquals(0, listEnroll.size());
        System.assertEquals('05', errorLogs[0].Error_Code__c);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * Check Course offering is there, otherwise throw error
     * ------------------------------------------------------------------------------------------------
     * @author	    Shubham Singh 
     * @method      courseOfferingError
     * @param       None
     * @return      None
     * @Reference   RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    public static testMethod void courseOfferingError() {
        RestRequest req = new RestRequest();

        RestResponse res = new RestResponse();

        // adding header to the request
        req.addHeader('httpMethod', 'POST');

        req.requestUri = '/services/apexrest/HEDA/v1.1/Enrolment';
        //classNumber not found error
        String jsonMsg = '[{"studentId": "88002P","academicCareer": "PGRD","institution": "RMITU","term": "1310","classNumber": 15546,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';

        req.requestBody = Blob.valueof(jsonMsg);
        Exception except = null;
        RestContext.request = req;

        RestContext.response = res;

        Test.startTest();
        try {
            //store response
            Map < String, String > check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
            req.requestBody = Blob.valueOf(JsonMsg);
        } catch (Exception e) {
            except = e;
            System.debug('@@@@This is Exception ' + e);
        }

        //assert condition         
        Test.stopTest();
        List < hed__Course_Enrollment__c > listEnroll = [Select id, Course_Connection_Unique_Key__c from hed__Course_Enrollment__c];
        List < RMErrLog__c > errorLogs = [Select Id, Class_Name__c, Business_Function_Name__c, Error_Code__c, Error_Cause__c, Error_Message__c, Error_Time__c, Error_Type__c, Logged_In_User__c, Method_Name__c from RMErrLog__c];
        System.assertEquals(0, listEnroll.size());
        System.assertEquals('05', errorLogs[0].Error_Code__c);
    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * To throw an exception if running user does not have access to the course connection
     * ------------------------------------------------------------------------------------------------
     * @author	 Shubham Singh 
     * @method    exampleException
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    @IsTest
    public static void exampleException() {
        //insert contact
        Contact con1 = new Contact(FirstName = 'TestCon', LastName = 'Test', Student_ID__c = '8700P');
        insert con1;
        //insert rmitu account
        Account rmit = new Account(Name = 'RMITU');
        insert rmit;
        //insert accounts Effective_Date__c(YY-MM-DD)
        Account acc1 = new Account(Name = 'Program Test 1', Academic_Institution__c = rmit.ID, AccountNumber = 'AN0001', Effective_Date__c = Date.ValueOf('1996-03-04'), Academic_Career__c = 'PGRD');
        insert acc1;
        //insert term
        Term__c trm = new Term__c();
        trm.Institution__c = rmit.Id;
        trm.Term_Code__c = '13190';
        insert trm;
        //insert term session
        hed__Term__c term = new hed__Term__c(Name = 'Term Test', Term_Code__c = trm.ID, Academic_Career__c = 'PGRD', hed__Account__c = rmit.ID, hed__Start_Date__c = Date.ValueOf('1996-05-05'));
        insert term;
        //insert course
        hed__Course__c course = new hed__Course__c();
        course.hed__Account__c = acc1.id;
        course.Status__c = 'Active';
        course.Name = 'Test Course';
        insert course;
        //insert coourse offering
        hed__Course_Offering__c hedCourseOffr = new hed__Course_Offering__c();
        hedCourseOffr.hed__Course__c = course.ID;
        hedCourseOffr.Name = '1554';
        hedCourseOffr.hed__Term__c = term.ID;
        hedCourseOffr.Class_Number__c = '1554';
        insert hedCourseOffr;
        //insert program enrollment
        hed__Program_Enrollment__c pe = new hed__Program_Enrollment__c(Institution__c = rmit.ID, hed__Contact__c = con1.ID, Academic_Program__c = 'GD110', Career__c = 'PGRD', Student_Career_Number__c = '1');
        insert pe;
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        Profile p = [SELECT Id FROM Profile WHERE Name = 'RMIT Read-Only User'];
        User newUser = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
            EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles', UserName = uniqueUserName);

        System.runAs(newUser) {
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.addHeader('httpMethod', 'POST');
            req.requestURI = '/HEDA/v1.1/Enrolment';
            String jsonMsg = '[{"studentId": "8700P","academicCareer": "PGRD","institution": "RMITU","term": "13190","classNumber": 1554,"session": "RAS","status": "D","statusReason": "DROP","lastAction": "D","lastActionReason": "WDRW","statusDate": "2013-01-15","addDate": "2013-01-15","dropDate": "2013-03-13","unitsTaken": 12,"progressUnits": 12,"billingUnits": 12,"gradingBasis": "G00","gradingBasisDate": "2013-01-15","officialGrade": "","gradeInput": "","gradeDate": "","academicProgram": "GD110","hecsExemptionStatusCode": "202","fundingSourceCode": "AA","studentCareerNumber": 5}]';
            req.requestBody = Blob.valueof(jsonMsg);
            RestContext.request = req;
            RestContext.response = res;
            Exception except = null;
            Test.startTest();
            Map < String, String > check;
            check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
        }
        Test.stopTest();
        List < hed__Course_Enrollment__c > listEnroll = [Select id, Course_Connection_Unique_Key__c from hed__Course_Enrollment__c];
        System.assertEquals(0, listEnroll.size());

    }
    /**
     * -----------------------------------------------------------------------------------------------+
     * To throw an exception for missiong parameter
     * ------------------------------------------------------------------------------------------------
     * @author	  Shubham Singh 
     * @method    exampleMissingParameter
     * @param     None
     * @return    None
     * @Reference RM-2580
     * -----------------------------------------------------------------------------------------------+
     */
    @IsTest
    public static void exampleMissingParameter() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addHeader('httpMethod', 'POST');
        req.requestURI = '/HEDA/v1.1/Enrolment';
        String jsonMsg = '["academicCareer": "PGRD"}]';

        req.requestBody = Blob.valueof(jsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        Exception except = null;
        Test.startTest();
        Map < String, String > check;
        try {
            check = EnrolmentInboundService.handlePostInboundEnrolmentCall();
        } catch (Exception e) {
            except = e;
        }
        Test.stopTest();
        List < RMErrLog__c > errorLogs = [Select Id, Class_Name__c, Business_Function_Name__c, Error_Code__c, Error_Cause__c, Error_Message__c, Error_Time__c, Error_Type__c, Logged_In_User__c, Method_Name__c from RMErrLog__c];
        System.assertEquals(errorLogs.size(), 1);
        System.assertEquals('EnrolmentInboundService', errorLogs[0].Business_Function_Name__c);
        System.assertEquals('Invalid Payload', errorLogs[0].Error_Cause__c);
        System.assertEquals('01', errorLogs[0].Error_Code__c);
        System.assertEquals('Invalid payload. Could not continue processing.', errorLogs[0].Error_Message__c);
        System.assertNotEquals(null, errorLogs[0].Error_Time__c);
        System.assertEquals('Integration', errorLogs[0].Error_Type__c);
        System.assertEquals(UserInfo.getUserId(), errorLogs[0].Logged_In_User__c);
        System.assertEquals(check.get('message'), 'Invalid payload on request');
        System.assertEquals(400, RestContext.response.statusCode);

    }

}