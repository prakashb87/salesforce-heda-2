/*****************************************************************
Name: CourseEnrollmentTriggerHandlerTest
Author: Pawan [CloudSense]
Purpose: Test Class For CourseEnrollmentTriggerHandler 
*****************************************************************/
/*==================================================================
History
--------
Version   Author           Date              Detail
1.0       Pawan            16/10/2018         Initial Version
********************************************************************/
@isTest 
public class CourseEnrollmentTriggerHandlerTest {
    
    @testSetup
    static void createTestData() {
        List<Config_Data_Map__c> configDataMaps = new List<Config_Data_Map__c>();
        configDataMaps.add(new Config_Data_Map__c(Name = 'OptOut', Config_Value__c = 'Opt Out'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Transferred', Config_Value__c = 'Transferred'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Cancelled', Config_Value__c = 'Cancelled'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'RMITOnline', Config_Value__c = 'RMIT Online'));
        
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxCall', Config_Value__c = 'iPaaS Open Edx Call Error'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassAPI', Config_Value__c = 'https://ipaas.com/api/openedx/enrollment'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassClientID', Config_Value__c = 'salesforce123test'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'OpenEdxIpassCientSecret', Config_Value__c = 'abc123'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'Enroll', Config_Value__c = 'Enroll'));
        configDataMaps.add(new Config_Data_Map__c(Name = 'UnEnroll', Config_Value__c = 'UnEnroll'));
        insert configDataMaps;
        TestDataFactoryUtilRefOne.deltaTestSuccess(); 
        
    }
    
    @isTest
    public static void testCourseEnrollmentTriggerHandler(){                
       
         Account accDepartment = TestUtility.createTestAccount(true
            , 'RMIT Online'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());
            
         Account accAdministrative = TestUtility.createTestAccount(true
            , 'Administrative Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Administrative').getRecordTypeId());

        Account accProgram = TestUtility.createTestAccount(true
            , 'Program Account 001'
            , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId());
        
        Opportunity opp = TestUtility.createTestOpportunity(true, 'Test Opportunity', accDepartment.Id);
        List<String> lstParams = new List<String>{'Test', 'Con'};        
        Contact con = TestUtility.createTestContact(true
            ,lstParams
            , accAdministrative.Id);
        
        User u = TestUtility.createUser('Partner Community User', false);
        u.ContactId = con.Id;
        insert u;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition'
            , cscfga__Product_Category__c = productCategory.Id
            , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        cscfga__Attribute_Definition__c attrDef = new cscfga__Attribute_Definition__c(Name = 'Test Attribute Definition'
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Is_Line_Item__c = true
            , cscfga__Line_Item_Description__c = 'Sample Attr'
            , cscfga__Line_Item_Sequence__c = 0 
            , cscfga__is_significant__c = true);
        insert attrDef;
        
        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;

        hed__Course__c course = new hed__Course__c(Name = 'Course 001'
            , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('21CC').getRecordTypeId()
            , hed__Account__c = accDepartment.Id);
        insert course;
       
        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
            , hed__Course__c = course.Id
            , hed__Term__c = term.Id 
            , hed__Start_Date__c = system.today()
            , hed__End_Date__c  = system.today());
        insert courseOffering;
        
        Test.startTest();  

        hed__Course_Enrollment__c courseEnrolment = new hed__Course_Enrollment__c(hed__Status__c = 'Enrolled'
            , hed__Course_Offering__c = courseOffering.id
            , hed__Contact__c = con.Id);
        insert courseEnrolment;
        
        courseEnrolment.hed__Status__c = 'Cancelled';
        update courseEnrolment;
        System.AssertEquals(courseEnrolment.hed__Status__c,'Cancelled','Invalid Data');
        Test.setMock(HttpCalloutMock.class, new iPassOpenEdxApiCallServiceMock()); 
        Test.stopTest();
        
    }        

    public class iPassOpenEdxApiCallServiceMock implements HttpCalloutMock {        
        public HTTPResponse respond(HTTPRequest request) {           
            String jsonBody = '{"id": "ID-dev-20180718T035647828","result": "ERROR","code": "400","application": "rmit-publisher-api-sfdc","provider": "iPaaS","payload": "Message Sending Failed"}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(jsonBody);
            response.setStatusCode(400);
            return response;
        }   
    }
  
}