//Author: Khushman Deomurari
//Cyclomatic Complexity Reached. No new code can be added in this file.
public with sharing class Student_REST_Helper {
    public static void handleGeneralDetails(Student_REST postString, Contact extractedContact){
        extractedContact.Birthdate = postString.DateOfBirth != null && postString.DateOfBirth.length() > 0 ? Date.valueOf(postString.DateOfBirth) : null;
        extractedContact.hed__Country_of_Origin__c = postString.CountryOfBirth;
        extractedContact.hed__Deceased__c = false;
        if(postString.DateOfDeath.length() > 0){
            extractedContact.hed__Deceased__c = true;    
        }
    }
    
    public static void setEffectiveDatesFromPersonalDetails(Student_REST postString, List<Date> personalEffDateList, Map<Date, Student_Rest.PersonalDetail> mapOfPersonalEffDate){
        // Parsing personal Details
        if(postString.PersonalDetail != null){
            for(Student_Rest.PersonalDetail perSRN : postString.PersonalDetail){
                if(Date.valueOf(perSRN.effectiveDate) <=System.today()){
                    personalEffDateList.add(Date.valueOf(perSRN.effectiveDate));
                    mapOfPersonalEffDate.put(Date.valueOf(perSRN.effectiveDate), perSRN);
                }
            }
        }
    }

	public static void handleEffectiveDatePRI(List<Date> effectiveDateListPRI, Map<Date, Student_REST.Name> mapOfdatePRIandName, Contact extractedContact){
	            Integer listSizePRI = effectiveDateListPRI.size()-1;
                extractedContact.FirstName = mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]) != null && mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).firstName != null ? mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).firstName : null;
                extractedContact.LastName = mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]) != null && mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).lastName != null ? mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).lastName : null;
                extractedContact.Middle_Name__c = mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]) != null && mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).middleName != null? mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).middleName : null;
                extractedContact.Salutation = mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]) != null && mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).prefix != null ? mapOfdatePRIandName.get(effectiveDateListPRI[listSizePRI]).prefix : null;
	}

	public static void handleEffectiveDatePREF(List<Date> effectiveDateListPREF, Map<Date, Student_REST.Name> mapOfdatePREFandName, Contact extractedContact){
            Integer listSizePREF;
            if(effectiveDateListPREF.size() > 0)
            {
                listSizePREF = effectiveDateListPREF.size()-1;
            }
            if(listSizePREF >= 0){
                extractedContact.Preferred_First_Name__c = mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF]) != null && mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF]).firstName != null ? mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF]).firstName : null;
                extractedContact.Preferred_Name__c = mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF])  != null && mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF]).displayName  != null ? mapOfdatePREFandName.get(effectiveDateListPREF[listSizePREF]).displayName : null;
            }
	}

	public static void updateDateList(Student_REST.Name srn, List<Date> effectiveDateList, Map<Date, Student_REST.Name> mapOfdateandName){
                if(Date.valueOf(srn.effectiveDate) <= System.today()){
                    effectiveDateList.add(Date.valueOf(srn.effectiveDate));
                    mapOfdateandName.put(Date.valueOf(srn.effectiveDate) ,srn);    
                }
	}
	public static void updateDateListPRI(Student_REST.Name srn, List<Date> effectiveDateListPRI, Map<Date, Student_REST.Name> mapOfdatePRIandName){
                if(srn.nameType == 'PRI' && Date.valueOf(srn.effectiveDate) <= System.today()){
                    effectiveDateListPRI.add(Date.valueOf(srn.effectiveDate));
                    mapOfdatePRIandName.put(Date.valueOf(srn.effectiveDate) ,srn);
                }
	}
	public static void updateDateListPRF(Student_REST.Name srn, List<Date> effectiveDateListPREF, Map<Date, Student_REST.Name> mapOfdatePREFandName){
                if(srn.nameType == 'PRF' && Date.valueOf(srn.effectiveDate) <= System.today()){
                    effectiveDateListPREF.add(Date.valueOf(srn.effectiveDate));
                    mapOfdatePREFandName.put(Date.valueOf(srn.effectiveDate) ,srn);
                }
	}
	public static void setStatusAsPerEffectiveStatus(Student_REST.Name nameStatus, Contact extractedContact){
		// Since JSON File has effectiveStatus for Contact is A for Active and I for Inactive whereas in Contact it is Active or Inactive
            if (nameStatus.effectiveStatus == 'A')
            {
                extractedContact.Status__c = 'Active';
            }
            else
            {
                extractedContact.Status__c = 'Inactive';
            }
	}
	
	public static void sortDateLists(List<Date> effectiveDateList, List<Date> effectiveDateListPRI, List<Date> effectiveDateListPREF){
            effectiveDateList.sort();
            effectiveDateListPRI.sort();
            effectiveDateListPREF.sort();
	}

}