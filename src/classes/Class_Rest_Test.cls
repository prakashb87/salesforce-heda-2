/*********************************************************************************
*** @TestClassName     : Class_Rest_Test
*** @Author            : Shubham Singh
*** @Requirement       : To Test the Location_Rest web service apex class.
*** @Created date      : 28/08/2018
**********************************************************************************/
@isTest
public class Class_Rest_Test {
    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'Testing Course';
        course.hed__Course_ID__c = '014794';
        course.hed__Account__c = acc.ID;
        insert course;
        
        Term__c termSession1 = new Term__c();
        termSession1.Institution__c = institution.Id;
        termSession1.Term_Code__c = '0250';
        insert termSession1;
        
        hed__Term__c termSession = new hed__Term__c();
        termSession.Name = 'Test';
        termSession.hed__Account__c = institution.Id;
        termSession.Term_Code__c = termSession1.Id;
        termSession.Academic_Career__c = 'UGRD';
        termSession.Session__c = 'RAS';
        insert termSession;
        
        Campus__c campus = new Campus__c();
        campus.Name = 'Campus name';
        //campus.Campus__c = 'AUSCY';
        campus.Institution__c = institution.ID;
        insert campus;
        
        location__C location = new location__C();
        location.name = 'Test location';
        location.Location__c = 'CTY';
        insert location;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"courseCode": "014794","courseOfferingNumber": 1,"term": "0250","session": "RAS","classSection": "T3","institution": "RMITU","academicgroup": "BUS","subject": "OMGT","catalogueNumber": "1053","academicCareer": "UGRD","description": "Advanced Supply Chain Managt","classNumber": 1657,"courseComponent": "TUT","enrolmentStatus": "O","classStatus": "A","classType": "E","enrolmentCapacity": 30,"enrolmentTotal": 28,"academicOrganisation": "167","classCoordinator": "","campus": "AUSCY","location": "CTY","instructionMode": "FF","startDate": "2002-07-22","endDate": "2002-10-27","lastDayToAdd": "2002-08-05","censusDate": "2002-08-31"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CLASS_REST.upsertClass();
        List < String > str = check.values();
        System.debug('Check----------------> '+ str);
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverError() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'Testing Course';
        course.hed__Course_ID__c = '014794';
        course.hed__Account__c = acc.ID;
        insert course;
        
        Term__c trm = new Term__c();
        trm.Institution__c = institution.Id;
        trm.Term_Code__c = '0250';
        insert trm;
        
        hed__Term__c termSession = new hed__Term__c();
        termSession.Name = 'Test';
        termSession.hed__Account__c = acc.Id;
        termSession.Term_Code__c = trm.ID;
        termSession.Academic_Career__c = 'UGRD';
        termSession.Session__c = 'RAS';
        insert termSession;
        
        Campus__c campus = new Campus__c();
        campus.Name = 'Campus name';
        //campus.Campus__c = 'AUSCY';
        campus.Institution__c = institution.ID;
        insert campus;
        
        location__C location = new location__C();
        location.name = 'Test location';
        location.Location__c = 'CTY';
        insert location;
        
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"courseCode": "","courseOfferingNumber": 1,"term": "","session": "RAS","classSection": "T3","institution": "RMITU","academicgroup": "BUS","subject": "OMGT","catalogueNumber": "1053","academicCareer": "UGRD","description": "Advanced Supply Chain Managt","classNumber": 1657,"courseComponent": "TUT","enrolmentStatus": "O","classStatus": "A","classType": "E","enrolmentCapacity": 30,"enrolmentTotal": 28,"academicOrganisation": "167","classCoordinator": "","campus": "AUSCY","location": "CTY","instructionMode": "FF","startDate": "2002-07-22","endDate": "2002-10-27","lastDayToAdd": "2002-08-05","censusDate": "2002-08-31"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CLASS_REST.upsertClass();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(False, str[0] == 'ok');
        
        Test.stopTest();
        
    }
    static testMethod void coverTryCatch() {
        Map < String, String > check;
        Test.startTest();
        try {
            //store response
            check = CLASS_REST.upsertClass();
        } catch (DMLException e) {
            List < String > str = check.values();
            //assert condition 
            System.assertEquals(False, str[0] == 'ok');
        }
        Test.stopTest();
    }
    static testMethod void updateMethod() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Account institution = new Account();
        institution.Name = 'RMITU';
        insert institution;
        
        hed__Course__c course = new hed__Course__c();
        course.Name = 'Testing Course';
        course.hed__Course_ID__c = '014794';
        course.hed__Account__c = acc.ID;
        insert course;
        
        Term__c trm = new Term__c();
        trm.Institution__c = institution.Id;
        trm.Term_Code__c = '0250';
        insert trm;
        
        hed__Term__c termSession = new hed__Term__c();
        termSession.Name = 'Test';
        termSession.hed__Account__c = institution.Id;
        termSession.Term_Code__c = trm.ID;
        termSession.Academic_Career__c = 'UGRD';
        termSession.Session__c = 'RAS';
        insert termSession;
        
        Campus__c campus = new Campus__c();
        campus.Name = 'Campus name';
        //campus.Campus__c = 'AUSCY';
        campus.Institution__c = institution.ID;
        insert campus;
        
        location__C location = new location__C();
        location.name = 'Test location';
        location.Location__c = 'CTY';
        insert location;
        
        hed__Course_Offering__c courseOff = new hed__Course_Offering__c();
        courseOff.Name = '014794';
        courseOff.hed__Course__c = course.ID;
        courseOff.hed__Term__c = termSession.ID;
        insert courseOff;
            
        RestRequest req = new RestRequest();
        
        RestResponse res = new RestResponse();
        // adding header to the request
        req.addHeader('httpMethod', 'POST');
        
        req.requestUri = '/services/apexrest/HEDA/v1/Class';
        
        //creating test data
        String JsonMsg = '{"courseCode": "014794","courseOfferingNumber": 1,"term": "0250","session": "RAS","classSection": "T3","institution": "RMITU","academicgroup": "BUS","subject": "OMGT","catalogueNumber": "1053","academicCareer": "UGRD","description": "Description is changed","classNumber": 1657,"courseComponent": "TUT","enrolmentStatus": "O","classStatus": "A","classType": "E","enrolmentCapacity": 30,"enrolmentTotal": 28,"academicOrganisation": "167","classCoordinator": "","campus": "AUSCY","location": "CTY","instructionMode": "FF","startDate": "2002-07-22","endDate": "2002-10-27","lastDayToAdd": "2002-08-05","censusDate": "2002-08-31"}';
        
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        
        RestContext.response = res;
        
        Test.startTest();
        //store response
        Map < String, String > check = CLASS_REST.upsertClass();
        List < String > str = check.values();
        //assert condition 
        System.assertEquals(True, str[0] == 'ok');
        
        Test.stopTest();
        
    }
}