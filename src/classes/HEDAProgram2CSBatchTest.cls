@isTest
private class HEDAProgram2CSBatchTest {

	@testSetup static void setup() {
		Account account21CC = New Account(name = '21CC');
		Insert account21CC;

		Account accountRMITOnline = New Account(name = 'RMIT Online');
		Insert accountRMITOnline;

		Config_Data_Map__c account21CCCustomeSetting = new Config_Data_Map__c();
		account21CCCustomeSetting.Name = 'AccountID_21CC';
		account21CCCustomeSetting.Config_Value__c = account21CC.id;
		Insert account21CCCustomeSetting;

		Config_Data_Map__c accountRMITOnlineCustomeSetting = new Config_Data_Map__c();
		accountRMITOnlineCustomeSetting.Name = 'AccountID_RMITOnline';
		accountRMITOnlineCustomeSetting.Config_Value__c = accountRMITOnline.id;
		Insert accountRMITOnlineCustomeSetting;


		cspmb__Add_On_Price_Item__c addOn1 = new cspmb__Add_On_Price_Item__c();
		addOn1.name = 'All';
		Insert addOn1;

		cspmb__Add_On_Price_Item__c addOn2 = new cspmb__Add_On_Price_Item__c();
		addOn2.name = 'Assessment Only';
		Insert addOn2;


		cspmb__Add_On_Price_Item__c addOn3 = new cspmb__Add_On_Price_Item__c();
		addOn3.name = 'Content Only';
		Insert addOn3;
	}

	@isTest static void testHEDAProgram2CSBatch() {
		Account rmitOnlineAccount = [Select Id from Account where name = 'RMIT Online'];

		// Create test data
		Id recordTypeIdProgram = Schema.SObjectType.account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
		Account testProgram = New Account();
		testProgram.Name = 'iOS App Development with Swift';
		testProgram.RecordTypeId = recordTypeIdProgram;
		testProgram.ParentId = rmitOnlineAccount.id;
		Insert testProgram;

		hed__Program_Plan__c testProgramPlan = New hed__Program_Plan__c ();
		testProgramPlan.Name = 'iOS App Development with Swift Program Plan V2.0';
		testProgramPlan.hed__Is_Primary__c = True;
		testProgramPlan.hed__Account__c = testProgram.Id;
		Insert testProgramPlan;


		Test.startTest();

		HEDAProgram2CSBatch batchJob = new HEDAProgram2CSBatch();
		Id batchId = Database.executeBatch(batchJob, 10);

		Test.stopTest();

		List<cspmb__Price_Item__c> priceItems = [Select Id, Name,
		                           Course_ID__c, Course_Offering_ID__c,
		                           cspmb__Product_Definition_Name__c,
		                           cspmb__Effective_Start_Date__c,
		                           cspmb__Effective_End_Date__c,
		                           External_Unique_ID__c from cspmb__Price_Item__c];


		List<cspmb__Price_Item_Add_On_Price_Item_Association__c> priceItemAddonAssociations = [Select Id, Name from cspmb__Price_Item_Add_On_Price_Item_Association__c];

		System.assertEquals(1, priceItems.size());

		System.assertEquals(3, priceItemAddonAssociations.size());

	}

}