public class EmbeddedCredentialsBatch implements Database.Batchable<Id> {
            
    list<csapi_BasketRequestWrapper.AddProductRequest> lstAddMicroCredentialRequest;
    
    set<id> microCourseOfferIds;
    set<id> embedCourseOfferIds;
    set<id> setStudentIdsAssigned;
    set<id> setStudentIdsToProcess;
    
    map<id, id> mapStudentToUser;
    map<id, set<id>> mapRelatedCourseToCourseSet;
    map<id, set<id>> mapStudentToMicroCredentialsSet;
    map<id, set<id>> mapStudentToEmbededCredentialsSet;
    map<Id, cspmb__Price_Item__c> mapCourseToPriceItemObj;
    
    csapi_BasketRequestWrapper.csapi_CourseDetails initializeCourse(cspmb__Price_Item__c priceItem) {
        
        csapi_BasketRequestWrapper.csapi_CourseDetails course = new csapi_BasketRequestWrapper.csapi_CourseDetails();
 
        course.CourseName = priceItem.Name;
        course.ProductLine = priceItem.Product_Line__c;
        course.CourseType = priceItem.Course_Program__c;
        course.CourseOfferingId = priceItem.Course_Offering_ID__c;
        course.ProductReference = priceItem.cspmb__Product_Definition_Name__c;
        course.SessionStartDate = String.valueOf(priceItem.cspmb__Effective_Start_Date__c);
        course.SessionEndDate = String.valueOf(priceItem.cspmb__Effective_End_Date__c);
        
        return course;
    }
    
    void initRelatedCourseToMicroCredentialsMap() {
        
                
        ///
        /// 1. condition: Today >= Trigger Date and Trigger Date < Census Date and Today =< census date
        /// 2. condition: Today == Trigger Date and Trigger Date >= Census Date
        /// 
        
        Date dtToday = Date.today();
        
        mapRelatedCourseToCourseSet = new map<id, set<id>>();
        microCourseOfferIds = new set<id>();   
        for (Course_Offering_Relationship__c cor: [select id
                , Course_Offering__c
                , Related_Course_Offering__c
                , Related_Course_Offering__r.Census_Date__c
                , Status__c
                , Enrolment_Trigger_Date__c
                , Enrolment_Census_Date__c
                , RecordType.Name
            from 
                Course_Offering_Relationship__c 
            where
                Status__c = 'Active' and
                RecordType.Name = 'Embedded' and 
                ((Enrolment_Trigger_Date__c = :dtToday) or
                 (Enrolment_Trigger_Date__c < :dtToday and Enrolment_Census_Date__c >= :dtToday))]) {
                
            if ((cor.Enrolment_Trigger_Date__c == dtToday && cor.Enrolment_Trigger_Date__c >= cor.Enrolment_Census_Date__c) || 
                (cor.Enrolment_Trigger_Date__c <= dtToday && cor.Enrolment_Trigger_Date__c < cor.Enrolment_Census_Date__c)) {
                
                if (!mapRelatedCourseToCourseSet.containsKey(cor.Related_Course_Offering__c)) {  
                    mapRelatedCourseToCourseSet.put(cor.Related_Course_Offering__c, new set<id>());
                }
                mapRelatedCourseToCourseSet.get(cor.Related_Course_Offering__c).add(cor.Course_Offering__c);
                microCourseOfferIds.add(cor.Course_Offering__c);
            }
        }
        
        embedCourseOfferIds = mapRelatedCourseToCourseSet.keySet();
        
        system.debug('initRelatedCourseToMicroCredentialsMap|mapRelatedCourseToCourseSet >> ' + mapRelatedCourseToCourseSet);
        system.debug('initRelatedCourseToMicroCredentialsMap|embedCourseOfferIds >> ' + embedCourseOfferIds);
        system.debug('initRelatedCourseToMicroCredentialsMap|microCourseOfferIds >> ' + microCourseOfferIds);
    }
    
    void initMicroCourseToPriceItemObjMap(set<id> microCourseOfferIds) {
        
        mapCourseToPriceItemObj = new map<Id, cspmb__Price_Item__c>();
        for (cspmb__Price_Item__c priceItem: [select Id
                    , Name
                    , Course_Program__c
                    , Product_Line__c
                    , Course_ID__c
                    , Course_Offering_ID__c
                    , cspmb__Effective_Start_Date__c
                    , cspmb__Effective_End_Date__c
                    , cspmb__Product_Definition_Name__c
                from
                    cspmb__Price_Item__c
                where 
                    Course_Offering_ID__c in :microCourseOfferIds]) {
                                
            mapCourseToPriceItemObj.put(priceItem.Course_Offering_ID__c, priceItem);
        }
        
        system.debug('initMicroCourseToPriceItemObjMap|mapCourseToPriceItemObj >> ' + mapCourseToPriceItemObj);
    }
    
    void initStudentToMicroCredentialsMap(set<id> microCourseOfferIds) {
        
        mapStudentToMicroCredentialsSet = new map<id, set<id>>();
        for (hed__Course_Enrollment__c enrollment: [select Id
                , hed__Contact__c
                , hed__Course_Offering__c
            from 
                hed__Course_Enrollment__c 
            where 
                hed__Course_Offering__c in :microCourseOfferIds and
                (hed__Status__c = :System.Label.Course_Assessment_Complete_Status or hed__Status__c = :System.Label.Course_Current_Status)]) {
            
            if (!mapStudentToMicroCredentialsSet.containsKey(enrollment.hed__Contact__c)) {
                mapStudentToMicroCredentialsSet.put(enrollment.hed__Contact__c, new set<id>());
            }
            mapStudentToMicroCredentialsSet.get(enrollment.hed__Contact__c).add(enrollment.hed__Course_Offering__c);
        }
        
        system.debug('initStudentToMicroCredentialsMap|mapStudentToMicroCredentialsSet >> ' + mapStudentToMicroCredentialsSet);
    }
    
    void initStudentToEmbededCredentialsMap(set<Id> embedCourseOfferIds) {
        
        mapStudentToEmbededCredentialsSet = new map<id, set<id>>();
        for (hed__Course_Enrollment__c enrollment: [select Id
                , hed__Contact__c
                , hed__Course_Offering__c
                , hed__Course_Offering__r.hed__Course__c
                , hed__Course_Offering__r.hed__Course__r.Name
            from 
                hed__Course_Enrollment__c 
            where                               
                Enrolment_Status__c = :System.Label.Embedded_Course_Enrollment_Status and 
                hed__Course_Offering__c in :embedCourseOfferIds]) {
            
            if (!mapStudentToEmbededCredentialsSet.containsKey(enrollment.hed__Contact__c)) {
                mapStudentToEmbededCredentialsSet.put(enrollment.hed__Contact__c, new set<id>());
            }
            mapStudentToEmbededCredentialsSet.get(enrollment.hed__Contact__c).add(enrollment.hed__Course_Offering__c);              
        }
        
        setStudentIdsAssigned = mapStudentToEmbededCredentialsSet.keySet();
        
        system.debug('initStudentToEmbededCredentialsMap|mapStudentToEmbededCredentialsSet.keySet()>> ' + mapStudentToEmbededCredentialsSet.keySet());
    }
    
    void initStudentToUserMap(set<Id> studentIds) {
        
        mapStudentToUser = new map<Id, Id>();
        for (User usr: [select Id, ContactId from User where ContactId in: studentIds]) {
            mapStudentToUser.put(usr.ContactId, usr.Id);
        }
        
        system.debug('initStudentToUserMap|mapStudentToUser >> ' + mapStudentToUser);
    }
    
    void initAddMicroCredentialRequests() {
        
        setStudentIdsToProcess = new set<Id>();
        lstAddMicroCredentialRequest = new list<csapi_BasketRequestWrapper.AddProductRequest>();
        for(id studentId: mapStudentToUser.keySet()) {
            for (id embededCredId: mapStudentToEmbededCredentialsSet.get(studentId)) {
                for (id microCredId: mapRelatedCourseToCourseSet.get (embededCredId)) {
                    
                    if (mapCourseToPriceItemObj.containsKey(microCredId) 
                        && (!mapStudentToMicroCredentialsSet.containsKey(studentId) 
                        || !mapStudentToMicroCredentialsSet.get(studentId).contains(microCredId))) {
                        
                        setStudentIdsToProcess.add(studentId);
                        csapi_BasketRequestWrapper.AddProductRequest addRequest = new csapi_BasketRequestWrapper.AddProductRequest();
                        addRequest.Course = initializeCourse(mapCourseToPriceItemObj.get(microCredId));
                        addRequest.UserId = mapStudentToUser.get(studentId);
                        addRequest.IsBatchProcessed = true;
                        lstAddMicroCredentialRequest.add(addRequest);
                        system.debug('initAddMicroCredentialRequests|AddMicroCredentialRequest >> ' + studentId + ':' + microCredId);   
                    }
                }
            }
        }
        
        system.debug('initAddMicroCredentialRequests|setStudentIdsToProcess >> ' + setStudentIdsToProcess );
        system.debug('initAddMicroCredentialRequests|lstAddMicroCredentialRequest >> ' + lstAddMicroCredentialRequest );
        system.debug('initAddMicroCredentialRequests|lstAddMicroCredentialRequest.size() >> ' + lstAddMicroCredentialRequest.size() );
    }
    
    public EmbeddedCredentialsBatch() {
        
        initRelatedCourseToMicroCredentialsMap();
        initMicroCourseToPriceItemObjMap(microCourseOfferIds);
        initStudentToMicroCredentialsMap(microCourseOfferIds);
        initStudentToEmbededCredentialsMap(embedCourseOfferIds);
        initStudentToUserMap(setStudentIdsAssigned);
        initAddMicroCredentialRequests();
    }
    
    public Iterable<Id> start(Database.BatchableContext ctx) {
        
        return new List<Id>(setStudentIdsToProcess);
    }

    public void execute(Database.BatchableContext bc, List<Id> studentIds) {
        system.debug('studentIds.size() ' + studentIds.size());
        system.debug('studentId ' + studentIds[0]);
        
        system.debug('execute|mapStudentToUser >> ' + mapStudentToUser);
        
        Id studentId = studentIds[0];
        Id userId = mapStudentToUser.get(studentId);
        
        Exception ee;
        System.Savepoint sp = Database.setSavepoint();
        try {
            
            //
            // create basket and opportunity for each student and update the opportunity and assigne it to contact.
            //
            csapi_BasketRequestWrapper.CreateBasketRequest basketRequest = new csapi_BasketRequestWrapper.CreateBasketRequest();
            basketRequest.UserId = userId;
            basketRequest.IsBatchProcessed = true;
            csapi_BasketRequestWrapper.CreateBasketResponse basketResponse = csapi_BasketExtension.createBasket(basketRequest);
            
            system.debug('EmbeddedCredentialsBatch|execute|basketId >> ' + basketResponse.BasketId);
            
            //
            // assign oppty to contact and crete add product request for each microcred for student.
            //
            if (basketResponse.BasketId != null) {
                cscfga__Product_Basket__c basket = [select id, cscfga__Opportunity__c from cscfga__Product_Basket__c where id = :basketResponse.BasketId];
                Opportunity opp = new Opportunity(Id = basket.cscfga__Opportunity__c, Assign_To_Contact__c = studentId);
                update opp;
            }
        } catch (Exception e) {
            Database.rollback(sp);
            ee = e;
        } finally {
            if (ee != null) {
                
                system.debug('EmbeddedCredentialsBatch.execute error: ' + ee);
                system.debug('EmbeddedCredentialsBatch.execute error msg: ' + ee.getMessage());
                system.debug('EmbeddedCredentialsBatch.execute error trace: ' + ee.getStackTraceString());
                
                //
                // log exception message, cause, line and other details
                //
                ApplicationErrorLogger.logError('Basket creation' //String BusinessFunctionName
                    , 'Student ID=' + userId//String FailedRecordId,
                    , ee.getMessage());   //String ErrorMessage 
            }
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        
        system.debug('EmbeddedCredentialsBatch >> finish');
        
        if(!Test.isRunningTest()) {
            AddMicroCredentialsBatch nextBatch = new AddMicroCredentialsBatch(lstAddMicroCredentialRequest);  
            Database.executeBatch(nextBatch, 1); 
        }
    }
}