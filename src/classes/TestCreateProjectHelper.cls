/*******************************************
Purpose: Test class for CreateProjectHelper
History:
Created by Ankit Bhagat on 21/09/2018
*******************************************/

@isTest
public class TestCreateProjectHelper {
    
    /************************************************************************************
    // Purpose      :  Creating Test data for Community User for all test methods 
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @testSetup static void testDataSetup() {
        
              
        RSTP_TestDataFactoryUtils.createNonCommunityUsersWithProfile(1,'System Administrator','Activator');
               
        User adminUser =[SELECT Id,ContactId FROM User WHERE UserName='test.nonCommunityUser1@test.com.testDev' LIMIT 1];
        System.runAs(adminUser)   
        {
            Map<String,Integer> allnumbersOfDataMap = new Map<String,Integer>();
            allnumbersOfDataMap.put('user',1);
            allnumbersOfDataMap.put('account',1);
            allnumbersOfDataMap.put('contact',1);
            
            Map<String,String> requiredInfoMap = new Map<String,String>();
            requiredInfoMap.put('userType','CommunityUsers');
            requiredInfoMap.put('profile',Label.testProfileName);
            
            Boolean userCreatedFlag = RSTP_TestDataFactoryUtils.testRSTPDataSetup(allnumbersOfDataMap,requiredInfoMap);
            System.assert(userCreatedFlag==true,'Community User Created'); 
        }
       
    }    
    
        
    /************************************************************************************
    // Purpose      :  Test functionality of positive case for Create ResearcherPortal Project functionality
    // Developer    :  Ankit
    // Created Date :  10/24/2018                 
    //***********************************************************************************/
    @isTest
    public static void testcreateResearcherProjectPositiveMethod(){

        User usr =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];

        system.runAs(usr)
        {
        CreateProjectHelper.projectDetailsWrapper  wrapperDetails = new  CreateProjectHelper.projectDetailsWrapper();
        wrapperDetails.projectTitle = 'newtest';
        wrapperDetails.status       = 'Idea';
        wrapperDetails.summary      = 'abc' ;
        wrapperDetails.startDate    = date.newInstance(2018, 09, 21) ;
        wrapperDetails.endDate      = date.newInstance(2018, 12, 14);
        wrapperDetails.access       ='Private';
        wrapperDetails.expectedImpact = 'test';
        system.assert( wrapperDetails.status == 'Idea');
        
        List<String> impactCategorytest = new List<String>();
        impactCategorytest.add('Social');
        impactCategorytest.add('Environmental');
        impactCategorytest.add('Cultural');
        wrapperDetails.impactCategory = impactCategorytest ;
        system.assert(impactCategorytest.size() > 2);
        
        List<String> forCodestest     = new List<String>();
        forCodestest.add('0101 - PURE MATHEMATICS');
        forCodestest.add('0102 - APPLIED MATHEMATICS');
        forCodestest.add('0103 - NUMERICAL AND COMPUTATIONAL MATHEMATICS');
        wrapperDetails.forCodes       = forCodestest;
        system.assert(forCodestest.size() > 2);
        
        
        List<String> keywordsList = new List<String>();
        keywordsList.add('Others');
        keywordsList.add('abd');
        keywordsList.add('csd');
        wrapperDetails.keywords = keywordsList ;
        system.assert(keywordsList.size() > 2);
        
        wrapperDetails.approachedImpact = 'new1'; 
        system.assert( wrapperDetails.approachedImpact == 'new1');

        String projectRecordTypeId=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRPPRecordtype).getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId, access);
        system.assert(projectObjList.size() > 0);
        
        String serailizedString = JSON.serialize(wrapperDetails); 
        
        Test.StartTest();
        CreateProjectHelper.createResearcherProject(serailizedString,projectObjList[0].id);        
        Test.StopTest();
        }
    }
    
    
      /************************************************************************************
    // Purpose      :  Test functionality of negative case forCreate ResearcherPortal Project functionality
    // Developer    :  Ali
    // Created Date :  11/23/2018                 
    //***********************************************************************************/
    @isTest
    public static void testcreateResearcherProjectNegativeMethod(){

        User usr1 =[SELECT Id,ContactId FROM User WHERE UserName='test.communityUser1@test.com.testDev' LIMIT 1];

        system.runAs(usr1)
        {
        Boolean result = false;
        Contact con =[SELECT Id,Firstname,lastname FROM contact WHERE LastName='Contact1' LIMIT 1 ];
        con.hed__UniversityEmail__c='raj@rmit.edu.au';
        con.lastname='';
        //update con; 
         try
            {
                update con;    
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
    
        CreateProjectHelper.projectDetailsWrapper  wrapperDetails = new  CreateProjectHelper.projectDetailsWrapper();
        wrapperDetails.projectTitle = 'newtest1';
        wrapperDetails.access       ='Private';
        
        List<String> impactCategorytest = new List<String>();
        impactCategorytest.add(''); 
        try
            {
                wrapperDetails.impactCategory = impactCategorytest ;   
            }
            catch(DmlException ex)
            {
                result =true;                   
               
            }
            system.assert(result);
    
        List<String> forCodestest     = new List<String>();
        forCodestest.add('');
     
        List<String> keywordsList = new List<String>();
        keywordsList.add('');
           
        wrapperDetails.approachedImpact = 'new2'; 
        system.assert( wrapperDetails.approachedImpact == 'new2');

        String projectRecordTypeId1=Schema.SObjectType.ResearcherPortalProject__c.getRecordTypeInfosByName().get(Label.RSTP_ResearchProjectRMRecordtype).getRecordTypeId();
        String access = 'Private';
        List<ResearcherPortalProject__c> projectObjList1 = RSTP_TestDataFactoryUtils.createResearchProj(1, projectRecordTypeId1, access);
        system.assert(projectObjList1.size() > 0);
        
        String serailizedString = JSON.serialize(wrapperDetails); 
        
        Test.StartTest();
        CreateProjectHelper.createResearcherProject(serailizedString,projectObjList1[0].id);        
        Test.StopTest();
        }
    }
}