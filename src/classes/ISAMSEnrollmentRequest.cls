public interface ISAMSEnrollmentRequest {
    void sendEnrollDetailToSAMS(List<CourseConnectionWrapper> samsEnrollmentRequestBody);
    Map<Id, JsonCourseWrapper.IpaasResponseWrapper> getSAMSEnrollResByCourseConnId();
    
}