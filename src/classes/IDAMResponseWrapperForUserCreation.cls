/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 13/09/2018     ****************************/ 
/***   @JIRA ID: ECB-4460                        ********************************************************/          
/*** This class is response wrapper class for checking email availability in IDAM                   *****/ 
/********************************************************************************************************/

public class IDAMResponseWrapperForUserCreation {
	public String id;
	public String result;
	public String code;
	public String application;
	public String provider;
	public Payload payload;

	public class Payload {
		public Boolean success;
		public String message;
		public String federationID;
	}

	public static IDAMResponseWrapperForUserCreation parse(String json) {
		return (IDAMResponseWrapperForUserCreation) System.JSON.deserialize(json, IDAMResponseWrapperForUserCreation.class);
	}
	
}