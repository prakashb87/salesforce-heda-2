/********************************************************************************************************/
/**    Author : Resmi Ramakrishnan                     Date : 04/12/2018     ****************************/               
/***   Test Class for CourseTransactionUpdateServiceTrigger                  ****************************/
/********************************************************************************************************/
@isTest
public class CourseTransactionUpdateTriggerTest
{
	@TestSetup
    static void makeData()
    {
    	TestDataFactoryUtil.Test();
  	
        Account accDepartment = TestUtility.createTestAccount(true
                                                              , 'Department Account 001'
                                                              , Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Institution').getRecordTypeId());


        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Con';
        con.AccountId = accDepartment.id;
        insert con;
        
        
        hed__Term__c term = new hed__Term__c(hed__Account__c = accDepartment.Id);
        insert term;
        
        hed__Course__c course = new hed__Course__c(Name = 'Course 001'
                                    , recordTypeId = Schema.SObjectType.hed__Course__c.getRecordTypeInfosByName().get('RMIT Online').getRecordTypeId()
                                    , hed__Account__c = accDepartment.Id);
        insert course;
        
    
        hed__Course_Offering__c courseOffering = new hed__Course_Offering__c(Name = 'Course Offering 001'
                                                     , hed__Course__c = course.Id
                                                     , hed__Term__c = term.Id 
                                                     , hed__Start_Date__c = system.today()
                                                     , hed__End_Date__c  = system.today());
        insert courseOffering;

        csord__Service__c serviceObj = [SELECT id from csord__Service__c LIMIT 1];
        
        hed__Course_Enrollment__c courseEnrolment = new hed__Course_Enrollment__c(hed__Status__c = 'Former'
                                                                                  , hed__Course_Offering__c = courseOffering.id,
                                                                                  Service__c = serviceObj.id);
        insert courseEnrolment;


        //Creating Parent Account.
        Account parent = new Account(name = 'RMIT Online');
        insert parent; 
		
        //Creating program
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Academic Program').getRecordTypeId();
        Account program = new Account(Name='Test Program',RecordTypeId=recordTypeId,ParentId=parent.Id);
        Insert program;
        //Creating Program Plan. 
        hed__Program_Plan__c plan = new hed__Program_Plan__c();
        plan.Name ='test Plan';
        plan.hed__Account__c=program.Id;  
        plan.hed__Is_Primary__c = true;      
        insert plan;
        hed__Program_Enrollment__c pgnEnrollObj = new hed__Program_Enrollment__c();
        pgnEnrollObj.hed__Contact__c = con.id;
        pgnEnrollObj.hed__Account__c = program.id;
        pgnEnrollObj.hed__Program_Plan__c = plan.id;
        pgnEnrollObj.Service__c = serviceObj.id;
        insert pgnEnrollObj;

    }

    @isTest
    static void testCourseTransactionCourseConnectionService() 
    {
    	Contact con =  [SELECT id from Contact LIMIT 1];
    	hed__Course_Enrollment__c courseEnrolObj = [SELECT id,Service__c from hed__Course_Enrollment__c LIMIT 1];

    	Course_Transaction__c cobj1 = new Course_Transaction__c();        
        cobj1.Contact__c = con.Id;
        cobj1.Course_Connection__c = courseEnrolObj.Id;
        cobj1.Discount_Amount__c = 200;
        cobj1.GL_Code__c = 'gl1';
        cobj1.IO_Code__c = 'io1';
        cobj1.GST_Amount__c = 72.72;
        cobj1.List_Price__c = 1000;
        cobj1.Receipt_Number__c = 'R1234';
        cobj1.Post_To_SAP__c = false;
        cobj1.Post_Action__c = 'Do Not Post';
        cobj1.RecordTypeId = Schema.SObjectType.Course_Transaction__c.getRecordTypeInfosByName().get('Revenue').getRecordTypeId();
        insert cobj1;

        Course_Transaction__c  ctObj= [SELECT id,Service__c from Course_Transaction__c where id =: cobj1.id];
        system.assertEquals(ctObj.Service__c,  courseEnrolObj.Service__c);
    
    }

    @isTest
    static void testCourseTransactionProgramEnrollmentService() 
    {

    	Contact con =  [SELECT id from Contact LIMIT 1];
    	hed__Program_Enrollment__c programEnrollObj = [SELECT id,Service__c from hed__Program_Enrollment__c LIMIT 1];

    	Course_Transaction__c cobj1 = new Course_Transaction__c();        
        cobj1.Contact__c = con.Id;
        cobj1.Program_Enrollment__c = programEnrollObj.Id;
        cobj1.Discount_Amount__c = 200;
        cobj1.GL_Code__c = 'gl1';
        cobj1.IO_Code__c = 'io1';
        cobj1.GST_Amount__c = 72.72;
        cobj1.List_Price__c = 1000;
        cobj1.Receipt_Number__c = 'R1234';
        cobj1.Post_To_SAP__c = false;
        cobj1.Post_Action__c = 'Do Not Post';
        cobj1.RecordTypeId = Schema.SObjectType.Course_Transaction__c.getRecordTypeInfosByName().get('Revenue').getRecordTypeId();
        insert cobj1;

        Course_Transaction__c  ctObj= [SELECT id,Service__c from Course_Transaction__c where id =: cobj1.id];
        system.assertEquals(ctObj.Service__c,  programEnrollObj.Service__c);
    }
	
}