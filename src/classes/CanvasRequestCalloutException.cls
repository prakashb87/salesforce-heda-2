/**
 * @description Custom Exception for any problems with Canvas callouts
 * This can include failing to recieve a valid response from IPaaS or 
 * canvas and also any unintended responses.
 */
public class CanvasRequestCalloutException extends Exception {}