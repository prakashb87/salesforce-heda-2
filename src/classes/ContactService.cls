/**
 * Created by mmarsson on 7/03/19.
 */

public with sharing class ContactService {



    public class validationAllObjectsExistWrapper{
        public Boolean allExist = true;
        public List<Contact> contacts= new List<Contact>();
        public Set<String> notFound = new Set<String>();
    }

    public static validationAllObjectsExistWrapper getContactsByStudentIdEnsureAllExists(Set<String> studentIds){

        validationAllObjectsExistWrapper wrapper = new validationAllObjectsExistWrapper();
        //USE METHOD THAT ALREADY EXISTS.
        List<Contact> contactList = getContactsByStudentId(studentIds);
        wrapper.contacts = contactList;

        if(studentIds.size()!= contactList.size()){
            wrapper.notFound = studentIds.clone();
            wrapper.allExist=false;

            for(Contact cont: contactList){
                wrapper.notFound.remove(cont.Student_ID__c);
            }
            return wrapper;
        }

        return wrapper;
    }

    public static List<Contact> getContactsByStudentId(Set<String> studentIds){


         if (Schema.sObjectType.Contact.isAccessible()) {

             return [Select Id, Student_Id__c, Name from Contact where Student_ID__c In :studentIds];
         }

        return null;

    }

    public static void setContactActionDateasToday(List<Contact> contactList){

        for(Contact cont : contactList){
            cont.Action_Date__c = System.today();
        }

        if (Schema.sObjectType.Contact.isUpdateable()) {
            update contactList;
        }

    }
}