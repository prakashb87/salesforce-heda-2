//HttpPost methods must be declared as global at line 5 column 26 

@RestResource(urlMapping='/student/openedx/result/*')
global with sharing class StudentOpenEdxResultController{

    @HttpPost
    global static String updateOpenEdxResult() {
              
        RestRequest request = RestContext.request;
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(request.requestbody.tostring());

       User usrRec;
       hed__Course_Enrollment__c cenrollRec;
       ErrorWrapper errObj;
        
       
       //Request contains userId and courseOfferingId 
           try{
                if(Schema.sObjectType.User.isAccessible() && params.get('userId')!=null &&
                   Schema.sObjectType.hed__Course_Enrollment__c.isAccessible() && params.get('courseOfferingId')!=null){
                   //querying the user record
                   usrRec = [SELECT ContactId FROM User WHERE FederationIdentifier = :String.valueOf(params.get('userId')) LIMIT 1];
                   
                     //querying the course enrollment record
                   cenrollRec = [SELECT Id,hed__Status__c,hed__Grade__c,Certificate_URL__c,hed__Course_Offering__c,hed__Contact__c,Enrolment_Status__c
                                  FROM hed__Course_Enrollment__c WHERE hed__Contact__c = :usrRec.ContactId AND hed__Course_Offering__r.Name = :String.valueOf(params.get('courseOfferingId')) 
                                  AND (hed__Status__c = :ConfigDataMapController.getCustomSettingValue('EnrollmentStatus') OR 
                                  hed__Status__c = :ConfigDataMapController.getCustomSettingValue('CourseConnectionStatus_Completed') OR 
                                  hed__Status__c = :ConfigDataMapController.getCustomSettingValue('AssessmentFailed')) 
                                  LIMIT 1]; //ECB-4685 - For re-upload of open edx results,added condiiton  Status = 'Current' or 'Assessment Complete' or 'Assessment Failed'
                                            // & Removed the Enrolment_status condition          
                    if(cenrollRec!= null){
                        updateCourseConnection(cenrollRec,params,String.valueOf(params.get('status')));                             
                    }  
                } else{                 
                    errObj = new ErrorWrapper();
                    errObj.errorMsg = ConfigDataMapController.getCustomSettingValue('OpenEdxResultError');
                    errObj.statusCode = ConfigDataMapController.getCustomSettingValue('OpenEdxResult_Failure'); 
                } 
               
                           
           }catch(QueryException ex){
              errObj = createErrorWrapObj(ex,ConfigDataMapController.getCustomSettingValue('OpenEdxResult_NoRecordFound'));
           }catch(Exception ex){               
                errObj = createErrorWrapObj(ex,ConfigDataMapController.getCustomSettingValue('OpenEdxResult_Failure'));
               
           }
                           

       Map<String, Object> obj = new Map<String, Object>();
        
       if(errObj!=null){
            obj.put('success', false);
            obj.put('message', errObj.errorMsg);           
            obj.put('statusCode', errObj.statusCode);
            //createRMErrorLog(errorMsg,errorLineNumber,errorType);
            createRMErrorLog(errObj);
            
                    
       }else{
            obj.put('success', true);
            obj.put('message', '');
            obj.put('statusCode', ConfigDataMapController.getCustomSettingValue('OpenEdxResult_Success'));
        }
        

        String reqJSONString = JSON.serialize(obj);
        System.debug('JSONString'+reqJSONString);
        
        createIntegrationLog(request,reqJSONString);
        
        return reqJSONString;
        
    
  }
  
    public static void createIntegrationLog(RestRequest request,String jsonString){

        String ipassReq = String.valueOf(request);      
        IntegrationLog requestLog = new IntegrationLog();
        requestLog.setRequestBody(ipassReq);
        requestLog.setType('Open_Edx_Result');
        requestLog.setServiceType('Inbound Service');
        requestLog.log();

        IntegrationLog responseLog = new IntegrationLog();
        responseLog.setRequestBody(jsonString);
        responseLog.setType('Open_Edx_Result');
        responseLog.setServiceType('Acknowledgement');
        responseLog.log();
        
    }
  

    private static void createRMErrorLog(ErrorWrapper errWrapObj){
        RMErrLog__c errObj = new RMErrLog__c();
        errObj.Error_Message__c = errWrapObj.errorMsg;
        errObj.Error_Line_Number__c = errWrapObj.errorLineNumber; 
        errObj.Error_Type__c = errWrapObj.errorType ;
        if (Schema.sObjectType.RMErrLog__c.isCreateable()){ 
            Insert errObj;
        } 

    } 
  
    private static void updateCourseConnection(hed__Course_Enrollment__c cenrollRec,Map<String, Object> params,String status){
       if(params.get('grade')!=null){                       
            cenrollRec.hed__Grade__c = Double.valueOf(params.get('grade'))*100;
            
        }    
        cenrollRec.Certificate_URL__c = String.valueOf(params.get('certUrl'));
        if(status!=null && status!='' && status == ConfigDataMapController.getCustomSettingValue('Notpassing')){
            cenrollRec.hed__Status__c = ConfigDataMapController.getCustomSettingValue('AssessmentFailed');
        }else if(status!=null && status!='' && status == ConfigDataMapController.getCustomSettingValue('Downloadable')){
            cenrollRec.hed__Status__c = ConfigDataMapController.getCustomSettingValue('CourseConnectionStatus_Completed');
        }
        
        if (Schema.sObjectType.hed__Course_Enrollment__c.isUpdateable()){ 
            Update cenrollRec;
        }       
    }
    
    private static ErrorWrapper createErrorWrapObj(Exception ex,String statusCode){
        ErrorWrapper obj = new ErrorWrapper();
        obj.errorMsg = ex.getMessage();
        obj.errorLineNumber = ex.getLineNumber();
        obj.errorType = ex.getTypeName();
        obj.statusCode = statusCode;
        
        return obj;
    }
    
    public class ErrorWrapper{
        public String errorMsg;
        public Integer errorLineNumber;
        public String errorType;
        public String statusCode;
    }
  
  
  
  
}