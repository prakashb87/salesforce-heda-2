/**
* -------------------------------------------------------------------------------------------------+
* @description This helper processes the Lead records coming through from Lead Trigger
* --------------------------------------------------------------------------------------------------
* @author         BJIRA Squad
* @version        1.0
* @created        2019-03-05
* @modified       2019-03-06
* @modified by    Anupam Singhal
* @Reference      RM-2436,RM-2538
* -------------------------------------------------------------------------------------------------+
*/
public with sharing class LeadTriggerHandler extends TriggerHandlerExt{
	private Map < ID, Schema.RecordTypeInfo > recordTypeInfoById = LeadService.RECORDTYPEINFOBYID;
    private static final String RMIT_ONLINE_RECORDTYPE = 'Prospective Student - RMIT Online';
    private static final String ACADPROGRAM_RECORDTYPE = 'Academic Program';
    private static final String RMITO = 'RMIT Online';
	LeadTriggerHandlerHelper triggerHelper = new LeadTriggerHandlerHelper();

    //Handler should do different things based on record type.
    public List<Lead> activatorLeads = new List<Lead>();
	public List<Lead> onlineLeads = new List<Lead>();
    public List<Lead> rmitProspectiveStudentleads = new List<Lead>();


    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * Constuctor separetes the leads in the correct lists.
    * ------------------------------------------------------------------------------------------------
    * @author	 Marcelo Marsson
    * @Reference
    * -----------------------------------------------------------------------------------------------+
    */
    public LeadTriggerHandler(){
        for(Lead aLead :(List<Lead>) Trigger.new ){
            if(recordTypeInfoById.get(aLead.RecordTypeId).getName() =='Activator Registration' ){
                //Should verify for owner ID if activator queue, ignore.
                activatorLeads.add(aLead);
                continue;
            }else if(recordTypeInfoById.get(aLead.RecordTypeId).getName() == 'Prospective Student - RMIT Online'){
                onlineLeads.add(aLead);
                continue;
            }
            else if(recordTypeInfoById.get(aLead.RecordTypeId).getName() == 'Prospective Student'){
                rmitProspectiveStudentleads.add(aLead);
                continue;
            }
            
        }
        
    }

    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * This overridable method is used to call the beforeInsert context on lead
    * ------------------------------------------------------------------------------------------------
    * @author	 Anupam Singhal     
    * @Reference RM-2436, RM-2538
    * -----------------------------------------------------------------------------------------------+
    */
    public override void beforeInsert() {
        
        LeadService.setDeduplicationField(Trigger.new);
        triggerHelper.updateMobileTextFields(Trigger.new);
        if (!onlineLeads.isEmpty()) {
            populateCourse(onlineLeads);
            populateCloseofEnrolment(onlineLeads);
            populateProgramNameLookup(onlineLeads);
        }
         if (!rmitProspectiveStudentleads.isEmpty()) {
            triggerHelper.admitTermFields(rmitProspectiveStudentleads);
        }
        
    }
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * This overridable method is used to call the beforeUpdate context on lead
    * ------------------------------------------------------------------------------------------------
    * @author	 Anupam Singhal     
    * @Reference RM-2436, RM-2538
    * -----------------------------------------------------------------------------------------------+
    */
    public override void beforeUpdate() {
        triggerHelper.updateMobileTextFields(Trigger.new);
        if (!onlineLeads.isEmpty()) {
            populateCloseofEnrolment(onlineLeads);
        }
        if (!rmitProspectiveStudentleads.isEmpty()) {
            triggerHelper.admitTermFields(rmitProspectiveStudentleads);
        }
    }
    
    public override void afterInsert(){
        if (!onlineLeads.isEmpty()) {
            triggerHelper.updateDuplicateCountOnInsert(onlineLeads); 
        }
    }
    
    public override void afterUpdate() {
        if (!onlineLeads.isEmpty()) {
            triggerHelper.updateDuplicateCountOnUpdate(onlineLeads); 
        } 
    }  
    
    
    /**
	* -----------------------------------------------------------------------------------------------+
	* @description
	* This method populates the Course lookup of RMITO Leads based on the Course ID from webform/API
	* ------------------------------------------------------------------------------------------------
	* @param     leadRecord
	* @author	 Maria Luisa Esmero 11/03/2019
	* @Reference RM-2538
	* -----------------------------------------------------------------------------------------------+
	*/        
    private void populateCourse(List<Lead> leadRecord) {
        Map<string, List<Lead>> courseIdLeadsMap = new Map<String, list<Lead>>();
        courseIdLeadsMap = fillcourseIdLeadsMap(leadRecord, 'Interested_Course_ID__c');
        
        for (hed__Course__c course: [SELECT id, hed__Course_ID__c FROM hed__Course__c                                     	
                                     WHERE hed__Course_ID__c IN: courseIdLeadsMap.keySet() 
                                     AND Product_Line__c =: RMITO ]) {
                                         for (Lead le: courseIdLeadsMap.get(course.hed__Course_ID__c )) {
                                             le.Course__c = course.id;
                                         }                  
                                     }  
    } 
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * This method populates the Close of Enrolment on RMITO Leads based on the Course ID of the Course
    * ------------------------------------------------------------------------------------------------
    * @author	 Maria Luisa Esmero 11/03/2019
    * @param     leadRecord
    * @Reference RM-2538
    * -----------------------------------------------------------------------------------------------+
    */    
    public void populateCloseofEnrolment(List<Lead> leadRecord) {         
        
        Map<string, List<Lead>> courseIdLeadsmap = new Map<String, List<Lead>>();
        Map <String, hed__Course_Offering__c> mapCourseOffering = new Map <String, hed__Course_Offering__c>();
        
        courseIdLeadsMap  = fillcourseIdLeadsMap(leadRecord, 'Course_ID__c');			          
        mapCourseOffering = fetchCourseOfferings(courseIdLeadsMap.keySet());
        
        for(String courseID:courseIdLeadsMap.keySet() ){
            If (mapCourseOffering.containskey(CourseID)) {
                for (Lead le: courseIdLeadsMap.get(courseID)) {                   
                    le.Close_of_Enrolment__c = mapCourseOffering.get(CourseID).hed__Start_Date__c;
                }
            }   
        }
    } 
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * This method fills a mapping of Course IDs with corresponding Leads 
    * ------------------------------------------------------------------------------------------------
    * @author	 Maria Luisa Esmero 11/03/2019
    * @param     leadRecord, courseIdFieldname
    * @return    courseIdLeadsMap
    * @Reference RM-2538
    * -----------------------------------------------------------------------------------------------+
    */
    private Map<String, List<Lead>> fillcourseIdLeadsMap(List<Lead> leadRecord, String courseIdFieldname) {
        List<Lead> lstLeads;
        Map<String, List<Lead>> courseIdLeadsMap = new Map<String, List<Lead>>(); 
        
        for (Lead rmitoLead: leadRecord) {               
            String courseIDvalue = (String)rmitoLead.get(courseIdFieldname);               
            rmitoLead.Close_of_Enrolment__c = null;
            
          	   if (String.isNotBlank(courseIDvalue)) {
                if(!courseIdLeadsMap.containsKey(courseIDvalue)) {
                    lstLeads= new List<Lead>();
                    lstLeads.add(rmitoLead);
                    courseIdLeadsMap.put(courseIDvalue, lstLeads);
                } 
                else {
                    courseIdLeadsMap.get(courseIDvalue).add(rmitoLead);
                }                 
            }
        }  
        return courseIdLeadsMap;
    }
    
    
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * This method fetches the course offerings which is to be used in populateCloseofEnrolment
    * ------------------------------------------------------------------------------------------------
    * @author	 Anupam Singhal 15/03/2019
    * @param     keyset
    * @Reference RM-2538
    * -----------------------------------------------------------------------------------------------+
    */
    private Map <String, hed__Course_Offering__c> fetchCourseOfferings(Set<String> keyset){
        
        Map <String, hed__Course_Offering__c> returnResultingMap = new Map <String, hed__Course_Offering__c>();
        for (hed__Course_Offering__c courseoffering: [SELECT hed__Course__r.hed__Course_ID__c , Last_Day_to_Add__c, hed__Start_Date__c 
                                                      FROM hed__Course_Offering__c
                                                      WHERE hed__Course__r.hed__Course_ID__c IN: keyset
                                                      AND hed__Course__r.Product_Line__c =: RMITO 
                                                      AND Last_Day_to_Add__c >= TODAY 
                                                      AND hed__Start_Date__c != NULL
                                                      ORDER BY hed__Course__r.hed__Course_ID__c, Last_Day_To_Add__c, hed__Start_Date__c]) {
                                                          if(!returnResultingMap.containsKey(courseoffering.hed__Course__r.hed__Course_ID__c)){                                     
                                                              returnResultingMap.put(courseoffering.hed__Course__r.hed__Course_ID__c,courseOffering);
                                                          }                
                                                      }
        return returnResultingMap;
    }
        
    /**
    * -----------------------------------------------------------------------------------------------+
    * @description
    * Method to update the sams program name based on the given program code(from web to lead form)
    * Custom Label: 
    * ------------------------------------------------------------------------------------------------
    * @author	 RM Squad
    * @param     leadRecord
    * @Reference RM-
    * -----------------------------------------------------------------------------------------------+
    */
    private void populateProgramNameLookup(List<Lead> leadRecord){
        RMErrLogHandlerClassException re = new RMErrLogHandlerClassException();
        map<String, List<Lead>> mapLead= new map<String, List<Lead>>();  
        List<lead> leads;
        Id accRecordtypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(ACADPROGRAM_RECORDTYPE).getRecordTypeId();
        for(Lead le:leadRecord){
            if(String.isNotBlank(le.Program_Code__c)){
                if(!mapLead.containsKey(le.Program_Code__c)){
                    leads= new List<Lead>();
                    leads.add(le);
                    mapLead.put(le.Program_Code__c,leads);
                }else{
                    mapLead.get(le.Program_Code__c).add(le);
                }
            }
        }
        for(Account acc:[SELECT ID,AccountNumber FROM Account 
                         WHERE  	AccountNumber!=NULL 
                         AND  RecordTypeID=: accRecordtypeID
                         AND AccountNumber IN :mapLead.keyset()]){
                             for(Lead leadRec:mapLead.get(acc.AccountNumber)){
                                 leadRec.SAMS_Program_name__c=acc.ID;
                             }
                             
                         }       
    }
    
}