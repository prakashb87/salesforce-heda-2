@isTest
public class ResendTest {
    @isTest static void resendMail() {
        test.startTest();
        Contact c = new Contact(LastName = 'Test', Email ='shubhamsingh@rmit.com');
        insert c;
        EmailMessage emailDetails = new EmailMessage();
        emailDetails.FromAddress = 'shubhamsingh@rmit.com';
        emailDetails.FromName = 'Shubham';
        emailDetails.Subject = 'New Mail';
        emailDetails.TextBody = 'Sending test mail to student';
        emailDetails.ToAddress = 'shubhamsingh@rmit.com';
        emailDetails.CcAddress = 'shubham@rmit.com';
        insert emailDetails;
        System.assertEquals(emailDetails.FromAddress,'shubhamsingh@rmit.com');
        //ReSendMail.sendMailConfirm();
        Attachment att = new Attachment();
        att.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
    	att.body = bodyBlob;
    	att.parentId = emailDetails.id;
        insert att;
        System.assertEquals(att.parentId,emailDetails.id);
        PageReference pageRef = Page.Resend_Mail;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',emailDetails.id);
   		ApexPages.StandardController sc = new ApexPages.standardController(emailDetails);
        ReSendMail  mail = new ReSendMail(sc);
        mail.sendMailConfirm();
		test.stopTest();

    }


}