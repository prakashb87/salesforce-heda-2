/*****************************************************************************************************
 *** @Class             : EmbeddedMicroCredsScheduler
 *** @Author            : Avinash Machineni
 *** @Requirement       : Scheduler Class for transforming the embedded micro credentials data from HEDA into PriceItem table
 *** @Created date      : 27/06/2018
 *** @JIRA ID           : ECB-3731
 *****************************************************************************************************/
/*****************************************************************************************************
 *** @About Class
 *** This class is used to transform the embedded micro credentials data from HEDA
 *****************************************************************************************************/
public class EmbeddedMicroCredsScheduler implements schedulable{
	
    public void execute(SchedulableContext SC) {  
        
        EmbeddedMicroCreds.createPriceItemRec();
           
    }
}