/**************************************************************
Purpose: RPORW-1041, Trigger for Research project Funding, To update 
Access Type of RM Projects on the change of Organisation Ecode to ARC/NHMRC 
History:
Created by Ankit on 12/04/2019
*************************************************************/ 

trigger ResearcherProjectFundingTrigger on Research_Project_Funding__c  (after Insert, after update, before delete,after delete) {
   
  if((Trigger.isAfter && Trigger.isInsert)  || (Trigger.isAfter && Trigger.isUpdate))
   {
          System.debug('@@@Newvalues'+Trigger.new);
          ResearcherProjectFundingTriggerHandler.getFundingDataforOrganisationECodeForUpsert(Trigger.new, Trigger.oldMap);

   }
   
   if(Trigger.isAfter && Trigger.isDelete)
   {
          System.debug('@@@oldvalues'+Trigger.old);
          ResearcherProjectFundingTriggerHandler.getFundingDataforOrganisationECodeForDelete(Trigger.old);
   }   
   
}