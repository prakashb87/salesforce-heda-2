trigger ApplicationTrigger on Application__c (after insert, after update, after delete, after undelete,before update,before insert) {
    //Application trigger handler
    ApplicationTriggerHandler applicationHandler = new ApplicationTriggerHandler();
    ApplicationTouchPointsUpdate appTouchPoint = new ApplicationTouchPointsUpdate();
	if(trigger.isAfter && trigger.isDelete){
    //update lead on delete
		applicationHandler.updateLeadRecords(trigger.old);
	}else if(trigger.isAfter){
		applicationHandler.updateLeadRecords(trigger.new);
	}
    //update application conversion based on application status
	if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore){
		appTouchPoint.beforeUpdateApplication(trigger.new,trigger.old); 
   	}
}