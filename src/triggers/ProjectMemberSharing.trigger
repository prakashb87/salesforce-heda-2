/*****************************************************************************************
    // JIRA No      :  RPORW-124
    // SPRINT       :  SPRINT-4
    // Purpose      :  Trigger on Researcher Member Sharing Object
    // Parameters   :  
    // Developer    :  Ankit Bhagat 
    // Created Date :  10/10/2018                 
//************************************************************************************/ 

trigger ProjectMemberSharing on Researcher_Member_Sharing__c (before insert, before update,after insert, after update ,before delete) {

    if (trigger.isbefore && (Trigger.isInsert || Trigger.Isupdate)) {
      
        System.debug('@@@Inside Trigger1');
        ResearcherMemberSharingHandler.populateUserIdByContactId(Trigger.new);
        ResearcherMemberSharingHandler.getRRIRolesvsSFRole(Trigger.new, Trigger.old);

    }
    
    if(trigger.isAfter && (Trigger.isInsert || Trigger.Isupdate)){
        System.debug('@@@Inside Trigger2');

        ResearcherMemberSharingHandler.memberSharingAccessProcess(Trigger.newmap, Trigger.oldmap);
    }
        
    if(trigger.isbefore && Trigger.IsDelete){
         
        ResearcherMemberSharingHandler.deleteResearcherSharing(Trigger.old);
    }
}