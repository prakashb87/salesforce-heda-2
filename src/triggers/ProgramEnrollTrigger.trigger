/*********************************************************************************
 *** @TriggerClass      : ProgramEnrollTrigger
 *** @Author		    : Shubham Singh
 *** @Requirement     	: To update values Of ProgramEnrollment on Contact
 *** @Created date    	: 13/11/2017
 *** @Modified by		: Anupam Singhal
 **********************************************************************************/
/*********************************************************************************
 *** @About Class
 *** This class is a Trigger class for ProgramEnrollTrigger and works on four events
 *** (after insert,after update, after delete, after undelete) of ProgramEnrollment. 
 *** It is used to update the values on Contact object.
 ***********************************************************************************/
trigger ProgramEnrollTrigger on hed__Program_Enrollment__c (after insert, after update, after delete, after undelete) {
    new ProgramEnrollmentHelper().run();
}