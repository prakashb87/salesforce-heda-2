trigger cmsLifeCycleEventTrigger on cms__LifeCycleEvent__e (after insert) {
    ResearcherPortalContentSearch.processLifecycleEvents(Trigger.new);
}