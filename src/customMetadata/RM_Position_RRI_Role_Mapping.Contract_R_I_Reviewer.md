<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contract_R&amp;I Reviewer</label>
    <protected>false</protected>
    <values>
        <field>RM_Position_Name__c</field>
        <value xsi:type="xsd:string">R&amp;I Reviewer</value>
    </values>
    <values>
        <field>RRIRoleName__c</field>
        <value xsi:type="xsd:string">Researcher_Contract</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">Research Project Contract</value>
    </values>
</CustomMetadata>
