<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Ethics_CI</label>
    <protected>false</protected>
    <values>
        <field>RM_Position_Name__c</field>
        <value xsi:type="xsd:string">Chief Investigator</value>
    </values>
    <values>
        <field>RRIRoleName__c</field>
        <value xsi:type="xsd:string">CI_Ethics</value>
    </values>
    <values>
        <field>RecordType__c</field>
        <value xsi:type="xsd:string">Ethics</value>
    </values>
</CustomMetadata>
