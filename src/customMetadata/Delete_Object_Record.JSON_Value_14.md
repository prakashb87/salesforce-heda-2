<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>JSON Value 14</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Field_To_be_Translated__c</value>
    </values>
    <values>
        <field>JSON_Field_Name__c</field>
        <value xsi:type="xsd:string">value</value>
    </values>
    <values>
        <field>JSON_Object_Name__c</field>
        <value xsi:type="xsd:string">TranslationService</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">Translation_Service__c</value>
    </values>
</CustomMetadata>
