<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Budget_Activities__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Common expenses in this category include the A/V equipment, Guest speakers, Nightlife, Golf, and other entertainment expenses.</inlineHelpText>
        <label>Activities &amp; Entertainment Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Catering__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Includes all food and beverage charges, including tips and gratuities.</inlineHelpText>
        <label>Catering Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Contingency__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Depending on the size or complexity of this event, you may want to add as much as up to 20% as a contingency.</inlineHelpText>
        <label>Contingency Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Decor__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Included expenses for decor, such as centerpieces, florals, tent rentals, etc.</inlineHelpText>
        <label>Decor Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Other__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Other expenses not included in previous categories.</inlineHelpText>
        <label>Other Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Printing__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Items include invitations, name badges, program booklets, event signage and banners.</inlineHelpText>
        <label>Printing Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Site_Rental__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Projected rental fees for the event and function space, housekeeping, baggage handling, and related expenses.</inlineHelpText>
        <label>Site Rental Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Total__c</fullName>
        <externalId>false</externalId>
        <formula>Budget_Site_Rental__c + Budget_Catering__c + Budget_Transportation__c + Budget_Decor__c +  Budget_Printing__c + Budget_Activities__c + Budget_Other__c + Budget_Contingency__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Total projected expenses</inlineHelpText>
        <label>Total Budget (Expected Gross Cost)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Budget_Transportation__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Includes shuttles, coaches, event transfers, and any related expenses.</inlineHelpText>
        <label>Transportation Budget</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Capacity_Full__c</fullName>
        <externalId>false</externalId>
        <formula>Registered_Attendees__c /  Maximum_Registration__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Capacity Full</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Event_Description__c</fullName>
        <externalId>false</externalId>
        <label>Event Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Event_End_Date__c</fullName>
        <externalId>false</externalId>
        <label>Event End Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Event_Evaluation__c</fullName>
        <description>Enter Event Evaluation Score post-Event (1-5, with 5 as the highest). Average of Session Rating.</description>
        <externalId>false</externalId>
        <label>Event Evaluation</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Event Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Events</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Event_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Event Start Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Event_Type__c</fullName>
        <externalId>false</externalId>
        <label>Event Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Executive Event</fullName>
                    <default>false</default>
                    <label>Executive Event</label>
                </value>
                <value>
                    <fullName>Internal Event</fullName>
                    <default>false</default>
                    <label>Internal Event</label>
                </value>
                <value>
                    <fullName>Launch Event</fullName>
                    <default>false</default>
                    <label>Launch Event</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Trade Show</fullName>
                    <default>false</default>
                    <label>Trade Show</label>
                </value>
                <value>
                    <fullName>User Conference</fullName>
                    <default>false</default>
                    <label>User Conference</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Event_Vendor_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Event Vendor Account primarily responsible for managing this event</description>
        <externalId>false</externalId>
        <inlineHelpText>Select an Account with the Type = &apos;Event Vendor&apos; for this event.</inlineHelpText>
        <label>Event Vendor Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Events</relationshipLabel>
        <relationshipName>Events</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Event_Vision__c</fullName>
        <externalId>false</externalId>
        <label>Event Vision</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>15</visibleLines>
    </fields>
    <fields>
        <fullName>Event_Website__c</fullName>
        <externalId>false</externalId>
        <label>Event Website</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Expenses_Activities__c</fullName>
        <externalId>false</externalId>
        <label>Activities &amp; Entertainment Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Activities &amp; Entertainment</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Catering__c</fullName>
        <externalId>false</externalId>
        <label>Catering Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Catering</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Decor__c</fullName>
        <externalId>false</externalId>
        <label>Decor Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Decor</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Other__c</fullName>
        <externalId>false</externalId>
        <label>Other Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Other</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Printing__c</fullName>
        <externalId>false</externalId>
        <label>Printing Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Printing</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Site_Rental__c</fullName>
        <externalId>false</externalId>
        <label>Site Rental Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Site Rental</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Expenses_Transportation__c</fullName>
        <externalId>false</externalId>
        <label>Transportation Expenses</label>
        <summarizedField>Event_Expense__c.Site_Rental__c</summarizedField>
        <summaryFilterItems>
            <field>Event_Expense__c.Expense_Type__c</field>
            <operation>equals</operation>
            <value>Transportation</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_Expense__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Final_Attendance__c</fullName>
        <externalId>false</externalId>
        <label>Final Attendance</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Map_to_Venue__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;http://maps.google.com/maps?q=&quot;+ Venue_Street_Address_1__c + &quot;, &quot; + Venue_City__c + &quot;, &quot; + Venue_State__c + &quot; &quot; +  Venue_Postal_Code__c + &quot; &quot; +  Venue_Country__c , &quot;Google Map&quot;, &quot;_blank&quot;)</formula>
        <label>Map to Venue</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Maximum_Registration__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Enter Maximum Registration # allowed for this event.</inlineHelpText>
        <label>Maximum Registration #</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Actual_Cost__c</fullName>
        <externalId>false</externalId>
        <formula>Total_Expenses__c - Total_Actual_MDF__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Net Actual Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Expected_Cost__c</fullName>
        <externalId>false</externalId>
        <formula>Budget_Total__c - Total_Expected_MDF__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Net Expected Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Primary_Vendor_Contact_Phone__c</fullName>
        <externalId>false</externalId>
        <formula>Primary_Vendor_Contact__r.Phone</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Phone</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary_Vendor_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Primary vendor contact for this event.</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the primary vendor contact for this event, filtered by list of available contacts for this vendor account.</inlineHelpText>
        <label>Primary Vendor Contact</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.Event_Vendor_Account__c</valueField>
            </filterItems>
            <infoMessage>Please select the primary vendor contact for this event. Choose from the list of available contacts for this vendor</infoMessage>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Events</relationshipLabel>
        <relationshipName>Events</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <externalId>false</externalId>
        <label>Region</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>North America</fullName>
                    <default>false</default>
                    <label>North America</label>
                </value>
                <value>
                    <fullName>Latin America</fullName>
                    <default>false</default>
                    <label>Latin America</label>
                </value>
                <value>
                    <fullName>EMEA</fullName>
                    <default>false</default>
                    <label>EMEA</label>
                </value>
                <value>
                    <fullName>Japan</fullName>
                    <default>false</default>
                    <label>Japan</label>
                </value>
                <value>
                    <fullName>APAC</fullName>
                    <default>false</default>
                    <label>APAC</label>
                </value>
                <value>
                    <fullName>Worldwide</fullName>
                    <default>false</default>
                    <label>Worldwide</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Registered_Attendees__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Running total of Event Registrations for this Event</inlineHelpText>
        <label>Registered Attendees</label>
        <summaryForeignKey>Event_Registration__c.Event__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Targeted_Attendance__c</fullName>
        <externalId>false</externalId>
        <label>Targeted Attendance</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Total_Actual_MDF__c</fullName>
        <externalId>false</externalId>
        <label>Total Actual MDF</label>
        <summarizedField>Event_MDF__c.MDF_Amount__c</summarizedField>
        <summaryFilterItems>
            <field>Event_MDF__c.Received_Payment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Event_MDF__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Expected_MDF__c</fullName>
        <externalId>false</externalId>
        <label>Total Expected MDF</label>
        <summarizedField>Event_MDF__c.MDF_Amount__c</summarizedField>
        <summaryForeignKey>Event_MDF__c.Event__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Expenses__c</fullName>
        <externalId>false</externalId>
        <formula>Expenses_Site_Rental__c + Expenses_Catering__c +  Expenses_Transportation__c +  Expenses_Decor__c  + Expenses_Printing__c + Expenses_Activities__c + Expenses_Other__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Expenses (Actual Gross Cost)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Venue_City__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.City__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue City</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Country__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.Country__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue Country</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Postal_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.Postal_Code_Zip__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue Postal Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_State__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.State__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue State</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Status__c</fullName>
        <externalId>false</externalId>
        <label>Venue Status</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>false</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Confirmed</fullName>
                    <default>false</default>
                    <label>Confirmed</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
                <value>
                    <fullName>Ready to Publish</fullName>
                    <default>false</default>
                    <label>Ready to Publish</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Venue_Street_Address_1__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.Street_Address_1__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue Street Address #1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Street_Address_2__c</fullName>
        <externalId>false</externalId>
        <formula>Venue__r.Street_Address_2__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue Street Address #2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Venue</label>
        <referenceTo>Venue__c</referenceTo>
        <relationshipLabel>Events</relationshipLabel>
        <relationshipName>Events</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Event</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Venue_City__c</columns>
        <columns>Venue_State__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Event Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Events</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Event_Start_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Venue_City__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Venue_State__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OWNER.ALIAS</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Event_Start_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Venue_City__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Venue_State__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
