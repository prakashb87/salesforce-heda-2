<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>To store the error records</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Business_Function_Name__c</fullName>
        <description>Stores the name of the Salesforce project / implementation / business specific instance name for the origin of the error</description>
        <externalId>false</externalId>
        <label>Business Function Name</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Class_Name__c</fullName>
        <description>The name of the calling class where the error originates from.</description>
        <externalId>false</externalId>
        <label>Class Name</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Error_Cause__c</fullName>
        <externalId>false</externalId>
        <label>Error Cause</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Error_Code__c</fullName>
        <externalId>false</externalId>
        <label>Error Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Error_Line_Number__c</fullName>
        <externalId>false</externalId>
        <label>Error Line Number</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Error_Message__c</fullName>
        <externalId>false</externalId>
        <label>Error Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Error_Payload__c</fullName>
        <externalId>false</externalId>
        <label>Error Payload</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Error_Time__c</fullName>
        <externalId>false</externalId>
        <label>Error Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Error_Type__c</fullName>
        <externalId>false</externalId>
        <label>Error Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Application</fullName>
                    <default>false</default>
                    <label>Application</label>
                </value>
                <value>
                    <fullName>Integration</fullName>
                    <default>false</default>
                    <label>Integration</label>
                </value>
                <value>
                    <fullName>System</fullName>
                    <default>false</default>
                    <label>System</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Is_Admin_Notified__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field will specify if the Administrator was notified of the error</description>
        <externalId>false</externalId>
        <label>Is Admin Notified</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Case_Created_c__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field will specify if a case created for this error</description>
        <externalId>false</externalId>
        <label>Is Case Created</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Integration_Error__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This signifies if the error is during an inbound / outbound API call</description>
        <externalId>false</externalId>
        <label>Is Integration Error</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Logged_In_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Logged-In User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>RMErrLog</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Method_Name__c</fullName>
        <description>The name of the method / function in the class where the error originates from</description>
        <externalId>false</externalId>
        <label>Method Name</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Record_Id__c</fullName>
        <externalId>false</externalId>
        <label>Failed Record Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>All_Created_today</fullName>
        <columns>NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>All Created today</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Logs</fullName>
        <columns>CREATED_DATE</columns>
        <columns>NAME</columns>
        <columns>Business_Function_Name__c</columns>
        <columns>Class_Name__c</columns>
        <columns>Error_Message__c</columns>
        <columns>Error_Type__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Logs</label>
    </listViews>
    <label>Error Log</label>
    <nameField>
        <displayFormat>Log-{00000}</displayFormat>
        <label>Log Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Error Logs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
